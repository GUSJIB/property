'use strict';

/* Filters */
// need load the moment.js to use this filter. 
angular.module('app')
  .filter('fromNow', function() {
    return function(date) {
      return moment(date).fromNow();
    }
  });


//==CUSTOM FILTERs =====================================================================================================
//For convert string date to other format
//{{item.createdDate | asDate | date:'dd-MM-yyyy'}}
angular.module('app').filter("asDate", function () {
    return function (input) {
        if(input == null || input=='')
            return '';
        return new Date(input);
    }
});