
app.service(
    "ProvincesService",
    function( $http, $q, BaseService ) {

        // Return public API.
        return({
            listing: function (searchModel) {
                return BaseService.post('/admin/provinces/listing', JSON.stringify(searchModel));
            },
            read: function (id) {
                return BaseService.post('/admin/provinces/read/' + id);
            },
            create: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/provinces/create', JSON.stringify(model));
            },
            update: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/provinces/update', JSON.stringify(model));
            },
            remove: function(id) {
                return BaseService.post('/admin/provinces/remove/' + id);
            },
            getLastPosition: function(){
                return BaseService.post('/admin/provinces/getLastPosition');
            }
        });

    }
);