'use strict';

//element #app render only after user logged in.
var isAfterLogin = angular.element('#app').length > 0? true: false;

//Load base module for usage in anothor sections.
var app =  angular.module('app', LOAD_ANGULAR_MODULES);


app.config(
    [        '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
    function ($controllerProvider,   $compileProvider,   $filterProvider,   $provide) {
        
        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.filter     = $filterProvider.register;
        app.factory    = $provide.factory;
        app.service    = $provide.service;
        app.constant   = $provide.constant;
        app.value      = $provide.value;
    }
  ]);
  
  
  
/**
* jQuery plugin config use ui-jq directive , config the js and css files that required
* key: function name of the jQuery plugin
* value: array of the css js file located
*/
/*app.constant('JQ_CONFIG', {
      easyPieChart:   ['assets/vendor/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
      sparkline:      ['assets/vendor/jquery/charts/sparkline/jquery.sparkline.min.js'],
      plot:           ['assets/vendor/jquery/charts/flot/jquery.flot.min.js', 
                          'assets/vendor/jquery/charts/flot/jquery.flot.resize.js',
                          'assets/vendor/jquery/charts/flot/jquery.flot.tooltip.min.js',
                          'assets/vendor/jquery/charts/flot/jquery.flot.spline.js',
                          'assets/vendor/jquery/charts/flot/jquery.flot.orderBars.js',
                          'assets/vendor/jquery/charts/flot/jquery.flot.pie.min.js'],
      slimScroll:     ['assets/vendor/jquery/slimscroll/jquery.slimscroll.min.js'],
      sortable:       ['assets/vendor/jquery/sortable/jquery.sortable.js'],
      nestable:       ['assets/vendor/jquery/nestable/jquery.nestable.js',
                          'assets/vendor/jquery/nestable/nestable.css'],
      filestyle:      ['assets/vendor/jquery/file/bootstrap-filestyle.min.js'],
      slider:         ['assets/vendor/jquery/slider/bootstrap-slider.js',
                          'assets/vendor/jquery/slider/slider.css'],
      chosen:         ['assets/vendor/jquery/chosen/chosen.jquery.min.js',
                          'assets/vendor/jquery/chosen/chosen.css'],
      TouchSpin:      ['assets/vendor/jquery/spinner/jquery.bootstrap-touchspin.min.js',
                          'assets/vendor/jquery/spinner/jquery.bootstrap-touchspin.css'],
      wysiwyg:        ['assets/vendor/jquery/wysiwyg/bootstrap-wysiwyg.js',
                          'assets/vendor/jquery/wysiwyg/jquery.hotkeys.js'],
      dataTable:      ['assets/vendor/jquery/datatables/jquery.dataTables.min.js',
                          'assets/vendor/jquery/datatables/dataTables.bootstrap.js',
                          'assets/vendor/jquery/datatables/dataTables.bootstrap.css'],
      vectorMap:      ['assets/vendor/jquery/jvectormap/jquery-jvectormap.min.js', 
                          'assets/vendor/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
                          'assets/vendor/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
                          'assets/vendor/jquery/jvectormap/jquery-jvectormap.css'],
      footable:       ['assets/vendor/jquery/footable/footable.all.min.js',
                          'assets/vendor/jquery/footable/footable.core.css']
      }
  )
  // oclazyload config
;*/




/**
 * Config for the router
 */
/*app.run(
    [          '$rootScope', '$state', '$stateParams',
      function ($rootScope,   $state,   $stateParams) {
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;        
      }
    ]
  );*/
  
  
  
  
/* Controllers */
var AppCtrl = app.controller('AppCtrl', ['$scope', '$localStorage', '$window', '$interval', '$http',
    function(              $scope, $localStorage,   $window, $interval, $http ) {
      // add 'ie' classes to html
      var isIE = !!navigator.userAgent.match(/MSIE/i);
      isIE && angular.element($window.document.body).addClass('ie');
      isSmartDevice( $window ) && angular.element($window.document.body).addClass('smart');

      // config
      $scope.app = {
        name: 'SIA Property',
        version: '1.0.0',
        // for chart colors
        color: {
          primary: '#7266ba',
          info:    '#23b7e5',
          success: '#27c24c',
          warning: '#fad733',
          danger:  '#f05050',
          light:   '#e8eff0',
          dark:    '#3a3f51',
          black:   '#1c2b36'
        },
        settings: {
          themeID: 1,
          navbarHeaderColor: 'bg-success',// 'bg-black',
          navbarCollapseColor: 'bg-white-only',
          asideColor: 'bg-dark',//'bg-black',
          headerFixed: true,
          asideFixed: false,
          asideFolded: false,
          asideDock: false,
          container: false
        }
      };

      // save settings to local storage
      //if ( angular.isDefined($localStorage.settings) ) {
      //  $scope.app.settings = $localStorage.settings;
      //} 
      //else {
        $localStorage.settings = $scope.app.settings;
      //}
      
        $scope.$watch('app.settings', function(){
            if( $scope.app.settings.asideDock  &&  $scope.app.settings.asideFixed ){
              // aside dock and fixed must set the header fixed.
              $scope.app.settings.headerFixed = true;
            }
            // save to local storage
            $localStorage.settings = $scope.app.settings;
        }, true);


        //implement function to loop for load notification every 60 second
        //initial value
        $scope.notifications = {
            new_enquiry_count: 0,
            new_property_count: 0,
            new_contact_count: 0
        };

        $scope.loadNotification = function()
        {
            $http.post('/admin/webservice/notifications')
                .then(function(response) {
                    var data = response.data;
                    if ( data.success)
                    {
                        $scope.notifications = data.data;
                    }
                    else{
                        //alert('Server Error:\n'+data.message);
                    }
                }, function(x) {
                    //alert('Server Error');
                });
        }
        $scope.loadNotification();//call first when it loaded
        $interval( function(){ $scope.loadNotification(); }, 10000);


        function isSmartDevice( $window )
        {
            // Adapted from http://www.detectmobilebrowsers.com
            var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
            // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
            return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
        }


  }]);


app.config(['$httpProvider',
    function($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        // Set the httpProvider "not authorized" interceptor
        $httpProvider.interceptors.push(['$q',
            function($q) {
                return {
                    responseError: function(rejection) {
                        //console.log('aaaaaa');
                        //console.log(rejection);
                        //console.log(rejection.status);
                        if(rejection.status == 401 || rejection.status == 403)
                        {
                            // Redirect to signin page
                            window.location.href = '/auth/login';
                        }
                        return $q.reject(rejection);
                    }
                };
            }
        ]);
    }
]);



//== BASE FACTORY FUNCTIONS ==================================================================================================
app.factory('BaseService', function($http)
{
    var factory = {};

    factory.post = function(url, data, cache)
    {
        cache = typeof cache !== 'undefined' ? cache : false;
        var request = $http({
            method: "post",
            url: url,
            data: data,
            cache: cache
        });
        return ( request.then(handleSuccess, handleError) );
    }


    // I transform the successful response, unwrapping the application data from the API response payload.
    function handleSuccess (response) {
        return ( response.data );
    }

    // I transform the error response, unwrapping the application dta from the API response payload.
    function handleError(response) {
        // The API response from the server should be returned in a
        // nomralized format. However, if the request was not handled by the
        // server (or what not handles properly - ex. server error), then we
        // may have to normalize it on our end, as best we can.
        if (!angular.isObject(response.data) || !response.data.message) {
            return {success: false, message: 'An unknown error occurred.'};
        }
        // Otherwise, use expected error message.
        else {
            return {success: false, message: response.data.message};
        }
    }

    return factory;
});


//== COMMON FUNCTIONS ==================================================================================================

function getDateFromNow(dayFromNow){
    var day = new Date();
    day.setDate(day.getDate() + dayFromNow);

    var dd = day.getDate();
    var mm = day.getMonth()+1; //January is 0!
    var yyyy = day.getFullYear();
    if(dd<10){
        dd='0'+dd
    }
    if(mm<10){
        mm='0'+mm
    }
    var rDate = dd+'/'+mm+'/'+yyyy;
    return rDate;
}


function longDateStringFormat(dateString){
    var date = new Date(dateString);

    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth()+1).toString();
    var dd  = date.getDate().toString();

    return yyyy+'-'+mm+'-'+dd+'-';
}

function isFileSelected(inputId)
{
    var nme= document.getElementById(inputId);
    if(nme.value.length<4){
        return false;
    }
    else{
        return true;
    }
}
