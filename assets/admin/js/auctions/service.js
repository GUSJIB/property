
app.service(
    "AuctionsService",
    function( $http, $q, BaseService ) {

        // Return public API.
        return({
            listing: function (searchModel) {
                return BaseService.post('/admin/auctions/listing', JSON.stringify(searchModel));
            },
            read: function (id) {
                return BaseService.post('/admin/auctions/read/' + id);
            },
            create: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/auctions/create', JSON.stringify(model));
            },
            update: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/auctions/update', JSON.stringify(model));
            },
            remove: function(id) {
                return BaseService.post('/admin/auctions/remove/' + id);
            },
            status: function() {
                return BaseService.post('/admin/auctions/status');
            }
        });

    }
);