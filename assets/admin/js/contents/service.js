
app.service(
    "ContentsService",
    function( $http, $q, BaseService ) {

        // Return public API.
        return({
            read: function (page_target) {
                return BaseService.post('/admin/contents/read/' + page_target);
            },
            update: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/contents/update', JSON.stringify(model));
            }
        });

    }
);