
app.controller( "ContentController", function( $scope, GoogleMapsInitializer, ContentsService)
{
    $scope.pageLoading = false;
    $scope.dataSubmitting = false;

    // set default setting object.
    $scope.item = [];
    read();

    //========================================================
    function read()
    {
        var page_target = $('#page_target').val();
        ContentsService
            .read(page_target)
            .then(function( response ) {
                $scope.pageLoading = false;
                if(response.success){
                    $scope.item = response.data;
                    if(page_target == 'contact_us'){
                        loadMap();
                    }
                }
                else{
                    alert("Server Error:\n"+response.message);
                }
            }
        );
    }

    $scope.submit = function()
    {
        //Prevent duplicate submit.
        if($scope.dataSubmitting == true){
            return;
        }
        $scope.dataSubmitting = true;

        ContentsService.update($scope.item).then(function(response){
            if(response.success){
                $scope.item = response.data;
                alert('Update record successful.');
            }
            else{
                alert("Server Error:\n"+response.message);
            }
            $scope.dataSubmitting = false;
        });
    }//End function



    //--GOOGLE MAP -----------------------------------------------------------------------------------------------------
    var PropertyGoogleMap = false;

    function loadGoogleMap()
    {
        // if(typeof(google) === 'undefined' ) {
        GoogleMapsInitializer.mapsInitialized.then(function () {
            initialize();
        });
        // }
        // else{
        //     initialize();
        // }
    }

    function initialize() {
        var mapOptions = {};
        if($scope.item.lat != 0 && $scope.item.lon != 0)
        {
            mapOptions = {
                scrollwheel: false,
                zoom: 14,
                center: new google.maps.LatLng($scope.item.lat, $scope.item.lon)
            };
        }
        else{
            mapOptions = {
                scrollwheel: false,
                zoom: 7,
                center: new google.maps.LatLng(13.76479215908216, 100.53863525390625)
            };
        }

        var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
        PropertyGoogleMap = true;
        $scope.googleMap = map;

        google.maps.event.addListener(map, 'click', function(e) {
            placeMarker(e.latLng, map);
        });


        if($scope.item.lat != 0 && $scope.item.lon != 0)
        {
            var position = new google.maps.LatLng($scope.item.lat, $scope.item.lon);
            $scope.marker = new google.maps.Marker({
                position: position,
                map: map,
                title: 'Property position',
                animation: google.maps.Animation.DROP
            });
        }
    }

    function placeMarker(position, map) {
        if ($scope.marker) {
            $scope.marker.setPosition(position);
            $scope.marker.setAnimation(google.maps.Animation.DROP);
        }
        else {
            $scope.marker = new google.maps.Marker({
                position: position,
                map: map,
                title: 'Property position',
                animation: google.maps.Animation.DROP
            });
        }
        //map.panTo(position);

        //set selected position to input box
        $scope.item.lat = position.lat();
        $scope.item.lon = position.lng();
        $scope.$apply();
    }

    function loadMap()
    {
        if(!PropertyGoogleMap) {
            loadGoogleMap();
        }
        else{
            setTimeout(initialize, 700);
            //$scope.initialize();
        }
    }


});