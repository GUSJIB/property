
app.service(
    "SettingsService",
    function( $http, $q, BaseService ) {

        // Return public API.
        return({
            read: function () {
                return BaseService.post('/admin/settings/read');
            },
            update: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/settings/update', JSON.stringify(model));
            },
            uploadPhoto: function(file, field, save_file_name){
                return uploadFile('/admin/settings/upload_photo', file, field, save_file_name);
            }
        });


        // PRIVATE METHODS ---------------------------------------------------------------------------------------------
        function uploadFile(url, file, field, file_name){
            var fd = new FormData();
            fd.append('file', file);
            fd.append('field', field);
            fd.append('file_name', file_name);
            return $http.post(url, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            });
        }

    }
);