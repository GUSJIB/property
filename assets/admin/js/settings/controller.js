
app.controller('SettingsController', function ($scope, $modal, SettingsService)
{
    $scope.showLoading = false;
    $scope.item = false;

    read();


    //========================================================
    function setItem(response)
    {
        if(response.success){
            $scope.item = response.data;
        }
        else{
            alert("Server Error:\n"+response.message);
        }
    }

    // I load the preferences data from the server.
    function read()
    {
        if($scope.pageLoading == true){
            return;
        }
        $scope.pageLoading = true;
        SettingsService
            .read()
            .then(function(response)
            {
                $scope.pageLoading = false;
                setItem(response);
            });
    }//End read function

    $scope.submit = function()
    {
        //Prevent duplicate submit.
        if($scope.dataSubmitting || $scope.pageLoading){
            return;
        }
        $scope.dataSubmitting = true;


        if($scope.item.alert_start) {
            $scope.item.alert_start = longDateStringFormat($scope.item.alert_start);
        }

        if($scope.item.alert_end) {
            $scope.item.alert_end = longDateStringFormat($scope.item.alert_end);
        }

        SettingsService.update($scope.item).then(function(response)
        {
            $scope.dataSubmitting = false;
            setItem(response);
        });
    }//End function


    //Upload photo
    $scope.photoUploading = [];
    $scope.photoLoadTime = [];//used for append in photo url on html
    $scope.uploadPhoto = function($event, files, fieldName, save_file_name)
    {
        if(files && files.length > 0)
        {
            //Prevent duplicate submit.
            if($scope.photoUploading[fieldName]){
                return;
            }
            $scope.photoUploading[fieldName] = true;

            SettingsService.uploadPhoto(files[0], fieldName, save_file_name).then(function(response)
            {
                $scope.photoUploading[fieldName] = false;
                $scope.photoLoadTime[fieldName] = (new Date()).getTime();
                setItem(response.data);
            });
        }
    }


});


