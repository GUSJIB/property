
var app =  angular.module('app', ['angularFileUpload']);

app.config(['$httpProvider',
    function($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        // Set the httpProvider "not authorized" interceptor
        $httpProvider.interceptors.push(['$q',
            function($q) {
                return {
                    responseError: function(rejection) {
                        //console.log('aaaaaa');
                        //console.log(rejection);
                        //console.log(rejection.status);
                        if(rejection.status == 401 || rejection.status == 403)
                        {
                            // Redirect to signin page
                            window.location.href = '/auth/login';
                        }
                        return $q.reject(rejection);
                    }
                };
            }
        ]);
    }
]);


app.controller('UploadController', function ($scope, UploadService, FileUploader)
{
    $scope.folder_path = $('#folder_path').val();
    $scope.photos = [];

    $scope.formData = {folder_path: $scope.folder_path};

    $scope.uploading = false;

    listing();



    var uploader = $scope.uploader = new FileUploader({
        url: '/admin/uploads/upload',
        formData: [$scope.formData],
        autoUpload: true,
        removeAfterUpload: true
    });

    // FILTERS
    uploader.filters.push({
        name: 'limitFiles',
        fn: function(item, options) {
            return this.queue.length < 10;
        }
    });

    uploader.filters.push({
        name: 'allowExtension',
        fn: function(item, options) {
            return (item.type == "image/jpeg" || item.type == "image/gif" || item.type == "image/png");
        }
    });

    // CALLBACKS
    uploader.onWhenAddingFileFailed = function(item, filter, options)
    {
        alert("we can't upload file type '" + item.type + "'");
        // console.info('onWhenAddingFileFailed', item, filter, options);
    };

    uploader.onBeforeUploadItem = function(item)
    {
        $scope.uploading = true;
    };

    uploader.onCompleteItem = function(fileItem, response, status, headers)
    {
        if(response.success){
            $scope.photos.unshift(response.data[0]);
        }
        else{
            alert("Server Error:\n"+response.message);
        }
        $scope.uploading = false;
    };


    //list all record from database
    function listing()
    {
        UploadService
            .listing($scope.folder_path)
            .then(function( response ) {
                $scope.showLoading = false;
                $scope.photos = response.data;
            }
        );
    }//End function

    $scope.destroy = function(item)
    {
        // console.log(item);
        var confirm = window.confirm("Confirm to delete photo?");
        if(confirm){
            UploadService
                .remove(item.path)
                .then(function(response) {
                    if(response.success) {
                        for (var i = 0; i < $scope.photos.length; i++)
                        {
                            if($scope.photos[i].name == item.name){
                                $scope.photos.splice(i, 1);
                            }
                        }
                    }
                    else{
                        alert("Server Error:\n"+response.message);
                    }
                }
            );
        }
    };

    $scope.fileSelected = function(item)
    {
        var win = (window.opener?window.opener:window.parent);
        win.document.getElementById(GetUrlParam('input')).value = item.url;
        if (typeof(win.ImageDialog) != "undefined") {
            if (win.ImageDialog.getImageData)
                win.ImageDialog.getImageData();
            if (win.ImageDialog.showPreviewImage)
                win.ImageDialog.showPreviewImage(item.url);
        }
        win.tinyMCE.activeEditor.windowManager.close();
    };

    function GetUrlParam(varName, url)
    {
        var ret = '';
        if(!url)
            url = self.location.href;
        if(url.indexOf('?') > -1){
            url = url.substr(url.indexOf('?') + 1);
            url = url.split('&');
            for(i = 0; i < url.length; i++){
                var tmp = url[i].split('=');
                if(tmp[0] && tmp[1] && tmp[0] == varName){
                    ret = tmp[1];
                    break;
                }
            }
        }

        return ret;
    };



});






function isFileSelected(inputId)
{
    var nme= document.getElementById(inputId);
    if(nme.value.length<4){
        return false;
    }
    else{
        return true;
    }
}