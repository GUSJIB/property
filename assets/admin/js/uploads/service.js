app.factory('BaseService', function($http)
{
    var factory = {};

    factory.post = function(url, data, cache)
    {
        cache = typeof cache !== 'undefined' ? cache : false;
        var request = $http({
            method: "post",
            url: url,
            data: data,
            cache: cache
        });
        return ( request.then(handleSuccess, handleError) );
    }


    // I transform the successful response, unwrapping the application data from the API response payload.
    function handleSuccess (response) {
        return ( response.data );
    }

    // I transform the error response, unwrapping the application dta from the API response payload.
    function handleError(response) {
        // The API response from the server should be returned in a
        // nomralized format. However, if the request was not handled by the
        // server (or what not handles properly - ex. server error), then we
        // may have to normalize it on our end, as best we can.
        if (!angular.isObject(response.data) || !response.data.message) {
            return {success: false, message: 'An unknown error occurred.'};
        }
        // Otherwise, use expected error message.
        else {
            return {success: false, message: response.data.message};
        }
    }

    return factory;
});

app.service(
    "UploadService",
    function( $http, $q, BaseService )
    {
        // Return public API.
        return({
            listing: function (folder_path) {
                var data = {'folder_path' : folder_path};
                return BaseService.post('/admin/uploads/listing', JSON.stringify(data));
            },
            upload: function(file, folder_path){
                var fd = new FormData();
                fd.append('file', file);
                fd.append('folder_path', folder_path);
                return $http.post('/admin/uploads/upload', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                });
            },
            remove: function (file_path) {
                var data = {'file_path' : file_path};
                return BaseService.post('/admin/uploads/delete', JSON.stringify(data));
            }
        });

    }
);