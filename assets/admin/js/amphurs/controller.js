
app.controller('AmphursController', function ($scope, $modal, AmphursService)
{
    $scope.provincesList = [];
    $scope.searchModel = newSearch();

    $scope.showLoading = false;
    $scope.listing = listing;
    listing();

    function newSearch()
    {
        var searchModel = {
            name: '',
            province_id: '',
            lat: '',
            lon: '',
            position: '',
            post_count: '',

            pageSize: 50,
            currentPage: 1,
            totalRows: 0,
            beginRow: 0,
            endRow: 0,
            totalPages: 0,
            rows: {}
        };

        return searchModel;
    }

    //list all record from database
    function listing()
    {
        $scope.showLoading = true;
        AmphursService
            .listing($scope.searchModel)
            .then(function( response ) {
                $scope.searchModel = response.data;
                if($scope.provincesList.length < 1){
                    loadProvinces();
                }
                else{
                    $scope.showLoading = false;
                }
            }
        );
    }//End function

    function loadProvinces()
    {
        AmphursService
            .loadProvinces()
            .then(function( response ) {
                $scope.showLoading = false;
                $scope.provincesList = response.data;
            }
        );

    }//End function

    $scope.goPage = function(){
        listing();
    }

    $scope.destroy = function(item)
    {
        var confirm = window.confirm("Confirm to delete record?");
        if(confirm){
            AmphursService
                .remove(item.id)
                .then(function(response) {
                    if(response.success) {
                        $scope.showLoading = false;
                        listing();

                    }
                    else{
                        alert("Server Error:\n"+response.message);
                    }
                }
            );
        }
    };

    $scope.search = function()
    {
        $scope.searchModel.currentPage = 1;
        listing();
    }//End function

    $scope.reset = function()
    {
        $scope.searchModel = newSearch();
        listing();
    }//End function


    $scope.status_label_class = {
        1: "bg-warning",
        2: "bg-info",
        3: "bg-success",
        4: "bg-dark"
    };


    $scope.openPopup = function(item)
    {
        if(item == undefined){
            item = {
                id: 0,
                name: '',
                lat: '',
                lon: '',
                position: '',
                post_count: ''
            };
        }

        //show popup
        $scope.pupupForm = $modal.open({
            templateUrl: 'PopupForm.html',
            controller: 'PopupFormController',
            size: 'sn',
            resolve: {
                item: function () {
                    return item;
                },
                status: function() {
                    return $scope.status;
                }
            }
        });

        $scope.pupupForm.result.then(
            function (selectedItem) {
                selectedItem.province_name = getProvinceName(selectedItem.province_id);

                if(selectedItem.action === 'create') {
                    // insert data row in table at the end.
                    $scope.searchModel.rows.unshift(selectedItem);
                }
                else{
                    //update data row in table.
                    var item;
                    for (var i = 0; i < $scope.searchModel.rows.length; i++) {
                        item = $scope.searchModel.rows[i];
                        if (item.id == selectedItem.id) {
                            $scope.searchModel.rows[i] = selectedItem;
                        }
                    }
                }
                $scope.showLoading = false;
            }
        );

    }//End function

    function getProvinceName(provinceId){
       var len = $scope.provincesList.length;
        for(var i=0;i<len;i++){
            if ($scope.provincesList[i].id == provinceId)
            {
                return $scope.provincesList[i].name;
            }
        }
        return '';
    }

});




/********************************************************************************************************************
 * ADD / EDIT POPUP
 *******************************************************************************************************************/

var AmphursGoogleMap = false;
app.controller( "PopupFormController", function( $scope, $modalInstance, item, AmphursService,GoogleMapsInitializer )
    {
        $scope.pageLoading = false;
        $scope.dataSubmitting = false;
        $scope.provincesList = {};

        loadProvinces();
        $scope.googleMap = false;
        $scope.marker = false;

        // set default setting object.
        $scope.item = item;
        if($scope.item.id > 0){
            $scope.pageLoading = true;
            read();
        }

        //========================================================
        // I load the preferences data from the server.
        function read()
        {
            AmphursService
                .read($scope.item.id)
                .then(function( response ) {
                    $scope.pageLoading = false;
                    if(response.success){
                        $scope.item = response.data;
                    }
                    else{
                        alert("Server Error:\n"+response.message);
                        $modalInstance.dismiss('cancel');
                    }
                }
            );
        }//End read function

        $scope.submit = function()
        {
            //Prevent duplicate submit.
            if($scope.dataSubmitting == true){
                return;
            }
            $scope.dataSubmitting = true;

            var request;
            var action = 'create';
            if($scope.item.id < 1){
                request = AmphursService.create($scope.item)
            }
            else{
                action = 'update';
                request = AmphursService.update($scope.item)
            }
            request.then(function(response){
                    if(response.success){
                        //close dialog and refresh listing.
                        //case: insert, then append object to listing as lasted.
                        //case: update, update row information.
                        response.data['action'] = action;
                        $modalInstance.close(response.data);
                    }
                    else{
                        alert("Server Error:\n"+response.message);
                    }
                    $scope.dataSubmitting = false;
                }
            );
        }//End function

        $scope.cancel = function()
        {
            $scope.pageLoading = false;
            $modalInstance.dismiss('cancel');
        }//End function


        function loadProvinces()
        {
            AmphursService
                .loadProvinces()
                .then(function( response ) {
                    $scope.provincesList = response.data;
                }
            );

        }//End function

        function loadGoogleMap()
        {
            if(typeof(google) === 'undefined' ) {
                GoogleMapsInitializer.mapsInitialized.then(function () {
                    initialize();
                });
            }
            else{
                initialize();
            }
        }

        function initialize() {

            var mapOptions = {};
            if($scope.item.lat != 0 && $scope.item.lon != 0)
            {
                mapOptions = {
                    scrollwheel: false,
                    zoom: 12,
                    center: new google.maps.LatLng($scope.item.lat, $scope.item.lon)
                };
            }
            else{
                mapOptions = {
                    scrollwheel: false,
                    zoom: 7,
                    center: new google.maps.LatLng(13.76479215908216, 100.53863525390625)
                };
            }

            var map = new google.maps.Map(document.getElementById('amphursPopupmap'), mapOptions);
            AmphursGoogleMap = true;
            $scope.googleMap = map;

            google.maps.event.addListener(map, 'click', function(e) {
                placeMarker(e.latLng, map);
            });


            if($scope.item.lat != 0 && $scope.item.lon != 0)
            {
                var position = new google.maps.LatLng($scope.item.lat, $scope.item.lon);
                $scope.marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: 'Property position',
                    animation: google.maps.Animation.DROP
                });
            }
        }


        function placeMarker(position, map)
        {
            if ($scope.marker) {
                $scope.marker.setPosition(position);
                $scope.marker.setAnimation(google.maps.Animation.DROP);
            }
            else {
                $scope.marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: 'Property position',
                    animation: google.maps.Animation.DROP
                });
            }
            //map.panTo(position);

            //set selected position to input box
            $scope.item.lat = position.lat();
            $scope.item.lon = position.lng();
        }

        if(!AmphursGoogleMap) {
            loadGoogleMap();
        }
        else{
            setTimeout(initialize, 700);
            
        }
    });