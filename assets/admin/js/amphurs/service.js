
app.service(
    "AmphursService",
    function( $http, $q, BaseService ) {

        // Return public API.
        return({
            listing: function (searchModel) {
                return BaseService.post('/admin/amphurs/listing', JSON.stringify(searchModel));
            },
            read: function (id) {
                return BaseService.post('/admin/amphurs/read/' + id, {});
            },
            create: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/amphurs/create', JSON.stringify(model));

            },
            update: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/amphurs/update', JSON.stringify(model));
            },
            remove: function(id) {
                return BaseService.post('/admin/amphurs/remove/' + id), {};
            },
            loadProvinces: function () {
                return BaseService.post('/admin/provinces/loadProvinces', {}, true);

            }
        });

    }
);