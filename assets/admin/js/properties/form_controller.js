
app.controller('PropertyFormController', function ($scope, $modal, $location, PropertiesService, GoogleMapsInitializer)
{
    $scope.googleMap = false;
    $scope.marker = false;

    $scope.cover = {};
    $scope.provinces = {};
    $scope.amphurs = {};

    $scope.item = {};
    $scope.isInitializing = true;

    // on change country code
    $scope.$watch('item.country_code', function() {
        if(!$scope.isInitializing){
            $scope.item.province_id = '';
            $scope.item.amphur_id = '';
        }
    });

    // on change province
    $scope.$watch('item.province_id', function(newVal) {
        if(!$scope.isInitializing){
            $scope.item.amphur_id = '';
        }
    });

    $scope.loadProvinces = function()
    {
        $scope.provinces = [];
        $scope.amphurs = [];
        if($scope.item.country_code != undefined && $scope.item.country_code != ""){
            PropertiesService.provinces($scope.item.country_code).then(function(response){
                $scope.provinces = response.data;
                $scope.loadAmphurs();
            });
        }
    };

    $scope.loadAmphurs = function()
    {
        var provinceId = $scope.item.province_id;
        console.log(provinceId);
        $scope.amphurs = [];
        if(provinceId != undefined && provinceId != ""){
            PropertiesService.amphurs(provinceId).then(function(response){
                $scope.amphurs = response.data;
                $scope.isInitializing = false;
            });
        }
    };

    $scope.init = function(id)
    {
        if(id > 0) {
            $scope.property_id = id;
            PropertiesService.read(id).then(function (response) {
                $scope.item = response.data;
                $scope.item.area_sqm = parseInt(response.data.area_sqm);
                $scope.item.sale_price = parseInt(response.data.sale_price);
                $scope.item.monthly_rate = parseInt(response.data.monthly_rate);
                $scope.item.yearly_rate = parseInt(response.data.yearly_rate);
                $scope.item.building_floor_qty = parseInt(response.data.building_floor_qty);
                $scope.item.sub_floor_qty_in_room = parseInt(response.data.sub_floor_qty_in_room);
                $scope.item.built_year = parseInt(response.data.built_year);
                $scope.loadProvinces();
                loadMap();
            });
        }
        else{
            loadMap();
        }
    };

    $scope.submit = function()
    {
        if(typeof($scope.item.id) === 'undefined' || $scope.item.id < 1){
            createForm();
        }
        else{
            updateForm();
        }
    };

    function createForm()
    {
        // console.log($scope.item);
        PropertiesService.create($scope.item).then(function(response)
        {
            if(response.success)
            {
                if(isFileSelected('cover'))
                {
                    //upload cover
                    PropertiesService.upload($scope.cover, response.data.id).then(function(response){
                        console.log(response);
                        window.location.href = '/admin/properties';
                    });
                }
                else{
                    window.location.href = '/admin/properties';
                }
            }
            else{
                alert("Server Error:\n"+response.message);
            }
        });
    }

    function updateForm()
    {
        PropertiesService.update($scope.item).then(function(response){

            if(response.success)
            {
                if(isFileSelected('cover') != false)
                {
                    //upload cover
                    PropertiesService.upload($scope.cover, response.data.id).then(function(response){
                        window.location.href = '/admin/properties';
                    });
                }
                else{
                    window.location.href = '/admin/properties';
                }
            }
            else{
                alert("Server Error:\n"+response.message);
            }
        });
    }


    $scope.cancel = function()
    {
        window.location.href = '/admin/properties';
    }





    //--GOOGLE MAP -----------------------------------------------------------------------------------------------------
    var PropertyGoogleMap = false;

    function loadGoogleMap()
    {
        // if(typeof(google) === 'undefined' ) {
        GoogleMapsInitializer.mapsInitialized.then(function () {
            initialize();
        });
        // }
        // else{
        //     initialize();
        // }
    }

    function initialize() {
        var mapOptions = {};
        if($scope.item.lat != 0 && $scope.item.lon != 0)
        {
            mapOptions = {
                scrollwheel: false,
                zoom: 14,
                center: new google.maps.LatLng($scope.item.lat, $scope.item.lon)
            };
        }
        else{
            mapOptions = {
                scrollwheel: false,
                zoom: 7,
                center: new google.maps.LatLng(13.76479215908216, 100.53863525390625)
            };
        }

        var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
        PropertyGoogleMap = true;
        $scope.googleMap = map;

        google.maps.event.addListener(map, 'click', function(e) {
            placeMarker(e.latLng, map);
        });


        if($scope.item.lat != 0 && $scope.item.lon != 0)
        {
            var position = new google.maps.LatLng($scope.item.lat, $scope.item.lon);
            $scope.marker = new google.maps.Marker({
                position: position,
                map: map,
                title: 'Property position',
                animation: google.maps.Animation.DROP
            });
        }
    }

    function placeMarker(position, map) {
        if ($scope.marker) {
            $scope.marker.setPosition(position);
            $scope.marker.setAnimation(google.maps.Animation.DROP);
        }
        else {
            $scope.marker = new google.maps.Marker({
                position: position,
                map: map,
                title: 'Property position',
                animation: google.maps.Animation.DROP
            });
        }
        //map.panTo(position);

        //set selected position to input box
        $scope.item.lat = position.lat();
        $scope.item.lon = position.lng();
        $scope.$apply();
    }

    function loadMap()
    {
        if(!PropertyGoogleMap) {
            loadGoogleMap();
        }
        else{
            setTimeout(initialize, 700);
            //$scope.initialize();
        }
    }




});





(function($, ng) {
    'use strict';

    var $val = $.fn.val; // save original jQuery function

    // override jQuery function
    $.fn.val = function (value) {
        // if getter, just return original
        if (!arguments.length) {
            return $val.call(this);
        }

        // get result of original function
        var result = $val.call(this, value);

        // trigger angular input (this[0] is the DOM object)
        ng.element(this[0]).triggerHandler('input');

        // return the original result
        return result;
    }
})(window.jQuery, window.angular);