
app.controller('ListsController', function ($scope, $modal, PropertiesService)
{
    PropertiesService.all_country().then(function(response){
        $scope.countries = response.data;
    });
    PropertiesService.all_property_type().then(function(response){
        $scope.property_types = response.data;
    });

    $scope.searchModel = newSearch();

    $scope.showLoading = false;
    $scope.listing = listing;
    listing();

    function newSearch()
    {
        var searchModel = {
            username: '',
            first_name: '',
            last_name: '',
            email: '',
            phone: '',
            active: '',
            created_at_from: '',
            created_at_to: '',

            pageSize: 50,
            currentPage: 1,
            totalRows: 0,
            beginRow: 0,
            endRow: 0,
            totalPages: 0,
            rows: {}
        };

        return searchModel;
    }

    //list all record from database
    function listing()
    {
        if($scope.searchModel.first_name.length > 0){
            var names = $scope.searchModel.first_name.split(' ');
            $scope.searchModel.first_name = names[0];
            if(names.length > 1) {
                $scope.searchModel.last_name = names[1];
            }
            else{
                $scope.searchModel.last_name = '';
            }
        }
        $scope.showLoading = true;
        PropertiesService
            .listing($scope.searchModel)
            .then(function( response ) {
                $scope.showLoading = false;
                $scope.searchModel = response.data;

                //compact name after search to show in table filter input.
                $scope.searchModel.first_name = $scope.searchModel.first_name + ' ' + $scope.searchModel.last_name;
            }
        );
    }//End function

    $scope.goPage = function(){
        listing();
    }

    $scope.destroy = function(item)
    {
        var confirm = window.confirm("Confirm to delete record?");
        if(confirm){
            PropertiesService
                .remove(item.id)
                .then(function(response) {
                    if(response.success) {
                        $scope.showLoading = false;
                        listing();
                    }
                    else{
                        alert("Server Error:\n"+response.message);
                    }
                }
            );
        }
    };

    $scope.search = function()
    {
        $scope.searchModel.currentPage = 1;
        listing();
    }//End function

    $scope.reset = function()
    {
        $scope.searchModel = newSearch();
        listing();
    }//End function

    $scope.statusName = function(statusId)
    {
        if(statusId==1)
            return 'new';
        if(statusId==2)
            return 'published';
        if(statusId==3)
            return 'Hidden';
        if(statusId==10)
            return 'Removed';
        return 'Unknown';
    }

    $scope.statusClass = function(statusId)
    {
        if(statusId==1)
            return 'label-warning';
        if(statusId==2)
            return 'label-success';
        if(statusId==3)
            return 'label-primary';
        if(statusId==10)
            return 'label-default';
        return 'Unknown';
    }

});