
app.service(
    "PropertiesService",
    function( $http, $q, BaseService ) {

        // Return public API.
        return({
            listing: function (searchModel) {
                return BaseService.post('/admin/properties/listing', JSON.stringify(searchModel));
            },
            read: function (id) {
                return BaseService.post('/admin/properties/read/' + id);
            },
            create: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/properties/create', JSON.stringify(model));
            },
            update: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/properties/update', JSON.stringify(model));
            },
            remove: function(id) {
                return BaseService.post('/admin/properties/remove/' + id);
            },
            provinces: function(country) {
                return BaseService.post('/admin/properties/provinces/' + country);
            },
            amphurs: function(province) {
                return BaseService.post('/admin/properties/amphurs/' + province);
            },
            status: function() {
                return BaseService.post('/admin/properties/status');
            },
            all_country: function(){
                return BaseService.post('/admin/properties/all_country');
            },
            all_property_type: function(){
                return BaseService.post('/admin/properties/all_property_type');
            },
            upload: function(file, property_id){
                var fd = new FormData();
                fd.append('file', file);
                fd.append('property_id', property_id);
                return $http.post('/admin/properties/cover', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                });
            },
            galleries: function(id){
                return BaseService.post('/admin/properties/gallery/' + id);
            },
            photo_remove: function(photo_id){
                return BaseService.post('/admin/properties/photo_remove/' + photo_id);
            }
        });

    }
);