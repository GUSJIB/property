
app.controller('PhotosController', function ($scope, $modal, PropertiesService, FileUploader)
{
    $scope.formData = {};
    $scope.init = function(property_id){
        $scope.formData.property_id = property_id;

        PropertiesService.galleries(property_id).then(function (response) {
            $scope.photos = response.data;
        });
    }

    var uploader = $scope.uploader = new FileUploader({
        url: '/admin/properties/upload',
        formData: [$scope.formData],
        autoUpload: true,
        removeAfterUpload: true
    });

    // FILTERS
    uploader.filters.push({
        name: 'limitFiles',
        fn: function(item, options) {
            return this.queue.length < 10;
        }
    });

    uploader.filters.push({
        name: 'allowExtension',
        fn: function(item, options) {
            return (item.type == "image/jpeg" || item.type == "image/gif" || item.type == "image/png");
        }
    });

    // CALLBACKS
    uploader.onWhenAddingFileFailed = function(item, filter, options) {
        alert("we can't upload file type '" + item.type + "'");
        // console.info('onWhenAddingFileFailed', item, filter, options);
    };
    // uploader.onAfterAddingFile = function(fileItem) {
    //     console.info('onAfterAddingFile', fileItem);
    // };
    // uploader.onAfterAddingAll = function(addedFileItems) {
    //     console.info('onAfterAddingAll', addedFileItems);
    // };
    // uploader.onBeforeUploadItem = function(item) {
    //     console.info('onBeforeUploadItem', item);
    // };
    // uploader.onProgressItem = function(fileItem, progress) {
    //     console.info('onProgressItem', fileItem, progress);
    // };
    // uploader.onProgressAll = function(progress) {
    //     console.info('onProgressAll', progress);
    // };
    // uploader.onSuccessItem = function(fileItem, response, status, headers) {
    //     console.info('onSuccessItem', fileItem, response, status, headers);
    // };
    // uploader.onErrorItem = function(fileItem, response, status, headers) {
    //     console.info('onErrorItem', fileItem, response, status, headers);
    // };
    // uploader.onCancelItem = function(fileItem, response, status, headers) {
    //     console.info('onCancelItem', fileItem, response, status, headers);
    // };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        // console.info('onCompleteItem', fileItem, response, status, headers);
        $scope.init($scope.formData.property_id);
    };
    // uploader.onCompleteAll = function() {
    //     console.info('onCompleteAll');
    // };

    // console.info('uploader', uploader);

    $scope.destroy = function(item)
    {
        // console.log(item);
        var confirm = window.confirm("Confirm to delete photo?");
        if(confirm){
            PropertiesService
                .photo_remove(item.id)
                .then(function(response) {
                    if(response.success) {
                        $scope.init($scope.formData.property_id);
                    }
                    else{
                        alert("Server Error:\n"+response.message);
                    }
                }
            );
        }
    };

});