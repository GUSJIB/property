
app.controller('ContactsController', function ($scope, $modal, ContactsService)
{
    loadStatus();
    $scope.searchModel = newSearch();

    $scope.showLoading = false;
    $scope.listing = listing;
    listing();

    function newSearch()
    {
        var searchModel = {
            subject: '',
            name: '',
            phone: '',
            email: '',
            status: '',
            created_at_from: '',
            created_at_to: '',

            pageSize: 50,
            currentPage: 1,
            totalRows: 0,
            beginRow: 0,
            endRow: 0,
            totalPages: 0,
            rows: {}
        };

        return searchModel;
    }

    function loadStatus()
    {
        ContactsService.status().then(function(response){
            if(response.hasOwnProperty('data')){
                $scope.status = response.data;
            }
        });
    }

    //list all record from database
    function listing()
    {
        $scope.showLoading = true;
        ContactsService
            .listing($scope.searchModel)
            .then(function( response ) {
                $scope.showLoading = false;
                $scope.searchModel = response.data;
            }
        );
    }//End function

    $scope.goPage = function(){
        listing();
    }

    $scope.destroy = function(item)
    {
        var confirm = window.confirm("Confirm to delete record?");
        if(confirm){
            ContactsService
                .remove(item.id)
                .then(function(response) {
                    if(response.success) {
                        $scope.showLoading = false;
                        listing();
                    }
                    else{
                        alert("Server Error:\n"+response.message);
                    }
                }
            );
        }
    };

    $scope.search = function()
    {
        $scope.searchModel.currentPage = 1;
        listing();
    }//End function

    $scope.reset = function()
    {
        $scope.searchModel = newSearch();
        listing();
    }//End function


    $scope.status_label_class = {
        1: "bg-warning",
        2: "bg-info",
        3: "bg-success",
        4: "bg-dark"
    };


    $scope.openPopup = function(item)
    {
        if(item == undefined){
            item = {
                id: 0,
                name: '',
                email: '',
                phone: '',
                subject: '',
                detail: '',
                remark: '',
                status: '1',
                post_ip: ''
            };
        }

        //show popup
        $scope.pupupForm = $modal.open({
            templateUrl: 'PopupForm.html',
            controller: 'PopupFormController',
            size: 'lg',
            resolve: {
                item: function () {
                    return item;
                },
                status: function() {
                    return $scope.status;
                }
            }
        });

        $scope.pupupForm.result.then(
            function (selectedItem) {
                if(selectedItem.action === 'create') {
                    // insert data row in table at the end.
                    $scope.searchModel.rows.unshift(selectedItem);
                }
                else{
                    //update data row in table.
                    var item;
                    for (var i = 0; i < $scope.searchModel.rows.length; i++) {
                        item = $scope.searchModel.rows[i];
                        if (item.id == selectedItem.id) {
                            $scope.searchModel.rows[i] = selectedItem;
                        }
                    }
                }
                $scope.showLoading = false;
            }
        );

    }//End function

});




//htpassword apache

app.controller( "PopupFormController",
    function( $scope, $modalInstance, item, ContactsService )
    {
        $scope.pageLoading = false;
        $scope.dataSubmitting = false;

        // set default setting object.
        $scope.item = item;
        if($scope.item.id > 0){
            $scope.pageLoading = true;
            read();
        }

        //========================================================
        // I load the preferences data from the server.
        function read()
        {
            ContactsService
                .read($scope.item.id)
                .then(function( response ) {
                    $scope.pageLoading = false;
                    if(response.success){
                        $scope.item = response.data;
                    }
                    else{
                        alert("Server Error:\n"+response.message);
                        $modalInstance.dismiss('cancel');
                    }
                }
            );
        }//End read function

        $scope.submit = function()
        {
            //Prevent duplicate submit.
            if($scope.dataSubmitting == true){
                return;
            }
            $scope.dataSubmitting = true;

            var request;
            var action = 'create';
            if($scope.item.id < 1){
                request = ContactsService.create($scope.item)
            }
            else{
                action = 'update';
                request = ContactsService.update($scope.item)
            }
            request.then(function(response){
                    if(response.success){
                        //close dialog and refresh listing.
                        //case: insert, then append object to listing as lasted.
                        //case: update, update row information.
                        response.data['action'] = action;
                        $modalInstance.close(response.data);
                    }
                    else{
                        alert("Server Error:\n"+response.message);
                    }
                    $scope.dataSubmitting = false;
                }
            );
        }//End function

        $scope.cancel = function()
        {
            $scope.pageLoading = false;
            $modalInstance.dismiss('cancel');
        }//End function

    }
);