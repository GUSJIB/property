
app.service(
    "ContactsService",
    function( $http, $q, BaseService ) {

        // Return public API.
        return({
            listing: function (searchModel) {
                return BaseService.post('/admin/contacts/listing', JSON.stringify(searchModel));
            },
            read: function (id) {
                return BaseService.post('/admin/contacts/read/' + id);
            },
            create: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/contacts/create', JSON.stringify(model));
            },
            update: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/contacts/update', JSON.stringify(model));
            },
            remove: function(id) {
                return BaseService.post('/admin/contacts/remove/' + id);
            },
            status: function() {
                return BaseService.post('/admin/contacts/status');
            }
        });

    }
);