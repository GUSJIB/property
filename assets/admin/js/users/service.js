
app.service(
    "UsersService",
    function( $http, $q, BaseService ) {

        // Return public API.
        return({
            listing: function (searchModel) {
                return BaseService.post('/admin/users/listing', JSON.stringify(searchModel));
            },
            read: function (id) {
                return BaseService.post('/admin/users/read/' + id);
            },
            create: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/users/create', JSON.stringify(model));
            },
            update: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/users/update', JSON.stringify(model));
            },
            remove: function(id) {
                return BaseService.post('/admin/users/remove/' + id);
            },
            status: function() {
                return BaseService.post('/admin/users/status');
            },
            upload: function(file, user_id){
                var fd = new FormData();
                fd.append('file', file);
                fd.append('user_id', user_id);
                return $http.post('/admin/users/upload', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                });
            }
        });

    }
);