
app.controller('UsersController', function ($scope, $modal, UsersService)
{
    $scope.searchModel = newSearch();

    $scope.showLoading = false;
    $scope.listing = listing;
    listing();

    function newSearch()
    {
        var searchModel = {
            username: '',
            first_name: '',
            last_name: '',
            email: '',
            phone: '',
            active: '',
            created_at_from: '',
            created_at_to: '',
            select_group: $('#select_group').val(),

            pageSize: 50,
            currentPage: 1,
            totalRows: 0,
            beginRow: 0,
            endRow: 0,
            totalPages: 0,
            rows: {}
        };

        return searchModel;
    }

    //list all record from database
    function listing()
    {
        if($scope.searchModel.first_name.length > 0){
            var names = $scope.searchModel.first_name.split(' ');
            $scope.searchModel.first_name = names[0];
            if(names.length > 1) {
                $scope.searchModel.last_name = names[1];
            }
            else{
                $scope.searchModel.last_name = '';
            }
        }
        $scope.showLoading = true;
        UsersService
            .listing($scope.searchModel)
            .then(function( response ) {
                $scope.showLoading = false;
                if(response.success) {
                    $scope.searchModel = response.data;

                    //compact name after search to show in table filter input.
                    $scope.searchModel.first_name = $scope.searchModel.first_name + ' ' + $scope.searchModel.last_name;
                }
                else{
                    alert('Error: '+response.message);
                    $scope.searchModel = {};
                }
            }
        );
    }//End function

    $scope.goPage = function(){
        listing();
    }



    $scope.checkboxClick = function(){
        //No action just force update flag from ui to angular
        $scope.$apply();
    }

    $scope.destroy = function(item)
    {
        var confirm = window.confirm("Confirm to delete record?");
        if(confirm){
            UsersService
                .remove(item.id)
                .then(function(response) {
                    if(response.success) {
                        $scope.showLoading = false;
                        listing();
                    }
                    else{
                        alert("Server Error:\n"+response.message);
                    }
                }
            );
        }
    };

    $scope.search = function()
    {
        $scope.searchModel.currentPage = 1;
        listing();
    }//End function

    $scope.reset = function()
    {
        $scope.searchModel = newSearch();
        listing();
    }//End function


    $scope.openPopup = function(item)
    {
        if(item == undefined){
            item = {
                id: 0,
                username: '',
                first_name: '',
                last_name: '',
                gender: '',
                birthday: '',
                email: '',
                mobile: '',
                phone: '',
                fax: '',
                company: '',
                facebook_id: '',
                facebook_url: '',
                is_agent: false,
                is_co_agent: false,
                active: true
            };
        }

        //show popup
        $scope.pupupForm = $modal.open({
            templateUrl: 'PopupForm.html',
            controller: 'PopupFormController',
            size: 'lg',
            resolve: {
                item: function () {
                    return item;
                },
                status: function() {
                    return $scope.status;
                }
            }
        });

        $scope.pupupForm.result.then(
            function (selectedItem) {
                if(selectedItem.action === 'create') {
                    // insert data row in table at the end.
                    $scope.searchModel.rows.unshift(selectedItem);
                }
                else{
                    //update data row in table.
                    var item;
                    for (var i = 0; i < $scope.searchModel.rows.length; i++) {
                        item = $scope.searchModel.rows[i];
                        if (item.id == selectedItem.id) {
                            $scope.searchModel.rows[i] = selectedItem;
                        }
                    }
                }
                $scope.showLoading = false;
            }
        );

    }//End function

});




//htpassword apache

app.controller( "PopupFormController",
    function( $scope, $modalInstance, item, UsersService )
    {
        $scope.photo = {};
        $scope.pageLoading = false;
        $scope.dataSubmitting = false;

        // set default setting object.
        $scope.item = item;
        if($scope.item.id > 0){
            $scope.pageLoading = true;
            read();
        }

        //========================================================
        // I load the preferences data from the server.
        function read()
        {
            UsersService
                .read($scope.item.id)
                .then(function( response ) {
                    $scope.pageLoading = false;
                    if(response.success){
                        $scope.item = response.data;

                        //fix checkbox bug in case return value is 0 or 1 then checkbox not work.
                        //but if value is true/false this work perfectly
                        $scope.item.is_agent =  ($scope.item.is_agent=='1');
                        $scope.item.is_co_agent = ($scope.item.is_co_agent=='1');
                        $scope.item.active = ($scope.item.active=='1');
                    }
                    else{
                        alert("Server Error:\n"+response.message);
                        $modalInstance.dismiss('cancel');
                    }
                }
            );
        }//End read function

        $scope.submit = function()
        {
            //Prevent duplicate submit.
            if($scope.dataSubmitting == true){
                return;
            }
            $scope.dataSubmitting = true;

            $scope.item.username = $scope.item.email;

            if($scope.item.birthday) {
                $scope.item.birthday = longDateStringFormat($scope.item.birthday);
            }

            var request;
            var action = 'create';
            if($scope.item.id < 1){
                request = UsersService.create($scope.item)
            }
            else{
                action = 'update';
                request = UsersService.update($scope.item)
            }
            request.then(function(response)
            {
                if(response.success){
                    if(isFileSelected('photo')){
                        //upload photo
                        UsersService.upload($scope.photo, response.data.id).then(function(response){
                            console.log(response);
                        });
                    }

                    //close dialog and refresh listing.
                    //case: insert, then append object to listing as lasted.
                    //case: update, update row information.
                    response.data['action'] = action;
                    $modalInstance.close(response.data);
                }
                else{
                    alert("Server Error:\n"+response.message);
                }
                $scope.dataSubmitting = false;
            });
        }//End function

        $scope.cancel = function()
        {
            $scope.pageLoading = false;
            $modalInstance.dismiss('cancel');
        }//End function

    }
);

