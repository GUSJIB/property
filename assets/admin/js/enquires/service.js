
app.service(
    "EnquiresService",
    function( $http, $q, BaseService ) {

        // Return public API.
        return({
            listing: function (searchModel) {
                return BaseService.post('/admin/enquires/listing', JSON.stringify(searchModel));
            },
            read: function (id) {
                return BaseService.post('/admin/enquires/read/' + id);
            },
            create: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/enquires/create', JSON.stringify(model));

            },
            update: function(model) {
                model = angular.copy(model);
                return BaseService.post('/admin/enquires/update', JSON.stringify(model));
            },
            remove: function(id) {
                return BaseService.post('/admin/enquires/remove/' + id);
            },
            status: function() {
                return BaseService.post('/admin/enquires/status');
            }
        });

    }
);