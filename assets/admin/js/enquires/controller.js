
app.controller('EnquiresController', function ($scope, $modal, EnquiresService)
{
    loadStatus();
    $scope.searchModel = newSearch();

    $scope.showLoading = false;
    $scope.listing = listing;
    listing();


    function loadStatus()
    {
        EnquiresService.status().then(function(response){
            if(response.hasOwnProperty('data')){
                $scope.status = response.data;
            }
        });
    }

    function newSearch()
    {
        var searchModel = {
            property_name: '',
            name: '',
            email: '',
            phone: '',
            request_type: '',
            status:'',
            created_at_from: '',
            created_at_to: '',

            pageSize: 50,
            currentPage: 1,
            totalRows: 0,
            beginRow: 0,
            endRow: 0,
            totalPages: 0,
            rows: {}
        };

        return searchModel;
    }

    //list all record from database
    function listing()
    {
        $scope.showLoading = true;
        EnquiresService
            .listing($scope.searchModel)
            .then(function( response ) {
                $scope.showLoading = false;
                $scope.searchModel = response.data;
            }
        );
    }//End function

    $scope.goPage = function(){
        listing();
    }

    $scope.destroy = function(item)
    {
        var confirm = window.confirm("Confirm to delete record?");
        if(confirm){
            EnquiresService
                .remove(item.id)
                .then(function(response) {
                    if(response.success) {
                        $scope.showLoading = false;
                        listing();
                    }
                    else{
                        alert("Server Error:\n"+response.message);
                    }
                }
            );
        }
    };

    $scope.search = function()
    {
        $scope.searchModel.currentPage = 1;
        listing();
    }//End function

    $scope.reset = function()
    {
        $scope.searchModel = newSearch();
        listing();
    }//End function


    $scope.status_label_class = {
        1: "bg-warning",
        2: "bg-info",
        3: "bg-success",
        4: "bg-dark"
    };


    $scope.openPopup = function(item)
    {
        if(item == undefined){
            item = {
                id: 0,
                property_id: '',
                name: '',
                email: '',
                message: '',
                request_type:'',
                status:''
            };
        }

        //show popup
        $scope.pupupForm = $modal.open({
            templateUrl: 'PopupForm.html',
            controller: 'PopupFormController',
            size: 'sn',
            resolve: {
                item: function () {
                    return item;
                },
                status: function() {
                    return $scope.status;
                }
            }
        });

        $scope.pupupForm.result.then(
            function (selectedItem) {
                if(selectedItem.action === 'create') {
                    // insert data row in table at the end.
                    $scope.searchModel.rows.unshift(selectedItem);
                }
                else{
                    //update data row in table.
                    var item;
                    for (var i = 0; i < $scope.searchModel.rows.length; i++) {
                        item = $scope.searchModel.rows[i];
                        if (item.id == selectedItem.id) {
                            $scope.searchModel.rows[i] = selectedItem;
                        }
                    }
                }
                $scope.showLoading = false;
            }
        );

    }//End function

});




/********************************************************************************************************************
 * ADD / EDIT POPUP
 *******************************************************************************************************************/

app.controller( "PopupFormController", function( $scope, $modalInstance, item, EnquiresService)
    {
        $scope.pageLoading = false;
        $scope.dataSubmitting = false;
        $scope.provincesList = {};

        // set default setting object.
        $scope.item = item;
        if($scope.item.id > 0){
            $scope.pageLoading = true;
            read();
        }

        //========================================================
        // I load the preferences data from the server.
        function read()
        {
            EnquiresService
                .read($scope.item.id)
                .then(function( response ) {
                    $scope.pageLoading = false;
                    if(response.success){
                        $scope.item = response.data;
                    }
                    else{
                        alert("Server Error:\n"+response.message);
                        $modalInstance.dismiss('cancel');
                    }
                }
            );
        }//End read function

        $scope.submit = function()
        {
            //Prevent duplicate submit.
            if($scope.dataSubmitting == true){
                return;
            }
            $scope.dataSubmitting = true;

            var request;
            var action = 'create';
            if($scope.item.id < 1){
                request = EnquiresService.create($scope.item)
            }
            else{
                action = 'update';
                request = EnquiresService.update($scope.item)
            }
            request.then(function(response){
                    if(response.success){
                        //close dialog and refresh listing.
                        //case: insert, then append object to listing as lasted.
                        //case: update, update row information.
                        response.data['action'] = action;
                        $modalInstance.close(response.data);
                    }
                    else{
                        alert("Server Error:\n"+response.message);
                    }
                    $scope.dataSubmitting = false;
                }
            );
        }//End function

        $scope.cancel = function()
        {
            $scope.pageLoading = false;
            $modalInstance.dismiss('cancel');
        }//End function
    });