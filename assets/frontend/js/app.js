(function($) {
    "use strict";

    var windowHeight;
    var windowWidth;
    var contentHeight;

    // calculations for elements that changes size on window resize
    var windowResizeHandler = function() {
        windowHeight = window.innerHeight;
        windowWidth = $(window).width();
        contentHeight = windowHeight - $('#header').height();

        $('#leftSide').height(contentHeight);
        $('.closeLeftSide').height(contentHeight);
        $('#wrapper').height(contentHeight);
        $('#mapView').height(contentHeight);
        $('#content').height(contentHeight);
    }

    windowResizeHandler();

    $(window).resize(function() {
        windowResizeHandler();
    });

    setTimeout(function() {
        $('body').removeClass('notransition');
    }, 300);

    if(!(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch)) {
        $('body').addClass('no-touch');
    }


    // functionality for custom dropdown select list
    $('.dropdown-select li span').click(function() {
        if (!($(this).parent().hasClass('disabled'))) {
            $(this).prev().prop("checked", true);
            $(this).parent().siblings().removeClass('active');
            $(this).parent().addClass('active');
            $(this).parent().parent().siblings('.dropdown-toggle').children('.dropdown-label').html($(this).text());
        }
    });

    //Volume options
    $('.volume .btn-round-right').click(function() {
        var currentVal = parseInt($(this).siblings('input').val());
        if (currentVal < 10) {
            $(this).siblings('input').val(currentVal + 1);
        }
    });
    $('.volume .btn-round-left').click(function() {
        var currentVal = parseInt($(this).siblings('input').val());
        if (currentVal > 1) {
            $(this).siblings('input').val(currentVal - 1);
        }
    });

    //Enable swiping
    $(".carousel-inner").swipe( {
        swipeLeft:function(event, direction, distance, duration, fingerCount) {
            $(this).parent().carousel('next'); 
        },
        swipeRight: function() {
            $(this).parent().carousel('prev');
        }
    });

    $(".carousel-inner .card").click(function() {
        window.open($(this).attr('data-linkto'), '_self');
    });

    $('.btn').click(function() {
        if ($(this).is('[data-toggle-class]')) {
            $(this).toggleClass('active ' + $(this).attr('data-toggle-class'));
        }
    });

    $('.tabsWidget .tab-scroll').slimScroll({
        height: '235px',
        size: '5px',
        position: 'right',
        color: '#939393',
        alwaysVisible: false,
        distance: '5px',
        railVisible: false,
        railColor: '#222',
        railOpacity: 0.3,
        wheelStep: 10,
        allowPageScroll: true,
        disableFadeOut: false
    });

    $("ul.colors li a").click(function() {
        $("#app").attr("href",$(this).attr('data-style'));
        $.cookie("css",$(this).attr('data-style'), {expires: 365, path: '/'});
        location.reload();
        return false;
    });

    $('.progress-bar[data-toggle="tooltip"]').tooltip();
    $('.tooltipsContainer .btn').tooltip();

    var showSliderValue = function(e, ui) {
        var rootId = $(ui.handle).parent().attr('id');
        $('#'+rootId+"_from").html(ui.values[0]);
        $('#'+rootId+"_to").html(ui.values[1]);

        $('#'+rootId+"_val1").val(ui.values[0]);
        $('#'+rootId+"_val2").val(ui.values[1]);

        //force update angular model
        $('#'+rootId+"_val1").trigger('input');
        $('#'+rootId+"_val2").trigger('input');
    }

    var showSliderPriceValue = function(e, ui) {
        var rootId = $(ui.handle).parent().attr('id');
        $('#'+rootId+"_from").html('฿ '+ui.values[0]);
        $('#'+rootId+"_to").html('฿ '+ui.values[1]);

        $('#'+rootId+"_val1").val(ui.values[0]);
        $('#'+rootId+"_val2").val(ui.values[1]);

        //force update angular model
        $('#'+rootId+"_val1").trigger('input');
        $('#'+rootId+"_val2").trigger('input');
    }

    var bedroomSlider = $("#bedroomSlider");
    bedroomSlider.slider({
        range: true,
        min: parseInt(bedroomSlider.attr('min')),
        max: parseInt(bedroomSlider.attr('max')),
        values: [ parseInt(bedroomSlider.attr('value1')), parseInt(bedroomSlider.attr('value2')) ],
        slide: showSliderValue,
        stop: showSliderValue
    });

    var bathroomSlider = $("#bathroomSlider");
    bathroomSlider.slider({
        range: true,
        min: parseInt(bathroomSlider.attr('min')),
        max: parseInt(bathroomSlider.attr('max')),
        values: [ parseInt(bathroomSlider.attr('value1')), parseInt(bathroomSlider.attr('value2')) ],
        slide: showSliderValue,
        stop: showSliderValue
    });

    var buildYearSlider = $("#buildYearSlider");
    buildYearSlider.slider({
        range: true,
        min: parseInt(buildYearSlider.attr('min')),
        max: parseInt(buildYearSlider.attr('max')),
        values: [ parseInt(buildYearSlider.attr('value1')), parseInt(buildYearSlider.attr('value2')) ],
        slide: showSliderValue,
        stop: showSliderValue
    });

    $("#monthlyPriceSlider").slider({
        range: true,
        min: 0,
        max: 300000,
        step: 10000,
        values: [ 0, 290000 ],
        slide: showSliderPriceValue,
        stop: showSliderPriceValue
    });

    $('#autocomplete').autocomplete({
        source: ["ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl", "PHP", "Python", "Ruby", "Scala", "Scheme"],
        focus: function (event, ui) {
            var label = ui.item.label;
            var value = ui.item.value;
            var me = $(this);
            setTimeout(function() {
                me.val(value);
            }, 1);
        }
    });

    //$('#tags').tagsInput({
    //    'height': 'auto',
    //    'width': '100%',
    //    'defaultText': 'Add a tag',
    //});

    $('#datepicker').datepicker();
    //
    //// functionality for autocomplete address field
    //if ($('#address').length > 0) {
    //    var address = document.getElementById('address');
    //    var addressAuto = new google.maps.places.Autocomplete(address);
    //
    //    google.maps.event.addListener(addressAuto, 'place_changed', function() {
    //        var place = addressAuto.getPlace();
    //
    //        if (!place.geometry) {
    //            return;
    //        }
    //        if (place.geometry.viewport) {
    //            map.fitBounds(place.geometry.viewport);
    //        } else {
    //            map.setCenter(place.geometry.location);
    //        }
    //        newMarker.setPosition(place.geometry.location);
    //        newMarker.setVisible(true);
    //        $('#latitude').text(newMarker.getPosition().lat());
    //        $('#longitude').text(newMarker.getPosition().lng());
    //
    //        return false;
    //    });
    //}

    $('input, textarea').placeholder();



    $('.mapHandler').click(function() {
        if ($('#mapView').hasClass('mob-min') ||
            $('#mapView').hasClass('mob-max') ||
            $('#content').hasClass('mob-min') ||
            $('#content').hasClass('mob-max')) {
            $('#mapView').toggleClass('mob-max');
            $('#content').toggleClass('mob-min');
        } else {
            $('#mapView').toggleClass('min');
            $('#content').toggleClass('max');
        }

        setTimeout(function() {
            if (typeof(map) != 'undefined') {
                google.maps.event.trigger(map, 'resize');
            }
        }, 300);

        setTimeout(function() {
            var priceSliderRangeLeft = parseInt($('.priceSlider .ui-slider-range').css('left'));
            var priceSliderRangeWidth = $('.priceSlider .ui-slider-range').width();
            var priceSliderLeft = priceSliderRangeLeft + ( priceSliderRangeWidth / 2 ) - ( $('.priceSlider .sliderTooltip').width() / 2 );
            $('.priceSlider .sliderTooltip').css('left', priceSliderLeft);

            var areaSliderRangeLeft = parseInt($('.areaSlider .ui-slider-range').css('left'));
            var areaSliderRangeWidth = $('.areaSlider .ui-slider-range').width();
            var areaSliderLeft = areaSliderRangeLeft + ( areaSliderRangeWidth / 2 ) - ( $('.areaSlider .sliderTooltip').width() / 2 );
            $('.areaSlider .sliderTooltip').css('left', areaSliderLeft);

            var bedroomMSliderRangeLeft = parseInt($('.bedroomMSlider .ui-slider-range').css('left'));
            var bedroomMSliderRangeWidth = $('.bedroomMSlider .ui-slider-range').width();
            var bedroomMSliderLeft = bedroomMSliderRangeLeft + ( bedroomMSliderRangeWidth / 2 ) - ( $('.bedroomMSlider .sliderTooltip').width() / 2 );
            $('.bedroomMSlider .sliderTooltip').css('left', bedroomMSliderLeft);

            var bathroomMSliderRangeLeft = parseInt($('.bathroomMSlider .ui-slider-range').css('left'));
            var bathroomMSliderRangeWidth = $('.bathroomMSlider .ui-slider-range').width();
            var bathroomMSliderLeft = bathroomMSliderRangeLeft + ( bathroomMSliderRangeWidth / 2 ) - ( $('.bathroomMSlider .sliderTooltip').width() / 2 );
            $('.bathroomMSlider .sliderTooltip').css('left', bathroomMSliderLeft);

            $('.commentsFormWrapper').width($('#content').width());
        }, 300);

    });


    // Handler Auto complete search ------------------------------------------------------------------------------------
    $('.autocompleteInput').autocomplete({
        lookup: city_area_list,
        onSelect: function (suggestion) {
            $('#cityid').val(suggestion.data);
            $('.autocompleteInput').trigger('input');
            $('#cityid').trigger('input');
        }
    });

    // Handler top navigation-------------------------------------------------------------------------------------------
    $('.top_menu_links').click(function()
    {
        $(this).addClass('active');
        $('.top_menu_user').removeClass('active');
        $('.top_menu_box').removeClass('active2');
        $('.top_menu_box').toggleClass('active1');

        //$('#main_nav').toggleClass('active');
        //$('#user_nav').removeClass('active');
    });

    $('.top_menu_user').click(function()
    {
        $(this).addClass('active');
        $('.top_menu_links').removeClass('active');
        $('.top_menu_box').removeClass('active1');
        $('.top_menu_box').toggleClass('active2');
        //$('#main_nav').removeClass('active');
        //$('#user_nav').toggleClass('active');
    });

})(jQuery);








/***********************************************************************************************************************
 * Member Register
***********************************************************************************************************************/
(function($) {
    //Binding input fields -- email and password
    //reg_firstname
    //reg_lastname
    //reg_email
    //reg_password
    //reg_con_password
    //reg_submit
    $('#reg_firstname').keyup(function() {
        activeButton();
    });
    $('#reg_lastname').keyup(function() {
        activeButton();
    });
    $('#reg_email').keyup(function() {
        activeButton();
    });
    $('#reg_password').keyup(function() {
        activeButton();
    });
    $('#reg_con_password').keyup(function() {
        activeButton();
    });

    function activeButton()
    {
        var firstname = $('#reg_firstname').val();
        var lastname = $('#reg_lastname').val();
        var email = $('#reg_email').val();
        var password = $('#reg_password').val();
        var con_password = $('#reg_con_password').val();
        var validForm = true;

        if (!((firstname.search(/(a-z)+/)) && (firstname.search(/(0-9)+/))) || firstname.length < 1)
        {
            validForm = false;
        }
        if (!((lastname.search(/(a-z)+/)) && (lastname.search(/(0-9)+/))) || lastname.length < 1)
        {
            validForm = false;
        }


        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            validForm = false;
        }

        var illegalChars = /[\W_]/; // allow only letters and numbers
        if ((password.length < 6) || (password.length > 20))
        {
            validForm = false;
        }
        else if (illegalChars.test(password))
        {
            validForm = false;
        }
        else if (!((password.search(/(a-z)+/)) && (password.search(/(0-9)+/))))
        {
            validForm = false;
        }

        if (con_password !== password)
        {
            validForm = false;
        }

        if(validForm){
            $('#reg_submit').attr('disabled', false);
            $('#reg_submit').addClass('btn-blue');

        }
        else{
            $('#reg_submit').attr('disabled', true);
            $('#reg_submit').removeClass('btn-blue');
        }
    }

    //binding register submit button
    $('#reg_submit').click(function()
    {
        var firstname       = $('#reg_firstname').val();
        var lastname        = $('#reg_lastname').val();
        var company         = '';//$('#reg_company').val();
        var phone           = '';//$('#reg_phone').val();
        var email           = $('#reg_email').val();
        var password        = $('#reg_password').val();
        var con_password    = $('#reg_con_password').val();

        //disable submit button
        $('#reg_submit').attr('disabled', true);
        $('#reg_modal_message').hide();
        $('#reg_submit').removeClass('btn-blue');

        $.post(
            '/auth/register_ajax',
            {
                'first_name':       firstname,
                'last_name':        lastname,
                'company':          company,
                'phone':            phone,
                'email':            email,
                'password':         password,
                'password_confirm': con_password
            },
            reg_response
        );
    });

    function reg_response(res)
    {
        console.log(res);
        if(res.success==true){
            //show user menu
            //Show menu after user activate account
            $('#visitor_top_menu').hide();
            $('#member_top_menu').show();

            //close popup
            $('#signupPopup').modal('hide');

            $('#reg_firstname').val('');
            $('#reg_lastname').val('');
            $('#reg_company').val('');
            $('#reg_phone').val('');
            $('#reg_email').val('');
            $('#reg_password').val('');
            $('#reg_con_password').val('');

            //alert("Register complete! \nPlease check your email to activate account.");
            alert("Register complete!");
            //$('#signinPopup').modal('show');

            //if user passed login and this is not popup, then send user to homepage.
            if($('#reg_submit').attr('forward') == 'true')
            {
                window.location = '/';
            }
        }
        else{
            $('#reg_submit').attr('disabled', false);
            $('#reg_modal_message').html('Register failed due to: '+res.message);
            $('#reg_modal_message').show();
            $('#reg_submit').addClass('btn-blue');
        }
    }



})(jQuery);






/***********************************************************************************************************************
 * Member Login
 ***********************************************************************************************************************/
(function($) {
    //Binding input fields -- name, email and password
    //signin_email
    //signin_password
    //signin_remember_me
    //login_submit
    $('#signin_email').keyup(function(e) {
        var validForm = activeButton();
        if(validForm && e.which == 13) {
            login_request()
        }
    });
    $('#signin_password').keyup(function(e) {
        var validForm = activeButton();
        if(validForm && e.which == 13) {
            login_request()
        }
    });

    function activeButton(){
        var email = $('#signin_email').val();
        var password = $('#signin_password').val();
        var validForm = true;


        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            validForm = false;
        }

        var illegalChars = /[\W_]/; // allow only letters and numbers
        if ((password.length < 6) || (password.length > 20))
        {
            validForm = false;
        }
        else if (illegalChars.test(password))
        {
            validForm = false;
        }
        else if (!((password.search(/(a-z)+/)) && (password.search(/(0-9)+/))))
        {
            validForm = false;
        }

        if(validForm){
            $('#login_submit').attr('disabled', false);
            $('#login_submit').addClass('btn-blue');
        }
        else{
            $('#login_submit').attr('disabled', true);
            $('#login_submit').removeClass('btn-blue');
        }

        return validForm;
    }

    //binding login button
    $('#login_submit').click(login_request);

    function login_request()
    {
        var email = $('#signin_email').val();
        var password = $('#signin_password').val();

        //disable submit button
        $('#login_submit').attr('disabled', true);
        $('#login_modal_message').hide();
        $('#login_submit').removeClass('btn-blue');

        $.post(
            '/auth/login_ajax',
            {'identity':email,'password':password},
            login_response
        );
    }

    function login_response(res)
    {
        if(res.success==true)
        {
            //show user menu
            $('#visitor_top_menu').hide();
            $('#member_top_menu').show();

            //close popup
            $('#signinPopup').modal('hide');

            $('#signin_email').val('');
            $('#signin_password').val('');

            //if user passed login and this is not popup, then send user to homepage.
            if($('#login_submit').attr('forward') == 'true')
            {
                window.location = '/';
            }
        }
        else
        {
            $('#login_submit').attr('disabled', false);
            $('#login_modal_message').show();
            $('#login_submit').addClass('btn-blue');
        }
    }

})(jQuery);


/***********************************************************************************************************************
 * Member Forgot Password
 ***********************************************************************************************************************/
(function($) {
    $('#forgot_email').keyup(function(e) {
        var validForm = activeButton();
        if(validForm && e.which == 13) {
            forgot_password_request()
        }
    });

    function activeButton(){
        var email = $('#forgot_email').val();
        var validForm = true;


        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            validForm = false;
        }

        if(validForm){
            $('#forgot_submit').attr('disabled', false);
            $('#forgot_submit').addClass('btn-blue');
        }
        else{
            $('#forgot_submit').attr('disabled', true);
            $('#forgot_submit').removeClass('btn-blue');
        }

        return validForm;
    }

    //binding login button
    $('#forgot_submit').click(forgot_password_request);

    function forgot_password_request()
    {
        var email = $('#forgot_email').val();

        //disable submit button
        $('#forgot_submit').attr('disabled', true);
        $('#forgot_modal_message').hide();
        $('#forgot_submit').removeClass('btn-blue');

        $.post(
            '/auth/forgot_password_ajax',
            {'email':email},
            forgot_password_response
        );
    }

    function forgot_password_response(res)
    {
        if(res.success==true)
        {
            alert('Request reset password send to your email.\nPlease check link in your email for next step.');
            $('#forgot_email').val('');

            //send to home page
            window.location = '/auth/login';
        }
        else
        {
            $('#forgot_submit').attr('disabled', false);
            $('#forgot_modal_message').show();
            $('#forgot_submit').addClass('btn-blue');
        }
    }

})(jQuery);


/***********************************************************************************************************************
 * Member Reset Password
 ***********************************************************************************************************************/
(function($) {
    $('#new_password').keyup(function(e) {
        var validForm = activeButton();
        if(validForm && e.which == 13) {
            reset_password_request()
        }
    });
    $('#confirm_new_password').keyup(function(e) {
        var validForm = activeButton();
        if(validForm && e.which == 13) {
            reset_password_request()
        }
    });

    function activeButton(){
        var password1 = $('#new_password').val();
        var password2 = $('#confirm_new_password').val();
        var validForm = true;

        var illegalChars = /[\W_]/; // allow only letters and numbers
        if ((password1.length < 6) || (password1.length > 20))
        {
            validForm = false;
        }
        else if (illegalChars.test(password1))
        {
            validForm = false;
        }
        else if (!((password1.search(/(a-z)+/)) && (password1.search(/(0-9)+/))))
        {
            validForm = false;
        }

        if (password1 !== password2)
        {
            validForm = false;
        }

        if(validForm){
            $('#reset_submit').attr('disabled', false);
            $('#reset_submit').addClass('btn-blue');
        }
        else{
            $('#reset_submit').attr('disabled', true);
            $('#reset_submit').removeClass('btn-blue');
        }

        return validForm;
    }

    //binding login button
    $('#reset_submit').click(reset_password_request);

    function reset_password_request()
    {
        var reset_password_code = $('#reset_password_code').val();
        var password1 = $('#new_password').val();
        var password2 = $('#confirm_new_password').val();
        var csrfkey = $('#csrfkey').val();
        var csrfval = $('#csrfval').val();

        //disable submit button
        $('#reset_submit').attr('disabled', true);
        $('#reset_modal_message').hide();
        $('#reset_submit').removeClass('btn-blue');

        var url = window.location.href;
        url = url.replace('reset_password','reset_password_ajax');

        var data = {'new':password1, 'new_confirm':password2};
        data[csrfkey] = csrfval;

        $.post(
            url,
            data,
            reset_password_response
        );
    }

    function reset_password_response(res)
    {
        $('#new_password').val('');
        $('#confirm_new_password').val('');
        if(res.success==true)
        {
            alert('Change password successful!!.\nSystem will be redirect to login page.');

            //send to home page
            window.location = '/auth/login';
        }
        else
        {
            $('#reset_modal_message').show();
        }
    }

})(jQuery);


/***********************************************************************************************************************
 * Member Change Password
 ***********************************************************************************************************************/
(function($) {
    $('#old_password').keyup(function(e) {
        var validForm = activeButton();
        if(validForm && e.which == 13) {
            change_password_request()
        }
    });
    $('#new_password').keyup(function(e) {
        var validForm = activeButton();
        if(validForm && e.which == 13) {
            change_password_request()
        }
    });
    $('#confirm_new_password').keyup(function(e) {
        var validForm = activeButton();
        if(validForm && e.which == 13) {
            change_password_request()
        }
    });

    function activeButton()
    {
        var password0 = $('#old_password').val();
        var password1 = $('#new_password').val();
        var password2 = $('#confirm_new_password').val();
        var validForm = true;

        var illegalChars = /[\W_]/; // allow only letters and numbers
        if ((password0.length < 6) || (password0.length > 20))
        {
            validForm = false;
        }
        else if (illegalChars.test(password0))
        {
            validForm = false;
        }
        else if (!((password0.search(/(a-z)+/)) && (password0.search(/(0-9)+/))))
        {
            validForm = false;
        }

        var illegalChars = /[\W_]/; // allow only letters and numbers
        if ((password1.length < 6) || (password1.length > 20))
        {
            validForm = false;
        }
        else if (illegalChars.test(password1))
        {
            validForm = false;
        }
        else if (!((password1.search(/(a-z)+/)) && (password1.search(/(0-9)+/))))
        {
            validForm = false;
        }

        if (password1 !== password2)
        {
            validForm = false;
        }

        if(validForm){
            $('#change_password_submit').attr('disabled', false);
            $('#change_password_submit').addClass('btn-blue');
        }
        else{
            $('#change_password_submit').attr('disabled', true);
            $('#change_password_submit').removeClass('btn-blue');
        }

        return validForm;
    }

    //binding login button
    $('#change_password_submit').click(change_password_request);

    function change_password_request()
    {
        var password0 = $('#old_password').val();
        var password1 = $('#new_password').val();
        var password2 = $('#confirm_new_password').val();

        //disable submit button
        $('#change_password_submit').attr('disabled', true);
        $('#change_password_message').hide();
        $('#change_password_submit').removeClass('btn-blue');

        $.post(
            '/auth/change_password_ajax',
            {'old':password0, 'new':password1, 'new_confirm':password2},
            change_password_response
        );
    }

    function change_password_response(res)
    {
        $('#old_password').val('');
        $('#new_password').val('');
        $('#confirm_new_password').val('');
        if(res.success==true)
        {
            alert('Change password successful!!.\nSystem will be redirect to login page.');

            //send to home page
            window.location = '/auth/login';
        }
        else
        {
            $('#change_password_message').show();
        }
    }

})(jQuery);




function mortgargeCal(){
    var msg = '';
    if(isNaN($('#mort_totalPrice').val())){
        msg += "property price will be only number\n";
    }
    if(isNaN($('#mort_downPayment').val())){
        msg += "Down payment will be only number\n";
    }
    if(isNaN($('#mort_noOfYears').val())){
        msg += "Term in years will be only number\n";
    }
    if(isNaN($('#mort_interestPerYear').val())){
        msg += "Annual interest  will be only number\n";
    }
    if(isNaN($('#mort_installmentPerYear').val())){
        msg += "Installments per year will be only number\n";
    }

    if(msg==''){
        var price,down_payment,years,rate,peryear;

        price= $('#mort_totalPrice').val();
        down_payment = $('#mort_downPayment').val();
        years = $('#mort_noOfYears').val();
        rate = $('#mort_interestPerYear').val();
        peryear = $('#mort_installmentPerYear').val();


        if(price!='' && down_payment!='' && years!='' && rate!='' && peryear!='')
        {
            var monthly_calculation = calculation(price,down_payment,years,rate,peryear);
            if(monthly_calculation !=''){
                $('#mort_result').val(monthly_calculation);
            }
        }
        else{
            alert('please fill all fields.')
        }
    }
    else
    {
        alert(msg);
    }
}
function calculation(price, down, term, rate, peryear){
    if(rate<=0)
    {
        loan = price - down;
        //rate = (rate/100) / 12;
        month = term * peryear;
        payment = ((loan / month));
        payment = payment.toFixed(2);
    }
    else
    {
        loan = price - down;
        rate = (rate/100) / 12;
        month = term * peryear;
        payment = (((loan * rate / (1 - Math.pow(1+rate,(-1*month)))) * 100) / 100);
        payment = payment.toFixed(2);
    }
    return payment;
}

function mortgargeReset(){
    $('#mort_totalPrice').val('');
    $('#mort_downPayment').val('');
    $('#mort_noOfYears').val('');
    $('#mort_interestPerYear').val();
    $('#mort_installmentPerYear').val('');
}



/***********************************************************************************************************************
 * Enquire Modal
 ***********************************************************************************************************************/
//purpose -- 'rent', 'buy', 'rent_buy', false value
function showEnquireModal(propertyId, purpose)
{
    $('#enq_property_id').val('');
    if(typeof(propertyId) != 'undefined' && propertyId > 0){
        $('#enq_property_id').val(propertyId);
    }
    $('#enq_purpose').val('');
    if(typeof(purpose) != 'undefined' && purpose != false && purpose.length > 1){
        $('#enq_purpose').val(purpose);
    }

    $('#enquireModal').modal('show');
}

(function($) {
    //enq_property_id
    //enq_name
    //enq_email
    //enq_purpose
    //enq_message
    //
    //enq_submit
    //enq_modal_message

    $('#enq_name').keyup(function(e) {
        activeEnquireButton();
    });
    $('#enq_email').keyup(function(e) {
        activeEnquireButton();
    });
    $('#enq_phone').keyup(function(e) {
        activeEnquireButton();
    });
    $('#enq_name').keyup(function(e) {
        activeEnquireButton();
    });
    $('#enq_purpose').change(function(e) {
        activeEnquireButton();
    });
    $('#enq_message').keyup(function(e) {
        activeEnquireButton();
    });

    function activeEnquireButton(){
        var name = $('#enq_name').val();
        var email = $('#enq_email').val();
        var purpose = $('#enq_purpose').val();
        var message = $('#enq_message').val();
        var validForm = true;

        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            validForm = false;
        }

        if (name.length < 1)
        {
            validForm = false;
        }
        if (purpose.length < 1)
        {
            validForm = false;
        }
        if (message.length < 1)
        {
            validForm = false;
        }

        if(validForm){
            $('#enq_submit').attr('disabled', false);
            $('#enq_submit').addClass('btn-blue');
        }
        else{
            $('#enq_submit').attr('disabled', true);
            $('#enq_submit').removeClass('btn-blue');
        }

        return validForm;
    }

    //binding login button
    $('#enq_submit').click(enquire_request);

    function enquire_request()
    {
        var enq_property_id = $('#enq_property_id').val();
        var enq_name = $('#enq_name').val();
        var enq_email = $('#enq_email').val();
        var enq_phone = $('#enq_phone').val();
        var enq_purpose = $('#enq_purpose').val();
        var enq_message = $('#enq_message').val();

        //disable submit button
        $('#enq_submit').attr('disabled', true);
        $('#enq_modal_message').hide();
        $('#enq_submit').removeClass('btn-blue');

        $.post(
            '/enquire/create',
            JSON.stringify({
                'property_id': enq_property_id,
                'name': enq_name,
                'email': enq_email,
                'phone': enq_phone,
                'request_type': enq_purpose,
                'message': enq_message
            }),
            enquire_response,
            'json'
        );
    }

    function enquire_response(res)
    {
        if(res.success==true)
        {
            //close popup
            $('#enquireModal').modal('hide');

            $('#enq_property_id').val('');
            $('#enq_name').val('');
            $('#enq_email').val('');
            $('#enq_phone').val('');
            $('#enq_purpose').val('');
            $('#enq_message').val('');

            //if user passed login and this is not popup, then send user to homepage.
            if($('#enq_submit').attr('forward') == 'true')
            {
                alert('Submit enquire is successful!');
            }
        }
        else
        {
            $('#enq_submit').attr('disabled', false);
            $('#enq_modal_message').show();
            $('#enq_submit').addClass('btn-blue');
        }
    }

})(jQuery);

