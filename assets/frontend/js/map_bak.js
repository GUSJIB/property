(function($) {
    "use strict";

var map;
    var windowHeight;
    var windowWidth;
    var contentHeight;
    var contentWidth;

    // calculations for elements that changes size on window resize
    var windowResizeHandler = function() {
        windowHeight = window.innerHeight;
        windowWidth = $(window).width();
        contentHeight = windowHeight - $('#header').height();
        contentWidth = $('#content').width();

        $('#wrapper').height(contentHeight);
        $('#mapView').height(contentHeight);
        $('#content').height(contentHeight);
        setTimeout(function() {
            $('.commentsFormWrapper').width(contentWidth);
        }, 300);

        if (map) {
            google.maps.event.trigger(map, 'resize');
        }


        setTimeout(function() {
        // reposition of prices, area, bedroom, and bathroom reange sliders tooltip
        var priceSliderRangeLeft = parseInt($('.priceSlider .ui-slider-range').css('left'));
        var priceSliderRangeWidth = $('.priceSlider .ui-slider-range').width();
        var priceSliderLeft = priceSliderRangeLeft + ( priceSliderRangeWidth / 2 ) - ( $('.priceSlider .sliderTooltip').width() / 2 );
        $('.priceSlider .sliderTooltip').css('left', priceSliderLeft);

        var areaSliderRangeLeft = parseInt($('.areaSlider .ui-slider-range').css('left'));
        var areaSliderRangeWidth = $('.areaSlider .ui-slider-range').width();
        var areaSliderLeft = areaSliderRangeLeft + ( areaSliderRangeWidth / 2 ) - ( $('.areaSlider .sliderTooltip').width() / 2 );
        $('.areaSlider .sliderTooltip').css('left', areaSliderLeft);


        var bedroomMSliderRangeLeft = parseInt($('.bedroomMSlider .ui-slider-range').css('left'));
        var bedroomMSliderRangeWidth = $('.bedroomMSlider .ui-slider-range').width();
        var bedroomMSliderLeft = bedroomMSliderRangeLeft + ( bedroomMSliderRangeWidth / 2 ) - ( $('.bedroomMSlider .sliderTooltip').width() / 2 );
        $('.bedroomMSlider .sliderTooltip').css('left', bedroomMSliderLeft);


        var bathroomMSliderRangeLeft = parseInt($('.bathroomMSlider .ui-slider-range').css('left'));
        var bathroomMSliderRangeWidth = $('.bathroomMSlider .ui-slider-range').width();
        var bathroomMSliderLeft = bathroomMSliderRangeLeft + ( bathroomMSliderRangeWidth / 2 ) - ( $('.bathroomMSlider .sliderTooltip').width() / 2 );
        $('.bathroomMSlider .sliderTooltip').css('left', bathroomMSliderLeft);

        }, 300);
    }

    windowResizeHandler();

    if($.cookie("css")) {
        $("#app").attr("href",$.cookie("css"));
    }
    var themeColorPath = $("#app").attr("href");
    var themeColorFile = themeColorPath.replace("css/app-", "");
    var themeColor = themeColorFile.replace(".css", "");
    var markerImg = "marker-blue.png";

    switch(themeColor) {
        case "red":
            markerImg = "marker-red.png";
            break;
        case "blue":
            markerImg = "marker-blue.png";
            break;
        case "magenta":
            markerImg = "marker-magenta.png";
            break;
        case "yellow":
            markerImg = "marker-yellow.png";
            break;
    }

    // Custom options for map
    var options = {
            zoom : 14,
            mapTypeId : 'Styled',
            disableDefaultUI: true,
            mapTypeControlOptions : {
                mapTypeIds : [ 'Styled' ]
            }
        };
    var styles = [{
        stylers : [ {
            hue : "#cccccc"
        }, {
            saturation : -100
        }]
    }, {
        featureType : "road",
        elementType : "geometry",
        stylers : [ {
            lightness : 100
        }, {
            visibility : "simplified"
        }]
    }, {
        featureType : "road",
        elementType : "labels",
        stylers : [ {
            visibility : "on"
        }]
    }, {
        featureType: "poi",
        stylers: [ {
            visibility: "off"
        }]
    }];

    var newMarker = null;
    var markers = [];

    // json for properties markers on map
    var props = [
        {
        title : 'Modern Residence in New York',
        image : '1-1-thmb.png',
        type : 'For Sale',
        price : '$1,550,000',
        address : '39 Remsen St, Brooklyn, NY 11201, USA',
        bedrooms : '3',
        bathrooms : '2',
        area : '3430 Sq Ft',
        position : {
            lat : 40.696047,
            lng : -73.997159
        },
        markerIcon : markerImg
    }, {
        title : 'Hauntingly Beautiful Estate',
        image : '2-1-thmb.png',
        type : 'For Rent',
        price : '$1,750,000',
        address : '169 Warren St, Brooklyn, NY 11201, USA',
        bedrooms : '2',
        bathrooms : '2',
        area : '4430 Sq Ft',
        position : {
            lat : 40.688042,
            lng : -73.996472
        },
        markerIcon : markerImg
    }, {
        title : 'Sophisticated Residence',
        image : '3-1-thmb.png',
        type : 'For Sale',
        price : '$1,340,000',
        address : '38-62 Water St, Brooklyn, NY 11201, USA',
        bedrooms : '2',
        bathrooms : '3',
        area : '2640 Sq Ft',
        position : {
            lat : 40.702620,
            lng : -73.989682
        },
        markerIcon : markerImg
    }, {
        title : 'House With a Lovely Glass-Roofed Pergola',
        image : '4-1-thmb.png',
        type : 'For Sale',
        price : '$1,930,000',
        address : 'Wunsch Bldg, Brooklyn, NY 11201, USA',
        bedrooms : '3',
        bathrooms : '2',
        area : '2800 Sq Ft',
        position : {
            lat : 40.694355,
            lng : -73.985229
        },
        markerIcon : markerImg
    }, {
        title : 'Luxury Mansion',
        image : '5-1-thmb.png',
        type : 'For Rent',
        price : '$2,350,000',
        address : '95 Butler St, Brooklyn, NY 11231, USA',
        bedrooms : '2',
        bathrooms : '2',
        area : '2750 Sq Ft',
        position : {
            lat : 40.686838,
            lng : -73.990078
        },
        markerIcon : markerImg
    }, {
        title : 'Modern Residence in New York',
        image : '1-1-thmb.png',
        type : 'For Sale',
        price : '$1,550,000',
        address : '39 Remsen St, Brooklyn, NY 11201, USA',
        bedrooms : '3',
        bathrooms : '2',
        area : '3430 Sq Ft',
        position : {
            lat : 40.703686,
            lng : -73.982910
        },
        markerIcon : markerImg
    }, {
        title : 'Hauntingly Beautiful Estate',
        image : '2-1-thmb.png',
        type : 'For Rent',
        price : '$1,750,000',
        address : '169 Warren St, Brooklyn, NY 11201, USA',
        bedrooms : '2',
        bathrooms : '2',
        area : '4430 Sq Ft',
        position : {
            lat : 40.702189,
            lng : -73.995098
        },
        markerIcon : markerImg
    }, {
        title : 'Sophisticated Residence',
        image : '3-1-thmb.png',
        type : 'For Sale',
        price : '$1,340,000',
        address : '38-62 Water St, Brooklyn, NY 11201, USA',
        bedrooms : '2',
        bathrooms : '3',
        area : '2640 Sq Ft',
        position : {
            lat : 40.687417,
            lng : -73.982653
        },
        markerIcon : markerImg
    }, {
        title : 'House With a Lovely Glass-Roofed Pergola',
        image : '4-1-thmb.png',
        type : 'For Sale',
        price : '$1,930,000',
        address : 'Wunsch Bldg, Brooklyn, NY 11201, USA',
        bedrooms : '3',
        bathrooms : '2',
        area : '2800 Sq Ft',
        position : {
            lat : 40.694120,
            lng : -73.974413
        },
        markerIcon : markerImg
    }, {
        title : 'Luxury Mansion',
        image : '5-1-thmb.png',
        type : 'For Rent',
        price : '$2,350,000',
        address : '95 Butler St, Brooklyn, NY 11231, USA',
        bedrooms : '2',
        bathrooms : '2',
        area : '2750 Sq Ft',
        position : {
            lat : 40.682665,
            lng : -74.000934
        },
        markerIcon : markerImg
    }];

    // custom infowindow object
    var infobox = new InfoBox({
        disableAutoPan: false,
        maxWidth: 202,
        pixelOffset: new google.maps.Size(-101, -285),
        zIndex: null,
        boxStyle: {
            background: "url('assets/images/infobox-bg.png') no-repeat",
            opacity: 1,
            width: "202px",
            height: "245px"
        },
        closeBoxMargin: "28px 26px 0px 0px",
        closeBoxURL: "",
        infoBoxClearance: new google.maps.Size(1, 1),
        pane: "floatPane",
        enableEventPropagation: false
    });

    // function that adds the markers on map
    var addMarkers = function(props, map) {
        $.each(props, function(i,prop) {
            var latlng = new google.maps.LatLng(prop.position.lat,prop.position.lng);
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: new google.maps.MarkerImage( 
                    'assets/images/' + prop.markerIcon,
                    null,
                    null,
                    null,
                    new google.maps.Size(36, 36)
                ),
                draggable: false,
                animation: google.maps.Animation.DROP
            });
            var infoboxContent = '<div class="infoW">' +
                                    '<div class="propImg">' +
                                        '<img src="assets/images/prop/' + prop.image + '">' +
                                        '<div class="propBg">' +
                                            '<div class="propPrice">' + prop.price + '</div>' +
                                            '<div class="propType">' + prop.type + '</div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="paWrapper">' +
                                        '<div class="propTitle">' + prop.title + '</div>' +
                                        '<div class="propAddress">' + prop.address + '</div>' +
                                    '</div>' +
                                    '<ul class="propFeat">' +
                                        '<li><span class="fa fa-moon-o"></span> ' + prop.bedrooms + '</li>' +
                                        '<li><span class="icon-drop"></span> ' + prop.bathrooms + '</li>' +
                                        '<li><span class="icon-frame"></span> ' + prop.area + '</li>' +
                                    '</ul>' +
                                    '<div class="clearfix"></div>' +
                                    '<div class="infoButtons">' +
                                        '<a class="btn btn-sm btn-round btn-gray btn-o closeInfo">Close</a>' +
                                        '<a href="single.html" class="btn btn-sm btn-round btn-blue viewInfo btn-' + themeColor + '">View</a>' +
                                    '</div>' +
                                 '</div>';

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infobox.setContent(infoboxContent);
                    infobox.open(map, marker);
                }
            })(marker, i));

            $(document).on('click', '.closeInfo', function() {
                infobox.open(null,null);
            });

            markers.push(marker);
        });
    }


    
    $(window).resize(function() {
        windowResizeHandler();
    });

    setTimeout(function() {
        $('body').removeClass('notransition');

        map = new google.maps.Map(document.getElementById('mapView'), options);
        var styledMapType = new google.maps.StyledMapType(styles, {
            name : 'Styled'
        });

        map.mapTypes.set('Styled', styledMapType);
        map.setCenter(new google.maps.LatLng(40.6984237,-73.9890044));
        map.setZoom(14);

        if ($('#address').length > 0) {
            newMarker = new google.maps.Marker({
                position: new google.maps.LatLng(40.6984237,-73.9890044),
                map: map,
                icon: new google.maps.MarkerImage( 
                    'assets/images/marker-new.png',
                    null,
                    null,
                    // new google.maps.Point(0,0),
                    null,
                    new google.maps.Size(36, 36)
                ),
                draggable: true,
                animation: google.maps.Animation.DROP,
            });

            google.maps.event.addListener(newMarker, "mouseup", function(event) {
                var latitude = this.position.lat();
                var longitude = this.position.lng();
                $('#latitude').text(this.position.lat());
                $('#longitude').text(this.position.lng());
            });
        }

        addMarkers(props, map);
    }, 300);













    // functionality for custom dropdown select list
    $('.dropdown-select li a').click(function() {
        if (!($(this).parent().hasClass('disabled'))) {
            $(this).prev().prop("checked", true);
            $(this).parent().siblings().removeClass('active');
            $(this).parent().addClass('active');
            $(this).parent().parent().siblings('.dropdown-toggle').children('.dropdown-label').html($(this).text());
        }
    });

    //Price slider control
    $('.priceSlider').slider({
        range: true,
        min: 0,
        max: 2000000,
        values: [500000, 1500000],
        step: 10000,
        slide: function(event, ui) {
            $('.priceSlider .sliderTooltip .stLabel').html(
                '$' + ui.values[0].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + 
                ' <span class="fa fa-arrows-h"></span> ' +
                '$' + ui.values[1].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
            );
            var priceSliderRangeLeft = parseInt($('.priceSlider .ui-slider-range').css('left'));
            var priceSliderRangeWidth = $('.priceSlider .ui-slider-range').width();
            var priceSliderLeft = priceSliderRangeLeft + ( priceSliderRangeWidth / 2 ) - ( $('.priceSlider .sliderTooltip').width() / 2 );
            $('.priceSlider .sliderTooltip').css('left', priceSliderLeft);
        }
    });
    $('.priceSlider .sliderTooltip .stLabel').html(
        '$' + $('.priceSlider').slider('values', 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + 
        ' <span class="fa fa-arrows-h"></span> ' +
        '$' + $('.priceSlider').slider('values', 1).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
    );
    var priceSliderRangeLeft = parseInt($('.priceSlider .ui-slider-range').css('left'));
    var priceSliderRangeWidth = $('.priceSlider .ui-slider-range').width();
    var priceSliderLeft = priceSliderRangeLeft + ( priceSliderRangeWidth / 2 ) - ( $('.priceSlider .sliderTooltip').width() / 2 );
    $('.priceSlider .sliderTooltip').css('left', priceSliderLeft);


    //Area slider control
    $('.areaSlider').slider({
        range: true,
        min: 0,
        max: 20000,
        values: [5000, 10000],
        step: 10,
        slide: function(event, ui) {
            $('.areaSlider .sliderTooltip .stLabel').html(
                ui.values[0].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' Sq M' +
                ' <span class="fa fa-arrows-h"></span> ' +
                ui.values[1].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' Sq M'
            );
            var areaSliderRangeLeft = parseInt($('.areaSlider .ui-slider-range').css('left'));
            var areaSliderRangeWidth = $('.areaSlider .ui-slider-range').width();
            var areaSliderLeft = areaSliderRangeLeft + ( areaSliderRangeWidth / 2 ) - ( $('.areaSlider .sliderTooltip').width() / 2 );
            $('.areaSlider .sliderTooltip').css('left', areaSliderLeft);
        }
    });
    $('.areaSlider .sliderTooltip .stLabel').html(
        $('.areaSlider').slider('values', 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' Sq M' +
        ' <span class="fa fa-arrows-h"></span> ' +
        $('.areaSlider').slider('values', 1).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' Sq M'
    );
    var areaSliderRangeLeft = parseInt($('.areaSlider .ui-slider-range').css('left'));
    var areaSliderRangeWidth = $('.areaSlider .ui-slider-range').width();
    var areaSliderLeft = areaSliderRangeLeft + ( areaSliderRangeWidth / 2 ) - ( $('.areaSlider .sliderTooltip').width() / 2 );
    $('.areaSlider .sliderTooltip').css('left', areaSliderLeft);


    //Bedroom slider control
    $('.bedroomMSlider').slider({
        range: true,
        min: 0,
        max: 10,
        values: [0, 9],
        slide: function(event, ui) {
            $('.bedroomMSlider .sliderTooltip .stLabel').html(
                ui.values[0].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ' +
                ' <span class="fa fa-arrows-h"></span> ' +
                ui.values[1].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' rooms'
            );
            var bedroomMSliderRangeLeft = parseInt($('.bedroomMSlider .ui-slider-range').css('left'));
            var bedroomMSliderRangeWidth = $('.bedroomMSlider .ui-slider-range').width();
            var bedroomMSliderLeft = bedroomMSliderRangeLeft + ( bedroomMSliderRangeWidth / 2 ) - ( $('.bedroomMSlider .sliderTooltip').width() / 2 );
            $('.bedroomMSlider .sliderTooltip').css('left', bedroomMSliderLeft);
        }
    });
    $('.bedroomMSlider .sliderTooltip .stLabel').html(
        $('.bedroomMSlider').slider('values', 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ' +
        ' <span class="fa fa-arrows-h"></span> ' +
        $('.bedroomMSlider').slider('values', 1).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' rooms'
    );
    var bedroomMSliderRangeLeft = parseInt($('.bedroomMSlider .ui-slider-range').css('left'));
    var bedroomMSliderRangeWidth = $('.bedroomMSlider .ui-slider-range').width();
    var bedroomMSliderLeft = bedroomMSliderRangeLeft + ( bedroomMSliderRangeWidth / 2 ) - ( $('.bedroomMSlider .sliderTooltip').width() / 2 );
    $('.bedroomMSlider .sliderTooltip').css('left', bedroomMSliderLeft);

    //Bathroom slider control
    $('.bathroomMSlider').slider({
        range: true,
        min: 0,
        max: 10,
        values: [0, 9],
        slide: function(event, ui) {
            $('.bathroomMSlider .sliderTooltip .stLabel').html(
                ui.values[0].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ' +
                ' <span class="fa fa-arrows-h"></span> ' +
                ui.values[1].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' rooms'
            );
            var bathroomMSliderRangeLeft = parseInt($('.bathroomMSlider .ui-slider-range').css('left'));
            var bathroomMSliderRangeWidth = $('.bathroomMSlider .ui-slider-range').width();
            var bathroomMSliderLeft = bathroomMSliderRangeLeft + ( bathroomMSliderRangeWidth / 2 ) - ( $('.bathroomMSlider .sliderTooltip').width() / 2 );
            $('.bathroomMSlider .sliderTooltip').css('left', bathroomMSliderLeft);
        }
    });
    $('.bathroomMSlider .sliderTooltip .stLabel').html(
        $('.bathroomMSlider').slider('values', 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' ' +
        ' <span class="fa fa-arrows-h"></span> ' +
        $('.bathroomMSlider').slider('values', 1).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' rooms'
    );
    var bathroomMSliderRangeLeft = parseInt($('.bathroomMSlider .ui-slider-range').css('left'));
    var bathroomMSliderRangeWidth = $('.bathroomMSlider .ui-slider-range').width();
    var bathroomMSliderLeft = bathroomMSliderRangeLeft + ( bathroomMSliderRangeWidth / 2 ) - ( $('.bathroomMSlider .sliderTooltip').width() / 2 );
    $('.bathroomMSlider .sliderTooltip').css('left', bathroomMSliderLeft);


    //SHow / Hide filter box
    $('.handleFilter').click(function() {
        $('.filterForm').slideToggle(200);
    });



})(jQuery);