

/***********************************************************************************************************************
 * Member Register
***********************************************************************************************************************/
(function($)
{
    $('#acc_firstname').keyup(function() {
        activeButton();
    });
    $('#acc_lastname').keyup(function() {
        activeButton();
    });
    $('#acc_company').keyup(function() {
        activeButton();
    });
    $('#acc_phone').keyup(function() {
        activeButton();
    });
    $('#acc_email').keyup(function() {
        activeButton();
    });
    $('#acc_password').keyup(function() {
        activeButton();
    });
    $('#acc_con_password').keyup(function() {
        activeButton();
    });

    function activeButton()
    {
        var firstname = $('#acc_firstname').val();
        var lastname = $('#acc_lastname').val();
        var email = $('#acc_email').val();
        var password = $('#acc_password').val();
        var con_password = $('#acc_con_password').val();
        var validForm = true;

        if (!((firstname.search(/(a-z)+/)) && (firstname.search(/(0-9)+/))) || firstname.length < 1)
        {
            validForm = false;
        }
        if (!((lastname.search(/(a-z)+/)) && (lastname.search(/(0-9)+/))) || lastname.length < 1)
        {
            validForm = false;
        }


        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            validForm = false;
        }

        var illegalChars = /[\W_]/; // allow only letters and numbers
        if (password.length >0 && ((password.length < 6) || (password.length > 20)))
        {
            validForm = false;
        }
        else if (password.length >0 &&illegalChars.test(password))
        {
            validForm = false;
        }
        else if (password.length >0 && !(password.search(/(a-z)+/) && password.search(/(0-9)+/)))
        {
            validForm = false;
        }

        if (password.length >0 && con_password !== password)
        {
            validForm = false;
        }

        if(validForm){
            $('#acc_submit').attr('disabled', false);
            $('#acc_submit').addClass('btn-blue');

        }
        else{
            $('#acc_submit').attr('disabled', true);
            $('#acc_submit').removeClass('btn-blue');
        }
    }

    //binding register submit button
    $('#acc_submit').click(function()
    {
        var id              = $('#id').val();
        var firstname       = $('#acc_firstname').val();
        var lastname        = $('#acc_lastname').val();
        var company         = $('#acc_company').val();
        var phone           = $('#acc_phone').val();
        var email           = $('#acc_email').val();
        var password        = $('#acc_password').val();
        var con_password    = $('#acc_con_password').val();
        var csrfkey         = $('#csrfkey').val();
        var csrfval         = $('#csrfval').val();

        //disable submit button
        $('#acc_submit').attr('disabled', true);
        $('#acc_modal_message').hide();
        $('#acc_submit').removeClass('btn-blue');

        var data =
        {
            'id':               id,
            'first_name':       firstname,
            'last_name':        lastname,
            'company':          company,
            'phone':            phone,
            'email':            email,
            'password':         password,
            'password_confirm': con_password
        };
        data[csrfkey] = csrfval;

        $.post(
            '/auth/account_ajax',
            data,
            acc_response
        );
    });

    function acc_response(res)
    {
        if(res.success==true)
        {
            //$('#acc_firstname').val('');
            //$('#acc_lastname').val('');
            //$('#acc_company').val('');
            //$('#acc_phone').val('');
            //$('#acc_email').val('');
            $('#acc_password').val('');
            $('#acc_con_password').val('');

            alert("Update account successful!");
        }
        else{
            $('#acc_submit').attr('disabled', false);
            $('#acc_modal_message').html('Register failed due to: '+res.message);
            $('#acc_modal_message').show();
            $('#acc_submit').addClass('btn-blue');
        }
    }



})(jQuery);



