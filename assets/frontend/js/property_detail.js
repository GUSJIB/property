window.renderGoogleMap = function() {
    "use strict";

    var locations = ['SIA Property', $('#property-map').attr('lat'), $('#property-map').attr('lon'), 4];

    var map = new google.maps.Map(document.getElementById('property-map'), {
        zoom: 14,
        scrollwheel: false,
        center: new google.maps.LatLng($('#property-map').attr('lat'), $('#property-map').attr('lon')),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[1], locations[2]),
        map: map
    });

    google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
            infowindow.setContent(locations[0]);
            infowindow.open(map, marker);
        }
    })(marker));

};