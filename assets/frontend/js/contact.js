window.renderGoogleMap = function() {
    "use strict";

    var locations = ['SIA Property', $('#company_location').attr('lat'), $('#company_location').attr('lon'), 4];

    var map = new google.maps.Map(document.getElementById('company_location'), {
        zoom: 14,
        scrollwheel: false,
        center: new google.maps.LatLng($('#company_location').attr('lat'), $('#company_location').attr('lon')),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[1], locations[2]),
        map: map
    });

    google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
            infowindow.setContent(locations[0]);
            infowindow.open(map, marker);
        }
    })(marker));

};



/***********************************************************************************************************************
 * Contact us Form
 ***********************************************************************************************************************/
(function($)
{
    $('#con_fullname').keyup(function() {
        activeButton();
    });
    $('#con_email').keyup(function() {
        activeButton();
    });
    $('#con_subject').keyup(function() {
        activeButton();
    });
    $('#con_message').keyup(function() {
        activeButton();
    });

    function activeButton()
    {
        var fullname = $('#con_fullname').val();
        var email = $('#con_email').val();
        var subject = $('#con_subject').val();
        var message = $('#con_message').val();
        var validForm = true;

        $('#con_alert_message').hide();

        if (fullname.length < 2)
        {
            validForm = false;
            $('#con_fullname').removeClass('input-green');
        }
        else{
            $('#con_fullname').addClass('input-green');
        }
        if (subject.length < 2)
        {
            validForm = false;
            $('#con_subject').removeClass('input-green');
        }
        else{
            $('#con_subject').addClass('input-green');
        }
        if (message.length < 2)
        {
            validForm = false;
            $('#con_message').removeClass('input-green');
        }
        else{
            $('#con_message').addClass('input-green');
        }

        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            validForm = false;
            $('#con_email').removeClass('input-green');
        }
        else{
            $('#con_email').addClass('input-green');
        }

        if(validForm){
            $('#con_submit').attr('disabled', false);
            $('#con_submit').addClass('btn-blue');

        }
        else{
            $('#con_submit').attr('disabled', true);
            $('#con_submit').removeClass('btn-blue');
        }
    }

    //binding register submit button
    $('#con_submit').click(function()
    {
        var name        = $('#con_fullname').val();
        var email       = $('#con_email').val();
        var subject     = $('#con_subject').val();
        var message     = $('#con_message').val();
        var con_agent_id= $('#con_agent_id').val();

        //disable submit button
        $('#con_submit').attr('disabled', true);
        $('#con_alert_message').hide();
        $('#con_submit').removeClass('btn-blue');

        var data = {
            'name':        name,
            'email':       email,
            'subject':     subject,
            'detail':     message
        }
        $.post(
            '/contact/create',
            JSON.stringify(data),
            con_response
        );
    });

    function con_response(res)
    {
        if(res.success==true)
        {
            $('#con_fullname').val('');
            $('#con_email').val('');
            $('#con_subject').val('');
            $('#con_message').val('');
            $('#con_alert_message_in').html('Submit message successful.');
        }
        else{
            $('#con_submit').attr('disabled', false);
            $('#con_alert_message_in').html('Submit message failed due to: '+res.message);
            $('#con_submit').addClass('btn-blue');
        }
        $('#con_alert_message').removeClass('hide');
        $('#con_alert_message').show();
    }



})(jQuery);



