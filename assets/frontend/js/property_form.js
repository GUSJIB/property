'use strict';
var app =  angular.module('app', []);

var PropertyGoogleMap = false;
app.controller('PropertyController', function ($scope, GoogleMapsInitializer, PropertyService)
{

    $scope.item = {
        bedroom_qty: 0,
        bathroom_qty: 0,
        monthly_min_month: 0,

    };
    $scope.dataSubmitting = false;


    // on change country code
    $scope.$watch('item.country_code', function(newVal) {
        if(newVal != undefined && newVal != ""){
            PropertyService.provinces(newVal).then(function(response){
                $scope.provinces = response.data;
                if(response.data.length > 0){
                    PropertyService.amphurs(response.data[0].id).then(function(response){
                        $scope.amphurs = response.data;
                    });
                }
            });
        }
    });

    // on change province
    $scope.$watch('item.province_id', function(newVal) {
        if(newVal != undefined && newVal != ""){
            PropertyService.amphurs(newVal).then(function(response){
                $scope.amphurs = response.data;
            });
        }
    });

    $scope.submit = function()
    {
        //Prevent duplicate submit.
        if($scope.dataSubmitting == true){
            return;
        }
        $scope.dataSubmitting = true;
        PropertyService.create($scope.item)
            .then(function(response){
                if(response.success){
                    alert('Your property submitted! \nPlease wait for operator approving and call back.');
                    window.location = '/';
                }
                else{
                    alert("Submitting failed:\n"+response.message);
                }
                $scope.dataSubmitting = false;
            }
        );
    }//End function


    function loadGoogleMap()
    {
        if(typeof(google) === 'undefined' ) {
            GoogleMapsInitializer.mapsInitialized.then(function () {
                initialize();
            });
        }
        else{
            initialize();
        }
    }

    function initialize() {
        var mapOptions = {
            scrollwheel: false,
            zoom: 7,
            center: new google.maps.LatLng(13.76479215908216, 100.53863525390625)
        };

        var map = new google.maps.Map(document.getElementById('mapView'), mapOptions);
        PropertyGoogleMap = true;
        $scope.googleMap = map;

        google.maps.event.addListener(map, 'click', function(e) {
            placeMarker(e.latLng, map);
        });
    }

    function placeMarker(position, map) {
        if ($scope.marker) {
            $scope.marker.setPosition(position);
            $scope.marker.setAnimation(google.maps.Animation.DROP);
        }
        else {
            $scope.marker = new google.maps.Marker({
                position: position,
                map: map,
                title: 'Property position',
                animation: google.maps.Animation.DROP
            });
        }
        //map.panTo(position);

        //set selected position to input box
        $scope.item.lat = position.lat();
        $scope.item.lon = position.lng();
        $scope.$apply();
    }




    if(!PropertyGoogleMap) {
        loadGoogleMap();
    }
    else{
        setTimeout(initialize, 700);
    }

});



app.service(
    "PropertyService",
    function( $http, $q, BaseService ) {

        // Return public API.
        return({
            create: function(model) {
                model = angular.copy(model);
                return BaseService.post('/propertyForm/create', JSON.stringify(model));
            },
            provinces: function(country) {
                return BaseService.post('/propertyForm/provinces/' + country);
            },
            amphurs: function(province) {
                return BaseService.post('/propertyForm/amphurs/' + province);
            }
        });

    }
);


//== BASE FACTORY FUNCTIONS ==================================================================================================
app.factory('BaseService', function($http)
{
    var factory = {};

    factory.post = function(url, data, cache)
    {
        cache = typeof cache !== 'undefined' ? cache : false;
        var request = $http({
            method: "post",
            url: url,
            data: data,
            cache: cache
        });
        return ( request.then(handleSuccess, handleError) );
    }


    // I transform the successful response, unwrapping the application data from the API response payload.
    function handleSuccess (response) {
        return ( response.data );
    }

    // I transform the error response, unwrapping the application dta from the API response payload.
    function handleError(response) {
        // The API response from the server should be returned in a
        // nomralized format. However, if the request was not handled by the
        // server (or what not handles properly - ex. server error), then we
        // may have to normalize it on our end, as best we can.
        if (!angular.isObject(response.data) || !response.data.message) {
            return {success: false, message: 'An unknown error occurred.'};
        }
        // Otherwise, use expected error message.
        else {
            return {success: false, message: response.data.message};
        }
    }

    return factory;
});

app.factory('GoogleMapsInitializer', function($window, $q)
{
    //Google's url for async maps initialization accepting callback function
    var asyncUrl = 'https://maps.googleapis.com/maps/api/js?callback=',
        mapsDefer = $q.defer();

    //Callback function - resolving promise after maps successfully loaded
    $window.googleMapsInitialized = mapsDefer.resolve; // removed ()

    //Async loader
    var asyncLoad = function(asyncUrl, callbackName) {
        var script = document.createElement('script');
        //script.type = 'text/javascript';
        script.src = asyncUrl + callbackName;
        document.body.appendChild(script);
    };
    //Start loading google maps
    asyncLoad(asyncUrl, 'googleMapsInitialized');

    //Usage: Initializer.mapsInitialized.then(callback)
    return {
        mapsInitialized : mapsDefer.promise
    };
});

