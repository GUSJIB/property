'use strict';
var app =  angular.module('app', ['ui.bootstrap']);

var PropertyGoogleMap = false;
var PropertyWindowView = false;

app.controller('MapController', function ($scope, MapService)
{
    $scope.searchModel = newSearch();
    $scope.proopertyMarkers = [];
    $scope.dataSubmitting = false;

    function newSearch()
    {
        var searchModel = {
            cityName: '',
            cityId:'',
            property_type: '',
            purpose: '',
            price1: '',
            price2: '',
            bedroom1: '',
            bedroom2: '',
            bathroom1: '',
            bathroom2: '',

            pageSize: 30,
            currentPage: 1,
            totalRows: 0,
            beginRow: 0,
            endRow: 0,
            totalPages: 0,
            rows: {}
        };

        return searchModel;
    }


    $scope.submit = function()
    {
        //Prevent duplicate submit.
        if($scope.dataSubmitting == true){
            return;
        }
        $scope.dataSubmitting = true;
        MapService.search($scope.searchModel)
            .then(function(response){
                if(response.success){
                    $scope.dataSubmitting = false;
                    $scope.searchModel = response.data;
                    showPropertyPoints($scope.searchModel.rows);
                }
                else{
                    alert("Searching failed:\n"+response.message);
                }
                $scope.dataSubmitting = false;
            }
        );
    }//End function


    $scope.goPage = function(){
        $scope.submit();
    }

    // function that adds the markers on map
    function showPropertyPoints(props)
    {
        //clear map
        if($scope.proopertyMarkers.length > 0)
        {
            for (var i = 0; i < $scope.proopertyMarkers.length; i++) {
                $scope.proopertyMarkers[i].setMap(null);
            }
        }

        $scope.proopertyMarkers = [];


        if(props.length < 1)
            return false;


        //step 1: clear all pin on map

        //step 2: render all pin on google map

        //step 3: center to first property pin

        var latlng = new google.maps.LatLng(props[0].lat, props[0].lon);
        PropertyGoogleMap.panTo(latlng);

        //step 4: binding mouse hover event

        $.each(props, function(i,prop) {
            var latlng = new google.maps.LatLng(prop.lat, prop.lon);
            var marker = new google.maps.Marker({
                position: latlng,
                map: PropertyGoogleMap,
                icon: new google.maps.MarkerImage(
                    '/assets/frontend/images/marker-blue.png',
                    null,
                    null,
                    null,
                    new google.maps.Size(36, 36)
                ),
                draggable: false,
                animation: google.maps.Animation.DROP
            });
            var infoboxContent = '<div class="infoW">' +
                '<div class="propImg">' +
                '<img src="' + prop.cover_url + '">' +
                '<div class="propBg">' +
                '<div class="propPrice">' + $scope.selectPrice(prop) + '</div>' +
                '<div class="propType">' + prop.property_type + '</div>' +
                '</div>' +
                '</div>' +
                '<div class="paWrapper">' +
                '<div class="propTitle">' + prop.title + '</div>' +
                '<div class="propAddress">' + prop.address_full + '</div>' +
                '</div>' +
                '<ul class="propFeat">' +
                '<li><span class="fa fa-moon-o"></span> ' + prop.bedroom_qty + '</li>' +
                '<li><span class="icon-drop"></span> ' + prop.bathroom_qty + '</li>' +
                '<li><span class="icon-frame"></span> ' + $scope.formatNumber(prop.area_sqm, 0,',','.','') + '</li>' +
                '</ul>' +
                '<div class="clearfix"></div>' +
                '<div class="infoButtons">' +
                '<a class="btn btn-sm btn-round btn-gray btn-o closeInfo">Close</a>' +
                '<a href="' + prop.url + '" class="btn btn-sm btn-round btn-blue viewInfo btn-blue" target="_blank">View</a>' +
                '</div>' +
                '</div>';

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                var infobox = getPropertyWindowView();
                return function() {
                    infobox.setContent(infoboxContent);
                    infobox.open(PropertyGoogleMap, marker);
                }
            })(marker, i));

            $(document).on('click', '.closeInfo', function() {
                PropertyWindowView.open(null,null);
            });

            $scope.proopertyMarkers.push(marker);

        });
    }

    $scope.showMakerOnEnter = function(index)
    {
        if(PropertyGoogleMap) {
            google.maps.event.trigger($scope.proopertyMarkers[index], 'click');
        }
    }
    $scope.hideMakerOnLeave = function()
    {
        if(PropertyGoogleMap) {
            PropertyWindowView.open(null,null);
        }
    }

    function getPropertyWindowView()
    {
        if(PropertyWindowView == false)
        {
            // custom infowindow object
            PropertyWindowView = new InfoBox({
                disableAutoPan: false,
                maxWidth: 202,
                pixelOffset: new google.maps.Size(-101, -285),
                zIndex: null,
                boxStyle: {
                    background: "url('assets/frontend/images/infobox-bg.png') no-repeat",
                    opacity: 1,
                    width: "202px",
                    height: "245px"
                },
                closeBoxMargin: "28px 26px 0px 0px",
                closeBoxURL: "",
                infoBoxClearance: new google.maps.Size(1, 1),
                pane: "floatPane",
                enableEventPropagation: false
            });
        }
        return PropertyWindowView;
    }


    function initialize() {
        var mapOptions = {
            scrollwheel: false,
            zoom: 12,
            center: new google.maps.LatLng(13.76479215908216, 100.53863525390625)
        };

        var map = new google.maps.Map(document.getElementById('mapView'), mapOptions);
        PropertyGoogleMap = map;
    };




    if(!PropertyGoogleMap) {
        initialize();
    }
    $scope.submit();


    $scope.selectPrice = function(item)
    {
        if(item.for_sale == "1"){
            return $scope.formatNumber(item.sale_price,0,',','.','฿');
        }
        else if(item.for_rent == "1" && item.monthly_rent == "1"){
            return $scope.formatNumber(item.monthly_rate,0,',','.','฿') +' /month';
        }
        else if(item.for_rent == "1" && item.yearly_rent == "1"){
            return $scope.formatNumber(item.yearly_rate,0,',','.','฿') +' /year';
        }

        return 'hidden';
    };

    $scope.formatNumber = function(num, decPlaces, thouSeparator, decSeparator, currencySymbol)
    {
        // check the args and supply defaults:
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces;
        decSeparator = decSeparator == undefined ? "." : decSeparator;
        thouSeparator = thouSeparator == undefined ? "," : thouSeparator;
        currencySymbol = currencySymbol == undefined ? "$" : currencySymbol;

        var j;
        var n = num,
            sign = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;

        return sign + currencySymbol + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
    };


    $scope.selectPurpose = function(item)
    {
        if(item.for_sale == "1" && item.for_rent == "1"){
            return 'Sale/Rent';
        }
        else if(item.for_sale == "1"){
            return 'For Sale';
        }
        else if(item.for_rent == "1"){
            return 'For Rent'
        };
        return 'Request';
    };


});




app.service(
    "MapService",
    function( $http, $q, BaseService ) {

        // Return public API.
        return({
            search: function(searchModel) {
                return BaseService.post('/map/search', JSON.stringify(searchModel));
            }
        });

    }
);


//== BASE FACTORY FUNCTIONS ==================================================================================================
app.factory('BaseService', function($http)
{
    var factory = {};

    factory.post = function(url, data, cache)
    {
        cache = typeof cache !== 'undefined' ? cache : false;
        var request = $http({
            method: "post",
            url: url,
            data: data,
            cache: cache
        });
        return ( request.then(handleSuccess, handleError) );
    }


    // I transform the successful response, unwrapping the application data from the API response payload.
    function handleSuccess (response) {
        return ( response.data );
    }

    // I transform the error response, unwrapping the application dta from the API response payload.
    function handleError(response) {
        // The API response from the server should be returned in a
        // nomralized format. However, if the request was not handled by the
        // server (or what not handles properly - ex. server error), then we
        // may have to normalize it on our end, as best we can.
        if (!angular.isObject(response.data) || !response.data.message) {
            return {success: false, message: 'An unknown error occurred.'};
        }
        // Otherwise, use expected error message.
        else {
            return {success: false, message: response.data.message};
        }
    }

    return factory;
});


