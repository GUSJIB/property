<?php

    function line_dashed()
    {
        return '<div class="line line-dashed b-b line-lg pull-in"></div>';
    }

    function admin_text_field($field_type = 'text', $title, $ngModel, $class = '', $optional = '')
    {
        $str = '';
        $str .= '<div class="form-group">';
        $str .= '<label class="col-sm-2 control-label">'.$title.'</label>';
        $str .= '<div class="col-sm-10">';
        $str .= '<input type="'.$field_type.'" ng-model="'.$ngModel.'" class="form-control '.$class.'" '.$optional.'>';
        $str .= '</div>';
        $str .= '</div>';
        return $str;
    }

    function admin_checkbox_field($title, $ngModel, $class = '')
    {
        $str = '';
        $str .= '<div class="checkbox">';
        $str .= '<label class="i-checks">';
        $str .= '<input type="checkbox" ng-model="'.$ngModel.'" ng-checked="'.$ngModel.'==\'1\'" ng-true-value="\'1\'" ng-false-value="\'0\'">';
        $str .= '<i></i>';
        $str .= $title;
        $str .= '</label>';
        $str .= '</div>';
        return $str;
    }

    function admin_dropdownlist($title, $ngModel, $options = array(), $class = '')
    {
        $str = '';
        $str .= '<div class="form-group">';
        $str .= '<label class="col-sm-2 control-label">'.$title.'</label>';
        $str .= '<div class="col-sm-10">';
        $str .= '<select class="form-control '.$class.'" ng-model="'.$ngModel.'">';

        foreach($options as $key => $value) {
          $str .= '<option value="'.$key.'">'.$value.'</option>';
        }

        $str .= '</select>';
        $str .= '</div>';
        $str .= '</div>';
        return $str;
    }
