<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->helper('app_url_helper');
        $this->load->helper('url');
        $this->load->model('province_model');
        $this->load->model('amphur_model');
	}

	public function index()
	{
        //load useful record from content table.
        $country_list = config_item('countries');
        $country_array = array();
        $province_array = array();
        $amphur_array = array();
        foreach($country_list as $country_code=>$country_name) {
            $province_array[$country_code] = $this->province_model->findByCountry($country_code);
            foreach($province_array[$country_code] as $province) {
                $amphur_array[$province->id] = $this->amphur_model->findByProvince($province->id);
            }

            //assign country url
            $country = new stdClass();
            $country->code = $country_code;
            $country->name = $country_name;
            $country->url = $this->config->base_url().url_slug($country_name);
            $country_array[] = $country;
        }

        $data = array(
            'country_array'      => $country_array,
            'province_array'    => $province_array,
            'amphur_array'      => $amphur_array
        );
		$this->render_html($data, 'sitemap');
	}

}
