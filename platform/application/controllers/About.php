<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('content_model');
	}

	public function index()
	{
        //load useful record from content table.
        $data = $this->content_model->find('about_us');

        $data = array('content'=>$data);
		$this->render_html($data, 'aboutus');
	}

}
