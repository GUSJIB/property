<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Property extends Frontend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('property_model');
        $this->load->model('property_photo_model');
    }

	public function index($id)
	{
        $property = $this->property_model->findWithDetail($id);
        if(!empty($property))
        {
            $this->property_model->count_view($id);

            //featured property
            $featuredProperties = $this->property_model->featuredProperties(2,5);
            $excludeIds = array();
            foreach ($featuredProperties as $item)
            {
                $excludeIds[] = $item->id;
            }

            //recently property, not include featured property
            $recentlyProperties = $this->property_model->recentlyProperties($excludeIds, 2, 5);

            $photo = $this->property_photo_model->find_by_property_id($id);

            //find similarity properties
            $similarProperties = $this->property_model->find_similar_property($property->province_id, $property->property_type, 2, 6);

            //set location info
            $propCountry = new stdClass();
            $countries   = $this->config->item('countries');
            $propCountry->name =  $countries[$property->country_code];
            $propCountry->url  =  $this->config->base_url().url_slug($countries[$property->country_code]);

            $propProvince = new stdClass();
            $propProvince->name = $property->province_name;
            $propProvince->url  = $this->config->base_url().url_slug($propCountry->name).'/'.url_slug($propProvince->name);

            $propAmphur = new stdClass();
            $propAmphur->name = $property->amphur_name;
            $propAmphur->url  = $this->config->base_url().url_slug($propCountry->name).'/'.url_slug($propProvince->name).'/'.url_slug($propAmphur->name);

            $data = array(
                'property'                  => $property,
                'property_photos'           => $photo,
                'featured_property_list'    => $featuredProperties,
                'recently_property_list'    => $recentlyProperties,
                'similar_property_list'     => $similarProperties,
                'propCountry'               => $propCountry,
                'propProvince'              => $propProvince,
                'propAmphur'                => $propAmphur
            );

            $this->render_html($data, 'property');
        }
        else{
            redirect('/404');
        }
	}
    public function fake()
    {
        $data = array();
        $this->render_html($data, 'property_bak');
    }

}
