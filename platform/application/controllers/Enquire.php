<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enquire extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('enquire_model');
	}

	public function create()
	{
        try {
            $data = $this->read_json_param();

            //set default value for new property
            $data['status'] = 1;

            if((isset($data['property_id']) && $data['property_id'] < 1) || (isset($data['property_id']) && strlen($data['property_id'])) < 1) {
                unset($data['property_id']);
            }

            $id = $this->enquire_model->create($data);
            $data = $this->enquire_model->find($id);
            $this->render_json(true, $data);
        }
        catch(Exception $e) {
            $this->render_json(false, [], $e->getMessage());
        }
	}


}
