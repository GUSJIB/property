<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsefulLink extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('content_model');
	}

	public function index()
	{
        //load useful link record from content table.
        $data = $this->content_model->find('useful_link');

        $data = array('content'=>$data);
		$this->render_html($data, 'useful_link');
	}


}
