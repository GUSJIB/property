<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agents extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('user_model');
	}

	public function index()
	{
        //load agent record from users table.
        $data = $this->user_model->all_agent();

        $data = array('agents'=>$data);
		$this->render_html($data, 'agent_list');
	}

    public function detail($id)
    {
        //load agent record from users table.
        $data = $this->user_model->find($id);
        if($data->is_agent || $data->is_co_agent){
            $data = array('agent'=>$data);
            $this->render_html($data, 'agent_detail');
        }else{
            redirect('/404');
        }
    }

}
