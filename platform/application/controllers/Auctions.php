<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auctions extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('auction_model');
	}

	public function index()
	{
        $list = $this->auction_model->active_auctions();
        $data = array('auctions'=>$list);
		$this->render_html($data, 'auctions');
	}

}
