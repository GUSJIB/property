<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('content_model');
        $this->load->model('contact_model');
	}

	/**
	 * Render contact us form
	 */
	public function index()
	{
        //load useful record from content table.
        $data = $this->content_model->find('contact_us');

        $data = array('content'=>$data);
		$this->render_html($data, 'contact');
	}

	/**
	 * Contact us post back - via ajax json.
	 */
	public function create()
	{
        $data = $this->read_json_param();
        $data['post_ip'] = $this->ip();
        $data['status'] = 1; //new comming status

        $id = $this->contact_model->create($data);
        $data = $this->contact_model->find($id);
        $this->render_json(true, $data);
	}


}
