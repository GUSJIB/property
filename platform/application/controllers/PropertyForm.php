<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PropertyForm extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('property_model');
	}

	public function index()
	{
        $data = array();
		$this->render_html($data, 'property_form');
	}

    public function create()
    {
        try {
            $data = $this->read_json_param();

            //set default value for new property
            $data['featured'] = 0;
            $data['status'] = 1;

            $id = $this->property_model->create($data);
            $data = $this->property_model->find($id);
            $this->render_json(true, $data);
        }
        catch(Exception $e) {
            $this->render_json(false, [], $e->getMessage());
        }
    }

    public function provinces($country)
    {
        if(!empty($country)){
            $this->load->model('province_model');
            $data = $this->province_model->find_by_country($country);
            $this->render_json(true, $data);
        }
    }

    public function amphurs($province)
    {
        if(!empty($province)){
            $this->load->model('amphur_model');
            $data = $this->amphur_model->find_by_province($province);
            $this->render_json(true, $data);
        }
    }


}
