<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Home extends Frontend_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('property_model');
        $this->load->model('user_model');
        $this->load->model('province_model');
        $this->load->model('amphur_model');
    }

	public function index()
	{
        //agency listing
        $agentList = $this->user_model->all_agent();

        //featured property - 6
        $featuredProperties = $this->property_model->featuredProperties(2,6);
        $excludeIds = array();
        foreach ($featuredProperties as $item)
        {
            $excludeIds[] = $item->id;
        }

        //recently property, not include featured property - 6
        $recentlyProperties = $this->property_model->recentlyProperties($excludeIds, 2, 6);

        $data = array(
            'agent_list'                => $agentList,
            'featured_property_list'    => $featuredProperties,
            'recently_property_list'    => $recentlyProperties
        );

		$this->render_html($data, 'home');
	}

}
