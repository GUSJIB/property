<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('contact_model');
        $this->load->library('form_validation');
    }

	public function index()
	{
        $data = array();
		$this->render_html($data, 'contacts');
	}

    public function listing()
    {
        $searchModel =  $this->read_json_param();
        $searchModel = $this->contact_model->search($searchModel);
        $this->render_json(true, $searchModel);
    }

    public function read($id)
    {
        $data = $this->contact_model->find($id);
        $this->render_json(true, $data);
    }

    public function create()
    {
        $data = $this->read_json_param();
        $id = $this->contact_model->create($data);
        $data = $this->contact_model->find($id);
        $this->render_json(true, $data);
    }

    public function update()
    {
        $data = $this->read_json_param();
        $this->contact_model->update($data['id'], $data);
        $data = $this->contact_model->find($data['id']);
        $this->render_json(true, $data);
    }

    public function remove($id)
    {
        $this->contact_model->delete($id);
    }

    public function status()
    {
        $data = $this->config->item('contact_status');
        $this->render_json(true, $data);
    }

}
