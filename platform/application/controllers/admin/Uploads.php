<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploads extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $folder_path = $_GET['folder_path'];
        $data = array('folder_path'=>$folder_path);
        $this->render_html($data, 'upload', false);
    }
    public function listing()
    {
        $data = $this->read_json_param();
        if($data && isset($data['folder_path']) && $data['folder_path'])
        {
            $list = array();

            $path = config_item('root_upload_path').'/'.$data['folder_path'];

            $files = self::listDirectory(self::fixPath($path), 0);
            natcasesort($files);

            foreach ($files as $f){

                $fullPath = $path.'/'.$f;
                if(!is_file(self::fixPath($fullPath)) || !self::IsImage($f))
                    continue;

                $w = 0;
                $h = 0;
                if($this->IsImage($f))
                {
                    $tmp = @getimagesize(self::fixPath($fullPath));
                    if($tmp)
                    {
                        $w = $tmp[0];
                        $h = $tmp[1];
                    }
                }

                $model = new stdClass();
                $model->name = $f;
                $model->path = $fullPath;
                $model->url = config_item('root_upload_url').'/'.$data['folder_path'].'/'.$f;
                $model->width = $w;
                $model->height = $h;
                $list[] = $model;
            }
            $this->render_json(false, $list);
        }
        else
        {
            $this->render_json(false, [], 'Required upload folder.');
        }
    }

    public function upload()
    {
        if(isset($_FILES['file']) && isset($_POST['folder_path']) && $_POST['folder_path'] && $_FILES['file'])
        {
            $folder_path = $_POST['folder_path'];

            $file_element_name = 'file';
            $path = config_item('root_upload_path').'/'.$folder_path;

            $config['upload_path'] = $path;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 1024 * 8;
            $config['overwrite'] = TRUE;
            $config['file_name'] = round(microtime(true)*1000);

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($file_element_name))
            {
                $this->render_json(false, [], $this->upload->display_errors('', ''));
            }
            else
            {
                $data = $this->upload->data();
                if(file_exists($data['full_path'])){
                    $model = new stdClass();
                    $model->name = $data['file_name'];
                    $model->path = $path.'/'.$data['file_name'];
                    $model->url = config_item('root_upload_url').'/'.$_POST['folder_path'].'/'.$data['file_name'];
                    $model->width = $data['image_width'];
                    $model->height = $data['image_height'];

                    $this->render_json(true, [$model]);
                }
                else{
                    $this->render_json(false, [], 'Something went wrong when saving the file, please try again.');
                }
            }
        }
        else{
            $this->render_json(false, [], 'Required upload folder.');
        }
    }

    public function delete()
    {
        $data = $this->read_json_param();
        if($data && isset($data['file_path']) && $data['file_path'])
        {
            $path = $data['file_path'];
            if(is_file(self::fixPath($path))){
                if(unlink(self::fixPath($path))){
                    $this->render_json(true, $data);
                }
                else{
                    $this->render_json(false, [], 'Exception file try to delete file.');
                }
            }
            else{
                $this->render_json(false, $data, 'Invalid file path.');
            }
        }
        else
        {
            $this->render_json(false, [], 'Required file path.');
        }
    }


    /***HELPER METHODS*************************************************************************************************/

    static function IsImage($fileName)
    {
        $ret = false;
        $ext = strtolower(self::GetExtension($fileName));
        if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'jpe' || $ext == 'png' || $ext == 'gif' || $ext == 'ico')
            $ret = true;
        return $ret;
    }
    static function listDirectory($path){
        $ret = @scandir($path);
        if($ret === false){
            $ret = array();
            $d = opendir($path);
            if($d){
                while(($f = readdir($d)) !== false){
                    $ret[] = $f;
                }
                closedir($d);
            }
        }

        return $ret;
    }
    static public function FixPath($path){
        $path = mb_ereg_replace('[\\\/]+', '/', $path);
        return $path;
    }

    /**
     * Returns file extension without dot
     *
     * @param string $filename
     * @return string
     */
    static function GetExtension($filename) {
        $ext = '';

        if(mb_strrpos($filename, '.') !== false)
            $ext = mb_substr($filename, mb_strrpos($filename, '.') + 1);

        return strtolower($ext);
    }

}
