<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Properties extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('property_model');
        $this->load->model('property_photo_model');
        $this->load->library('form_validation');
        $this->load->helper('admin_form_helper');
    }

    public function index()
    {
        $data = array();
        $this->render_html($data, 'Properties/index');
    }

    public function form($id=0)
    {
        $data = array();
        if(isset($id) && $id > 0){
            $data['id'] = $id;
        }
        $this->render_html($data, 'Properties/form');
    }

    public function listing()
    {
        $searchModel =  $this->read_json_param();
        $searchModel = $this->property_model->search($searchModel);
        $this->render_json(true, $searchModel);
    }

    public function provinces($country)
    {
        if(!empty($country)){
            $this->load->model('province_model');
            $data = $this->province_model->find_by_country($country);
            $this->render_json(true, $data);    
        }
    }

    public function amphurs($province)
    {
        if(!empty($province)){
            $this->load->model('amphur_model');
            $data = $this->amphur_model->find_by_province($province);
            $this->render_json(true, $data);    
        }   
    }

    public function read($id)
    {
        $data = $this->property_model->find($id);
        $this->render_json(true, $data);
    }

    public function create()
    {
        try {
            $data = $this->read_json_param();
            $id = $this->property_model->create($data);
            $data = $this->property_model->find($id);
            $this->render_json(true, $data);
        }
        catch(Exception $e) {
            $this->render_json(false, [], $e->getMessage());
        }
    }

    public function update()
    {
        try {
            $data = $this->read_json_param();
            $this->property_model->update($data['id'], $data);
            $data = $this->property_model->find($data['id']);
            $this->render_json(true, $data);
        }
        catch(Exception $e) {
            $this->render_json(false, [], $e->getMessage());
        }
    }

    public function remove($id)
    {
        $this->property_model->delete($id);
        $this->render_json(true, array());
    }

    public function cover()
    {
        if(isset($_FILES['file']) && isset($_POST['property_id']) && $_POST['property_id'] && $_FILES['file'])
        {
            $file_element_name = 'file';
            $field_name = 'cover_ext';
            $property_id = $_POST['property_id'];
            $path = $this->property_model->get_uploads_path($property_id);

            $config['upload_path'] = $path;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 1024 * 8;
            // $config['encrypt_name'] = TRUE;
            $config['overwrite'] = TRUE;
            $config['file_name'] = 'cover';

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($file_element_name))
            {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            }
            else
            {
                $data = $this->upload->data();
                $this->property_model->update($property_id, array($field_name => $data['file_ext']));
                $filename = $data['file_name'];
                $image_path = $data['full_path'];
                if(file_exists($image_path)){
                    $status = "success";
                    $msg = "File successfully uploaded";
                }else{
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES[$file_element_name]);
            echo json_encode(array('status' => $status, 'msg' => $msg, 'file_path' => config_item('base_url').$path, 'file_name' => $filename));
            exit;
        }
    }

    public function photos($id)
    {
        $data = array('id' => $id);
        $this->render_html($data, 'Properties/photos');
    }

    public function gallery($id)
    {
        $galleries = $this->property_photo_model->find_by_property_id($id);
        $this->render_json(true, $galleries);
    }

    public function photo_remove($id)
    {
        $this->property_photo_model->delete($id);
        $this->render_json(true, array());
    }

    public function upload()
    {
        if(isset($_FILES['file']) && isset($_POST['property_id']) && $_POST['property_id'] && $_FILES['file'])
        {
            $property_id = $_POST['property_id'];
            
            // set property id to property photo model
            $this->property_photo_model->set_property_id($property_id);

            // create new record
            $row_id = $this->property_photo_model->create();

            $file_element_name = 'file';
            $path = $this->property_photo_model->get_gallery_path();

            $config['upload_path'] = $path;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 1024 * 8;
            // $config['encrypt_name'] = TRUE;
            $config['overwrite'] = TRUE;
            $config['file_name'] = $row_id;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload($file_element_name))
            {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            }
            else
            {
                $data = $this->upload->data();
                $this->property_photo_model->update($row_id, array('ext' => $data['file_ext']));
                $filename = $data['file_name'];
                $image_path = $data['full_path'];
                if(file_exists($image_path)){
                    $status = "success";
                    $msg = "File successfully uploaded";
                }
                else{
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES[$file_element_name]);
            echo json_encode(array('status' => $status, 'msg' => $msg, 'file_path' => config_item('base_url').$path, 'file_name' => $filename));
            exit;
        }
    }

    public function all_country()
    {
        $this->render_json(true, $this->config->item('countries'));
    }

    public function all_property_type()
    {
        $this->render_json(true, $this->config->item('property_types'));
    }

}
