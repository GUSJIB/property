<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auctions extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('auction_model');
        $this->load->library('form_validation');
    }

	public function index()
	{
        $data = array();
		$this->render_html($data, 'auctions');
	}

    public function listing()
    {
        $searchModel =  $this->read_json_param();
        $searchModel = $this->auction_model->search($searchModel);
        $this->render_json(true, $searchModel);
    }

    public function read($id)
    {
        $data = $this->auction_model->find($id);
        $this->render_json(true, $data);
    }

    public function create()
    {
        $data = $this->read_json_param();
        $id = $this->auction_model->create($data);
        $data = $this->auction_model->find($id);
        $this->render_json(true, $data);
    }

    public function update()
    {
        $data = $this->read_json_param();
        $this->auction_model->update($data['id'], $data);
        $data = $this->auction_model->find($data['id']);
        $this->render_json(true, $data);
    }

    public function remove($id)
    {
        $this->auction_model->delete($id);
    }

}
