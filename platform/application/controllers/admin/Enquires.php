<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enquires extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('enquire_model');
    }

    public function index()
    {
        $data = array();
        $this->render_html($data, 'enquires');
    }

    public function listing()
    {
        $searchModel =  $this->read_json_param();
        $searchModel = $this->enquire_model->search($searchModel);
        $this->render_json(true, $searchModel);
    }

    public function read($id)
    {
        $data = $this->enquire_model->find($id);
        $this->render_json(true, $data);
    }

    public function create()
    {
        $data = $this->read_json_param();
        $id = $this->enquire_model->create($data);
        $data = $this->enquire_model->find($id);
        $this->render_json(true, $data);
    }

    public function update()
    {
        $data = $this->read_json_param();
        $this->enquire_model->update($data['id'], $data);
        $data = $this->enquire_model->find($data['id']);
        $this->render_json(true, $data);
    }

    public function remove($id)
    {
        $this->enquire_model->delete($id);
        $this->render_json(true, array());
    }

    public function status()
    {
        $data = $this->config->item('enquire_status');
        $this->render_json(true, $data);
    }

}
