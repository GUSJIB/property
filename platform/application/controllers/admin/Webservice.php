<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webservice extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function notifications()
	{
        $this->load->model('enquire_model');
        $this->load->model('property_model');
        $this->load->model('contact_model');

        //load useful record from content table.
        $new_enquiry_count = $this->enquire_model->countByStatus(1); //1 = new enquire
        $new_property_count = $this->property_model->countByStatus(1); //1 = new property
        $new_contact_count = $this->contact_model->countByStatus(1); //1 = new contact message

        $data = array(
            'new_enquiry_count'=>$new_enquiry_count,
            'new_property_count'=>$new_property_count,
            'new_contact_count'=>$new_contact_count
        );

        $this->render_json(true, $data);
	}

}
