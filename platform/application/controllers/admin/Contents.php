<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contents extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('content_model');
        $this->load->library('form_validation');
    }

    public  function contact_us()
    {
        $data = array('page_target'=>'contact_us');
        $this->render_html($data, 'contents');
    }

    public  function about_us()
    {
        $data = array('page_target'=>'about_us');
        $this->render_html($data, 'contents');
    }

    public  function useful_link()
    {
        $data = array('page_target'=>'useful_link');
        $this->render_html($data, 'contents');
    }

    public  function read($page_target)
    {
        $data = $this->content_model->find($page_target);
        $this->render_json(true, $data);
    }

    public function update()
    {
        $data = $this->read_json_param();
        $this->content_model->update($data['id'], $data);
        $data = $this->content_model->find($data['target']);
        $this->render_json(true, $data);
    }

}
