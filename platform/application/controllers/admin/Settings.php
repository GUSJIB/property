<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('setting_model');
		$this->load->library('form_validation');

        $this->setting_id = config_item('settings_id');
	}

	public function index()
	{
		$data = array();
		$this->render_html($data, 'settings');
	}

	public function read()
	{
		$data = $this->setting_model->find($this->setting_id);
		$this->render_json(true, $data);
	}

	public function update()
	{
        $data = $this->read_json_param();

        //Cleanup data, prevent db operation error.
        if(isset($data['alert_start']) && $data['alert_start'] === ''){
            $data['alert_start'] = null;
        }
        if(isset($data['alert_end']) && $data['alert_end'] === ''){
            $data['alert_end'] = null;
        }

		$this->setting_model->update($this->setting_id, $data);

        // update into the cache
        $setting = $this->setting_model->find($this->setting_id);
        $this->cache->save('setting', $setting, 3600);

		$this->render_json(true, $setting);
	}

    public function  upload_photo()
    {
        if(isset($_FILES['file']))
        {
            $field = $_POST['field'];
            $fileName = $_POST['file_name'];

            $config['upload_path'] = config_item('settings_path');
            $config['allowed_types'] = 'gif|jpg|png|ico';
            $config['max_size'] = 1024 * 8;
            $config['overwrite'] = TRUE;
            $config['file_name'] = $fileName;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $msg = $this->upload->display_errors('', '');

                @unlink($_FILES['file']);

                $data = array();
                $this->render_json(false, $data, $msg);
            }
            else {
                $data = $this->upload->data();
                $ext = str_replace('.','',$data['file_ext']);
                $this->setting_model->update($this->setting_id, array($field => $ext));
                $image_path = $data['full_path'];
                @unlink($_FILES['file']);
                if(file_exists($image_path))
                {
                    // update into the cache
                    $setting = $this->setting_model->find($this->setting_id);
                    $this->cache->save('setting', $setting, 3600);

                    $this->read();
                }
                else
                {
                    $status = false;
                    $msg = "Something went wrong when saving the file, please try again.";
                    $data = array();
                    $this->render_json($status, $data, $msg);
                }
            }
        }
        else{
            $this->render_json(false, array(), 'No file uploaded');
        }
    }

}
