<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
        $this->load->model('user_model');
        $this->load->model('user_group_model');
		$this->load->library('form_validation');
	}

    public function admins()
    {
        $this->index('admins');
    }
    public function officers()
    {
        $this->index('officers');
    }
    public function agents()
    {
        $this->index('agents');
    }
    public function members()
    {
        $this->index('members');
    }

	private function index($group)
	{
		$data = array();
        $data['select_group'] = $group;
		$this->render_html($data, 'users');
	}

	public function listing()
	{
        $searchModel =  $this->read_json_param();
        $group = $searchModel['select_group'];
        if($group == 'admins'){
            if(!$this->ion_auth->is_admin()) {
                $this->render_json(false, [], 'No permission to view administrator accounts');
                return;
            }
            $searchModel = $this->user_model->search($searchModel);
        }
        else{
            $searchModel = $this->user_model->search($searchModel);
        }

		$this->render_json(true, $searchModel);
	}

	public function read($id)
	{
		$data = $this->user_model->find($id);
        $user_groups = $this->user_group_model->search($id);

        //assign roles flags
        if($this->ion_auth->is_admin()){
            $data->is_admin = false;
            $data->is_officer = false;
            $data->is_member = false;

            if(isset($user_groups) && count($user_groups) > 0){
                foreach ($user_groups as $group) {
                    if($group->group_id == '1'){
                        $data->is_admin = true;
                    }
                    else if($group->group_id == '2'){
                        $data->is_officer = true;
                    }
                    else if($group->group_id == '3'){
                        $data->is_member = true;
                    }
                }
            }
        }
        else{
            //Not allow officer to view/edit admin account.
            if(isset($user_groups) && count($user_groups) > 0) {
                foreach ($user_groups as $group) {
                    if ($group->group_id == '1') {
                        $this->render_json(false, [], 'No permission to view/edit administrator account');
                        return;
                    }
                }
            }
        }

		$this->render_json(true, $data);
	}

	public function create()
	{
        $data = $this->read_json_param();

        //clear user group flags from $data
        $is_admin = false;
        $is_officer = false;
        $is_member = false;
        if($this->ion_auth->is_admin()){
            //This is not db field, the remove attribute to prevent db operation error.
            if(isset($data['is_admin'])){
                $is_admin = $data['is_admin'];
                unset($data['is_admin']);
            }
            if(isset($data['is_officer'])){
                $is_officer = $data['is_officer'];
                unset($data['is_officer']);
            }
            if(isset($data['is_member'])){
                $is_member = $data['is_member'];
                unset($data['is_member']);
            }
        }
        else{
            $is_member = true;
        }

        $ip = $this->input->ip_address();
        $data['ip_address'] = $ip;

        $password = $this->generatePassword();
        $data['password'] = $password;

		$id = $this->user_model->create($data);
		$data = $this->user_model->find($id);

        $this->manage_user_groups($id, $is_admin, $is_officer, $is_member);

		$this->render_json(true, $data);
	}

	public function update()
	{
		$data = $this->read_json_param();

        //clear user group flags from $data
        $is_admin = false;
        $is_officer = false;
        $is_member = false;
        if($this->ion_auth->is_admin()){
            //This is not db field, the remove attribute to prevent db operation error.
            if(isset($data['is_admin'])){
                $is_admin = $data['is_admin'];
                unset($data['is_admin']);
            }
            if(isset($data['is_officer'])){
                $is_officer = $data['is_officer'];
                unset($data['is_officer']);
            }
            if(isset($data['is_member'])){
                $is_member = $data['is_member'];
                unset($data['is_member']);
            }
        }

		$this->user_model->update($data['id'], $data);
		$data = $this->user_model->find($data['id']);

        //Remove + insert user groups
        $this->manage_user_groups($data->id, $is_admin, $is_officer, $is_member);

        $this->render_json(true, $data);
	}

    private function manage_user_groups($user_id, $is_admin, $is_officer, $is_member)
    {
        if($this->ion_auth->is_admin()){
            //remove previous user groups if existing.
            $this->user_group_model->delete_by_user_id($user_id);

            //insert user groups
            if($is_admin) {
                $user_group = new stdClass();
                $user_group->user_id = $user_id;
                $user_group->group_id = 1;
                $this->user_group_model->create($user_group);
            }
            if($is_officer) {
                $user_group = new stdClass();
                $user_group->user_id = $user_id;
                $user_group->group_id = 2;
                $this->user_group_model->create($user_group);
            }
            if($is_member) {
                $user_group = new stdClass();
                $user_group->user_id = $user_id;
                $user_group->group_id = 3;
                $this->user_group_model->create($user_group);
            }
        }

    }

	public function remove($id)
	{
		$this->user_model->delete($id);
	}

	public function upload()
	{

		if(isset($_FILES['file']) && isset($_POST['user_id']) && $_POST['user_id'] && $_FILES['file'])
        {
			$file_element_name = 'file';

			$config['upload_path'] = config_item('user_avatar_path');
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 1024 * 8;
			// $config['encrypt_name'] = TRUE;
			$config['overwrite'] = TRUE;
			$new_filename = $_POST['user_id'];
			$config['file_name'] = $new_filename;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload($file_element_name))
            {
				$status = 'error';
				$msg = $this->upload->display_errors('', '');
			}
            else
            {
				$data = $this->upload->data();
				$this->user_model->update($new_filename, array('photo_ext' => $data['file_ext']));
				$filename = $data['file_name'];
				$image_path = $data['full_path'];
				if(file_exists($image_path)){
					$status = "success";
				  	$msg = "File successfully uploaded";
				}else{
					$status = "error";
				  	$msg = "Something went wrong when saving the file, please try again.";
				}
			}
			@unlink($_FILES[$file_element_name]);
		    echo json_encode(array('status' => $status, 'msg' => $msg, 'file_path' => config_item('base_url').config_item('user_avatar_path'), 'file_name' => $filename));
		    exit;
		}
	}


    private function generatePassword($length = 10) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#$&-_=+';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }

}
