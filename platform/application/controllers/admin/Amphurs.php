<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Amphurs extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('amphur_model');
        $this->load->library('form_validation');
    }

	public function index()
	{
        $data = array();
		$this->render_html($data, 'amphurs');
	}

    public function listing()
    {
        $searchModel =  $this->read_json_param();
        $searchModel = $this->amphur_model->search($searchModel);
        $this->render_json(true, $searchModel);
    }

    public function read($id)
    {
        $data = $this->amphur_model->find($id);
        $this->render_json(true, $data);
    }

    public function create()
    {
        $data = $this->read_json_param();
        $id = $this->amphur_model->create($data);
        $data = $this->amphur_model->find($id);
        $this->render_json(true, $data);
    }

    public function update()
    {
        $data = $this->read_json_param();
        $this->amphur_model->update($data['id'], $data);
        $data = $this->amphur_model->find($data['id']);
        $this->render_json(true, $data);
    }

    public function remove($id)
    {
        $this->amphur_model->delete($id);
        $this->render_json(true, array());
    }

}
