<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinces extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('province_model');
        $this->load->library('form_validation');
    }

	public function index()
	{
        $data = array();
		$this->render_html($data, 'provinces');
	}

    public function listing()
    {
        $searchModel =  $this->read_json_param();
        $searchModel = $this->province_model->search($searchModel);
        $this->render_json(true, $searchModel);
    }

    public function read($id)
    {
        $data = $this->province_model->find($id);
        $this->render_json(true, $data);
    }

    public function create()
    {
        $data = $this->read_json_param();
        $id = $this->province_model->create($data);
        $data = $this->province_model->find($id);
        $this->render_json(true, $data);
    }

    public function update()
    {
        $data = $this->read_json_param();
        $this->province_model->update($data['id'], $data);
        $data = $this->province_model->find($data['id']);
        $this->render_json(true, $data);
    }

    public function remove($id)
    {
        $this->province_model->delete($id);
        $this->render_json(true, array());
    }


    public  function loadProvinces()
    {
        $provincesList = $this->province_model->loadProvinces();
        $this->render_json(true, $provincesList);
    }

    public  function getLastPosition()
    {
        $getLastPosition = $this->province_model->getlastPosition();
        $this->render_json(true, $getLastPosition);
    }

}
