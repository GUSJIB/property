<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends Frontend_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('property_model');
    }

	public function index()
	{
        $data = array();

		$this->render_html($data, 'map');

	}

    public function search()
    {
        $searchModel =  $this->read_json_param();

        $searchModel['country_code'] = '';
        $searchModel['province_id'] = '';
        $searchModel['amphur_id'] = '';
        if(isset($searchModel['cityId']) && !empty($searchModel['cityId']))
        {
            $id = explode(':',$searchModel['cityId'])[1];
            if(preg_match('#^country#i', $searchModel['cityId']) === 1)
            {
                $searchModel['country_code'] = $id;
            }
            else if(preg_match('#^province#i', $searchModel['cityId']) === 1)
            {
                $searchModel['province_id'] = $id;
            }
            else if(preg_match('#^amphur#i', $searchModel['cityId']) === 1)
            {
                $searchModel['amphur_id'] = $id;
            }
        }

        $searchModel = $this->property_model->searchMap($searchModel);
        $this->render_json(true, $searchModel);

    }

}
