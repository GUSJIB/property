<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Search property page
 */
class Search extends Frontend_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('inflector');
        $this->load->model('amphur_model');
        $this->load->model('province_model');
        $this->load->library('pagination');
    }

	public function index()
	{
        $params = $this->input->get(); //get with page number

        $queryParams = $params;
        if(isset($queryParams['page'])){
            unset($queryParams['page']);
        }
        $uri = current_url().'?'.http_build_query($queryParams);

        $this->search($uri,$params);
	}

    public function searchByPurposePropertyType($purpose, $property_type=false)
    {
        $params = $this->input->get(); //get with page number

        $queryParams = $params;
        if(isset($queryParams['page'])){
            unset($queryParams['page']);
        }
        $uri = current_url().'?'.http_build_query($queryParams);

        $params['purpose'] = $purpose;
        $params['type'] = $property_type;

        $this->search($uri, $params);
    }

    public function searchByLocation($countryName, $provinceName=false, $amphurName=false)
    {
        $params = $this->input->get(); //get with page number

        $queryParams = $params;
        if(isset($queryParams['page'])){
            unset($queryParams['page']);
        }
        $uri = current_url().'?'.http_build_query($queryParams);

        $amphur_id = 0;
        if($amphurName){
            $amphurName = str_replace('_', ' ', $amphurName);
            $amphurName = str_replace('-', ' ', $amphurName);

            $amphur_id = $this->amphur_model->findIdByName($amphurName);
            if($amphur_id)
                $params['amphur_id'] = $amphur_id;
        }

        $province_id = 0;
        if($provinceName && $amphur_id == 0){
            $provinceName = str_replace('_', ' ', $provinceName);
            $provinceName = str_replace('-', ' ', $provinceName);

            $province_id = $this->province_model->findIdByName($provinceName);
            if($province_id)
            $params['province_id'] = $province_id;
        }

        if($countryName && $amphur_id == 0 && $province_id == 0){
            $countryName = str_replace('_', ' ', $countryName);
            $countryName = str_replace('-', ' ', $countryName);
            $countryName = ucfirst($countryName);

            $countries = config_item('countries');
            foreach($countries as $code=>$name){
                if($name === $countryName){
                    $params['country_code'] = $code;
                }
            }
        }


        $this->search($uri, $params);
    }

    private function search($baseUrl, $params)
    {
        //print_r($params);

        $setting = $this->getSetting();

        //Input format is 'type:id' following this
        //   >> country:country_code
        //   >> province:province_id
        //   >> amphur:amphur_id
        if(isset($params['cityid']) && !empty($params['cityid'])){
            $_city = $params['cityid'];
            $city = explode(':', $_city);
            if(count($city) > 0){
                if($city[0] ==  'country'){
                    $params['country_code'] = $city[1];
                }
                else if($city[0] == 'province'){
                    $params['province_id'] = $city[1];
                }
                else if($city[0]=='amphur'){
                    $params['amphur_id'] = $city[1];
                }
            }
        }


        $properties = $this->property_model->frontend_search_form(2, $params, $setting->search_page_size);
        //$properties = $this->property_model->frontend_search_form(2, $params, 1);

        $data = array();
        $data['current_url'] = current_url();
        $data['searchResult'] = $properties;
        $data['pagination'] = $this->init_pagination($baseUrl, $properties);

        //additional parameter
        $countries   = $this->config->item('countries');
        if(isset($params['amphur_id'])){
            $amphur = $this->amphur_model->findWithDetail($params['amphur_id']);
            $params['province_id'] = $amphur->province_id;
            $data['propAmphur'] = $amphur;

            if(!isset($params['cityid'])){
                $params['cityid'] = 'amphur:'.$params['amphur_id'];
                $params['city'] = $amphur->name.' (District), '.$amphur->province_name.', '.$countries[$amphur->country_code];
            }
        }
        if(isset($params['province_id'])){
            $province = $this->province_model->findWithDetail($params['province_id']);
            $params['country_code'] = $province->country_code;
            $data['propProvince'] = $province;

            if(!isset($params['cityid'])){
                $params['cityid'] = 'province:'.$params['province_id'];
                $params['city'] = $province->name.' (City), '.$countries[$province->country_code];
            }
        }
        if(isset($params['country_code'])){
            $propCountry = new stdClass();
            $propCountry->name =  $countries[$params['country_code']];
            $propCountry->url  =  $this->config->base_url().url_slug($countries[$params['country_code']]);
            $data['propCountry'] = $propCountry;

            if(!isset($params['cityid'])){
                $params['cityid'] = 'country:'.$params['country_code'];
                $params['city'] = $propCountry->name.' (Country)';
            }
        }

        $data['params'] = $params;

        $this->render_html($data, 'search');
    }

    private function getSetting()
    {
        if ( ! $setting = $this->cache->get('setting'))
        {
            $setting_id = config_item('settings_id');
            $this->load->model('setting_model');
            $setting = $this->setting_model->find($setting_id);

            // Save into the cache for 1 hr
            $this->cache->save('setting', $setting, 3600);
        }

        return $setting;
    }

    private function init_pagination($baseUrl, $properties)
    {
        $config = array();
        $config["base_url"] = $baseUrl;
        $config['num_links'] = 3;
        $config['query_string_segment'] = 'page';
        $config["total_rows"] = $properties->totalRows;
        $config["per_page"] = $properties->pageSize;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;

        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        $config['first_link'] = '<span class="fa fa-angle-left"></span> First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="fa fa-angle-left"></span>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '<span class="fa fa-angle-right"></span>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_link'] = 'Last <span class="fa fa-angle-right"></span>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $str_links = $this->pagination->create_links();

        return $str_links;
    }

}
