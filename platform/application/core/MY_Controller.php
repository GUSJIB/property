<?php

class MY_Controller extends CI_Controller {

	// Values and objects to be overrided or accessible from child controllers
	protected $mSite = 'frontend';
	protected $mSiteConfig = array();
	protected $mBaseUrl = '';
	protected $mBodyClass = '';
	protected $mDefaultLayout = 'empty';
	protected $mLanguage = '';
	protected $mAvailableLanguages = '';
	protected $mTitlePrefix = '';
	protected $mTitle = '';
	protected $mMetaData = array();
	protected $mMenu = array();

	// Values to be obtained automatically from router
	protected $mCtrler = 'home';		// current controller
	protected $mAction = 'index';		// controller function being called
	protected $mMethod = 'GET';			// HTTP request method

	// Scripts and stylesheets to be embedded on each page
	protected $mStylesheets = array();
	protected $mScripts = array();

	// Data to pass into views
	protected $mViewData = array();

	// Constructor
	public function __construct()
	{
		parent::__construct();
        $this->load->driver('cache',
            array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => $this->config->item('apc_cache_prefix', 'config'))
        );
        $this->load->library('ion_auth');

		$this->mCtrler = $this->router->fetch_class();
		$this->mAction = $this->router->fetch_method();
		$this->mMethod = $this->input->server('REQUEST_METHOD');
		
		$this->mScripts['head'] = array();		// for scripts that need to be loaded from the start
		$this->mScripts['foot'] = array();		// for scripts that can be loaded after page render

		// Initial setup
		$this->_setup();

		// (optional) enable profiler
		if (ENVIRONMENT=='development')
		{
			//$this->output->enable_profiler(TRUE);
		}
	}

    protected function read_json($name){
        $json = $this->input->post($name);
        $json = stripslashes($json);
        $json = json_decode($json);

        return $json;
    }

    protected function read_json_param()
    {
        return json_decode(file_get_contents("php://input"), true);
    }
	
	// Output template for Frontend Website
	protected function render_html($data, $view, $layout = '')
	{
        $this->mViewData = array_merge($this->mViewData, $data);

		// automatically generate page title
		if ( empty($this->mTitle) )
		{
			if ( $this->mAction=='index' )
				$this->mTitle = humanize($this->mCtrler);
			else
				$this->mTitle = humanize($this->mAction);
		}

        if(!isset($this->mViewData['setting']))
        {
            if ( ! $setting = $this->cache->get('setting'))
            {
                $setting_id = config_item('settings_id');
                $this->load->model('setting_model');
                $setting = $this->setting_model->find($setting_id);

                // Save into the cache for 1 hr
                $this->cache->save('setting', $setting, 3600);
            }
            $this->mViewData['setting'] = $setting;
        }

        $this->mViewData['footerScripts'] = array();

        $this->mViewData['ion_auth'] = $this->ion_auth;
		$this->mViewData['base_url'] = $this->mBaseUrl;
		#$this->mViewData['inner_view'] = $this->mSite.'/'.$view;
		$this->mViewData['body_class'] = $this->mBodyClass;

		$this->mViewData['site'] = $this->mSite;
		$this->mViewData['ctrler'] = $this->mCtrler;
		$this->mViewData['action'] = $this->mAction;

        $this->mViewData['upload_url'] = '/assets/uploads';
		$this->mViewData['current_uri'] = ($this->mSite==='frontend') ? uri_string(): str_replace($this->mSite.'/', '', uri_string());
		$this->mViewData['stylesheets'] = $this->mStylesheets;
		$this->mViewData['scripts'] = $this->mScripts;
		$this->mViewData['title'] = $this->mTitlePrefix.$this->mTitle;

		// load view files
		//$this->load->view('common/head', $this->mViewData);
        $inner_view = $this->mSite.'/'.$view;

        $main_layout = false;
        if(empty($layout) && $layout !== false){
            $main_layout = $this->mSite.'/'.$this->mDefaultLayout;
        }
        else if($layout !== false){
            $main_layout = $this->mSite.'/'.$layout;
        }

        //Render html
        if($main_layout !== false){
            $this->mViewData['inner_view'] = $this->load->view($inner_view, $this->mViewData, true);
            $this->load->view($main_layout, $this->mViewData);
            //$this->load->view('common/foot', $this->mViewData);
        }
        else{
            //render without template.
            $this->load->view($inner_view, $this->mViewData);
        }
	}
	
	// Output JSON string
	protected function render_json($isSuccess, $data, $message='')
	{
        $result = array('success'=>$isSuccess, 'data'=>$data, 'message'=>$message);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
	}

	// Setup for different sites
	private function _setup()
	{
		// called at first so config/sites.php can contains lang() function
//		$this->_setup_language();

		// get site-specific configuration from file "application/config/sites.php"
        $this->config->load('sites');
        $config = $this->config->item('sites')[$this->mSite];

		// setup autoloading (libraries, helpers, models, language files, etc.)
		if ( !empty($config['autoload']) )
			$this->_setup_autoload($config['autoload']);

		$this->mSiteConfig = $config;
	}

//	// Setup localization
//	private function _setup_language()
//	{
//		// language settings from: application/config/language.php
//		$this->config->load('language');
//		$config = $this->config->item('site_languages')[$this->mSite];
//
//		// default language from config (NOT the one from CodeIgniter: application/config/config.php)
//		$this->mLanguage = $this->session->has_userdata('language') ? $this->session->userdata('language') : $config['default'];
//
//		if ( !empty($config['enabled']) )
//		{
//			$this->mAvailableLanguages = $config['available'];
//
//			foreach ($config['autoload'] as $file)
//				$this->lang->load($file, $this->mAvailableLanguages[$this->mLanguage]['value']);
//
//			$this->mViewData['available_languages'] = $this->mAvailableLanguages;
//		}
//
//		$this->mViewData['language'] = $this->mLanguage;
//	}

	// Setup autoloading
	private function _setup_autoload($config)
	{
		foreach ($config['libraries'] as $file)
		{
			if ($file==='database')
				$this->load->database();
			else
				$this->load->library($file);
		}
		
		foreach ($config['helpers'] as $file)
			$this->load->helper($file);

		foreach ($config['models'] as $file => $alias)
			$this->load->model($file, $alias);
	}

    public function ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress ;
    }
}


/**
 * Base controller for Frontend Website
 */
class Frontend_Controller extends MY_Controller
{
	protected $mSite = 'frontend';
	protected $mDefaultLayout = '_layout';

	public function __construct()
	{
		parent::__construct();
		$this->mBaseUrl = site_url();

        $this->loadNewListedProperties();
	}

    private function loadNewListedProperties()
    {
        //property type listing.
        $propertyTypes = $this->config->item('property_types');
        $this->mViewData['property_type_list'] = $propertyTypes;

        $this->load->model('property_model');
        $searchModel = $this->property_model->getNewListedProperties(2, 3);
        $this->mViewData['footer_listed_property'] = $searchModel;

        //Load business zone listing for footer page.
        $this->load->model('amphur_model');
        $list = $this->amphur_model->getHotBusinessArea(10);
        $this->mViewData['footer_business_zone'] = $list;

        //city with area listing for auto lookup search box.
        $this->load->model('province_model');
        $this->load->model('amphur_model');
        $listCou   = $this->config->item('countries');
        $listPro = $this->province_model->getProvinceList();
        $listAmp = $this->amphur_model->getAmphurList();


        //Country   >> Thailand (Country)
        //City      >> Bangkok (City), Thailand
        //District  >> Silom (Districts), Bangkok, Thailand
        $areaList = array();//label, value
        foreach ($listCou as $key => $name) {
            $areaList[] = ['label'=> $name.' (Country)', 'value'=> 'country:'.$key];
        }
        foreach ($listPro as $province) {
            $areaList[] = ['label'=> $province->name.' (City), '.$listCou[$province->country_code], 'value'=> 'province:'.$province->id];
        }
        foreach ($listAmp as $amphur) {
            $areaList[] = ['label'=> $amphur->name.' (District), '.$amphur->province_name.', '.$listCou[$province->country_code], 'value'=> 'amphur:'.$amphur->id];
        }
        $this->mViewData['city_area_list'] = $areaList;
    }
}


/**
 * Base controller for Admin Panel
 */
class Admin_Controller extends MY_Controller {

	// override parent values
	protected $mSite = 'admin';
	protected $mDefaultLayout = '_layout';

	// values for Admin Panel only
	protected $mUser = array();

	public function __construct()
	{
		parent::__construct();
		$this->mBaseUrl = site_url($this->mSite).'/';
		if ($this->mCtrler!='login')
		{
			// Check with user login
			$this->_verify_auth();
		}
	}

	// Override parent
	protected function render_html($data, $view, $layout = '')
	{
		$this->mBodyClass = ($this->mCtrler=='login') ? 'login-page' : 'skin-purple';
		parent::render_html($data, $view, $layout);
	}

	// Verify authentication
	private function _verify_auth()
	{
        //if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
        if ($this->ion_auth->logged_in())
        {
            $this->mUser = $this->ion_auth->user()->row();
            if ($this->_verify_role(array('admin','officer')))
            {
                $this->mViewData['user'] = $this->mUser;
                return;
            }
        }

        if($this->input->is_ajax_request()){
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode( array('response_code' => 401, 'message' => 'unauthorized') ))
                ->set_status_header('401');
            exit();
        }
        else {
            //redirect to login page
            redirect('auth/login');
            exit();
        }
	}

	// Verify if the login user belongs to target role
	// $role can be string or string array
	protected function _verify_role($roles)
	{
		if ( empty($this->mUser) )
			return false;

        $this->load->model('user_group_model');

        $existingRole = $this->user_group_model->search_with_name($this->mUser->id);
        foreach($existingRole as $roleRow)
        {
            foreach($roles as $role)
            {
                if ($roleRow->name === $role) {
                    return true;
                }
            }
        }

        return false;
	}
}


/**
 * Base controller for API Site
 */
class Api_Controller extends MY_Controller {

	protected $mSite = 'api';

	public function __construct()
	{
		parent::__construct();
		$this->mBaseUrl = site_url($this->mSite).'/';
		$this->_verify_auth();
	}

	// Verify authentication
	private function _verify_auth()
	{
		// to be completed
	}
}