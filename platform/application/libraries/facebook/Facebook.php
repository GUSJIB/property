<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( session_status() == PHP_SESSION_NONE )
{
    session_start();
}

// Autoload the required files
require_once( APPPATH . 'libraries/facebook/Facebook/autoload.php' );
require_once( APPPATH . 'libraries/facebook/FacebookCommon.php' );

use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookSession;


class Facebook {
    var $ci;
    var $helper;
    var $session;

    var $app_id;
    var $app_secret;
    var $permissions;
    var $callbackUrl;

    var $facebook;

    public function __construct($callbackUrl) {
        $this->ci =& get_instance();

        // Initialize the SDK
        $this->app_id = $this->ci->config->item('app_id', 'ion_auth');
        $this->app_secret = $this->ci->config->item('app_secret', 'ion_auth');
        $this->permissions = $this->ci->config->item('scope', 'ion_auth');

        $this->facebook = new FacebookCommon($this->app_id, $this->app_secret, $this->permissions);
    }


    function loginStart($callbackUrl)
    {
        $this->callbackUrl = $callbackUrl;
        if ($this->facebook->passedAutheticate($this->callbackUrl, true)) {
            $this->loginCallback();
        }
        else {
            exit;
        }
    }

    /**
     * case new signup - create member record
     * case duplicate signup - update member record
     *
     * case If fbId is exist
     * case email is exist
     * case new register
     */
    function loginCallback()
    {
        // not allow no session.
        if (!$this->facebook->passedAutheticate($this->callbackUrl, false)) {
            throw new Exception('ผู้ใช้ไม่ผ่านการล็อกอินด้วย facebook.');
        }

        // graph api request for user data
        $data = $this->facebook->getData();

        $fbId       = $data->getProperty('id');
        $fbGender   = $data->getProperty('gender'); //male, female
        $fbFirstname= $data->getProperty('first_name');
        $fbLastname = $data->getProperty('last_name');
        $fbEmail    = $data->getProperty('email');
        $fbBirthday    = $data->getProperty('birthday');
        $fbLink     = $data->getProperty('link'); //facebook page
        $fbAvartar   = "//graph.facebook.com/$fbId/picture?type=large";

        if(empty($fbEmail)) {
            throw new Exception('ไม่สามารถอ่านอีเมล์จาก facebook ได้');
        }

        // check if this user is already registered
        $hasEmail = $this->CI->ion_auth_model->identity_check($fbEmail);
        $hasFacebookId = $this->CI->ion_auth_model->facebook_id_check($fbId);
        if(!$hasEmail && !$hasFacebookId)
        {
            $gender = 'f';
            if ($fbGender=='male')
            {
                $gender = 'm';
            }
            // convert birthdate from m/d/Y to Y-m-d date mysql
            $date = date("Y-m-d", strtotime($fbBirthday));
            $password = $this->generatePassword();
            $register = $this->CI->ion_auth->register($fbEmail, $password, $fbEmail,
                array(
                    'first_name'    => $fbFirstname,
                    'last_name'     => $fbLastname,
                    'gender'        => $gender,
                    //'bio'         => $user->bio,
                    'facebook_id'   => $fbId,
                    'facebook_url'  => $fbLink,
                    'birthday'      => $date,
                    //'location'    => $user->location->name
                )
            );
        }
        else if ($hasFacebookId)
        {
            $member = $this->CI->ion_auth_model->user_by_facebook_id($fbId);
            $login = $this->CI->ion_auth->login($member->email, $member->password, 1);
        }
        else
        {
            throw new Exception("อีเมล์ $fbEmail ถูกใช้ไปแล้ว");
        }

    }


    //==LINK EXISTING ACCOUNT TO FACEBOOK ACCOUNT ======================================================================
    function linkAccountStart($callbackUrl)
    {
        $this->callbackUrl = $callbackUrl;
        if ($this->facebook->passedAutheticate($this->callbackUrl, true)) {
            $this->linkAccountCallback();
        }
        else {
            exit;
        }
    }

    /**
     * case If fbId is exist
     * case not exist fbId
     */
    function linkAccountCallback()
    {
        // not allow no session.
        if (!$this->facebook->passedAutheticate($this->callbackUrl, false)) {
            throw new Exception('ผู้ใช้ไม่ผ่านการล็อกอินด้วย facebook.');
        }

        // graph api request for user data
        $data = $this->facebook->getData();

        $fbId       = $data->getProperty('id');
        $fbGender   = $data->getProperty('gender'); //male, female
        $fbFirstname= $data->getProperty('first_name');
        $fbLastname = $data->getProperty('last_name');
        $fbEmail    = $data->getProperty('email');
        $fbBirthday    = $data->getProperty('birthday');
        $fbLink     = $data->getProperty('link'); //facebook page
        $fbAvartar   = "//graph.facebook.com/$fbId/picture?type=large";

        if(empty($fbEmail)) {
            throw new Exception('ไม่สามารถอ่านอีเมล์จาก facebook ได้');
        }

        // check if this user is already registered
        $hasFacebookId = $this->CI->ion_auth_model->facebook_id_check($fbId);
        if(!$hasFacebookId)
        {
            $member = $this->ion_auth->user()->row();
            $gender = 'f';
            if ($fbGender=='male')
            {
                $gender = 'm';
            }
            // convert birthdate from m/d/Y to Y-m-d date mysql
            $date = date("Y-m-d", strtotime($fbBirthday));

            $update = $this->ion_auth->update($member->id,
                array(
                    'gender'        => $gender,
                    //'bio'         => $user->bio,
                    'facebook_id'   => $fbId,
                    'facebook_url'  => $fbLink,
                    'birthday'      => $date,
                    //'location'    => $user->location->name
                )
            );
        }
        else
        {
            throw new Exception("เฟสบุคนี้ถูกลิงค์กับบัญชีผู้ใช้อื่นแล้ว");
        }

    }
    //==================================================================================================================



    function generatePassword($length = 10) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#$&-_=+';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }
}