<?php
// added in v4.0.0
require_once 'Facebook/autoload.php';
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;

class FacebookCommon {

    /**
     * @var Facebook\FacebookRedirectLoginHelper
     */
    private $app_id;
    private $app_secret;
    private $scope;
    private $callbackUrl;
    private $fbHelper;
    private $session;
    final function __construct($app_id, $app_secret, $scope)
    {
        if (session_status() == PHP_SESSION_NONE) {
            @session_start();
        }
        $this->app_id = $app_id;
        $this->app_secret = $app_secret;
        $this->scope = $scope;
    }

    function passedAutheticate($cUrl, $sendToFacebookLoginForm=false)
    {
        $this->callbackUrl = $cUrl;

        $this->session = $this->FacebookSession();

        // not allow no session.
        if(!isset($this->session)) {
            //if require to send to facebook system for check login, then send user to that page.
            if($sendToFacebookLoginForm){
                $scope = array("req_perms"=>"email");
                $loginUrl = $this->fbHelper->getLoginUrl($scope);
                header("Location: ".$loginUrl);
                return;
            }
            else{
                return false;
            }
        }
        return true;
    }

    function getData()
    {
        $request = new FacebookRequest($this->session, 'GET', '/me');
        $response = $request->execute();
        $data = $response->getGraphObject();

        return $data;
    }

    function facebookSession()
    {
        //STEP 1: start php session
        if (session_status() == PHP_SESSION_NONE) {
            @session_start();
        }

        //STEP 2: init app with app id and secret
        FacebookSession::setDefaultApplication($this->app_id,$this->app_secret);

        //STEP 3: If a existing session exists
        if ( isset( $_SESSION ) && isset( $_SESSION['fb_token'] ) ) {
            // create new session from saved access_token
            $fsession = new FacebookSession( $_SESSION['fb_token'] );

            // validate the access_token to make sure it's still valid
            try {
                if ( $fsession->validate() ) {
                    return $fsession;
                }
            }
            catch ( Exception $e ) {
                // catch any exceptions
                $fsession = null;
            }
        }

        //STEP 4: If no facebook session, the get from facebook service.
        try {
            // login helper with redirect_uri
            $this->fbHelper = new FacebookRedirectLoginHelper($this->callbackUrl);
            $fsession = $this->fbHelper->getSessionFromRedirect();
            if (isset( $fsession ) && $fsession !== null) {
                // save the session's token, to prevent facebook double request token error.
                // =>> This authorization code has been used. //OAuthException","code":100
                $_SESSION['fb_token'] = $fsession->getToken();
            }
            return $fsession;
        }
        catch( FacebookRequestException $ex ) {
            // When Facebook returns an error
            //print_r($this->callbackUrl);
            //print_r($ex);
            throw new Exception('ไม่สามารถล็อกอินผ่านเฟสบุ๊คได้ case request.');
        }
        catch( Exception $ex ) {
            //print_r($ex);
            // When validation fails or other local issues
            throw new Exception('ไม่สามารถล็อกอินผ่านเฟสบุ๊คได้ case other.');
        }
    }
}