<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( session_status() == PHP_SESSION_NONE )
{
    session_start();
}

// Autoload the required files
require_once( APPPATH . 'libraries/facebook/Facebook/autoload.php' );
require_once( APPPATH . 'libraries/facebook/FacebookCommon.php' );

use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;


class Ion_auth_facebook {
    var $ion_auth;
    var $helper;
    var $session;

    var $app_id;
    var $app_secret;
    var $permissions;
    var $callbackUrl;

    var $facebook;

    public function __construct() {

        $this->config =& get_instance()->config;

        // Initialize the SDK
        $this->app_id = $this->config->item('app_id', 'ion_auth');
        $this->app_secret = $this->config->item('app_secret', 'ion_auth');
        $this->permissions = $this->config->item('scope', 'ion_auth');

        $this->facebook = new FacebookCommon($this->app_id, $this->app_secret, $this->permissions);
    }


    function loginStart($ion_auth)
    {
        $this->ion_auth = $ion_auth;
        $this->callbackUrl = $this->config->item('redirect_uri', 'ion_auth');
        if ($this->facebook->passedAutheticate($this->callbackUrl, true)) {
            return $this->loginCallback($ion_auth);
        }
        else {
            exit;
        }
    }

    /**
     * case new signup - create member record
     * case duplicate signup - update member record
     *
     * case If fbId is exist
     * case email is exist
     * case new register
     */
    function loginCallback($ion_auth)
    {
        $this->ion_auth = $ion_auth;
        // not allow no session.
        if (!$this->facebook->passedAutheticate($this->callbackUrl, false)) {
            throw new Exception('ผู้ใช้ไม่ผ่านการล็อกอินด้วย facebook.');
        }

        // graph api request for user data
        $data = $this->facebook->getData();

        $fbId       = $data->getProperty('id');
        $fbGender   = $data->getProperty('gender'); //male, female
        $fbFirstname= $data->getProperty('first_name');
        $fbLastname = $data->getProperty('last_name');
        $fbEmail    = $data->getProperty('email');
        $fbBirthday    = $data->getProperty('birthday');
        $fbLink     = $data->getProperty('link'); //facebook page
        $fbAvartar   = "//graph.facebook.com/$fbId/picture?type=large";

        if(empty($fbEmail)) {
            throw new Exception('ไม่สามารถอ่านอีเมล์จาก facebook ได้');
        }

        // check if this user is already registered
        $hasEmail = $this->ion_auth->ion_auth_model->identity_check($fbEmail);
        $hasFacebookId = $this->ion_auth->ion_auth_model->facebook_id_check($fbId);
        if(!$hasEmail && !$hasFacebookId)
        {
            $gender = 'f';
            if ($fbGender=='male')
            {
                $gender = 'm';
            }
            // convert birthdate from m/d/Y to Y-m-d date mysql
            $date = date("Y-m-d", strtotime($fbBirthday));
            $password = $this->generatePassword();
            $register = $this->ion_auth->register($fbEmail, $password, $fbEmail,
                array(
                    'first_name'    => $fbFirstname,
                    'last_name'     => $fbLastname,
                    'gender'        => $gender,
                    //'bio'         => $user->bio,
                    'facebook_id'   => $fbId,
                    'facebook_url'  => $fbLink,
                    'birthday'      => $date,
                    //'location'    => $user->location->name
                )
            );

            //auto signin

            $this->ion_auth->login($fbEmail, $password, 0);
            return true;
        }
        else if ($hasFacebookId)
        {
            $member = $this->ion_auth->ion_auth_model->user_by_facebook_id($fbId)->row();
            $this->ion_auth->loginWithHashedPassword($member->email, $member->password, 0);
            return true;
        }
        else
        {
            throw new Exception("อีเมล์ $fbEmail ถูกใช้ไปแล้ว");
        }

    }


    //==LINK EXISTING ACCOUNT TO FACEBOOK ACCOUNT ======================================================================
    function linkAccountStart($ion_auth)
    {
        $this->ion_auth = $ion_auth;
        $this->callbackUrl = $this->config->item('redirect_uri_link', 'ion_auth');
        if ($this->facebook->passedAutheticate($this->callbackUrl, true)) {
            $this->linkAccountCallback($ion_auth);
        }
        else {
            exit;
        }
    }

    /**
     * case If fbId is exist
     * case not exist fbId
     */
    function linkAccountCallback($ion_auth)
    {
        $this->ion_auth = $ion_auth;
        // not allow no session.
        if (!$this->facebook->passedAutheticate($this->callbackUrl, false)) {
            throw new Exception('ผู้ใช้ไม่ผ่านการล็อกอินด้วย facebook.');
        }

        // graph api request for user data
        $data = $this->facebook->getData();

        $fbId       = $data->getProperty('id');
        $fbGender   = $data->getProperty('gender'); //male, female
        $fbFirstname= $data->getProperty('first_name');
        $fbLastname = $data->getProperty('last_name');
        $fbEmail    = $data->getProperty('email');
        $fbBirthday    = $data->getProperty('birthday');
        $fbLink     = $data->getProperty('link'); //facebook page
        $fbAvartar   = "//graph.facebook.com/$fbId/picture?type=large";

        if(empty($fbEmail)) {
            throw new Exception('ไม่สามารถอ่านอีเมล์จาก facebook ได้');
        }

        // check if this user is already registered
        $hasFacebookId = $this->ion_auth_model->facebook_id_check($fbId);
        if(!$hasFacebookId)
        {
            $member = $this->ion_auth->user()->row();
            $gender = 'f';
            if ($fbGender=='male')
            {
                $gender = 'm';
            }
            // convert birthdate from m/d/Y to Y-m-d date mysql
            $date = date("Y-m-d", strtotime($fbBirthday));

            $update = $this->ion_auth->update($member->id,
                array(
                    'gender'        => $gender,
                    //'bio'         => $user->bio,
                    'facebook_id'   => $fbId,
                    'facebook_url'  => $fbLink,
                    'birthday'      => $date,
                    //'location'    => $user->location->name
                )
            );
        }
        else
        {
            throw new Exception("เฟสบุคนี้ถูกลิงค์กับบัญชีผู้ใช้อื่นแล้ว");
        }

    }
    //==================================================================================================================



    function generatePassword($length = 10) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#$&-_=+';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }
}