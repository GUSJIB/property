<?php
$this->view_data['showFooterBox'] = false;
$this->view_data['meta_title'] = 'Register new property';
$this->view_data['meta_keywords'] = '';
$this->view_data['meta_description'] = 'Register a new property to our listing';
$this->view_data['title'] = 'Register a new property';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array('/assets/frontend/vendors/angular/angular.min.js', '/assets/frontend/js/property_form.js');
$this->view_data['footerScripts'] = array();
$this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_small.php';

$this->load->config('config', true);
$countries = $this->config->item('countries');
$property_types = $this->config->item('property_types');
?>


<!-- Content -->
<div id="wrapper" ng-app="app" ng-controller="PropertyController">

    <div id="mapView"><div class="mapPlaceholder"><span class="fa fa-spin fa-spinner"></span> Loading map...</div></div>

    <div id="content">


        <div class="rightContainer">
            <h1>Submit Your Property</h1>
            <form name="PropertyForm" ng-submit="submit()" class="clear form-validation" novalidate>


                <div class="row">
                    <div class="col col-lg-12">
                        <div class="form-group">
                            <label>Property name <span class="text-red">*</span></label>
                            <input ng-model="item.title" type="text" class="form-control" placeholder="Enter property name" maxlength="250" required/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Description <span class="text-red">*</span></label>
                    <textarea ng-model="item.detail" rows="20" class="form-control" required></textarea>
                </div>

                <div class="row">
                    <div class="col col-sm-6">
                        <div class="form-group">
                            <label>Type <span class="text-red">*</span></label>
                            <select ng-model="item.property_type" class="form-control" required>
                                <?php
                                foreach($property_types as $key => $value) {
                                    echo '<option value="'.$key.'">'.$value.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col col-sm-6">
                        <div class="form-group">
                            <label>Address line 1</label>
                            <input ng-model="item.address1" type="text" class="form-control" maxlength="150" placeholder="Enter address line 1" />
                        </div>
                        <div class="form-group">
                            <label>Address line 2</label>
                            <input ng-model="item.address2" type="text" class="form-control" maxlength="150" placeholder="Enter address line 2" />
                        </div>
                        <div class="form-group">
                            <label>District </label>
                            <input ng-model="item.district" type="text" class="form-control" maxlength="100" placeholder="Enter district name" />
                        </div>
                        <div class="form-group">
                            <label>Postal code</label>
                            <input ng-model="item.postal_code" type="text" class="form-control" maxlength="10" placeholder="Enter postal code" />
                        </div>
                    </div>

                    <div class="col col-sm-6">
                        <div class="form-group">
                            <label>Country<span class="text-red">*</span></label>
                            <select ng-model="item.country_code" ng-change="changeCountry()" class="form-control" required>
                                <? foreach ($countries as $key => $value) { ?>
                                    <option value="<?=$key?>"><?=$value?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>City <span class="text-red">*</span></label>
                            <select ng-model="item.province_id" class="form-control" required>
                                <option
                                    ng-repeat="province in provinces"
                                    value="{{ province.id }}"
                                    ng-selected="province.id == item.province_id">
                                    {{ province.name }}
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Area <span class="text-red">*</span></label>
                            <select ng-model="item.amphur_id" class="form-control" required>
                                <option
                                    ng-repeat="amphur in amphurs"
                                    value="{{ amphur.id }}"
                                    ng-selected="amphur.id == item.amphur_id">
                                    {{ amphur.name }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col col-sm-6">
                        <div class="form-group">
                            <label>Latitude <span class="text-red">*</span></label>
                            <input ng-model="item.lat" type="text" class="form-control" placeholder="Latitude" required disabled/>
                        </div>
                    </div>
                    <div class="col col-sm-6">
                        <div class="form-group">
                            <label>Longitude <span class="text-red">*</span></label>
                            <input ng-model="item.lon" type="text" class="form-control" placeholder="Longitude" required disabled/>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col col-sm-3">
                        <div class="form-group">
                            <label>Bedrooms</label>
                            <select ng-model="item.bedroom_qty" class="form-control" required>
                                <option value="0">none</option>
                                <option value="1">1 room</option>
                                <option value="2">2 room</option>
                                <option value="3">3 room</option>
                                <option value="4">4 room</option>
                                <option value="5">5 room</option>
                                <option value="6">6 room</option>
                                <option value="7">7 room</option>
                                <option value="8">8 room</option>
                                <option value="9">9 room</option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-sm-3">
                        <div class="form-group">
                            <label>Bathrooms</label>
                            <select ng-model="item.bathroom_qty" class="form-control" required>
                                <option value="0">none</option>
                                <option value="1">1 room</option>
                                <option value="2">2 room</option>
                                <option value="3">3 room</option>
                                <option value="4">4 room</option>
                                <option value="5">5 room</option>
                                <option value="6">6 room</option>
                                <option value="7">7 room</option>
                                <option value="8">8 room</option>
                                <option value="9">9 room</option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-sm-6">
                        <div class="form-group">
                            <label>Property area<span class="text-red">*</span></label>
                            <div class="input-group">
                                <input ng-model="item.area_sqm" type="number" class="form-control" maxlength="10" required placeholder="Property area" />
                                <div class="input-group-addon" id="addressPinBtn">sq.m</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-sm-3">
                        <div class="form-group">
                            <label>Property floor</label>
                            <input ng-model="item.building_floor_qty" type="number" class="form-control" maxlength="10" placeholder="Number of floor"/>
                        </div>
                    </div>
                    <div class="col col-sm-3">
                        <div class="form-group">
                            <label>Floor in room</label>
                            <input ng-model="item.sub_floor_qty_in_room" type="number" class="form-control" maxlength="10" placeholder="Number of floor in room"/>
                        </div>
                    </div>
                    <div class="col col-sm-6">
                        <div class="form-group">
                            <label>Built year</label>
                            <input ng-model="item.built_year" type="number" class="form-control" maxlength="4" placeholder="Built year"/>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col col-sm-3">
                        <div class="form-group">
                            <label>Active sale target</label>
                            <div class="checkbox custom-checkbox">
                                <label><input ng-model="item.for_sale" type="checkbox" value="1" /><span class="fa fa-check"></span> For sale</label>
                            </div>
                        </div>
                    </div>
                    <div class="col col-sm-6">
                        <div class="form-group">
                            <label>Sale price</label>
                            <div class="input-group">
                                <input ng-model="item.sale_price" type="number" class="form-control" maxlength="10" placeholder="Sale price">
                                <div class="input-group-addon" id="addressPinBtn" title="price">THB</div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col col-sm-12">
                        <div class="form-group">
                            <label>Active rental target</label>
                            <div class="checkbox custom-checkbox">
                                <label><input ng-model="item.for_rent" type="checkbox" value="1" /><span class="fa fa-check"></span> For rent</label>
                            </div>
                        </div>
                    </div>
                    <div class="col col-sm-12">
                        <div class="row">
                            <div class="col col-sm-4">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.monthly_rent" type="checkbox" value="1"/> <span class="fa fa-check"></span> Monthly rental</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-sm-4">
                                <div class="form-group">
                                    <label>Monthly rate</label>
                                    <div class="input-group">
                                        <input ng-model="item.monthly_rate" type="number" maxlength="10" class="form-control" placeholder="Monthly rate" />
                                        <div class="input-group-addon" title="Price">THB</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-sm-4">
                                <div class="form-group">
                                    <label>Min months</label>
                                    <select ng-model="item.monthly_min_month" class="form-control">
                                        <option value="0">not specified</option>
                                        <option value="1">1 month</option>
                                        <option value="2">2 months</option>
                                        <option value="3">3 months</option>
                                        <option value="4">4 months</option>
                                        <option value="5">5 months</option>
                                        <option value="6">6 months</option>
                                        <option value="7">7 months</option>
                                        <option value="8">8 months</option>
                                        <option value="9">9 months</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-sm-12">
                        <div class="row">
                            <div class="col col-sm-4">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.yearly_rent" type="checkbox" value="1" /><span class="fa fa-check"></span> Yearly rental</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-sm-4">
                                <div class="form-group">
                                    <label>Yearly rate</label>
                                    <div class="input-group">
                                        <input ng-model="item.yearly_rate" type="number" maxlength="10" class="form-control" placeholder="Yearly rate" />
                                        <div class="input-group-addon" id="addressPinBtn" title="Price">THB</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col col-sm-12">
                        <div class="form-group">
                            <label>Views</label>
                            <div class="row" id="new_amenities">
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.view_city" type="checkbox" value="1" /><span class="fa fa-check"></span> City</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.view_sea" type="checkbox" value="1" /><span class="fa fa-check"></span> Sea</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.view_river" type="checkbox" value="1" /><span class="fa fa-check"></span> River</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.view_skyline" type="checkbox" value="1" /><span class="fa fa-check"></span> Skyline</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col col-sm-12">
                        <div class="form-group">
                            <label>Amenities</label>
                            <div class="row" id="new_amenities">
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_air_condition" type="checkbox" value="1" /><span class="fa fa-check"></span> Air condition</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_balcony" type="checkbox" value="1" /><span class="fa fa-check"></span> Balcony</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_outdoor_pool" type="checkbox" value="1" /><span class="fa fa-check"></span> Outdoor pool</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_terrace" type="checkbox" value="1" /><span class="fa fa-check"></span> Terrace</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_furniture" type="checkbox" value="1" /><span class="fa fa-check"></span> Furniture</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_kitken" type="checkbox" value="1" /><span class="fa fa-check"></span> Kitken</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_lift" type="checkbox" value="1" /><span class="fa fa-check"></span> Lift</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_garden" type="checkbox" value="1" /><span class="fa fa-check"></span> Garden</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_internet_lan" type="checkbox" value="1" /><span class="fa fa-check"></span> Internet Lan</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_parking" type="checkbox" value="1" /><span class="fa fa-check"></span> Parking</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_garage" type="checkbox" value="1" /><span class="fa fa-check"></span> Garage</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_fitness" type="checkbox" value="1" /><span class="fa fa-check"></span> Fitness</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_spa" type="checkbox" value="1" /><span class="fa fa-check"></span> Spa</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_sauna_room" type="checkbox" value="1" /><span class="fa fa-check"></span> Sauna room</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_dress_room" type="checkbox" value="1" /><span class="fa fa-check"></span> Dress room</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_pet_friendly" type="checkbox" value="1" /><span class="fa fa-check"></span> Pet friendly</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_doorman" type="checkbox" value="1" /><span class="fa fa-check"></span> Doorman</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_internet_wifi" type="checkbox" value="1" /><span class="fa fa-check"></span> Internet WIFI</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_cable_tv" type="checkbox" value="1" /><span class="fa fa-check"></span> Cable TV</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_computer" type="checkbox" value="1" /><span class="fa fa-check"></span> Computer</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_grill" type="checkbox" value="1" /><span class="fa fa-check"></span> Grill</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_diskwasher" type="checkbox" value="1" /><span class="fa fa-check"></span> Diskwasher</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_washing_machine" type="checkbox" value="1" /><span class="fa fa-check"></span> Washing machine</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_refrigerator" type="checkbox" value="1" /><span class="fa fa-check"></span> Refrigerator</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_smoking" type="checkbox" value="1" /><span class="fa fa-check"></span> Smoking</label>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="checkbox custom-checkbox">
                                        <label><input ng-model="item.am_telephone" type="checkbox" value="1" /><span class="fa fa-check"></span> Telephone</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col col-sm-6">
                        <div class="form-group">
                            <label>Contact name <span class="text-red">*</span></label>
                            <input ng-model="item.contact_name" type="text" class="form-control" maxlength="100" required placeholder="Enter name"/>
                        </div>
                    </div>
                    <div class="col col-sm-6">
                        <div class="form-group">
                            <label>Contact position <span class="text-red">*</span></label>
                            <input ng-model="item.contact_position" type="text" class="form-control" maxlength="50" required placeholder="Enter position"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-sm-6">
                        <div class="form-group">
                            <label>Contact email <span class="text-red">*</span></label>
                            <input ng-model="item.contact_email" type="text" class="form-control" maxlength="100" required placeholder="Enter email"/>
                        </div>
                    </div>
                    <div class="col col-sm-6">
                        <div class="form-group">
                            <label>Contact phone <span class="text-red">*</span></label>
                            <input ng-model="item.contact_phone" type="text" class="form-control" maxlength="60" required placeholder="Enter phone"/>
                        </div>
                    </div>
                </div>

                <hr/>

                <div ng-messages="PropertyForm.myEmail.$error" role="alert">
                    <div ng-message="required">You did not enter your email address</div>
                    <div ng-repeat="errorMessage in errorMessages">
                        <!-- use ng-message-exp for a message whose key is given by an expression -->
                        <div ng-message-exp="errorMessage.type">{{ errorMessage.text }}</div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" ng-disabled="PropertyForm.$invalid" class="btn btn-primary"><span class="fa fa-save"></span> Submit Property</button>
                </div>


            </form>
        </div>

    </div>
    <div class="clearfix"></div>
</div>