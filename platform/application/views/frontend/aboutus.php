<?php
$this->view_data['meta_title'] = $content->meta_title;
$this->view_data['meta_keywords'] = $content->meta_keywords;
$this->view_data['meta_description'] = $content->meta_description;
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array();
$this->view_data['footerScripts'] = array();
$this->view_data['header_title'] = 'About Us';
$this->view_data['header_cover'] = $upload_url.'/settings/about_us_cover.'.$setting->about_us_cover_ext;
$this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';

$this->view_data['breadcrumb_urls'] = array(
    'About us' => $base_url.'/about',
);
?>


<div class="home-wrapper">
    <div class="home-content">
        <?php include(VIEWPATH.'frontend/_section/_breadcrumbs.php'); ?>

        <h2 class="osLight align-left">About Us</h2>

        <?=$content->content?>

    </div>
</div>
