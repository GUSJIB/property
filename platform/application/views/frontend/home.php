<?php
    $this->view_data['meta_title'] = 'Thailand property consultant';//TODO
    $this->view_data['meta_keywords'] = '';//TODO
    $this->view_data['meta_description'] = '';//TODO
    $this->view_data['stylesheet'] = array();
    $this->view_data['footer_js'] = array('/assets/frontend/js/home.js');
    $this->view_data['footerScripts'] = array();
    $this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_home.php';
?>


<div class="home-wrapper">
    <div class="home-content">

        <div class="row">
            <div class="col-sm-9">

                <?php if(count($featured_property_list) > 0) { ?>
                <h2 class="osLight">Featured Properties</h2>
                <div class="row pb40">
                    <?php foreach($featured_property_list as $item) {  ?>
                    <div class="col-lg-4">
                        <a href="<?=$item->url?>" class="card">
                            <div class="figure">
                                <img src="<?=$item->cover_url?>" alt="<?=$item->title?>">
                                <div class="figCaption">
                                    <div>฿<?php
                                        if($item->for_sale){
                                            echo number_format($item->sale_price,0,'.',',');
                                        }
                                        else if($item->for_rent && $item->monthly_rent){
                                            echo number_format($item->monthly_rate,0,'.',',') . ' /month';
                                        }
                                        else if($item->for_rent && $item->yearly_rent){
                                            echo number_format($item->yearly_rate,0,'.',','). ' /year';
                                        }
                                        ?></div>
                                    <span class="icon-bubble"> <?=$item->total_view?></span>
                                </div>
                                <div class="figView"><span class="icon-eye"></span></div>
                                <div class="figType"><?=$item->for_sale && $item->for_rent?'Sale/Rent':''?><?=$item->for_sale?'For Sale':($item->for_rent?'For Rent':'')?></div>
                            </div>
                            <h2><?=$item->title?></h2>
                            <div class="cardAddress"><span class="icon-pointer"></span> <?=$item->address_full?></div>
                            <ul class="cardFeat">
                                <?php if($item->bedroom_qty && $item->bedroom_qty > 0) { ?><li title="Number of bedroom"><span class="fa fa-moon-o"></span> <?=$item->bedroom_qty?></li><?php } ?>
                                <?php if($item->bathroom_qty && $item->bathroom_qty > 0) { ?><li title="Number of bathroom"><span class="icon-drop"></span> <?=$item->bathroom_qty?></li><?php } ?>
                                <li title="Property area"><span class="icon-frame"></span> <?=number_format($item->area_sqm,2,'.',',')?> sq.m</li>
                            </ul>
                        </a>
                    </div>
                    <?php } ?>
                </div>
                <?php } ?>

                <?php if(count($recently_property_list) > 0) { ?>
                <h2 class="osLight">Recently Listed Properties</h2>
                <div class="row pb40">
                    <?php foreach($recently_property_list as $item) {  ?>
                        <div class="col-lg-4">
                            <a href="<?=$item->url?>" class="card">
                                <div class="figure">
                                    <img src="<?=$item->cover_url?>" alt="<?=$item->title?>">
                                    <div class="figCaption">
                                        <div>฿<?php
                                            if($item->for_sale){
                                                echo number_format($item->sale_price,0,'.',',');
                                            }
                                            else if($item->for_rent && $item->monthly_rent){
                                                echo number_format($item->monthly_rate,0,'.',',') . ' /month';
                                            }
                                            else if($item->for_rent && $item->yearly_rent){
                                                echo number_format($item->yearly_rate,0,'.',','). ' /year';
                                            }
                                            ?></div>
                                        <span class="icon-bubble"> <?=$item->total_view?></span>
                                    </div>
                                    <div class="figView"><span class="icon-eye"></span></div>
                                    <div class="figType"><?=$item->for_sale && $item->for_rent?'Sale/Rent':''?><?=$item->for_sale?'For Sale':($item->for_rent?'For Rent':'')?></div>
                                </div>
                                <h2><?=$item->title?></h2>
                                <div class="cardAddress"><span class="icon-pointer"></span> <?=$item->address_full?></div>
                                <ul class="cardFeat">
                                    <?php if($item->bedroom_qty && $item->bedroom_qty > 0) { ?><li title="Number of bedroom"><span class="fa fa-moon-o"></span> <?=$item->bedroom_qty?></li><?php } ?>
                                    <?php if($item->bathroom_qty && $item->bathroom_qty > 0) { ?><li title="Number of bathroom"><span class="icon-drop"></span> <?=$item->bathroom_qty?></li><?php } ?>
                                    <li title="Property area"><span class="icon-frame"></span> <?=number_format($item->area_sqm,2,'.',',')?> sq.m</li>
                                </ul>
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <?php } ?>

                <?php if(count($agent_list) > 0) { ?>
                <h2 class="osLight">Our Agents</h2>
                <div class="row pb40">
                    <?php
                    foreach($agent_list as $item) {
                        $url='/agents/detail/'.$item->id;
                        ?>
                        <div class="col-lg-3">
                            <div class="agent">
                                <a href="<?=$url?>" class="agent-avatar">
                                    <img src="<?=$upload_url.'/avatar/'.$item->id.$item->photo_ext ?>" alt="<?=$item->first_name?> <?=$item->last_name?>">
                                    <div class="ring"></div>
                                </a>
                                <div class="agent-name osLight"><?=$item->first_name?> <?=$item->last_name?></div>
                                <div class="agent-contact">
                                    <a href="mailto:<?=$item->email?>" class="btn btn-sm btn-icon btn-round btn-o btn-green" target="_blank"><span class="fa fa-envelope-o"></span></a>
                                    <a href="<?=$item->facebook_url?>" class="btn btn-sm btn-icon btn-round btn-o btn-facebook" target="_blank"><span class="fa fa-facebook"></span></a>
                                    <a href="<?=$item->twitter_url?>" class="btn btn-sm btn-icon btn-round btn-o btn-twitter" target="_blank"><span class="fa fa-twitter"></span></a>
                                    <a href="<?=$item->gplus_url?>" class="btn btn-sm btn-icon btn-round btn-o btn-google" target="_blank"><span class="fa fa-google-plus"></span></a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php } ?>

            </div>






            <div class="col-sm-3">
                <div class="row">
                    <div class="col-lg-12 ">
                        <h2 class="osLight">Categories</h2>
                        <div class="list-group">
                            <ul class="nav nav-tabs" style="background: none">
                                <li class="active"><a data-toggle="tab" href="#buyingTab">Buying</a></li>
                                <li class=""><a data-toggle="tab" href="#rentingTab">Renting</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="buyingTab" class="tab-pane fade in active">
                                    <? foreach ($property_type_list as $key => $value) { ?>
                                        <a href="/buying/<?=$key?>" class="list-group-item"><span class="fa fa-list"></span> <?=$value?></a>
                                    <? } ?>
                                </div>
                                <div id="rentingTab" class="tab-pane fade">
                                    <? foreach ($property_type_list as $key => $value) { ?>
                                        <a href="/renting/<?=$key?>" class="list-group-item"><span class="fa fa-list"></span> <?=$value?></a>
                                    <? } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <h2 class="osLight">Property Filters</h2>
                        <div class="list-group">
                            <a href="/renting" class="list-group-item"><span class="fa fa-list"></span>Renting</a>
                            <a href="/buying" class="list-group-item"><span class="fa fa-list"></span> Buying</a>
                            <a href="/search" class="list-group-item"><span class="fa fa-list"></span> Renting & Buying</a>
                        </div>
                    </div>

                    <?php include(VIEWPATH.'frontend/_section/_mortgage.php'); ?>

                </div>

            </div>

        </div>


    </div>
</div>
