<?php
$this->view_data['meta_title'] = 'User Account';
$this->view_data['meta_keywords'] = '';
$this->view_data['meta_description'] = 'User Account';
$this->view_data['title'] = 'Sign in';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array('/assets/frontend/js/member/account.js');
$this->view_data['footerScripts'] = array();
$this->view_data['header_title'] = 'User Account';
$this->view_data['header_cover'] = $upload_url.'/settings/other_cover.'.$setting->other_cover_ext;
$this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';
?>


<div class="home-wrapper">
    <div class="home-content">

        <div class="row login_sign_bg">

            <div class="col-sm-5">
            </div>

            <div class="col-sm-1">
            </div>

            <div class="col-sm-6">
                <div class="overlay_box">
                    <h2 class="osLight align-left">My Account</h2>

                    <div id="acc_modal_message" class="form-group" style="display: none;">
                        Information not complete.
                    </div>
                    <input type="hidden" id="id" value="<?=$user->id?>">
                    <input type="hidden" id="csrfkey" value="<?=$this->session->flashdata('csrfkey')?>" />
                    <input type="hidden" id="csrfval" value="<?=$this->session->flashdata('csrfvalue')?>" />
                    <div class="form-group">
                        <label class="control-label">First Name:</label>
                        <input id="acc_firstname" type="text" value="<?=$user->first_name?>" placeholder="First Name" maxlength="50" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Last Name:</label>
                        <input id="acc_lastname" type="text" value="<?=$user->last_name?>" placeholder="Last Name" maxlength="50" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Company Name:</label>
                        <input id="acc_company" type="text" value="<?=$user->company?>" placeholder="Company" maxlength="100" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Phone:</label>
                        <input id="acc_phone" type="text" value="<?=$user->phone?>" placeholder="Phone number" maxlength="20" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Email Account:</label>
                        <input id="acc_email" type="email" value="<?=$user->email?>" placeholder="Email Address" maxlength="100" class="form-control" disabled />
                    </div>
                    <div class="signOr">-</div>
                    <div class="form-group">
                        <label class="control-label">Password: (if changing password)</label>
                        <input id="acc_password" type="password" placeholder="Password" maxlength="20" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Confirm Password: (if changing password)</label>
                        <input id="acc_con_password" type="password" placeholder="Confirm Password" maxlength="20" class="form-control" />
                    </div>
                    <div class="form-group">
                        <button id="acc_submit" class="btn btn-lg btn-block" forward="true" disabled>Submit</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



