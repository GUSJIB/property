<?php
$this->view_data['meta_title'] = 'New Member Registration';
$this->view_data['meta_keywords'] = '';
$this->view_data['meta_description'] = 'New Member Registration Form';
$this->view_data['title'] = 'Sign in';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array();
$this->view_data['footerScripts'] = array();
$this->view_data['header_title'] = 'Member Registration Form';
$this->view_data['header_cover'] = $upload_url.'/settings/other_cover.'.$setting->other_cover_ext;
$this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';
?>


<div class="home-wrapper">
    <div class="home-content">

        <div class="row login_sign_bg">

            <div class="col-sm-7">
            </div>

            <div class="col-sm-1">
            </div>

            <div class="col-sm-4">
                <div class="overlay_box">
                    <h2 class="osLight align-left">Member Registration Form</h2>
                    <div class="form-group">
                        <div class="btn-group-justified">
                            <a href="<?=site_url('/auth/facebook_login')?>" class="btn btn-lg btn-facebook"><span class="fa fa-facebook pull-left"></span>Register with Facebook</a>
                        </div>
                    </div>
                    <div class="signOr">OR</div>

                    <div id="reg_modal_message" class="form-group" style="display: none;">
                        Information not complete.
                    </div>
                    <div class="form-group">
                        <input id="reg_firstname" type="text" placeholder="First Name" maxlength="50" class="form-control">
                    </div>
                    <div class="form-group">
                        <input id="reg_lastname" type="text" placeholder="Last Name" maxlength="50" class="form-control">
                    </div>
                    <!--                    <div class="form-group">-->
                    <!--                        <input id="reg_company" type="text" placeholder="Company" maxlength="100" class="form-control">-->
                    <!--                    </div>-->
                    <!--                    <div class="form-group">-->
                    <!--                        <input id="reg_phone" type="text" placeholder="Phone number" maxlength="20" class="form-control">-->
                    <!--                    </div>-->
                    <div class="form-group">
                        <input id="reg_email" type="text" placeholder="Email Address" maxlength="100" class="form-control">
                    </div>
                    <div class="form-group">
                        <input id="reg_password" type="password" placeholder="Password" maxlength="20" class="form-control">
                    </div>
                    <div class="form-group">
                        <input id="reg_con_password" type="password" placeholder="Confirm Password" maxlength="20" class="form-control">
                    </div>
                    <div class="form-group">
                        <button id="reg_submit" class="btn btn-lg btn-block" forward="true" disabled>Sign Up</button>
                    </div>
                    <p class="help-block">
                        Already a Real member?
                        <a href="/auth/login" class="modal-si text-blue">Sign In</a>
                    </p>
                </div>
            </div>

        </div>
    </div>
</div>
