<?php
$this->view_data['meta_title'] = 'Change Password';
$this->view_data['meta_keywords'] = '';
$this->view_data['meta_description'] = 'Change Password';
$this->view_data['title'] = 'Sign in';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array();
$this->view_data['footerScripts'] = array();
$this->view_data['header_title'] = 'Change Password';
$this->view_data['header_cover'] = $upload_url.'/settings/other_cover.'.$setting->other_cover_ext;
$this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';
?>

<div class="home-wrapper">
    <div class="home-content">

        <div class="row login_sign_bg">

            <div class="row login_sign_bg">

                <div class="col-sm-5">
                </div>

                <div class="col-sm-1">
                </div>
                <div class="col-sm-6">
                    <div class="overlay_box">
                        <h2 class="osLight align-left">Change Password</h2>
                        <div id="change_password_message" class="form-group" style="display: none;">
                            Invalid password, please try again.
                        </div>

                        <div class="form-group">Old Password</div>
                        <div class="form-group">
                            <input id="old_password" type="password" maxlength="20" placeholder="Old Password" class="form-control" />
                        </div>

                        <div class="form-group"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?> Exclude special char.</div>
                        <div class="form-group">
                            <input id="new_password" type="password" maxlength="20" placeholder="New Password" class="form-control" />
                        </div>
                        <div class="form-group">
                            <input id="confirm_new_password" type="password" maxlength="20" placeholder="Confirm Password" class="form-control" />
                        </div>

                        <div class="form-group">
                            <button id="change_password_submit" class="btn btn-lg btn-block" disabled>Submit</button>
                        </div>
                        <p class="help-block">
                            Try to login again:
                            <a href="/auth/login" class="modal-su text-blue">Sign In</a>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>







<h1><?php echo lang('change_password_heading');?></h1>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/change_password");?>

      <p>
            <?php echo lang('change_password_old_password_label', 'old_password');?> <br />
            <?php echo form_input($old_password);?>
      </p>

      <p>
            <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
            <?php echo form_input($new_password);?>
      </p>

      <p>
            <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?> <br />
            <?php echo form_input($new_password_confirm);?>
      </p>

      <?php echo form_input($user_id);?>
      <p><?php echo form_submit('submit', lang('change_password_submit_btn'));?></p>

<?php echo form_close();?>
