<?php
$this->view_data['meta_title'] = 'Change Password';
$this->view_data['meta_keywords'] = '';
$this->view_data['meta_description'] = 'Change Password';
$this->view_data['title'] = 'Sign in';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array();
$this->view_data['footerScripts'] = array();
$this->view_data['header_title'] = 'Change Password';
$this->view_data['header_cover'] = $upload_url.'/settings/other_cover.'.$setting->other_cover_ext;
$this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';
?>


<div class="home-wrapper">
    <div class="home-content">

        <div class="row login_sign_bg">

            <div class="row login_sign_bg">

                <div class="col-sm-5">
                </div>

                <div class="col-sm-1">
                </div>
                <div class="col-sm-6">
                    <div class="overlay_box">
                        <h2 class="osLight align-left">Change Password</h2>
                        <input type="hidden" id="csrfkey" value="<?=$this->session->flashdata('csrfkey')?>" />
                        <input type="hidden" id="csrfval" value="<?=$this->session->flashdata('csrfvalue')?>" />
                        <div class="form-group"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?> Exclude special char.</div>

                        <div id="reset_modal_message" class="form-group" style="display: none;">
                            Invalid password, please try again.
                        </div>
                        <div class="form-group">
                            <input id="new_password" type="password" maxlength="20" placeholder="New Password" class="form-control" />
                        </div>
                        <div class="form-group">
                            <input id="confirm_new_password" type="password" maxlength="20" placeholder="Confirm Password" class="form-control" />
                        </div>
                        <div class="form-group">
                            <button id="reset_submit" class="btn btn-lg btn-block" forward="true" disabled>Submit</button>
                        </div>
                        <p class="help-block">
                            Try to login again:
                            <a href="/auth/login" class="modal-su text-blue">Sign In</a>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
