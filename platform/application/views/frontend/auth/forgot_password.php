<?php
$this->view_data['meta_title'] = 'Forgot Password';
$this->view_data['meta_keywords'] = '';
$this->view_data['meta_description'] = 'Forgot Password';
$this->view_data['title'] = 'Sign in';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array();
$this->view_data['footerScripts'] = array();
$this->view_data['header_title'] = 'Forgot Password';
$this->view_data['header_cover'] = $upload_url.'/settings/other_cover.'.$setting->other_cover_ext;
$this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';
?>


<div class="home-wrapper">
    <div class="home-content">

        <div class="row login_sign_bg">

            <div class="row login_sign_bg">

                <div class="col-sm-5">
                </div>

                <div class="col-sm-1">
                </div>
                <div class="col-sm-6">
                    <div class="overlay_box">
                        <h2 class="osLight align-left">Forgot Password</h2>
                        <div class="form-group">Please enter your Email so we can send you an email to reset your password.</div>

                        <div id="forgot_modal_message" class="form-group" style="display: none;">
                            Email does not existing, please try again.
                        </div>
                        <div class="form-group">
                            <input id="forgot_email" type="email" maxlength="100" placeholder="Email Address" class="form-control">
                        </div>
                        <div class="form-group">
                            <button id="forgot_submit" class="btn btn-lg btn-block" forward="true" disabled>Submit</button>
                        </div>
                        <p class="help-block">
                            Try to login again:
                            <a href="/auth/login" class="modal-su text-blue">Sign In</a>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
