<?php
$this->view_data['meta_title'] = 'Member Sign In';
$this->view_data['meta_keywords'] = '';
$this->view_data['meta_description'] = 'Member Sign In';
$this->view_data['title'] = 'Sign in';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array();
$this->view_data['footerScripts'] = array();
$this->view_data['header_title'] = 'Member Sign In Form';
$this->view_data['header_cover'] = $upload_url.'/settings/other_cover.'.$setting->other_cover_ext;
$this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';
?>


<div class="home-wrapper">
    <div class="home-content">

        <div class="row login_sign_bg">

            <div class="col-sm-7">
            </div>

            <div class="col-sm-1">
            </div>

            <div class="col-sm-4">
                <div class="overlay_box">
                    <h2 class="osLight align-left">Member Sign In</h2>
                    <div class="form-group">
                        <div class="btn-group-justified">
                            <a href="<?=site_url('/auth/facebook_login')?>" class="btn btn-lg btn-facebook"><span class="fa fa-facebook pull-left"></span>Sign In with Facebook</a>
                        </div>
                    </div>
                    <div class="signOr">OR</div>
                    <div id="login_modal_message" class="form-group" style="display: none;">
                        Wrong username or password, please try again.
                    </div>
                    <div class="form-group">
                        <input id="signin_email" type="email" maxlength="100" placeholder="Email Address" class="form-control">
                    </div>
                    <div class="form-group">
                        <input id="signin_password" type="password" maxlength="20" placeholder="Password" class="form-control">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="checkbox custom-checkbox"><label><input id="signin_remember_me" type="checkbox" /><span class="fa fa-check"></span> Remember me</label></div>
                            </div>
                            <div class="col-xs-6 align-right">
                                <p class="help-block"><a href="/auth/forgot_password" class="text-blue">Forgot password?</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button id="login_submit" class="btn btn-lg btn-block" forward="true" disabled>Sign In</button>
                    </div>
                    <p class="help-block">
                        Don't have an account?
                        <a href="/auth/register" class="modal-su text-blue">Sign Up</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>