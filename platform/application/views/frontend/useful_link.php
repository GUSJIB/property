<?php
$this->view_data['meta_title'] = $content->meta_title;
$this->view_data['meta_keywords'] = $content->meta_keywords;
$this->view_data['meta_description'] = $content->meta_description;
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array();
$this->view_data['footerScripts'] = array();
$this->view_data['header_title'] = 'Useful Links';
$this->view_data['header_cover'] = $upload_url.'/settings/other_cover.'.$setting->other_cover_ext;
$this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';

$this->view_data['breadcrumb_urls'] = array(
    'Useful links' => $base_url.'usefulLink'
);
?>




<div class="home-wrapper">
    <?php include(VIEWPATH.'frontend/_section/_breadcrumbs.php'); ?>
    <div class="home-content">

        <h2 class="osLight align-left">Useful Links</h2>

        <?=$content->content?>

    </div>
</div>


