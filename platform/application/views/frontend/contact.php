<?php
$this->view_data['meta_title'] = $content->meta_title;
$this->view_data['meta_keywords'] = $content->meta_keywords;
$this->view_data['meta_description'] = $content->meta_description;
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array('/assets/frontend/js/contact.js', 'http://maps.googleapis.com/maps/api/js?sensor=true&libraries=geometry&libraries=places&callback=renderGoogleMap');
$this->view_data['footerScripts'] = array();
$this->view_data['header_title'] = 'Contact Us';
$this->view_data['header_cover'] = $upload_url.'/settings/contact_us_cover.'.$setting->contact_us_cover_ext;
$this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';

$this->view_data['breadcrumb_urls'] = array(
    'Contact us' => $base_url.'contact'
);
?>


<div class="home-wrapper">
    <?php include(VIEWPATH.'frontend/_section/_breadcrumbs.php'); ?>
    <div class="home-content">

        <h2 class="osLight align-left">SIA Property</h2>


        <div class="row pb20">
            <div class="col-lg-6">
                <h4>Contact Details</h4>
                <div class="contact-details"><span class="contact-icon fa fa-phone"></span>  <?=$setting->office_phone?></div>
                <div class="contact-details"><span class="contact-icon fa fa-fax"></span> <?=$setting->office_fax?></div>
                <div class="contact-details"><span class="contact-icon fa fa-envelope-o"></span>  <?=$setting->office_email?></div>
                <div class="contact-details"><span class="contact-icon fa fa-map-marker"></span> <?=$setting->address_line1?> <?=$setting->address_line2?>, <?=$setting->city_name?>, <?=$setting->country_name?> <?=$setting->zip_code?></div>
            </div>
            <div class="col-lg-6">
                <h4>Follow Us</h4>
                <div class="contact-details"><a href="<?=$setting->social_facebook_url?>" class="text-facebook" target="_blank"><span class="contact-icon fa fa-facebook"></span> Facebook</a></div>
                <div class="contact-details"><a href="<?=$setting->social_twitter_url?>" class="text-twitter" target="_blank"><span class="contact-icon fa fa-twitter"></span> Twitter</a></div>
                <div class="contact-details"><a href="<?=$setting->social_gplus_url?>" class="text-google" target="_blank"><span class="contact-icon fa fa-google-plus"></span> Google+</a></div>
                <div class="contact-details"><a href="<?=$setting->social_in_url?>" class="text-linkedin" target="_blank"><span class="contact-icon fa fa-linkedin"></span> LinkedIn</a></div>
            </div>
        </div>

        <div class="entry-content">
            <?=$content->content?>
            <div class="clearfix"></div>
        </div>

        <div class="entry-content">
            <div id="company_location" lat="<?=$content->lat?>" lon="<?=$content->lon?>"> </div>
        </div>


        <h2 class="osLight align-left">Send Us a Message</h2>
        <div class="row">

            <div id="con_alert_message" class="col-md-12 hide">
                <div class="alert alert-info">
                    <span id="con_alert_message_in">Submit message successful.</span>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="name" class="control-label">Name <span class="text-red">*</span></label>
                    <input class="form-control" id="con_fullname" placeholder="Your Full Name" type="text" value="" />
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="email" class="control-label">Email ID <span class="text-red">*</span></label>
                    <input class="form-control" id="con_email" placeholder="Your Email ID" type="text" value="" />
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label for="subject" class="control-label">Subject <span class="text-red">*</span></label>
                    <input class="form-control" id="con_subject" placeholder="Your Subject" type="text" value="" />
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label for="message" class="control-label">Message <span class="text-red">*</span></label>
                    <textarea class="form-control" id="con_message" rows="10" cols="100" placeholder="Your Message"></textarea>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <input id="con_submit" type="submit" class="btn btn-primary" value="Send Message" />
                </div>
            </div>
        </div>

    </div>
</div>

