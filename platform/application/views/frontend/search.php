<?php
    $this->view_data['meta_title'] = 'Search real estate result ';
    $this->view_data['meta_keywords'] = 'real estate, properties, property listing';
    $this->view_data['meta_description'] = 'Search real estate result';
    $this->view_data['stylesheet'] = array();
    $this->view_data['footer_js'] = array('/assets/frontend/js/search.js');
    $this->view_data['footerScripts'] = array();
    $this->view_data['header_title'] = 'Search Properties';
    $this->view_data['header_cover'] = $upload_url.'/settings/listing_cover.'.$setting->listing_cover_ext;
    $this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';

    //set page title bar
    $searchTitle = 'Search result';
    $propertyType = 'properties';
    if(isset($params['type']) && !empty($params['type'])){
        $searchTitle = 'Search result : '.humanize($params['type']);
        $propertyType = $params['type'];
    }
    if(isset($params['purpose']) && $params['purpose']!= 'both' && !empty($params['purpose']) ){
        $searchTitle .= ' for '.$params['purpose'];
    }

    //breadcrumb
    $zone = '';
    $this->view_data['breadcrumb_urls'] = array();
    if(isset($propCountry)){
        $this->view_data['breadcrumb_urls'][$propCountry->name] = $propCountry->url;
        $zone = $propCountry->name;
    }
    if(isset($propProvince)){
        $this->view_data['breadcrumb_urls'][$propProvince->name] = $propProvince->url;
        $zone = $propProvince->name;
    }
    if(isset($propAmphur)){
        $this->view_data['breadcrumb_urls'][$propAmphur->name] = $propAmphur->url;
        $zone = $propAmphur->name;//.', '.$propAmphur->province_name;
    }
    if(empty($this->view_data['breadcrumb_urls'])){
        $this->view_data['breadcrumb_urls']['Search property'] = $base_url.'search';
    }

    //Set meta tag if searched by business area
    if(isset($params['city'])){
        $this->view_data['meta_title'] = humanize($propertyType).' listing in '.$params['city'];
        $this->view_data['meta_keywords'] = $params['city'].', property listing in '.$zone.', '.$zone.' real estate';
        $this->view_data['meta_description'] = humanize($propertyType).' listing in '.$zone.' and find more real estate in '.$zone;
        $this->view_data['header_title'] = humanize($propertyType).' in '.$zone;
    }

?>


<!-- Content -->

<div class="home-wrapper">
    <?php include(VIEWPATH.'frontend/_section/_breadcrumbs.php'); ?>
    <div class="home-content">

        <div class="row">

            <div class="col-sm-3">

                <div class="row">

                    <div class="col-lg-12">
                        <h2 class="osLight">Search Properties</h2>
                        <div class="panel panel-default" style="box-shadow: none;">
                            <div class="panel-body">
                                <form action="<?=$current_url?>" id="search-form" method="get">
                                    <div class="form-group">
                                        <label for="city" style="display: block;">City</label>
                                        <input type="hidden" name="cityid" id="cityid" value="<?=isset($params['cityid'])?$params['cityid']:''?>">
                                        <input id="city" name="city" type="text" value="<?=isset($params['city'])?$params['city']:''?>" class="form-control autocompleteInput" placeholder="City" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Property type</label>
                                        <select id="type" name="type" class="form-control">
                                            <option></option>
                                            <? foreach ($property_type_list as $key => $prop) { ?>
                                                <option value="<?=$key?>" <?=(isset($params['type']) && $params['type']==$key)?'selected':''?>><?=$prop?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="purpose" style="display: block;">Search purpose</label>
                                        <select id="purpose" name="purpose" class="form-control">
                                            <option value=""></option>
                                            <option value="buying" <?=(isset($params['purpose']) && $params['purpose']=='buying')?'selected':''?>>Buying</option>
                                            <option value="renting" <?=(isset($params['purpose']) && $params['purpose']=='renting')?'selected':''?>>Renting</option>
                                            <option value="both" <?=(isset($params['purpose']) && $params['purpose']=='both')?'selected':''?>>Buying & Renting</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">&#3647;</div>
                                            <input name="price1" value="<?=isset($params['price1'])?$params['price1']:''?>" class="form-control price" type="text" placeholder="From" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">&#3647;</div>
                                            <input name="price2" value="<?=isset($params['price2'])?$params['price2']:''?>" class="form-control price" type="text" placeholder="To"/>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-top: 30px;">
                                        <input type="submit" class="btn btn-blue" style="width: 100%" value="Search"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-12" style="margin-top: 10px;">
                        <h2 class="osLight">Show on Map</h2>
                        <a href="/map">
                            <img src="/assets/frontend/images/map.jpg" style="width: 100%;" />
                        </a>
                    </div>


                    <div class="col-lg-12" style="margin-top: 30px;">
                        <h2 class="osLight">Property filters</h2>
                        <div class="panel panel-default" style="box-shadow: none;">
                            <div class="panel-body">
                                <form action="<?=$current_url?>" id="filter-form" method="get">
                                    <div class="form-group">
                                        <input id="bedroomSlider_val1" name="bedroom1" type="hidden" value="<?=isset($params['bedroom1'])&&!empty($params['bedroom1'])?$params['bedroom1']:''?>"/>
                                        <input id="bedroomSlider_val2" name="bedroom2" type="hidden" value="<?=isset($params['bedroom2'])&&!empty($params['bedroom2'])?$params['bedroom2']:''?>"/>
                                        <label class="control-label">
                                            Bedroom
                                            <span id="bedroomSlider_from" class="badge"><?=isset($params['bedroom1'])&&!empty($params['bedroom1'])?$params['bedroom1']:'0'?></span> -
                                            <span id="bedroomSlider_to" class="badge"><?=isset($params['bedroom2'])&&!empty($params['bedroom2'])?$params['bedroom2']:'10'?></span>
                                        </label>
                                        <div class="slider" id="bedroomSlider" min="0", max="10" value1="<?=isset($params['bedroom1'])&&!empty($params['bedroom1'])?$params['bedroom1']:'0'?>" value2="<?=isset($params['bedroom2'])&&!empty($params['bedroom2'])?$params['bedroom2']:'10'?>"></div>
                                    </div>
                                    <div class="form-group" style="margin-top: 30px;">
                                        <input id="bathroomSlider_val1" name="bathroom1" type="hidden" value="<?=isset($params['bathroom1'])&&!empty($params['bathroom1'])?$params['bathroom1']:''?>"/>
                                        <input id="bathroomSlider_val2" name="bathroom2" type="hidden" value="<?=isset($params['bathroom2'])&&!empty($params['bathroom2'])?$params['bathroom2']:''?>"/>
                                        <label class="control-label">
                                            Bathroom
                                            <span id="bathroomSlider_from" class="badge"><?=isset($params['bathroom1'])&&!empty($params['bathroom1'])?$params['bathroom1']:'0'?></span> -
                                            <span id="bathroomSlider_to" class="badge"><?=isset($params['bathroom2'])&&!empty($params['bathroom2'])?$params['bathroom2']:'10'?></span>
                                        </label>
                                        <div class="slider" id="bathroomSlider" min="0", max="10" value1="<?=isset($params['bathroom1'])&&!empty($params['bathroom1'])?$params['bathroom1']:'0'?>" value2="<?=isset($params['bathroom2'])&&!empty($params['bathroom2'])?$params['bathroom2']:'10'?>"></div>
                                    </div>
                                    <?php $year = date("Y"); ?>
                                    <div class="form-group" style="margin-top: 30px;">
                                        <input id="buildYearSlider_val1" name="buildYear1" type="hidden" value="<?=isset($params['buildYear1'])&&!empty($params['buildYear1'])?$params['buildYear1']:''?>"/>
                                        <input id="buildYearSlider_val2" name="buildYear2" type="hidden" value="<?=isset($params['buildYear2'])&&!empty($params['buildYear2'])?$params['buildYear2']:''?>"/>
                                        <label class="control-label">
                                            Build years
                                            <span id="buildYearSlider_from" class="badge"><?=isset($params['buildYear1'])&&!empty($params['buildYear1'])?$params['buildYear1']:($year-100)?></span> -
                                            <span id="buildYearSlider_to" class="badge"><?=isset($params['buildYear2'])&&!empty($params['buildYear2'])?$params['buildYear2']:($year+4)?></span>
                                        </label>
                                        <div class="slider" id="buildYearSlider" min="<?=($year-100)?>", max="<?=($year+4)?>" value1="<?=isset($params['buildYear1'])&&!empty($params['buildYear1'])?$params['buildYear1']:($year-100)?>" value2="<?=isset($params['buildYear2'])&&!empty($params['buildYear2'])?$params['buildYear2']:($year+4)?>"></div>
                                    </div>

                                    <h4 style="margin-top: 25px;">Amenities</h4>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_garage" <?=isset($params['am_garage'])?'checked':''?> /><span class="fa fa-check"></span> Garage</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_parking" <?=isset($params['am_parking'])?'checked':''?> /><span class="fa fa-check"></span> Parking</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_internet_wifi" <?=isset($params['am_internet_wifi'])?'checked':''?> /><span class="fa fa-check"></span> Wi-fi</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_lift" <?=isset($params['am_lift'])?'checked':''?> /><span class="fa fa-check"></span> Elevator</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_spa" <?=isset($params['am_spa'])?'checked':''?> /><span class="fa fa-check"></span> Spa</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_pet_friendly" <?=isset($params['am_pet_friendly'])?'checked':''?> /><span class="fa fa-check"></span> Pet Friendly</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_refrigerator" <?=isset($params['am_refrigerator'])?'checked':''?> /><span class="fa fa-check"></span> Refrigerator</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_outdoor_pool" <?=isset($params['am_outdoor_pool'])?'checked':''?> /><span class="fa fa-check"></span> Outdoor Pool</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_balcony" <?=isset($params['am_balcony'])?'checked':''?> /><span class="fa fa-check"></span> Balcony</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_furniture" <?=isset($params['am_furniture'])?'checked':''?> /><span class="fa fa-check"></span> Furniture</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_doorman" <?=isset($params['am_doorman'])?'checked':''?> /><span class="fa fa-check"></span> Doorman</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_telephone" <?=isset($params['am_telephone'])?'checked':''?> /><span class="fa fa-check"></span> Telephone</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_cable_tv" <?=isset($params['am_cable_tv'])?'checked':''?> /><span class="fa fa-check"></span> TV Cable</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_kitken" <?=isset($params['am_kitken'])?'checked':''?> /><span class="fa fa-check"></span> Kitken</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_fitness" <?=isset($params['am_fitness'])?'checked':''?> /><span class="fa fa-check"></span> Fitness</label></div></div>
                                    <div class="form-group"><div class="checkbox custom-checkbox"><label><input type="checkbox" name="am_smoking" <?=isset($params['am_smoking'])?'checked':''?> /><span class="fa fa-check"></span> Smoking</label></div></div>
                                    
                                    <div class="form-group">
                                        <a href="javascript:void(0);" class="btn btn-blue btn-filter-form" style="width: 100%">Filter</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>

            </div>




            <div class="col-sm-9">

                <h2 class="osLight" style="text-align: left"><?= $searchTitle?></h2>
                <div class="header_result_box">showing <?=$searchResult->beginRow?> - <?=$searchResult->endRow?> of <?=$searchResult->totalRows?> properties</div>
                <div class="row pb40">

                    <?php if(count($searchResult->rows) > 0) { ?>

                        <?php foreach($searchResult->rows as $item) {  ?>
                            <div class="col-lg-4">
                                <a href="<?=$item->url?>" class="card">
                                    <div class="figure">
                                        <img src="<?=$item->cover_url?>" alt="<?=$item->title?>">
                                        <div class="figCaption">
                                            <div>฿<?php
                                                if($item->for_sale){
                                                    echo number_format($item->sale_price,0,'.',',');
                                                }
                                                else if($item->for_rent && $item->monthly_rent){
                                                    echo number_format($item->monthly_rate,0,'.',',') . ' /month';
                                                }
                                                else if($item->for_rent && $item->yearly_rent){
                                                    echo number_format($item->yearly_rate,0,'.',','). ' /year';
                                                }
                                                ?></div>
                                            <span class="icon-bubble"> <?=$item->total_view?></span>
                                        </div>
                                        <div class="figView"><span class="icon-eye"></span></div>
                                        <div class="figType"><?=$item->for_sale && $item->for_rent?'Sale/Rent':''?><?=$item->for_sale?'For Sale':($item->for_rent?'For Rent':'')?></div>
                                    </div>
                                    <h2><?=$item->title?></h2>
                                    <div class="cardAddress"><span class="icon-pointer"></span> <?=$item->address_full?></div>
                                    <ul class="cardFeat">
                                        <?php if($item->bedroom_qty && $item->bedroom_qty > 0) { ?><li title="Number of bedroom"><span class="fa fa-moon-o"></span> <?=$item->bedroom_qty?></li><?php } ?>
                                        <?php if($item->bathroom_qty && $item->bathroom_qty > 0) { ?><li title="Number of bathroom"><span class="icon-drop"></span> <?=$item->bathroom_qty?></li><?php } ?>
                                        <li title="Property area"><span class="icon-frame"></span> <?=number_format($item->area_sqm,2,'.',',')?> sq.m</li>
                                    </ul>
                                </a>
                            </div>
                        <?php } ?>

                    <?php } else { ?>

                        <p>Record Not Found</p>

                    <?php } ?>

                </div>

                <!-- Show pagination links -->
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="pagination pagination-round">
                        <?php
                            echo $pagination;
                        ?>
                        </ul>
                    </div>
                </div>

            </div>

        </div>


    </div>
</div>