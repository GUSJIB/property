<?php
$this->view_data['meta_title'] = 'Agent: '.$agent->first_name.' '.$agent->last_name;
$this->view_data['meta_keywords'] = 'agent,'.$agent->first_name.' '.$agent->last_name;;
$this->view_data['meta_description'] = 'Agent: '.$agent->first_name.' '.$agent->last_name;
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array();
$this->view_data['footerScripts'] = array();
$this->view_data['header_title'] = $agent->company;
$this->view_data['header_cover'] = $upload_url.'/settings/agency_cover.'.$setting->agency_cover_ext;
$this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';

$url= $base_url.'agents/detail/'.$agent->id;
$this->view_data['breadcrumb_urls'] = array(
    'Property agents' => $base_url.'agents',
    $agent->first_name.' '.$agent->last_name => $url,
);
?>


<div class="home-wrapper">
    <?php include(VIEWPATH.'frontend/_section/_breadcrumbs.php'); ?>
    <div class="home-content">

        <h2 class="osLight align-left"><?=$agent->company?></h2>


        <div class="row pb20">
            <div class="col col-lg-4">
                <h4>Contact Details</h4>
                <div class="contact-details"><span class="contact-icon fa fa-phone"></span>  <?=$agent->phone?></div>
                <div class="contact-details"><span class="contact-icon fa fa-fax"></span> <?=$agent->fax?></div>
                <div class="contact-details"><span class="contact-icon fa fa-envelope-o"></span>  <?=$agent->email?></div>
            </div>
            <div class="col col-lg-4">
                <h4>Follow Us</h4>
                <div class="contact-details"><a href="<?=$agent->facebook_url?>" class="text-facebook" target="_blank"><span class="contact-icon fa fa-facebook"></span> Facebook</a></div>
                <div class="contact-details"><a href="<?=$agent->twitter_url?>" class="text-twitter" target="_blank"><span class="contact-icon fa fa-twitter"></span> Twitter</a></div>
                <div class="contact-details"><a href="<?=$agent->gplus_url?>" class="text-google" target="_blank"><span class="contact-icon fa fa-google-plus"></span> Google+</a></div>
            </div>
            <div class="col col-lg-4">
                <div class="agent">
                    <div class="agent-avatar">
                        <img src="<?=$upload_url.'/avatar/'.$agent->id.$agent->photo_ext ?>" alt="<?=$agent->first_name?> <?=$agent->last_name?>" />
                    </div>
                </div>
            </div>
        </div>

        <div class="entry-content">
            <h4>About : <?=$agent->first_name?> <?=$agent->last_name?></h4>
            <?=nl2br($agent->agent_detail)?>
            <div class="clearfix"></div>
        </div>

    </div>
</div>

