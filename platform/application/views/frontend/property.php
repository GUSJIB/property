<?php
    $this->view_data['meta_title'] = $property->meta_title;
    $this->view_data['meta_keywords'] = $property->meta_keywords;
    $this->view_data['meta_description'] = $property->meta_description;
    $this->view_data['stylesheet'] = array();
    $this->view_data['footer_js'] = array('/assets/frontend/js/property_detail.js', 'http://maps.googleapis.com/maps/api/js?sensor=true&libraries=geometry&libraries=places&callback=renderGoogleMap');
    $this->view_data['footerScripts'] = array();
    $this->view_data['header_title'] = $property->title;
    //$this->view_data['header_cover'] = $upload_url.'/properties/'.$property->id.'/cover'.$property->cover_ext;
    $this->view_data['header_cover'] = $upload_url.'/settings/detail_cover.'.$setting->detail_cover_ext;
    $this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';

    $path_photo = $upload_url.'/properties/'.$property->id.'/gallery/';

    $this->load->config('config', true);
    //$countries = $this->config->item('countries');
    $property_types = $this->config->item('property_types');


    $this->view_data['breadcrumb_urls'] = array(
        $propCountry->name => $propCountry->url,
        $propProvince->name => $propProvince->url,
        $propAmphur->name => $propAmphur->url,
        $property->title => $property->url
    );
?>



<!-- Content -->

<div class="home-wrapper">
    <?php include(VIEWPATH.'frontend/_section/_breadcrumbs.php'); ?>
    <div class="home-content">

        <div class="row">
            <div class="col-sm-9">

                <h2 class="osLight"><?= $property->title ?> : <?= $property->province_name ?></h2>

                <div class="detailContent">
                    <div class="singleTop">
                        <div id="carouselFull" class="carousel ihome slide" data-ride="carousel" >
                            <ol class="carousel-indicators">
                                <? foreach ($property_photos as $key => $value) { ?>
                                    <li data-target="#carouselFull" data-slide-to="<?= $key ?>" class="<?= $key == 0 ? 'active' : '' ?>"></li>
                                <? } ?>
                            </ol>
                            <div class="carousel-inner">
                                <? foreach ($property_photos as $key => $value) { ?>
                                    <div class="item ihome <?= $key == 0 ? 'active' : '' ?>" style="background-image: url('<?=$path_photo.$value->id.$value->ext?>');">
                                        <div class="container">
                                            <div class="carousel-caption">

                                            </div>
                                        </div>
                                    </div>    
                                <? } ?>
                            </div>
                            <a class="left carousel-control" href="single.html#carouselFull" role="button" data-slide="prev"><span class="fa fa-chevron-left bigShadow"></span></a>
                            <a class="right carousel-control" href="single.html#carouselFull" role="button" data-slide="next"><span class="fa fa-chevron-right bigShadow"></span></a>
                        </div>
                        <div class="" style="margin: 10px 0;">
                                <div class="summaryItem">
                                    <h1 class="pageTitle"><?= $property->title ?></h1>
                                    <div class="address"><span class="icon-pointer"></span> <?= $property->address_full ?></div>

<!--                                    <div class="favLink"><span class="icon-eye"></span> --><?//= $property->total_view ?><!-- views</div>-->
                                    <div class="favLink"><button type="button" onclick="showEnquireModal(<?= $property->id ?>, false);" class="btn btn-blue">Submit Enquire</button></div>
                                    <div class="clearfix"></div>
                                    <ul class="features">
                                        <li><span class="fa fa-moon-o"></span><div><?= $property->bedroom_qty ?> Bedrooms</div></li>
                                        <li><span class="icon-drop"></span><div><?= $property->bathroom_qty ?>  Bathrooms</div></li>
                                        <li><span class="icon-frame"></span><div><?= $property->area_sqm ?>  Sq.M</div></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="description">
                        <h3>Description</h3>
                        <?= $property->detail ?>
                    </div>
                    <div class="clearfix"></div>

                    <div class="description">
                        <h3>Additional Informaiton</h3>
                        <ul class="additional-details clearfix">
                            <?php if(isset($property_type_list[$property->property_type])) {?>
                            <li>
                                <strong>TYPE:</strong>
                                <span><?=$property_type_list[$property->property_type]?></span>
                            </li>
                            <?php } ?>
                            <?php if(isset($property->built_year) && $property->built_year > 0) {?>
                            <li>
                                <strong>BUILT YEAR:</strong>
                                <span><?=$property->built_year?></span>
                            </li>
                            <?php } ?>
                            <?php if(isset($property->building_floor_qty) && $property->building_floor_qty > 0) {?>
                                <li>
                                    <strong>BUILD FLOOR:</strong>
                                    <span><?=$property->building_floor_qty?> floors</span>
                                </li>
                            <?php } ?>
                            <?php if(isset($property->floor_of_room_in_building) && $property->floor_of_room_in_building > 0) {?>
                                <li>
                                    <strong>NUMBER OF ROOM IN BUILDING:</strong>
                                    <span><?=$property->floor_of_room_in_building?> rooms</span>
                                </li>
                            <?php } ?>
                            <?php if(isset($property->sub_floor_qty_in_room) && $property->sub_floor_qty_in_room > 0) {?>
                                <li>
                                    <strong>SUB FLOOR IN ROOM:</strong>
                                    <span><?=$property->sub_floor_qty_in_room?> floors</span>
                                </li>
                            <?php } ?>

                        </ul>
                    </div>

                    <?php if($property->for_sale) {?>
                    <div class="description">
                        <h3>Selling Rate</h3>
                        <ul class="additional-details clearfix">
                            <li>
                                <strong>SALE PRICE:</strong>
                                <span><?=number_format($property->sale_price,0,'.',',')?> THB</span>
                            </li>
                        </ul>
                    </div>
                    <?php } ?>

                    <?php if($property->for_rent) {?>
                    <div class="description">
                        <h3>Renting Rate</h3>
                        <ul class="additional-details clearfix">
                            <?php if($property->monthly_rent) {?>
                                <li>
                                    <strong>MONTHLY RATE:</strong>
                                    <span>
                                        <?=number_format($property->monthly_rate,0,'.',',')?> THB/month
                                        <?php if(isset($property->monthly_min_month) && $property->monthly_min_month > 0) {?>
                                            -- minimum <?=$property->monthly_min_month?> month
                                        <?php } ?>
                                    </span>
                                </li>
                            <?php } ?>
                            <?php if($property->yearly_rent) {?>
                                <li>
                                    <strong>YEARLY RATE:</strong>
                                    <span><?=number_format($property->yearly_rate,0,'.',',')?> THB/year</span>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>

                    <div class="amenities">
                        <h3>Property Views</h3>
                        <div class="row">
                            <div class="col col-lg-4 amItem <?= (!$property->view_city) ? 'inactive' : '' ?>"><span class="fa fa-car"></span> City view</div>
                            <div class="col col-lg-8 amItem <?= (!$property->view_sea) ? 'inactive' : '' ?>"><span class="fa fa-tint"></span> Sea view</div>
                            <div class="col col-lg-4 amItem <?= (!$property->view_river) ? 'inactive' : '' ?>"><span class="fa fa-leaf"></span> River view</div>
                            <div class="col col-lg-8 amItem <?= (!$property->view_skyline) ? 'inactive' : '' ?>"><span class="fa fa-desktop"></span> Skyline view</div>
                        </div>
                    </div>

                    <div class="amenities">
                        <h3>Amenities</h3>
                        <div class="row">
                            <div class="col col-lg-4 amItem <?= (!$property->am_garage) ? 'inactive' : '' ?>"><span class="fa fa-car"></span> Garage</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_outdoor_pool) ? 'inactive' : '' ?>"><span class="fa fa-tint"></span> Outdoor Pool</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_garden) ? 'inactive' : '' ?>"><span class="fa fa-leaf"></span> Garden</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_internet_lan) ? 'inactive' : '' ?>"><span class="fa fa-desktop"></span> Internet Lan</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_internet_wifi) ? 'inactive' : '' ?>"><span class="fa fa-wifi"></span> Internet WIFI</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_telephone) ? 'inactive' : '' ?>"><span class="fa fa-phone"></span> Telephone</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_air_condition) ? 'inactive' : '' ?>"><span class="fa fa-asterisk"></span> Air Conditioning</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_balcony) ? 'inactive' : '' ?>"><span class="fa fa-arrows-v"></span> Balcony</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_cable_tv) ? 'inactive' : '' ?>"><span class="fa fa-desktop"></span> TV Cable</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_terrace) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Terrace</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_furniture) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Furniture</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_kitken) ? 'inactive' : '' ?>"><span class="fa fa-cutlery"></span> Kitken</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_lift) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Elevator</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_parking) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Parking</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_fitness) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Fitness</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_spa) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Spa</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_sauna_room) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Sauna Room</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_dress_room) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Dress Room</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_pet_friendly) ? 'inactive' : '' ?>"><span class="fa fa-bug"></span> Pet Friendly</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_doorman) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Doorman</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_computer) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Computer</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_grill) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Grill</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_diskwasher) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Diskwasher</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_washing_machine) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Washing Machine</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_refrigerator) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Refrigerator</div>
                            <div class="col col-lg-4 amItem <?= (!$property->am_smoking) ? 'inactive' : '' ?>"><span class="fa fa-certificate"></span> Smoking</div>
                        </div>
                    </div>

                    <div class="propertyMap">
                        <h3>Property Map</h3>
                        <div id="property-map" lon="<?= $property->lon ?>" lat="<?= $property->lat ?>" style="background-color:#ccc; height: 450px;">
                        </div>
                    </div>

                    <div class="share">
                        <h3>Share on Social Networks</h3>
                        <div class="row">

                            <div class="col col-lg-3 shareItem">
                                <?php
                                $title=urlencode($property->title . ' : ' . $property->province_name);
                                $url=urlencode($property->url);
                                $summary=urlencode($property->meta_description );
                                $image=urlencode($property->cover_url);
                                ?>
                                <button onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?=$title;?>&amp;p[summary]=<?=$summary;?>&amp;p[url]=<?=$url; ?>&amp;p[images][0]=<?=$image;?>','sharer','toolbar=0,status=0,width=548,height=325');" type="button" class="btn btn-sm btn-round btn-o btn-facebook"><span class="fa fa-facebook"></span> Facebook</button>
                            </div>
                            <div class="col col-lg-3 shareItem">
                                <button onClick="window.open('http://twitter.com/intent/tweet?source=sharethiscom&text=<?=$title;?>&url=<?=$url;?>','sharer','toolbar=0,status=0,width=800,height=600');" type="button" class="btn btn-sm btn-round btn-o btn-twitter"><span class="fa fa-twitter"></span> Twitter</button>
                            </div>
                            <div class="col col-lg-3 shareItem">
                                <button onClick="window.open('http://plus.google.com/share?url=<?=$url;?>','sharer','toolbar=0,status=0,width=800,height=600');" type="button" class="btn btn-sm btn-round btn-o btn-google"><span class="fa fa-google-plus"></span> Google+</button>
                            </div>
                            <div class="col col-lg-3 shareItem">
                                <button onClick="window.open('http://pinterest.com/pin/create/button/?url=<?=$url;?>&media=<?=$image;?>&description=<?=$summary;?>','sharer','toolbar=0,status=0,width=800,height=600');" type="button" class="btn btn-sm btn-round btn-o btn-pinterest"><span class="fa fa-pinterest"></span> Pinterest</button>
                            </div>
                        </div>
                    </div>



                    <?php if(count($featured_property_list) > 0) { ?>
                    <div class="similar">
                        <h3>Similar Properties</h3>
                        <div class="row">
                            <?php foreach($featured_property_list as $item) {  ?>
                            <div class="col-xs-4">
                                <a href="<?=$item->cover_url?>" class="card">
                                    <div class="figure">
                                        <img src="<?=$item->cover_url?>" alt="<?=$item->title?>" height="131" />
                                        <div class="figCaption">
                                            <div>฿<?php
                                                if($item->for_sale){
                                                    echo number_format($item->sale_price,0,'.',',');
                                                }
                                                else if($item->for_rent && $item->monthly_rent){
                                                    echo number_format($item->monthly_rate,0,'.',',') . ' /month';
                                                }
                                                else if($item->for_rent && $item->yearly_rent){
                                                    echo number_format($item->yearly_rate,0,'.',','). ' /year';
                                                }
                                                ?></div>
                                            <span class="icon-eye"> <?=$item->total_view?></span>
                                        </div>
                                        <div class="figView"><span class="icon-eye"></span></div>
                                        <div class="figType"><?=($item->for_sale && $item->for_rent?'Sale/Rent':($item->for_sale?'For Sale':($item->for_rent?'For Rent':'')))?></div>
                                    </div>
                                    <h2><?=$item->title?></h2>
                                    <div class="cardAddress"><span class="icon-pointer"></span> <?=$item->address_full?></div>
                                    <ul class="cardFeat">
                                        <?php if($item->bedroom_qty && $item->bedroom_qty > 0) { ?><li title="Number of bedroom"><span class="fa fa-moon-o"></span> <?=$item->bedroom_qty?></li><?php } ?>
                                        <?php if($item->bathroom_qty && $item->bathroom_qty > 0) { ?><li title="Number of bathroom"><span class="icon-drop"></span> <?=$item->bathroom_qty?></li><?php } ?>
                                        <li title="Property area"><span class="icon-frame"></span> <?=number_format($item->area_sqm,2,'.',',')?> sq.m</li>
                                    </ul>
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>


                </div>











            </div>

            <div class="col-sm-3">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="osLight">Search Properties</h2>
                        <div class="panel panel-default" style="box-shadow: none;">
                            <div class="panel-body">
                                <form action="/search" method="get">
                                    <div class="form-group">
                                        <label for="city" style="display: block;">Province</label>
                                        <input type="hidden" id="cityid" name="cityid" value="" />
                                        <input id="city" name="city" type="text" class="form-control autocompleteInput" placeholder="City" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Property type</label>
                                        <select id="type" name="property_type" class="form-control">
                                            <option></option>
                                            <? foreach ($property_type_list as $key => $value) { ?>
                                                <option value="<?=$key?>"><?=$value?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="purpose" style="display: block;">Search purpose</label>
                                        <select id="purpose" name="purpose" class="form-control">
                                            <option value=""></option>
                                            <option value="sale">Buying</option>
                                            <option value="rent">Renting</option>
                                            <option value="both">Buying & Renting</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">&#3647;</div>
                                            <input name="price1" class="form-control price" type="text" placeholder="From" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">&#3647;</div>
                                            <input name="price2" class="form-control price" type="text" placeholder="To"/>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-top: 30px;">
                                        <input type="submit" class="btn btn-blue" style="width: 100%" value="Search"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <h2 class="osLight">Property Filters</h2>
                        <div class="list-group">
                            <a href="/renting" class="list-group-item"><span class="fa fa-list"></span>Renting</a>
                            <a href="/buying" class="list-group-item"><span class="fa fa-list"></span> Buying</a>
                            <a href="/search" class="list-group-item"><span class="fa fa-list"></span> Renting & Buying</a>
                        </div>
                    </div>

                    <?php include(VIEWPATH.'frontend/_section/_mortgage.php'); ?>

                    <?php if(count($featured_property_list) > 0) { ?>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <h2 class="osLight">Featured Properties</h2>
                        <ul class="propList black">
                            <?php foreach($featured_property_list as $item) {  ?>
                                <li>
                                    <a href="<?=$item->url?>">
                                        <div class="image"><img src="<?=$item->cover_url?>" alt="<?=$item->title?>" /></div>
                                        <div class="info text-nowrap">
                                            <div class="name"><?=$item->title?></div>
                                            <div class="address"><?=$item->address_full?></div>
                                            <div class="price">
                                                ฿<?php
                                                if($item->for_sale){
                                                    echo number_format($item->sale_price,0,'.',',');
                                                }
                                                else if($item->for_rent && $item->monthly_rent){
                                                    echo number_format($item->monthly_rate,0,'.',',') . ' /month';
                                                }
                                                else if($item->for_rent && $item->yearly_rent){
                                                    echo number_format($item->yearly_rate,0,'.',','). ' /year';
                                                }
                                                ?>
                                                <span class="badge"><?=($item->for_sale && $item->for_rent?'Sale/Rent':($item->for_sale?'For Sale':($item->for_rent?'For Rent':'')))?></span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>

                    <?php if(count($recently_property_list) > 0) { ?>
                        <div class="col-lg-12" style="margin-top: 30px;">
                            <h2 class="osLight">Recently Properties</h2>
                            <ul class="propList black">
                                <?php foreach($recently_property_list as $item) {  ?>
                                    <li>
                                        <a href="<?=$item->url?>">
                                            <div class="image"><img src="<?=$item->cover_url?>" alt="<?=$item->title?>" /></div>
                                            <div class="info text-nowrap">
                                                <div class="name"><?=$item->title?></div>
                                                <div class="address"><?=$item->address_full?></div>
                                                <div class="price">
                                                    ฿<?php
                                                    if($item->for_sale){
                                                        echo number_format($item->sale_price,0,'.',',');
                                                    }
                                                    else if($item->for_rent && $item->monthly_rent){
                                                        echo number_format($item->monthly_rate,0,'.',',') . ' /month';
                                                    }
                                                    else if($item->for_rent && $item->yearly_rent){
                                                        echo number_format($item->yearly_rate,0,'.',','). ' /year';
                                                    }
                                                    ?>
                                                    <span class="badge"><?=($item->for_sale && $item->for_rent?'Sale/Rent':($item->for_sale?'For Sale':($item->for_rent?'For Rent':'')))?></span>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>


                </div>

            </div>

        </div>






    </div>
</div>

