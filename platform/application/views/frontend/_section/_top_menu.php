<?php
$this->load->config('config', true);
$property_types = $this->config->item('property_types');
?>

<a href="javascript:void(0);" class="top_menu_user visible-xs"><span class="icon-user"></span></a>
<a href="javascript:void(0);" class="top_menu_links visible-xs"><span class="fa fa-bars"></span></a>
<div class="top_menu_box">


    <div class="menu-top-menu-container user_nav">
        <ul id="visitor_top_menu" class="menu" <?php if($ion_auth->logged_in()) { ?>style="display:none;"<?php } ?>>
            <li><a href="<?=$isRegisterPage?'/auth/register':'#'?>" <?=$isRegisterPage?'':'data-toggle="modal" data-target="#signupPopup"'?>>Register</a></li>
            <li><a href="<?=$isLoginPage?'/auth/login':'#'?>" <?=$isLoginPage?'':'data-toggle="modal" data-target="#signinPopup"'?>>Login</a></li>
        </ul>
        <ul id="member_top_menu" class="menu" <?php if(!$ion_auth->logged_in()) { ?>style="display:none;"<?php } ?>>
            <li>
                <a href="#">My Account&nbsp;<span class="fa fa-angle-down"></span></a>
                <ul class="sub-menu">
                    <li><a href="/auth/account">My account</a></li>
                    <li><a href="/auth/change_password">Change password</a></li>
                    <li><a href="/auth/logout">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>

    <div class="menu-top-menu-container main_nav">
        <ul id="menu-top-menu" class="menu">
            <li><a href="/">Home</a></li>
            <li>
                <a href="#">Buying&nbsp;<span class="fa fa-angle-down"></span></a>
                <ul class="sub-menu">
                    <? foreach ($property_types as $key => $value) { ?>
                        <li><a href="/buying/<?=$key?>"><?=$value?></a></li>
                    <? } ?>
                    <li><a href="javascript:void(0);" onclick="showEnquireModal(false, 'buy'); return false;">Submit buying enquire</a></li>
                </ul>
            </li>
            <li>
                <a href="#">Renting&nbsp;<span class="fa fa-angle-down"></span></a>
                <ul class="sub-menu">
                    <? foreach ($property_types as $key => $value) { ?>
                        <li><a href="/renting/<?=$key?>"><?=$value?></a></li>
                    <? } ?>
                    <li><a href="javascript:void(0);" onclick="showEnquireModal(false, 'rent'); return false;">Submit renting enquire</a></li>
                </ul>
            </li>
            <li><a href="/auctions">Auctions</a></li>
            <li><a href="/propertyForm">Add Property</a></li>
            <li><a href="<?=$setting->blog_link?>"><?=$setting->blog_text?></a></li>
        </ul>
    </div>

</div>