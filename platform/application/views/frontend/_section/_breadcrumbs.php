<?php
if(!isset($this->view_data['breadcrumb_urls']) || empty($this->view_data['breadcrumb_urls']))
{
    return;
}

$urlList = $this->view_data['breadcrumb_urls'];
?>
<div class="breadcrumbs">
    <a href="/" title="Thailand property consultant home page.">Home</a>
    <? foreach ($urlList as $text => $url) { ?>
        <span class="fa fa-angle-right"></span>
        <a href="<?=$url?>"><?=$text?></a>
    <? } ?>
</div>