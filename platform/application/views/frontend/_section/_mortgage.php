
<div class="overflow_h col-lg-12">
    <h2 class="osLight">Follow us</h2>
    <div class="fb-like-box" data-href="<?=$setting->social_facebook_url?>" data-height="500" data-width="260" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=359102464260467&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
</div>


<div class="col-lg-12" style="margin-top: 30px;">
    <h2 class="osLight">Mortgage Calculator</h2>
    <div class="panel panel-default" style="box-shadow: none;">
        <div class="panel-body">
            <form role="form">
                <div class="form-group">
                    <label for="mort_totalPrice">Total Price*</label>
                    <input type="text" class="form-control" id="mort_totalPrice" />
                </div>
                <div class="form-group">
                    <label for="mort_downPayment">Down Payment*</label>
                    <input type="text" class="form-control" id="mort_downPayment" />
                </div>
                <div class="form-group">
                    <label for="mort_noOfYears">No of Years*</label>
                    <input type="text" class="form-control" id="mort_noOfYears" />
                </div>
                <div class="form-group">
                    <label for="mort_interestPerYear">Interest per Year(%)*</label>
                    <input type="text" class="form-control" id="mort_interestPerYear" />
                </div>
                <div class="form-group">
                    <label for="mort_installmentPerYear">Installment per Year*</label>
                    <input type="text" class="form-control" id="mort_installmentPerYear"/>
                </div>
                <div class="form-group">
                    <button type="button" onclick="mortgargeReset();" class="btn btn-blue">Reset</button>
                    <button type="button" onclick="mortgargeCal();" class="btn btn-blue">Calculate</button>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Monthly Payment</label>
                    <input id="mort_result" type="text" class="form-control" disabled/>
                </div>
            </form>
        </div>
    </div>
</div>