<div id="hero-container" style="background-image:url('<?=$this->view_data['header_cover']?>'); background-size: cover; background-position: 50% 50%; height: 300px;">

    <!-- M E N U  B O X -->
    <div class="home-header">
        <div class="home-logo osLight"><a href="/"><img src="<?=$upload_url?>/settings/logo.<?=$setting->logo_ext?>" alt="Thailand real estate agency" width="235" height="45"/></a></div>
        <?php include(VIEWPATH.'frontend/_section/_top_menu.php'); ?>
    </div>
    <div class="page-caption">
        <div class="home-title"><?=$this->view_data['header_title'] ?></div>
    </div>
</div>