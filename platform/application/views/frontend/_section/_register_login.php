<!-- N O N E  M E M B E R  B O X -->
<?php if(!$isLoginPage) { ?>
<div class="modal fade" id="signinPopup" role="dialog" aria-labelledby="signinLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="signinLabel">Sign In</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="btn-group-justified">
                        <a href="<?=site_url('/auth/facebook_login')?>" class="btn btn-lg btn-facebook"><span class="fa fa-facebook pull-left"></span>Sign In with Facebook</a>
                    </div>
                </div>
                <div class="signOr">OR</div>
                <div id="login_modal_message" class="form-group" style="display: none;">
                    Wrong username or password, please try again.
                </div>
                <div class="form-group">
                    <input id="signin_email" type="email" maxlength="100" placeholder="Email Address" class="form-control">
                </div>
                <div class="form-group">
                    <input id="signin_password" type="password" maxlength="20" placeholder="Password" class="form-control">
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="checkbox custom-checkbox"><label><input id="signin_remember_me" type="checkbox" /><span class="fa fa-check"></span> Remember me</label></div>
                        </div>
                        <div class="col-xs-6 align-right">
                            <p class="help-block"><a href="/auth/forgot_password" class="text-blue">Forgot password?</a></p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button id="login_submit" class="btn btn-lg btn-block" forward="false" disabled>Sign In</button>
                </div>
                <p class="help-block">
                    Don't have an account?
                    <a href="#" data-toggle="modal" data-target="#signupPopup" class="modal-su text-blue"  onclick="$('#signinPopup').modal('hide');">Sign Up</a>
                </p>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php if(!$isRegisterPage) { ?>
<div class="modal fade" id="signupPopup" role="dialog" aria-labelledby="signupLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="signupLabel">Sign Up</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <div class="btn-group-justified">
                        <a href="<?=site_url('/auth/facebook_login')?>" class="btn btn-lg btn-facebook"><span class="fa fa-facebook pull-left"></span>Sign Up with Facebook</a>
                    </div>
                </div>
                <div class="signOr">OR</div>

                <div id="reg_modal_message" class="form-group" style="display: none;">
                    Information not complete.
                </div>
                <div class="form-group">
                    <input id="reg_firstname" type="text" placeholder="First Name" maxlength="50" class="form-control">
                </div>
                <div class="form-group">
                    <input id="reg_lastname" type="text" placeholder="Last Name" maxlength="50" class="form-control">
                </div>
<!--                    <div class="form-group">-->
<!--                        <input id="reg_company" type="text" placeholder="Company" maxlength="100" class="form-control">-->
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <input id="reg_phone" type="text" placeholder="Phone number" maxlength="20" class="form-control">-->
<!--                    </div>-->
                <div class="form-group">
                    <input id="reg_email" type="text" placeholder="Email Address" maxlength="100" class="form-control">
                </div>
                <div class="form-group">
                    <input id="reg_password" type="password" placeholder="Password" maxlength="50" maxlength="20" class="form-control">
                </div>
                <div class="form-group">
                    <input id="reg_con_password" type="password" placeholder="Confirm Password" maxlength="20" class="form-control">
                </div>
                <div class="form-group">
                    <button id="reg_submit" class="btn btn-lg btn-block" forward="false" disabled>Sign Up</button>
                </div>
                <p class="help-block">
                    Already a Real member?
                    <a href="#" data-toggle="modal" data-target="#signinPopup" class="modal-si text-blue" onclick="$('#signupPopup').modal('hide');">Sign In</a>
                </p>

            </div>
        </div>
    </div>
</div>
<?php } ?>


<div class="modal fade" id="enquireModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="signupLabel">Create Enquire</h4>
            </div>
            <div class="modal-body">
                <input id="enq_property_id" type="hidden" />
                <div id="enq_modal_message" class="form-group" style="display: none;">
                    Information not complete.
                </div>
                <div class="form-group">
                    <label class="control-label">Name  <span class="text-red">*</span></label>
                    <input id="enq_name" type="text" placeholder="Contact name" maxlength="150" class="form-control" />
                </div>
                <div class="form-group">
                    <label class="control-label">Email <span class="text-red">*</span></label>
                    <input id="enq_email" type="text" placeholder="Email Address" maxlength="100" class="form-control" />
                </div>
                <div class="form-group">
                    <label class="control-label">Phone</label>
                    <input id="enq_phone" type="text" placeholder="Mobile Phone" maxlength="50" class="form-control" />
                </div>
                <div class="form-group">
                    <label class="control-label">Purpose <span class="text-red">*</span></label>
                    <select id="enq_purpose" class="form-control" required>
                        <option value=""></option>
                        <option value="rent">For renting</option>
                        <option value="buy">For buying</option>
                        <option value="rent_buy">For renting or buying</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Message <span class="text-red">*</span></label>
                    <textarea id="enq_message" rows="15" class="form-control" required></textarea>
                </div>
                <div class="form-group">
                    <button id="enq_submit" class="btn btn-lg" forward="false" disabled>Submit Enquire</button>
                </div>

            </div>
        </div>
    </div>
</div>