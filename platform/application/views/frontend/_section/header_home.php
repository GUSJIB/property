<?php
    function isInAlerxtPeriod(){
        return false;
        //alert_start
        //alert_end
    }


function isInAlertPeriod($setting)
{
    if(isset($setting->alert_start) && isset($setting->alert_end)) {
        $start_date = $setting->alert_start; //yyyy-mm-dd
        $end_date = $setting->alert_end; //yyyy-mm-dd
        $date_from_user = date('Y-m-d');

        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($date_from_user);

        // Check that user date is between start & end
        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }
    return false;
}

?>
<div id="hero-container">
    <ul class="cb-slideshow">
        <li><span style="background-image: url('<?=$upload_url?>/settings/home_cover1.<?=$setting->home_cover_ext1?>');"></span></li>
        <li><span style="background-image: url('<?=$upload_url?>/settings/home_cover2.<?=$setting->home_cover_ext2?>');"></span></li>
        <li><span style="background-image: url('<?=$upload_url?>/settings/home_cover3.<?=$setting->home_cover_ext3?>');"></span></li>
        <li><span style="background-image: url('<?=$upload_url?>/settings/home_cover4.<?=$setting->home_cover_ext4?>');"></span></li>
        <li><span style="background-image: url('<?=$upload_url?>/settings/home_cover5.<?=$setting->home_cover_ext5?>');"></span></li>
        <li><span style="background-image: url('<?=$upload_url?>/settings/home_cover6.<?=$setting->home_cover_ext6?>');"></span></li>
    </ul>

    <!-- M E N U  B O X -->
    <div class="home-header">
        <div class="home-logo osLight"><a href="/"><img src="<?=$upload_url?>/settings/logo.<?=$setting->logo_ext?>" alt="Thailand real estate agency" width="235" height="45"/></a></div>
        <?php include(VIEWPATH.'frontend/_section/_top_menu.php'); ?>
    </div>

    <div class="home-caption">
        <div class="home-title"><a href="<?=$setting->home_title_link?>"><?=$setting->home_title?></a></div>
        <div class="home-subtitle"><a href="<?=$setting->home_sub_title_link?>"><?=$setting->home_sub_title_text?></a></div>
        <a href="<?=$setting->home_button_link?>" class="btn btn-lg btn-black opacity"><?=$setting->home_button_text?></a>
        <?php if(isInAlertPeriod($setting)){ ?>
        <br/>
        <a href="<?=$setting->alert_link?>" class="btn btn-lg btn-black opacity iAlert" style="margin-top:25px;"><?=$setting->alert_text?></a>
        <?php } ?>
    </div>

    <div class="search-panel">
        <form method="GET" action="/search" class="form-inline" role="form">
            <div class="form-group">
                <input type="hidden" id="cityid" name="cityid" value="" />
                <input name="city" type="text" class="form-control autocompleteInput" placeholder="City" autocomplete="off">
            </div>
            <div class="form-group hidden-xs adv">
                <a href="#" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                    <span class="dropdown-label">-- Type --</span> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-select">
                    <li class="active"><input type="radio" name="type" checked="checked" value="0" /><span>-- Type --</span></li>
                    <? foreach ($property_type_list as $key => $value) { ?>
                        <li><input type="radio" name="type" value="<?=$key?>"/><span><?=$value?></span></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="form-group hidden-xs adv">
                <a href="#" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                    <span class="dropdown-label">Bedrooms</span> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-select">
                    <li class="active"><input type="radio" name="bedroom" value="0" checked="checked"/><span>Bedrooms</span></li>
                    <li><input type="radio" name="bedroom" value="1"/><span>1</span></li>
                    <li><input type="radio" name="bedroom" value="2"/><span>2</span></li>
                    <li><input type="radio" name="bedroom" value="3"/><span>3</span></li>
                    <li><input type="radio" name="bedroom" value="4"/><span>4</span></li>
                    <li><input type="radio" name="bedroom" value="5"/><span>5</span></li>
                </ul>
            </div>
            <div class="form-group hidden-xs adv">
                <a href="#" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                    <span class="dropdown-label">Bathrooms</span> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-select">
                    <li class="active"><input type="radio" name="bathroom" value="0" checked="checked"/><span>Bathrooms</span></li>
                    <li><input type="radio" name="bathroom" value="1"/><span>1</span></li>
                    <li><input type="radio" name="bathroom" value="2"/><span>2</span></li>
                    <li><input type="radio" name="bathroom" value="3"/><span>3</span></li>
                    <li><input type="radio" name="bathroom" value="4"/><span>4</span></li>
                    <li><input type="radio" name="bathroom" value="5"/><span>5</span></li>
                </ul>
            </div>
            <div class="form-group hidden-xs adv">
                <div class="input-group" style="width: 160px;">
                    <div class="input-group-addon">&#3647;</div>
                    <input name="price1" class="form-control price" type="text" placeholder="From" />
                </div>
            </div>
            <div class="form-group hidden-xs adv">
                <div class="input-group" style="width: 160px;">
                    <div class="input-group-addon">&#3647;</div>
                    <input name="price2" class="form-control price" type="text" placeholder="To"/>
                </div>
            </div>
            <div class="form-group hidden-xs adv">
                <div class="checkbox custom-checkbox"><label><input type="checkbox" name="renting" value="1"/><span class="fa fa-check"></span> For Rent</label></div><br/>
                <div class="checkbox custom-checkbox" style="margin-left: -3px;"><label><input type="checkbox" name="buying" value="1"/><span class="fa fa-check"></span> For Sale</label></div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-blue">Search</button>
                <a href="#" class="btn btn-o btn-white pull-right visible-xs" id="advanced">Advanced Search <span class="fa fa-angle-up"></span></a>
            </div>
        </form>
    </div>

</div>