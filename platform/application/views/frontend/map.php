<?php
    $this->view_data['showFooterBox'] = false;
    $this->view_data['meta_title'] = 'Property listing on map';
    $this->view_data['meta_keywords'] = 'property, listing, real estate, Thai property sonculting.';
    $this->view_data['meta_description'] = 'Search property listing on google map';
    $this->view_data['title'] = 'Dashboard';
    $this->view_data['stylesheet'] = array();
    $this->view_data['footer_js'] = array(
        '/assets/frontend/vendors/angular/angular.min.js',
        '/assets/frontend/vendors/angular/angular-bootstrap/ui-bootstrap-tpls.js',
        'https://maps.googleapis.com/maps/api/js?callback=',
        '/assets/frontend/vendors/infobox.js',
        '/assets/frontend/js/map.js'
    );
    $this->view_data['footerScripts'] = array();
    $this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_small.php';
?>




<!-- Content -->

<div id="wrapper"ng-app="app" ng-controller="MapController">


    <div id="mapView"><div class="mapPlaceholder"><span class="fa fa-spin fa-spinner"></span> Loading map...</div></div>


    <div id="content">
        <div class="filter">
            <h1 class="osLight">Filter your results</h1>
            <a href="#" class="handleFilter"><span class="fa fa-filter"></span></a>
            <div class="clearfix"></div>
            <form class="filterForm">
                <div class="row">
                    <div class="col-lg-6 formItem">
                        <div class="formField">
                            <label>City or Business zone</label>
                            <input ng-model="searchModel.cityId" id="cityid" type="text" style="display: none;"/>
                            <input ng-model="searchModel.cityName" type="text" class="form-control autocompleteInput" placeholder="City" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-6 formItem">
                        <div class="formField">
                            <label>Property type</label>
                            <select ng-model="searchModel.property_type" class="form-control">
                                <option></option>
                                <? foreach ($property_type_list as $key => $value) { ?>
                                    <option value="<?=$key?>"><?=$value?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 formItem">
                        <div class="formField">
                            <label>Search Purpose</label>
                            <select ng-model="searchModel.purpose" class="form-control">
                                <option value=""></option>
                                <option value="sale">Buying</option>
                                <option value="rent">Renting</option>
                                <option value="rent_sale">Buying & Renting</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 formItem">
                        <div class="row">
                            <div class="col col-lg-6">
                                <label for="price1">Price to</label>
                                <div class="input-group">
                                    <div class="input-group-addon">&#3647;</div>
                                    <input ng-model="searchModel.price1" ng-disabled="searchModel.purpose=='' || searchModel.purpose=='rent_sale'" class="form-control price" type="text" placeholder="From" />
                                </div>
                            </div>
                            <div class="col col-lg-6">
                                <label for="price2">Price from</label>
                                <div class="input-group">
                                    <div class="input-group-addon">&#3647;</div>
                                    <input ng-model="searchModel.price2" ng-disabled="searchModel.purpose=='' || searchModel.purpose=='rent_sale'" class="form-control price" type="text" placeholder="To" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-6 formItem">
                        <div class="row">
                            <div class="col col-lg-6 formItem">
                                <div class="form-group">
                                    <input ng-model="searchModel.bedroom1" id="bedroomSlider_val1" type="text" style="display: none"/>
                                    <input ng-model="searchModel.bedroom2" id="bedroomSlider_val2" type="text" style="display: none"/>
                                    <label class="control-label">
                                        Bedroom
                                        <span id="bedroomSlider_from" class="badge">0</span> -
                                        <span id="bedroomSlider_to" class="badge">10</span>
                                    </label>
                                    <div class="slider" id="bedroomSlider"></div>
                                </div>
                            </div>
                            <div class="col col-lg-6 formItem">
                                <div class="form-group">
                                    <input ng-model="searchModel.bathroom1" id="bathroomSlider_val1" type="text" style="display: none"/>
                                    <input ng-model="searchModel.bathroom2" id="bathroomSlider_val2" type="text" style="display: none"/>
                                    <label class="control-label">
                                        Bathroom
                                        <span id="bathroomSlider_from" class="badge">0</span> -
                                        <span id="bathroomSlider_to" class="badge">10</span>
                                    </label>
                                    <div class="slider" id="bathroomSlider"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-lg-6 formItem">
                        <button ng-click="submit()" type="button" class="btn btn-blue btn-lg btn-block">Search</button>
                    </div>
                </div>
            </form>
        </div>





        <div class="resultsList">
            <div class="row">
                <div ng-repeat="item in searchModel.rows" class="col col-lg-6">
                    <a ng-href="{{item.url}}" class="card" target="_blank" ng-mouseenter="showMakerOnEnter($index)" ng-mouseleave="hideMakerOnLeave()">
                        <div class="figure">
                            <img ng-src="{{item.cover_url}}" alt="{{item.title}}">
                            <div class="figCaption">
                                <div>{{selectPrice(item)}}</div>
                                <span class="icon-eye"> {{item.total_view}}</span>
                            </div>
                            <div class="figView"><span class="icon-eye"></span></div>
                            <div class="figType">{{selectPurpose(item)}}</div>
                        </div>
                        <h2>{{item.title}}</h2>
                        <div class="cardAddress"><span class="icon-pointer"></span> {{item.address_full}}</div>
                        <ul class="cardFeat">
                            <li ng-if="item.bedroom_qty > 0"><span class="fa fa-moon-o"></span> {{item.bedroom_qty}}</li>
                            <li ng-if="item.bathroom_qty > 0"><span class="icon-drop"></span> {{item.bathroom_qty}}</li>
                            <li><span class="icon-frame"></span> {{formatNumber(item.area_sqm,0,',','.','')}} Sq.M</li>
                        </ul>
                    </a>
                </div>
            </div>

            <pagination ng-show="searchModel.totalPages > 1" ng-change="goPage()" boundary-links="true" class="pagination pagination-sm m-t-none m-b-none"
                        total-items="searchModel.totalRows" ng-model="searchModel.currentPage"
                        items-per-page="searchModel.pageSize" max-size="9">
            </pagination>
        </div>
    </div>
    <div class="clearfix"></div>
</div>