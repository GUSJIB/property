<?php
$this->view_data['meta_title'] = 'Our Agents';
$this->view_data['meta_keywords'] = 'property angent, thailang property agent, thailand property consultant';
$this->view_data['meta_description'] = 'Thailand property consultants';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array();
$this->view_data['footerScripts'] = array();
$this->view_data['header_title'] = 'Our Agents';
$this->view_data['header_cover'] = $upload_url.'/settings/agency_cover.'.$setting->agency_cover_ext;
$this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';

$this->view_data['breadcrumb_urls'] = array(
    'Property agents' => $base_url.'agents'
);
?>



<div class="home-wrapper">
    <?php include(VIEWPATH.'frontend/_section/_breadcrumbs.php'); ?>
    <div class="home-content">

        <div class="row">


            <div class="col-sm-12">

                <h2 class="osLight">Agent Listing</h2>
                <div class="row pb40">

                    <?php
                    foreach($agents as $item) {
                        $url= $base_url.'agents/detail/'.$item->id;
                    ?>
                    <div class="col-lg-3">
                        <div class="agent">
                            <a href="<?=$url?>" class="agent-avatar">
                                <img src="<?=$upload_url.'/avatar/'.$item->id.$item->photo_ext ?>" alt="<?=$item->first_name?> <?=$item->last_name?>">
                                <div class="ring"></div>
                            </a>
                            <div class="agent-name osLight"><?=$item->first_name?> <?=$item->last_name?></div>
                            <div class="agent-contact">
                                <a href="mailto:<?=$item->email?>" class="btn btn-sm btn-icon btn-round btn-o btn-green" target="_blank"><span class="fa fa-envelope-o"></span></a>
                                <a href="<?=$item->facebook_url?>" class="btn btn-sm btn-icon btn-round btn-o btn-facebook" target="_blank"><span class="fa fa-facebook"></span></a>
                                <a href="<?=$item->twitter_url?>" class="btn btn-sm btn-icon btn-round btn-o btn-twitter" target="_blank"><span class="fa fa-twitter"></span></a>
                                <a href="<?=$item->gplus_url?>" class="btn btn-sm btn-icon btn-round btn-o btn-google" target="_blank"><span class="fa fa-google-plus"></span></a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>


            </div>









            </div>

        </div>


    </div>
</div>