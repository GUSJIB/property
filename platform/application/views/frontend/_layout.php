<?php
$passedLogin = false;
$showFooterBox = (isset($this->view_data['showFooterBox']) && $this->view_data['showFooterBox'] == false)? false: true;

$url = uri_string();
$isLoginPage = (strpos($url, 'auth/login') !== false);
$isRegisterPage = (strpos($url, 'auth/register') !== false);

$this->load->config('config', true);
$property_types = $this->config->item('property_types');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title><?=$this->view_data['meta_title'] ?></title>
    <meta name="keywords" content="<?=$this->view_data['meta_keywords'] ?>" />
    <meta name="description" content="<?=$this->view_data['meta_description'] ?>" />

    <link rel="<?=$setting->favicon_ext=='ico'?'shortcut':''?> icon" href="<?=$upload_url?>/settings/favicon.<?=$setting->favicon_ext?>"/>

    <link href="/assets/frontend/css/jquery-ui.css" rel="stylesheet" id="app">
    <link href="/assets/frontend/css/font-awesome.css" rel="stylesheet">
    <link href="/assets/frontend/css/simple-line-icons.css" rel="stylesheet">
    <link href="/assets/frontend/css/fullscreen-slider.css" rel="stylesheet">
    <link href="/assets/frontend/css/bootstrap.css" rel="stylesheet">
    <link href="/assets/frontend/css/app.css" rel="stylesheet" id="app">

    <!--[if lt IE 9]>
    <script src="/assets/frontend/vendors/jquery/html5shiv.min.js"></script>
    <script src="/assets/frontend/respond.min.js"></script>
    <![endif]-->
</head>
<body class="notransition no-hidden">



    <?php include($this->view_data['header_template_url']); ?>



    <!-- Content -->
    <?= $inner_view ?>



    <?php if($showFooterBox === true) { ?>
    <!-- F O O T E R  B O X -->
    <div class="home-footer">
        <div class="home-wrapper">
            <div class="row">
                <div class="col-lg-3">
                    <div class="osLight footer-header">Recently Listed Properties</div>
                    <div class="propsWidget">
                        <ul class="propList">
                            <?php
                            if(isset($footer_listed_property) && count($footer_listed_property) > 0){
                                foreach($footer_listed_property as $item){?>
                                    <li>
                                        <a href="<?=$item->url?>">
                                            <div class="image"><img src="<?=$item->cover_url?>" alt="<?=$item->title?>"></div>
                                            <div class="info text-nowrap">
                                                <div class="name"><?=$item->title?></div>
                                                <div class="address"><?=$item->address_full?></div>
                                                <div class="price">
                                                    ฿<?php
                                                        if($item->for_sale){
                                                            echo number_format($item->sale_price,0,'.',',');
                                                        }
                                                        else if($item->for_rent && $item->monthly_rent){
                                                            echo number_format($item->monthly_rate,0,'.',',') . ' /month';
                                                        }
                                                        else if($item->for_rent && $item->yearly_rent){
                                                            echo number_format($item->yearly_rate,0,'.',','). ' /year';
                                                        }
                                                    ?>
                                                    <span class="badge"><?=($item->for_sale && $item->for_rent?'Sale/Rent':($item->for_sale?'For Sale':($item->for_rent?'For Rent':'')))?></span>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </a>
                                    </li>
                                <?php }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="osLight footer-header">Business Zone</div>
                    <ul class="footer-nav pb20">
                        <?php
                        if(isset($footer_business_zone) && count($footer_business_zone) > 0){
                            foreach($footer_business_zone as $item){?>
                                <li><a href="<?=$item->url?>"><?=$item->name?></a></li>
                            <?php }
                        }
                        ?>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <div class="osLight footer-header">Resources Links</div>
                    <ul class="footer-nav pb20">
                        <li><a href="/about">About Us</a></li>
                        <li><a href="/contact">Contact Us</a></li>
                        <li><a href="/auctions">Auctions</a></li>
                        <li><a href="/usefulLink">Useful Links</a></li>
                        <li><a href="/propertyForm">Add Property</a></li>
                        <li><a href="<?=$setting->blog_link?>"><?=$setting->blog_text?></a></li>
                        <li><a href="/sitemap">Website Navigator</a></li>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <div class="osLight footer-header">Get in Touch</div>
                    <ul class="footer-nav pb0">
                        <li class="footer-phone"><span class="fa fa-phone"></span> <?=$setting->office_phone?></li>
                        <li class="footer-address osLight">
                            <p><?=$setting->address_line1?></p>
                            <p><?=$setting->address_line2?></p>
                            <p><?=$setting->city_name?></p>
                            <p><?=$setting->country_name?> <?=$setting->zip_code?></p>
                        </li>
                    </ul>
                    <div class="osLight footer-header">Follow Us</div>
                    <ul class="footer-nav pb20">
                        <li>
                            <a href="<?=$setting->social_facebook_url?>" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank"><span class="fa fa-facebook"></span></a>
                            <a href="<?=$setting->social_twitter_url?>" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank"><span class="fa fa-twitter"></span></a>
                            <a href="<?=$setting->social_gplus_url?>" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank"><span class="fa fa-google-plus"></span></a>
                            <a href="<?=$setting->social_in_url?>" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank"><span class="fa fa-linkedin"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="copyright"><?=$setting->copyright_text?></div>
        </div>
    </div>

    <?php } ?>



    <?php if($passedLogin === false) { ?>
        <?php include(VIEWPATH.'frontend/_section/_register_login.php'); ?>
    <?php } ?>


<script>
    var city_area_list = [
        <?php
        if(isset($city_area_list) && count($city_area_list) > 0){
            foreach($city_area_list as $item){
                echo '{ value: \''.$item['label'].'\', data: \''.$item['value'].'\' },';
            }
        }
        ?>
    ];
</script>
    <script src="/assets/frontend/vendors/jquery/jquery-2.1.1.min.js"></script>
    <script src="/assets/frontend/vendors/jquery/jquery-ui.min.js"></script>
    <script src="/assets/frontend/vendors/jquery/jquery-ui-touch-punch.js"></script>
    <script src="/assets/frontend/vendors/jquery/jquery.cookie.js"></script>
    <script src="/assets/frontend/vendors/jquery/jquery.placeholder.js"></script>
    <script src="/assets/frontend/vendors/jquery/jquery.slimscroll.min.js"></script>
    <script src="/assets/frontend/vendors/jquery/bootstrap.js"></script>
    <script src="/assets/frontend/vendors/jquery/jquery.touchSwipe.min.js"></script>
    <script src="/assets/frontend/vendors/jquery/jquery.tagsinput.min.js"></script>
    <script src="/assets/frontend/vendors/jquery/jquery.visible.js"></script>
    <script src="/assets/frontend/vendors/jquery/jquery.autocomplete.min.js"></script>

    <script src="/assets/frontend/js/app.js"></script>

    <?php if(isset($this->footerScripts)) echo $this->footerScripts; ?>

    <?php
    if(isset($this->view_data['footer_js']) && count($this->view_data['footer_js']) > 0){
        $js = $this->view_data['footer_js'];
        foreach($js as $url){
            echo '<script src="'.$url.'"></script>';
        }
    }
    ?>

    <!--Start of Zopim Live Chat Script-->
    <script type="text/javascript">
        window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
            _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
            $.src="//v2.zopim.com/?3HPc9O9dsvAyjGTpY1Mlshsl2Fl8k9A0";z.t=+new Date;$.
                type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
    </script>
    <!--End of Zopim Live Chat Script-->

</body>
</html>





