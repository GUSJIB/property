<?php
$this->view_data['meta_title'] = 'Auction Calendar';
$this->view_data['meta_keywords'] = '';
$this->view_data['meta_description'] = 'Property auction Calendar';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array();
$this->view_data['footerScripts'] = array();
$this->view_data['header_title'] = 'Property Actions';
$this->view_data['header_cover'] = $upload_url.'/settings/other_cover.'.$setting->other_cover_ext;
$this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';

$this->view_data['breadcrumb_urls'] = array(
    'Property auctions' => $base_url.'/auctions',
);
?>



<div class="home-wrapper">
    <?php include(VIEWPATH.'frontend/_section/_breadcrumbs.php'); ?>
    <div class="home-content">

        <div class="row">


            <div class="col-sm-12">

                <h2 class="osLight">Auction Calendar</h2>
                <div class="row pb40">

                    <?php
                        foreach($auctions as $item) {
                    ?>
                    <div class="col-lg-12">
                        <div class="card auctions">
                            <h2 class="l"><?=$item->title?></h2>
                            <div class="cardAddress pt5"><span class="fa fa-building"></span> <?=$item->event_place?></div>
                            <div class="cardAddress"><span class="icon-pointer"> </span><?=$item->event_address?></div>
                            <ul class="cardFeat">
                                <li><span class="fa fa-moon-o"></span> Event on <?=date("d/m/Y",strtotime($item->event_date))?></li>
                                <li><span class="icon-clock"></span> <?=$item->start_time?> - 1<?=$item->end_time?></li>
                            </ul>
                            <div class="acution_des">
                                <?=nl2br($item->detail)?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                </div>

            </div>

        </div>


    </div>
</div>