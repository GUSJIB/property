<?php
    $this->view_data['meta_title'] = 'Site map : Thailand property consultants';
    $this->view_data['meta_keywords'] = 'Thailand property consultants, Thailand property, property consultants';
    $this->view_data['meta_description'] = 'Thailand property consultants website.';
    $this->view_data['stylesheet'] = array();
    $this->view_data['footer_js'] = array();
    $this->view_data['footerScripts'] = array();
    $this->view_data['header_template_url'] = VIEWPATH.'frontend/_section/header_medium.php';
    $this->view_data['header_title'] = 'Sitemap';
    $this->view_data['header_cover'] = $upload_url.'/settings/other_cover.'.$setting->other_cover_ext;

    $this->view_data['breadcrumb_urls'] = array(
        'Website Navigator' => $base_url.'sitemap'
    );
?>

<div class="home-wrapper">
    <?php include(VIEWPATH.'frontend/_section/_breadcrumbs.php'); ?>
    <div class="home-content">

        <h2 class="osLight align-left">Website navigation</h2>

        <div class="row pb20">
            <h4 class="col-lg-12">Main Menus</h4>
            <div class="col-lg-3">
                <a href="/" title="Home page">Home</a>
            </div>
            <div class="col-lg-3">
                <a href="/map" title="Search Thailand properties on Google map.">Google map</a>
            </div>
            <div class="col-lg-3">
                <a href="/auctions" title="Property auction schedules">Auctions</a>
            </div>
            <div class="col-lg-3">
                <a href="/propertyForm" title="Add your property">Add property</a>
            </div>
            <div class="col-lg-3">
                <a href="<?=$setting->blog_link?>" title="<?=$setting->blog_text?>"><?=$setting->blog_text?></a>
            </div>
            <div class="col-lg-3">
                <a href="/about" title="About SIA Property">About us</a>
            </div>
            <div class="col-lg-3">
                <a href="/contact" title="Contact us">Contact us</a>
            </div>
            <div class="col-lg-3">
                <a href="/usefulLink" title="Useful links">Useful links</a>
            </div>
        </div>

        <div class="row pb20">
            <h4 class="col-lg-12">Enquires</h4>
            <div class="col-lg-3">
                <a href="javascript:void(0);" onclick="showEnquireModal(false, 'buy'); return false;" title="Create new buying enquire.">Submit buying enquire</a></li>
            </div>
            <div class="col-lg-3">
                <a href="javascript:void(0);" onclick="showEnquireModal(false, 'rent'); return false;" title="Create new renting enquire.">Submit renting enquire</a>
            </div>
            <div class="col-lg-3">
                <a href="/agents" title="Thailand property agent listing.">Our agents</a>
            </div>
        </div>

        <?php
        foreach($country_array as $country) {
            $province_list = $province_array[$country->code];

            foreach($province_list as $province) {
                $area_list = $amphur_array[$province->id];
                ?>
                <div class="row pb20">
                    <h4 class="col-lg-12">Property listing in <a href="<?=$province->url?>" title="<?=$province->name?>"><?=$province->name?></a> : <a href="<?=$country->url?>" title="<?=$country->name?>"><?=$country->name?></a></h4>
                        <?php foreach($area_list as $area) { ?>
                        <div class="col-lg-3">
                            <a href="<?=$area->url?>" title="Property listing in <?=$area->name?> area : <?=$province->name?>"><?=$area->name?></a>
                        </div>
                        <?php } ?>
                </div>
            <?php }
        } ?>

    </div>
</div>
