<?php
$this->view_data['module'] = 'settings';
$this->view_data['url'] = '/admin/settings';

$this->view_data['title'] = 'System Preferences';
$this->view_data['header_text'] = 'System Preferences';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array(
    '/assets/admin/js/settings/controller.js',
    '/assets/admin/js/settings/service.js',
    '/assets/admin/js/settings/directive.js',
    '/assets/admin/vendors/jquery/file-upload/file-upload.min.js'
);
//$this->view_data['angular_modules'] = array('aaaa','bbbbb');
$this->view_data['header_icon_classes'] = 'fa fa-send';

?>


<div class="wrapper" ng-controller="SettingsController">

    <div class="panel panel-default">
        <div class="panel-heading font-bold">
            General Settings
        </div>
        <div class="row panel-body">
            <div class="col-sm-6 form-horizontal">

                <div class="form-group">
                    <label class="col-sm-3 control-label">Setting Name:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.title" maxlength="200" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Office Email:</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" ng-model="item.office_email" maxlength="100" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Office Phone:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.office_phone" maxlength="50" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Office Fax:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.office_fax" maxlength="50" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Address Line1:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.address_line1" maxlength="100" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Address Line2:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.address_line2" maxlength="100" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">City Name:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.city_name" maxlength="50" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Country Name:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.country_name" maxlength="60" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Postal Code:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.zip_code" maxlength="20" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Home Title Text:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.home_title" maxlength="50" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Home Title Link:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.home_title_link" maxlength="150" placeholder="http://"/>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Home Sub title Text:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.home_sub_title_text" maxlength="100" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Home Sub title Link:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.home_sub_title_link" maxlength="150" placeholder="http://" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Home Button Text:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.home_button_text" maxlength="50" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Home Button Link:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.home_button_link" maxlength="150" placeholder="http://" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Alert Text:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.alert_text" maxlength="100" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Alert Link:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.alert_link" maxlength="150" placeholder="http://" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Alert Period:</label>
                    <div class="col-sm-4 datepicker">
                        <input
                            ng-model="item.alert_start"
                            datepicker-popup="yyyy-MM-dd"
                            ng-click="alert_start = true"
                            is-open="alert_start"
                            showWeeks="false"
                            close-text="Close"
                            type="text"
                            class="form-control"
                            placeholder="yyyy-MM-dd"/>
                    </div>
                    <div class="col-sm-1">-</div>
                    <div class="col-sm-4 datepicker">
                        <input
                            ng-model="item.alert_end"
                            datepicker-popup="yyyy-MM-dd"
                            ng-click="alert_end = true"
                            is-open="alert_end"
                            showWeeks="false"
                            close-text="Close"
                            type="text"
                            class="form-control"
                            placeholder="yyyy-MM-dd"/>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

            </div>


            <div class="col-sm-6 form-horizontal">

                <div class="form-group">
                    <label class="col-sm-3 control-label">Copyright Text:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.copyright_text" maxlength="300" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Blog Menu Text:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.blog_text" maxlength="50" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Blog Menu Link:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.blog_link" maxlength="150" placeholder="http://" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Home Recentry Item:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.home_recentry_item" maxlength="50" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Home Featured Item:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.home_featured_item" maxlength="50" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Search Page Size:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.search_page_size" maxlength="50" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Facebook Url:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.social_facebook_url" maxlength="150" placeholder="http://" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Twitter Url:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.social_tweeter_url" maxlength="150" placeholder="http://" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Link In Url:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.social_in_url" maxlength="150" placeholder="http://" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="input-id-1">Google Analytic Code:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="item.google_analytic_code" id="input-id-1" maxlength="200" />
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Header script:</label>
                    <div class="col-sm-9">
                        <textarea rows="4" class="form-control" ng-model="item.header_script"></textarea>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Footer script:</label>
                    <div class="col-sm-9">
                        <textarea rows="4" class="form-control" ng-model="item.footer_script"></textarea>
                    </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>

                <div class="form-group">
                    <div class="col-sm-4 col-sm-offset-2">
                        <button class="btn btn-primary" ng-click="submit()" ng-disabled="PopupForm.$invalid">
                            <i ng-show="dataSubmitting" ng-class="{'fa-spin':dataSubmitting}" class="fa fa-refresh"></i>
                            Save changes
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>







    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading font-bold">
                    Photo & Backgrounds
                    <small>(auto save after photo selected)</small>
                </div>
                <div class="panel-body">

                    <div class="form-horizontal">

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Header Logo:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['logo_ext']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['logo_ext']"></i> Browse Image</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'logo_ext', 'logo')" ng-hide="photoUploading['logo_ext']" /></span>
                                </div>
                                <div>235 x 45 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.logo_ext && {'background-image':'url(<?=$upload_url?>/settings/logo.'+item.logo_ext+'?tx='+photoLoadTime['logo_ext']+')'}" style="background-size: auto; background-color:#edf1f2;"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Header Logo2:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['logo2_ext']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['logo2_ext']"></i> Browse Image</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'logo2_ext', 'logo2')" ng-hide="photoUploading['logo2_ext']" /></span>
                                </div>
                                <div>235 x 45 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.logo2_ext && {'background-image':'url(<?=$upload_url?>/settings/logo2.'+item.logo2_ext+'?tx='+photoLoadTime['logo2_ext']+')'}" style="background-size: auto; background-color:#edf1f2;"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Home Cover 1:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['home_cover_ext1']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['home_cover_ext1']"></i> Browse Photo</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'home_cover_ext1', 'home_cover1')" ng-hide="photoUploading['home_cover_ext1']" /></span>
                                </div>
                                <div>2000 x 600 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.home_cover_ext1 && {'background-image':'url(<?=$upload_url?>/settings/home_cover1.'+item.home_cover_ext1+'?tx='+photoLoadTime['home_cover_ext1']+')'}"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Home Cover 2:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['home_cover_ext2']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['home_cover_ext2']"></i> Browse Photo</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'home_cover_ext2', 'home_cover2')" ng-hide="photoUploading['home_cover_ext2']" /></span>
                                </div>
                                <div>2000 x 600 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.home_cover_ext2 && {'background-image':'url(<?=$upload_url?>/settings/home_cover2.'+item.home_cover_ext2+'?tx='+photoLoadTime['home_cover_ext2']+')'}"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Home Cover 3:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['home_cover_ext3']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['home_cover_ext3']"></i> Browse Photo</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'home_cover_ext3', 'home_cover3')" ng-hide="photoUploading['home_cover_ext3']" /></span>
                                </div>
                                <div>2000 x 600 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.home_cover_ext3 && {'background-image':'url(<?=$upload_url?>/settings/home_cover3.'+item.home_cover_ext3+'?tx='+photoLoadTime['home_cover_ext3']+')'}"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Home Cover 4:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['home_cover_ext4']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['home_cover_ext4']"></i> Browse Photo</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'home_cover_ext4', 'home_cover4')" ng-hide="photoUploading['home_cover_ext4']" /></span>
                                </div>
                                <div>2000 x 600 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.home_cover_ext4 && {'background-image':'url(<?=$upload_url?>/settings/home_cover4.'+item.home_cover_ext4+'?tx='+photoLoadTime['home_cover_ext4']+')'}"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Home Cover 5:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['home_cover_ext5']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['home_cover_ext5']"></i> Browse Photo</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'home_cover_ext5', 'home_cover5')" ng-hide="photoUploading['home_cover_ext5']" /></span>
                                </div>
                                <div>2000 x 600 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.home_cover_ext5 && {'background-image':'url(<?=$upload_url?>/settings/home_cover5.'+item.home_cover_ext5+'?tx='+photoLoadTime['home_cover_ext5']+')'}"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Home Cover 6:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['home_cover_ext6']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['home_cover_ext6']"></i> Browse Photo</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'home_cover_ext6', 'home_cover6')" ng-hide="photoUploading['home_cover_ext6']" /></span>
                                </div>
                                <div>2000 x 600 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.home_cover_ext6 && {'background-image':'url(<?=$upload_url?>/settings/home_cover6.'+item.home_cover_ext6+'?tx='+photoLoadTime['home_cover_ext6']+')'}"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading font-bold">
                    Other Page Photos
                    <small>(auto save after photo selected)</small>
                </div>
                <div class="panel-body">

                    <div class="form-horizontal">

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Favicon icon:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['favicon_ext']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['favicon_ext']"></i> Browse Image</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'favicon_ext', 'favicon')" ng-hide="photoUploading['favicon_ext']" /></span>
                                </div>
                                <div>PNG/ICO - 16 x 16 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" style="background-size: initial;" ng-style="item && item.favicon_ext && {'background-image':'url(<?=$upload_url?>/settings/favicon.'+item.favicon_ext+'?tx='+photoLoadTime['favicon_ext']+')'}"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Listing page Cover:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['listing_cover_ext']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['listing_cover_ext']"></i> Browse Photo</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'listing_cover_ext', 'listing_cover')" ng-hide="photoUploading['listing_cover_ext']" /></span>
                                </div>
                                <div>2000 x 300 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.listing_cover_ext && {'background-image':'url(<?=$upload_url?>/settings/listing_cover.'+item.listing_cover_ext+'?tx='+photoLoadTime['listing_cover_ext']+')'}"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Detail page Cover:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['detail_cover_ext']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['detail_cover_ext']"></i> Browse Photo</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'detail_cover_ext', 'detail_cover')" ng-hide="photoUploading['detail_cover_ext']" /></span>
                                </div>
                                <div>2000 x 300 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.detail_cover_ext && {'background-image':'url(<?=$upload_url?>/settings/detail_cover.'+item.detail_cover_ext+'?tx='+photoLoadTime['detail_cover_ext']+')'}"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Contact us page Cover:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['contact_us_cover_ext']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['contact_us_cover_ext']"></i> Browse Photo</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'contact_us_cover_ext', 'contact_us_cover')" ng-hide="photoUploading['contact_us_cover_ext']" /></span>
                                </div>
                                <div>2000 x 300 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.contact_us_cover_ext && {'background-image':'url(<?=$upload_url?>/settings/contact_us_cover.'+item.contact_us_cover_ext+'?tx='+photoLoadTime['contact_us_cover_ext']+')'}"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">About us page Cover:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['about_us_cover_ext']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['about_us_cover_ext']"></i> Browse Photo</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'about_us_cover_ext', 'about_us_cover')" ng-hide="photoUploading['about_us_cover_ext']" /></span>
                                </div>
                                <div>2000 x 600 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.about_us_cover_ext && {'background-image':'url(<?=$upload_url?>/settings/about_us_cover.'+item.about_us_cover_ext+'?tx='+photoLoadTime['about_us_cover_ext']+')'}"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Agency page Cover:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['agency_cover_ext']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['agency_cover_ext']"></i> Browse Photo</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'agency_cover_ext', 'agency_cover')" ng-hide="photoUploading['agency_cover_ext']" /></span>
                                </div>
                                <div>2000 x 600 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.agency_cover_ext && {'background-image':'url(<?=$upload_url?>/settings/agency_cover.'+item.agency_cover_ext+'?tx='+photoLoadTime['agency_cover_ext']+')'}"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="control-label">Other page Cover:</label>
                                <div class="fileupload" data-provides="fileupload">
                                     <span class="btn btn-file" ng-class="photoUploading['other_cover_ext']?'btn-default':'btn-info'" ><span><i class="fa fa-refresh fa-spin" ng-show="photoUploading['other_cover_ext']"></i> Browse Photo</span>
                                     <input accept="image/*" type="file" file-change="uploadPhoto($event, files, 'other_cover_ext', 'other_cover')" ng-hide="photoUploading['other_cover_ext']" /></span>
                                </div>
                                <div>2000 x 600 pixels</div>
                            </div>
                            <div class="col-sm-9 icover">
                                <div class="img" ng-style="item && item.other_cover_ext && {'background-image':'url(<?=$upload_url?>/settings/other_cover.'+item.other_cover_ext+'?tx='+photoLoadTime['other_cover_ext']+')'}"></div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>

                    </div>
                </div>
            </div>

        </div>
    </div>









</div>






