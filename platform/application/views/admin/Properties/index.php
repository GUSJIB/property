<?php
$this->view_data['module'] = 'properties';
$this->view_data['url'] = '/admin/properties';

$this->view_data['title'] = 'Properties';
$this->view_data['header_text'] = 'Properties';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array(
  '/assets/admin/js/properties/lists_controller.js',
  '/assets/admin/js/properties/service.js',
  '/assets/admin/js/properties/directive.js'
);
// $this->view_data['angular_modules'] = array('aaaa','bbbbb');
$this->view_data['header_icon_classes'] = 'fa fa-list';

$this->load->config('config', true);
$statuses = $this->config->item('property_status');
$countries   =   $this->config->item('countries');
$property_types   =   $this->config->item('property_types');
?>


<div class="wrapper" ng-controller="ListsController">

    <div class="panel panel-default">
        <div class="row wrapper">
            <div class="col-sm-3">
                <small class="text-muted inline m-t-sm">Showing {{searchModel.beginRow}} - {{searchModel.endRow}} of {{searchModel.totalRows}} items</small>
            </div>
            <div class="col-sm-9 r">
                <a href="/admin/properties/form" class="btn btn-sm btn-success" style="margin-right:35px;">
                    <i class="fa fa-plus"></i>
                    Add new
                </a>
                <button ng-click="search()" class="btn btn-sm btn-primary" type="button">
                    <i class="fa" ng-class="{'fa-search': !showLoading, 'fa-refresh fa-spin':showLoading}"></i>
                    Search
                </button>
                <button ng-click="reset()" class="btn btn-sm btn-dark" type="button">
                    <i class="fa fa-times"></i>
                    Reset
                </button>
            </div>
        </div>

        <p id="notice"></p>

        <div class="table-responsive">
            <table class="table table-striped b-t b-light" style="border-top: 1px solid #cbd3d4;">
                <thead>
                <tr>
                    <th style="width:50px;" class="c">NO</th>
                    <th>title</th>
                    <th style="width:180px;">Property Type</th>
                    <th style="width:180px;">Country</th>
                    <th style="width:85px;" class="c">Status</th>
                    <th style="width:120px;" class="c">Created</th>
                    <th style="width:100px;" class="c">Actions</th>
                </tr>
                <tr class="filter">
                    <th>&nbsp;</th>
                    <th>
                      <input ng-model="searchModel.title" ng-keyup="$event.keyCode==13 ? search() : null" type="text" maxlength="150" class="form-control filter" />
                    </th>
                    <th>
                      <select ng-model="searchModel.property_type" class="form-control filter">
                            <? foreach ($property_types as $key => $value) { ?>
                                <option value="<?=$key?>"><?=$value?></option>
                            <? } ?>
                      </select>
                    </th>
                    <th>
                      <select ng-model="searchModel.country_code" class="form-control filter">
                            <? foreach ($countries as $key => $value) { ?>
                                <option value="<?=$key?>"><?=$value?></option>
                            <? } ?>
                      </select>
                    </th>
                    <th class="c">
                        <select ng-model="searchModel.status" class="form-control filter">
                            <option value=""></option>
                            <? foreach ($statuses as $key => $value) { ?>
                                <option value="<?=$key?>"><?=$value?></option>    
                            <? } ?>
                        </select>
                    </th>
                    <th class="datepicker">
                        <input 
                            ng-model="searchModel.created_at_from" 
                            datepicker-popup="dd/MM/yyyy" 
                            ng-click="dateFrom = true"
                            is-open="dateFrom"
                            showWeeks="false"
                            close-text="Close" 
                            ng-keyup="$event.keyCode==13 ? search() : null" 
                            type="text" 
                            maxlength="10" 
                            class="form-control filter" 
                            placeholder="dd/mm/yyyy"/>

                        <input 
                            ng-model="searchModel.created_at_to" 
                            datepicker-popup="dd/MM/yyyy" 
                            ng-click="dateTo = true" 
                            min-date="searchModel.created_at_from" 
                            is-open="dateTo"
                            showWeeks="false"
                            close-text="Close" 
                            ng-keyup="$event.keyCode==13 ? search() : null" 
                            type="text" 
                            maxlength="10" 
                            class="form-control filter" 
                            placeholder="dd/mm/yyyy"/>
                    </th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="item in searchModel.rows">
                    <td class="c">{{$index+searchModel.beginRow}}</td>
                    <td>{{item.title}}</td>
                    <td>{{property_types[item.property_type]}}</td>
                    <td>{{countries[item.country_code]}}</td>
                    <td class="c"><span class="label {{statusClass(item.status)}}" style="min-width: 70px; display: block;">{{statusName(item.status)}}</span></td>
                    <td class="c">
                        {{item.created_at | asDate | date:'dd/MM/yyyy'}} <span style="color:#018110;">{{item.created_at | asDate | date:'hh:mm'}}</span>
                    </td>
                    <td class="action c">
                        <a href="properties/photos/{{item.id}}" class="btn btn-default btn-xs" title="Images"><i class="fa fa-image text-success"></i></a>&nbsp;
                        <a href="properties/form/{{item.id}}" class="btn btn-default btn-xs" title="Edit record"><i class="fa fa-pencil text-success"></i></a>&nbsp;
                        <button ng-click="destroy(item)" class="btn btn-default btn-xs" title="Delete record"><i class="fa fa-times text-success"></i></button>
                    </td>
                </tr>
                <tr ng-show="searchModel.rows.length == 0 && !showLoading">
                    <td colspan="7"><span style="color:#aaa;">Found 0 records in system.</span></td>
                </tr>
                <tr ng-show="showLoading">
                    <td colspan="7"><span style="color:#aaa;">Loading ...</span></td>
                </tr>
                </tbody>
            </table>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-sm-5">
                    <small class="text-muted inline m-t-sm m-b-sm">Showing {{searchModel.beginRow}} - {{searchModel.endRow}} of {{searchModel.totalRows}} items</small>
                </div>
                <div class="col-sm-7 text-right text-center-xs" style="margin-top: 4px;">
                    <pagination ng-change="goPage()" boundary-links="true" class="pagination pagination-sm m-t-none m-b-none"
                                total-items="searchModel.totalRows" ng-model="searchModel.currentPage"
                                items-per-page="searchModel.pageSize" max-size="9">
                    </pagination>
                </div>
            </div>
        </footer>
    </div>

</div>
