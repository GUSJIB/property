<?php

$this->view_data['module'] = 'properties';
$this->view_data['url'] = '/admin/properties/form';

$this->view_data['title'] = 'Property Form';
$this->view_data['header_text'] = 'Property Form';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array(
    '/assets/admin/vendors/tinymce/tinymce.min.js',
    '/assets/admin/vendors/angular/ui-tinymce.js',
    '/assets/admin/vendors/angular/google-map-initializer.js',
    '/assets/admin/js/properties/form_controller.js',
    '/assets/admin/js/properties/service.js',
    '/assets/admin/js/properties/directive.js',
    '/assets/admin/vendors/jquery/file-upload/file-upload.min.js'
);
$this->view_data['angular_modules'] = array('ui.tinymce');
$this->view_data['header_icon_classes'] = 'fa fa-list';

$this->load->config('config', true);
$statuses = $this->config->item('property_status');
$countries = $this->config->item('countries');
$property_types = $this->config->item('property_types');
$agents = $this->user_model->all_agent();

if(!isset($id))
{
    $id = 0;
}
?>


<div class="wrapper ng-scope" ng-controller="PropertyFormController" ng-init="init(<?=$id?>)">

    <div class="panel panel-default">
        <div class="panel-heading font-bold">
            <?=$id<1?'Create Property':'Edit Property'?>
        </div>
        <div class="panel-body">



<form name="PropertyForm" ng-submit="submit()" class="clear form-validation" novalidate>
    <div class="form-horizontal">

        <div class="form-group">
            <label class="col-sm-2 control-label">Property Name *</label>
            <div class="col-sm-7">
                <input type="text" name="title" ng-model="item.title" class="form-control" ng-class="" maxlength="250" required />
            </div>
            <div class="col-sm-3">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Status</button>
                    </span>
                    <select ng-model="item.status" class="form-control">
                        <?php
                        foreach($statuses as $key => $value) {
                            echo '<option value="'.$key.'">'.$value.'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <?= line_dashed() ?>

        <div class="form-group">
            <label class="col-sm-2 control-label">Property Type</label>
            <div class="col-sm-4">
                <select ng-model="item.property_type" class="form-control">
                    <?php
                        foreach($property_types as $key => $value) {
                            echo '<option value="'.$key.'">'.$value.'</option>';
                        }
                    ?>
                </select>
            </div>
            <div class="col-sm-6">
                <div class="form-group m-b">
                    <label class="col-sm-2 control-label" type="button">Agent</label>
                    <div class="col-sm-10">
                        <select ng-model="agent" class="form-control">
                            <?php
                            foreach($agents as $value) {
                                echo '<option value="'.$value->id.'">'.$value->first_name.' '.$value->last_name.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <?= line_dashed() ?>

        <div class="form-group">
            <label class="col-sm-2 control-label">Detail</label>
            <div class="col-sm-10">
                <textarea ui-tinymce="" ng-model="item.detail"></textarea>
            </div>
        </div>
        <?= line_dashed() ?>

        <div class="form-group">
            <label class="col-sm-2 control-label">Address</label>

            <div class="col-sm-6">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Address Line 1</button>
                    </span>
                    <input type="text" ng-model="item.address1" class="form-control" />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" style="min-width: 75px;">Country</button>
                    </span>
                    <select ng-model="item.country_code" ng-change="loadProvinces()" class="form-control">
                        <? foreach ($countries as $key => $value) { ?>
                            <option value="<?=$key?>"><?=$value?></option>
                        <? } ?>
                    </select>
                </div>
            </div>

            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-6">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" style="min-width: 110px;">Address Line 2</button>
                    </span>
                    <input type="text" ng-model="item.address2" class="form-control" />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" style="min-width: 75px;">City</button>
                    </span>
                    <select ng-model="item.province_id" ng-change="loadAmphurs()" class="form-control">
                        <option ng-repeat="province in provinces" value="{{ province.id }}">
                            {{ province.name }}
                        </option>
                    </select>
                </div>
            </div>

            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-6">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" style="min-width: 110px;">District</button>
                    </span>
                    <input type="text" ng-model="item.district" class="form-control" />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" style="min-width: 75px;">Area</button>
                    </span>
                    <select ng-model="item.amphur_id" class="form-control">
                        <option ng-repeat="amphur in amphurs" value="{{ amphur.id }}">
                            {{ amphur.name }}
                        </option>
                    </select>
                </div>
            </div>

            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-6">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" style="min-width: 110px;">Postal Code</button>
                    </span>
                    <input type="text" ng-model="item.postal_code" class="form-control" />
                </div>
            </div>
        </div>
        <?= line_dashed() ?>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Location</label>
            <div class="col-sm-10">
                <div id="map_canvas" style="height: 450px; margin-bottom: 10px; background-color: #eee;"></div>
                <div class="row">
                    <div class="col-sm-6">
                        <input id="lat_value" type="text" ng-model="item.lat" placeholder="Latitude" class="form-control" />
                    </div>
                    <div class="col-sm-6">
                        <input id="lon_value" type="text" ng-model="item.lon" placeholder="Longitude" class="form-control" />
                    </div>
                </div>
            </div>
        </div>
        <?= line_dashed() ?>

        <div class="form-group">
            <label class="col-sm-2 control-label">Featured</label>
            <div class="col-sm-10">
                <div class="checkbox">
                    <label class="i-checks">
                        <input type="checkbox" ng-model="item.featured" ng-checked="item.featured=='1'" ng-true-value="'1'" ng-false-value="'0'">
                        <i></i>
                    </label>
                </div>
            </div>
        </div>
        <?= line_dashed() ?>

        <div class="form-group">
            <label class="col-sm-2 control-label">Unit in Room</label>
            <div class="col-sm-3">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Bedroom Unit</button>
                    </span>
                    <select ng-model="item.bedroom_qty" class="form-control">
                        <option value="0">none</option>
                        <option value="1">1 room</option>
                        <option value="2">2 room</option>
                        <option value="3">3 room</option>
                        <option value="4">4 room</option>
                        <option value="5">5 room</option>
                        <option value="6">6 room</option>
                        <option value="7">7 room</option>
                        <option value="8">8 room</option>
                        <option value="9">9 room</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Bathroom Unit</button>
                    </span>
                    <select ng-model="item.bathroom_qty" class="form-control">
                        <option value="0">none</option>
                        <option value="1">1 room</option>
                        <option value="2">2 room</option>
                        <option value="3">3 room</option>
                        <option value="4">4 room</option>
                        <option value="5">5 room</option>
                        <option value="6">6 room</option>
                        <option value="7">7 room</option>
                        <option value="8">8 room</option>
                        <option value="9">9 room</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Property Area</button>
                    </span>
                    <input type="number" ng-model="item.area_sqm" class="form-control ng-dirty ng-valid-number ng-touched" maxlength="7"/>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">sq.m</button>
                    </span>
                </div>
            </div>
        </div>
        <?= line_dashed() ?>

        <div class="form-group">
            <label class="col-sm-2 control-label">Property Info</label>
            <div class="col-sm-3">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Property Floor</button>
                    </span>
                    <input type="number" ng-model="item.building_floor_qty" class="form-control" maxlength="3"/>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Floor in Room</button>
                    </span>
                    <input type="number" ng-model="item.sub_floor_qty_in_room" class="form-control" maxlength="2"/>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Built Year</button>
                    </span>
                    <input type="number" ng-model="item.built_year" class="form-control" maxlength="4"/>
                </div>
            </div>
        </div>
        <?= line_dashed() ?>

        <div class="form-group">
            <label class="col-sm-2 control-label">Cover</label>
            <div class="col-sm-10">
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-info btn-file"><span class="fileupload-new">Select file</span>
                    <span class="fileupload-exists">Change</span>
                    <input id="cover" file-model="cover" type="file" /></span>
                    <span class="fileupload-preview"></span>
                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none;">×</a>
                </div>
            </div>
        </div>
        <?= line_dashed() ?>

        <div class="form-group">
            <label class="col-sm-2 control-label">For sale</label>
            <div class="col-sm-1">
                <div class="checkbox">
                    <label class="i-checks">
                        <input type="checkbox" ng-model="item.for_sale" ng-checked="item.for_sale=='1'" ng-true-value="'1'" ng-false-value="'0'">
                        <i></i>
                    </label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Sale Price</button>
                    </span>
                    <input type="number" ng-model="item.sale_price" class="form-control" maxlength="10"/>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">THB</button>
                    </span>
                </div>
            </div>
        </div>
        <?= line_dashed() ?>


        <div class="form-group">
            <label class="col-sm-2 control-label">For rent</label>
            <div class="col-sm-10">
                <div class="checkbox">
                    <label class="i-checks">
                        <input type="checkbox" ng-model="item.for_rent" ng-checked="item.for_rent=='1'" ng-true-value="'1'" ng-false-value="'0'" />
                        <i></i>
                    </label>
                </div>
            </div>
        </div>


        <div class="form-group">
            <label class="col-sm-2 control-label">Monthly rental</label>
            <div class="col-sm-1">
                <div class="checkbox">
                    <label class="i-checks">
                        <input type="checkbox" ng-model="item.monthly_rent" ng-checked="item.monthly_rent=='1'" ng-true-value="'1'" ng-false-value="'0'" />
                        <i></i>
                    </label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Monthly rate</button>
                    </span>
                    <input type="number" ng-model="item.monthly_rate" class="form-control" maxlength="10"/>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">THB</button>
                    </span>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Min months</button>
                    </span>
                    <select ng-model="item.monthly_min_month" class="form-control">
                        <option value="0">not specified</option>
                        <option value="1">1 month</option>
                        <option value="2">2 months</option>
                        <option value="3">3 months</option>
                        <option value="4">4 months</option>
                        <option value="5">5 months</option>
                        <option value="6">6 months</option>
                        <option value="7">7 months</option>
                        <option value="8">8 months</option>
                        <option value="9">9 months</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Yearly rental</label>
            <div class="col-sm-1">
                <div class="checkbox">
                    <label class="i-checks">
                        <input type="checkbox" ng-model="item.yearly_rent" ng-checked="item.yearly_rent=='1'" ng-true-value="'1'" ng-false-value="'0'" />
                        <i></i>
                    </label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Yearly rate</button>
                    </span>
                    <input type="number" ng-model="item.yearly_rate" class="form-control" maxlength="10"/>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">THB</button>
                    </span>
                </div>
            </div>
        </div>
        <?= line_dashed() ?>

        <div class="form-group">
            <label class="col-sm-2 control-label">View</label>
            <div class="col-sm-10">
                <?= admin_checkbox_field('City', 'item.view_city') ?>
                <?= admin_checkbox_field('Sea', 'item.view_sea') ?>
                <?= admin_checkbox_field('River', 'item.view_river') ?>
                <?= admin_checkbox_field('Skyline', 'item.view_skyline') ?>
            </div>
        </div>
        <?= line_dashed() ?>

        <div class="form-group">
            <label class="col-sm-2 control-label">Amenity</label>
            <div class="col-sm-3">
                <?= admin_checkbox_field('Air condition', 'item.am_air_condition') ?>
                <?= admin_checkbox_field('Balcony', 'item.am_balcony') ?>
                <?= admin_checkbox_field('Outdoor pool', 'item.am_outdoor_pool') ?>
                <?= admin_checkbox_field('Terrace', 'item.am_terrace') ?>
                <?= admin_checkbox_field('Furniture', 'item.am_furniture') ?>
                <?= admin_checkbox_field('Kitken', 'item.am_kitken') ?>
                <?= admin_checkbox_field('Lift', 'item.am_lift') ?>
                <?= admin_checkbox_field('Garden', 'item.am_garden') ?>
                <?= admin_checkbox_field('Internet Lan', 'item.am_internet_lan') ?>
            </div>
            <div class="col-sm-3">
                <?= admin_checkbox_field('Parking', 'item.am_parking') ?>
                <?= admin_checkbox_field('Garage', 'item.am_garage') ?>
                <?= admin_checkbox_field('Fitness', 'item.am_fitness') ?>
                <?= admin_checkbox_field('Spa', 'item.am_spa') ?>
                <?= admin_checkbox_field('Sauna room', 'item.am_sauna_room') ?>
                <?= admin_checkbox_field('Dress room', 'item.am_dress_room') ?>
                <?= admin_checkbox_field('Pet friendly', 'item.am_pet_friendly') ?>
                <?= admin_checkbox_field('Doorman', 'item.am_doorman') ?>
                <?= admin_checkbox_field('Internet WIFI', 'item.am_internet_wifi') ?>
            </div>
            <div class="col-sm-3">
                <?= admin_checkbox_field('Cable TV', 'item.am_cable_tv') ?>
                <?= admin_checkbox_field('Computer', 'item.am_computer') ?>
                <?= admin_checkbox_field('Grill', 'item.am_grill') ?>
                <?= admin_checkbox_field('Diskwasher', 'item.am_diskwasher') ?>
                <?= admin_checkbox_field('Washing machine', 'item.am_washing_machine') ?>
                <?= admin_checkbox_field('Refrigerator', 'item.am_refrigerator') ?>
                <?= admin_checkbox_field('Smoking', 'item.am_smoking') ?>
                <?= admin_checkbox_field('Telephone', 'item.am_telephone') ?>
            </div>
            <div class="col-sm-1"></div>
        </div>
        <?= line_dashed() ?>

        <div class="form-group">
            <label class="col-sm-2 control-label">Meta</label>
            <div class="col-sm-10">
                <input type="text" ng-model="item.meta_title" placeholder="meta title" class="form-control m-b">
                <input type="text" ng-model="item.meta_keywords" placeholder="meta keywords" class="form-control m-b">
                <input type="text" ng-model="item.meta_description" placeholder="meta description" class="form-control">
            </div>
        </div>
        <?= line_dashed() ?>

        <div class="form-group">
            <label class="col-sm-2 control-label">Property's contact</label>
            <div class="col-sm-5">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" style="min-width: 100px;">Name</button>
                    </span>
                    <input type="text" ng-model="item.contact_name" class="form-control" />
                </div>
            </div>
            <div class="col-sm-5">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" style="min-width: 100px;">Position</button>
                    </span>
                    <input type="text" ng-model="item.contact_position" class="form-control" />
                </div>
            </div>

            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-5">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" style="min-width: 100px;">Email</button>
                    </span>
                    <input type="text" ng-model="item.contact_email" class="form-control" />
                </div>
            </div>
            <div class="col-sm-5">
                <div class="input-group m-b">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" style="min-width: 100px;">Phone</button>
                    </span>
                    <input type="text" ng-model="item.contact_phone" class="form-control" />
                </div>
            </div>
        </div>
        <?= line_dashed() ?>
        
        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                <button type="submit" ng-disabled="PropertyForm.$invalid" class="btn btn-primary">Save changes</button>
                <a ng-really-message="Confirm to cancel form?" ng-really-click="cancel()" class="btn btn-default">Cancel</a>
            </div>
        </div>

    </div>
</form>



        </div>
    </div>

</div>