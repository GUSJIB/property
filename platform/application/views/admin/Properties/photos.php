<?php
$this->view_data['module'] = 'properties';
$this->view_data['url'] = '/admin/properties';

$this->view_data['title'] = 'Property Galleries';
$this->view_data['header_text'] = 'Property Galleries';
$this->view_data['stylesheet'] = array(
    '/assets/admin/vendors/jquery/blueimp-gallery/blueimp-gallery.min.css'
);
$this->view_data['footer_js'] = array(
    '/assets/admin/vendors/jquery/blueimp-gallery/jquery.blueimp-gallery.min.js',
    '/assets/admin/vendors/jquery/blueimp-gallery/bootstrap-image-gallery.js',
    '/assets/admin/vendors/modules/angular-file-upload/angular-file-upload.min.js',
    '/assets/admin/js/properties/photos_controller.js',
    '/assets/admin/js/properties/service.js',
    '/assets/admin/js/properties/directive.js'
);
$this->view_data['angular_modules'] = array('angularFileUpload');
$this->view_data['header_icon_classes'] = 'fa fa-list';

?>


<div ui-view="" class="fade-in ng-scope" ng-controller="PhotosController">
    <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope" nv-file-drop="" uploader="uploader" filters="queueLimit, customFilter" ng-init="init(<?=$id?>)">
        <div class="col b-r bg-auto">
            <div class="wrapper-md dker b-b">
              <h3 class="m-n font-thin">Select files</h3>
            </div>
            <div class="wrapper-md">      
              <div ng-show="uploader.isHTML5" class="m-b-md" aria-hidden="false">
                  <!-- 3. nv-file-over uploader="link" over-class="className" -->
                  <div onclick="$('input[type=file]').click()" class="b-a b-2x b-dashed wrapper-lg bg-white text-center m-b" nv-file-over="" over-class="b-info" uploader="uploader">
                      Base drop zone or click for choose files
                  </div>
              </div>

              <!-- Example: nv-file-select="" uploader="{Object}" options="{Object}" filters="{String}" -->
              <input class="hide" type="file" nv-file-select="" uploader="uploader" multiple="">

              <div>
                <p>progress:</p>
                <div class="progress bg-light dker" style="">
                    <div class="progress-bar progress-bar-striped bg-info" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }" style="width: 0%;"></div>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
        <div class="col b-r bg-auto">
            <div class="wrapper-md dker b-b">
              <h3 class="m-n font-thin">Photos</h3>
            </div>
            <div class="wrapper-md">
                <div id="wrap-photos">
                    <div class="row">
                        <div ng-repeat="photo in photos" class="col-sm-3">
                            <div class="fixed-ar fixed-ar-16-9" ng-style="{'background-image': 'url({{ photo.photo_url }})'}">
                                <button ng-click="destroy(photo)" class="btn remove btn-sm btn-icon btn-default"><i class="glyphicon glyphicon-trash"></i></button>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
