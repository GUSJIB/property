<?php
$this->view_data['module'] = 'auctions';
$this->view_data['url'] = '/admin/auctions';

$this->view_data['title'] = 'Auction Calendar';
$this->view_data['header_text'] = 'Auction Calendar';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array(
  '/assets/admin/js/auctions/controller.js',
  '/assets/admin/js/auctions/service.js'
);
//$this->view_data['angular_modules'] = array('aaaa','bbbbb');
$this->view_data['header_icon_classes'] = 'fa  fa-legal';

?>


<div class="wrapper" ng-controller="AuctionsController">

    <div class="panel panel-default">
        <div class="row wrapper">
            <div class="col-sm-3">
                <small class="text-muted inline m-t-sm">Showing {{searchModel.beginRow}} - {{searchModel.endRow}} of {{searchModel.totalRows}} items</small>
            </div>
            <div class="col-sm-9 r">
                <button ng-click="openPopup()" class="btn btn-sm btn-success" type="button" style="margin-right:35px;">
                    <i class="fa fa-plus"></i>
                    Add new
                </button>
                <button ng-click="search()" class="btn btn-sm btn-primary" type="button">
                    <i class="fa" ng-class="{'fa-search': !showLoading, 'fa-refresh fa-spin':showLoading}"></i>
                    Search
                </button>
                <button ng-click="reset()" class="btn btn-sm btn-dark" type="button">
                    <i class="fa fa-times"></i>
                    Reset
                </button>
            </div>
        </div>

        <p id="notice"></p>

        <div class="table-responsive">
            <table class="table table-striped b-t b-light" style="border-top: 1px solid #cbd3d4;">
                <thead>
                <tr>
                    <th style="width:50px;" class="c">NO</th>
                    <th>Event Name</th>
                    <th style="width:300px;">Event Place</th>
                    <th style="width:90px;">Event Date</th>
                    <th style="width:60px;">Published</th>
                    <th style="width:80px;" class="c">Actions</th>
                </tr>
                <tr class="filter">
                    <th>&nbsp;</th>
                    <th><input ng-model="searchModel.title" ng-keyup="$event.keyCode==13 ? search() : null" type="text" maxlength="200" class="form-control filter" /></th>
                    <th><input ng-model="searchModel.event_place" ng-keyup="$event.keyCode==13 ? search() : null" type="text" maxlength="150" class="form-control filter" /></th>
                    <th class="datepicker">
                        <input 
                            ng-model="searchModel.event_date_from"
                            datepicker-popup="dd/MM/yyyy" 
                            ng-click="dateFrom = true"
                            is-open="dateFrom"
                            showWeeks="false"
                            close-text="Close" 
                            ng-keyup="$event.keyCode==13 ? search() : null" 
                            type="text" 
                            maxlength="10" 
                            class="form-control filter" 
                            placeholder="dd/mm/yyyy"/>

                        <input 
                            ng-model="searchModel.event_date_to"
                            datepicker-popup="dd/MM/yyyy" 
                            ng-click="dateTo = true" 
                            min-date="searchModel.event_date_from"
                            is-open="dateTo"
                            showWeeks="false"
                            close-text="Close" 
                            ng-keyup="$event.keyCode==13 ? search() : null" 
                            type="text" 
                            maxlength="10" 
                            class="form-control filter" 
                            placeholder="dd/mm/yyyy"/>
                    </th>
                    <th>
                        <select ng-model="searchModel.published" class="form-control filter">
                            <option value=""></option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="item in searchModel.rows">
                    <td class="c">{{$index + searchModel.beginRow}}</td>
                    <td>{{item.title}}</td>
                    <td>{{item.event_place}}</td>
                    <td class="c">{{item.event_date | asDate | date:'dd/MM/yyyy'}}</td>
                    <td class="c">{{item.published==1?'Y':'N'}}</td>
                    <td class="action c">
                        <button ng-click="openPopup(item)" class="btn btn-default btn-xs" title="Edit record"><i class="fa fa-pencil text-success"></i></button>&nbsp;
                        <button ng-click="destroy(item)" class="btn btn-default btn-xs" title="Delete record"><i class="fa fa-times text-success"></i></button>
                    </td>
                </tr>
                <tr ng-show="searchModel.rows.length == 0 && !showLoading">
                    <td colspan="6"><span style="color:#aaa;">Found 0 records in system.</span></td>
                </tr>
                <tr ng-show="showLoading">
                    <td colspan="6"><span style="color:#aaa;">Loading ...</span></td>
                </tr>
                </tbody>
            </table>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-sm-5">
                    <small class="text-muted inline m-t-sm m-b-sm">Showing {{searchModel.beginRow}} - {{searchModel.endRow}} of {{searchModel.totalRows}} items</small>
                </div>
                <div class="col-sm-7 text-right text-center-xs" style="margin-top: 4px;">
                    <pagination ng-change="goPage()" boundary-links="true" class="pagination pagination-sm m-t-none m-b-none"
                                total-items="searchModel.totalRows" ng-model="searchModel.currentPage"
                                items-per-page="searchModel.pageSize" max-size="9">
                    </pagination>
                </div>
            </div>
        </footer>
    </div>

</div>


<script type="text/ng-template" id="PopupForm.html">
    <div class="modal-header">
        <h3 class="modal-title">{{item.id > 0 ? 'Edit auction event' : 'Create new auction event'}}</h3>
    </div>
    <div class="modal-body">
        <progressbar ng-show="pageLoading" class="loadingIcon progress-striped active m-b-sm" value="100" type="success">processing...</progressbar>
        <form name="PopupForm" ng-hide="pageLoading" class="clear" novalidate>
            <div class="row">
                <div class="col-sm-12 form-group" ng-class="{'has-error' : PopupForm.title.$invalid && !PopupForm.title.$pristine}">
                    <label class="control-label">Event title : *</label>
                    <span ng-show="PopupForm.title.$invalid && !PopupForm.title.$pristine" class="message">&rarr; required</span>
                    <input ng-model="item.title" class="form-control" name="subject" type="text" maxlength="100" required/>
                </div>
                <div class="col-sm-6">
                    <div class="form-group" ng-class="{'has-error' : PopupForm.event_place.$invalid && !PopupForm.event_place.$pristine}">
                        <label class="control-label">Event place : *</label>
                        <span ng-show="PopupForm.event_place.$invalid && !PopupForm.event_place.$pristine" class="message">&rarr; required</span>
                        <input ng-model="item.event_place" name="event_place" class="form-control" type="text" maxlength="100" required />
                    </div>
                    <div class="form-group" ng-class="{'has-error' : PopupForm.event_address.$invalid && !PopupForm.event_address.$pristine}">
                        <label class="control-label">Event place address : *</label>
                        <span ng-show="PopupForm.event_address.$invalid && !PopupForm.event_address.$pristine" class="message">&rarr; required</span>
                        <input ng-model="item.event_address" name="event_address" class="form-control" type="text" maxlength="200" required/>
                    </div>
                    <div class="form-group" ng-class="{'has-error' : PopupForm.published.$invalid && !PopupForm.published.$pristine}">
                        <label class="control-label">Publishing : *</label>
                        <span ng-show="PopupForm.published.$invalid && !PopupForm.published.$pristine" class="message">&rarr; required</span>
                        <select ng-model="item.published" name="status" class="form-control" required>
                            <option value="1">publish</option>
                            <option value="0">Hidden</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group" ng-class="{'has-error' : PopupForm.event_date.$invalid && !PopupForm.event_date.$pristine}">
                        <label class="control-label">Event date : *</label>
                        <span ng-show="PopupForm.event_date.$invalid && !PopupForm.event_date.$pristine" class="message">&rarr; required</span>
                        <input ng-model="item.event_date" datepicker-popup="yyyy-MM-dd" ng-click="birthday = true" is-open="birthday" name="birthday" class="form-control" type="text" required />
                    </div>
                    <div class="form-group" ng-class="{'has-error' : PopupForm.start_time.$invalid && !PopupForm.start_time.$pristine}">
                        <label class="control-label">Start time : *</label>
                        <span ng-show="PopupForm.start_time.$invalid && !PopupForm.start_time.$pristine" class="message">&rarr; required</span>
                        <input ng-model="item.start_time" name="start_time" class="form-control" type="text" maxlength="45" required/>
                    </div>
                    <div class="form-group" ng-class="{'has-error' : PopupForm.end_time.$invalid && !PopupForm.end_time.$pristine}">
                        <label class="control-label">Close time : *</label>
                        <span ng-show="PopupForm.end_time.$invalid && !PopupForm.end_time.$pristine" class="message">&rarr; required</span>
                        <input ng-model="item.end_time" name="end_time" class="form-control" type="text" maxlength="45" required/>
                    </div>
                </div>
                <div class="col-sm-12 form-group" ng-class="{ 'has-error' : PopupForm.detail.$invalid && !PopupForm.detail.$pristine }">
                    <label class="control-label">Detail : *</label>
                    <span ng-show="PopupForm.detail.$invalid && !PopupForm.detail.$pristine" class="message">&rarr; required</span>
                    <textarea ng-model="item.detail" name="detail" rows="7" class="form-control" required></textarea>
                </div>
            </div>
        </form>
    </div>
    <div ng-hide="pageLoading" class="modal-footer">
        <button class="btn btn-primary" ng-click="submit()" ng-disabled="PopupForm.$invalid"><i ng-show="dataSubmitting" ng-class="{'fa-spin':dataSubmitting}" class="fa fa-refresh"></i> Submit</button>
        <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
    </div>
</script>
