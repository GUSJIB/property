<?php
$this->view_data['module'] = 'contacts';
$this->view_data['url'] = '/admin/contacts';

$this->view_data['title'] = 'Contact us';
$this->view_data['header_text'] = 'Contact us';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array(
  '/assets/admin/js/contacts/controller.js',
  '/assets/admin/js/contacts/service.js'
);
//$this->view_data['angular_modules'] = array('aaaa','bbbbb');
$this->view_data['header_icon_classes'] = 'fa fa-send';

$this->load->config('config', true);
$statuses    = $this->config->item('contact_status');
?>


<div class="wrapper" ng-controller="ContactsController">

    <div class="panel panel-default">
        <div class="row wrapper">
            <div class="col-sm-3">
                <small class="text-muted inline m-t-sm">Showing {{searchModel.beginRow}} - {{searchModel.endRow}} of {{searchModel.totalRows}} items</small>
            </div>
            <div class="col-sm-9 r">
                <button ng-click="openPopup()" class="btn btn-sm btn-success" type="button" style="margin-right:35px;">
                    <i class="fa fa-plus"></i>
                    Add new
                </button>
                <button ng-click="search()" class="btn btn-sm btn-primary" type="button">
                    <i class="fa" ng-class="{'fa-search': !showLoading, 'fa-refresh fa-spin':showLoading}"></i>
                    Search
                </button>
                <button ng-click="reset()" class="btn btn-sm btn-dark" type="button">
                    <i class="fa fa-times"></i>
                    Reset
                </button>
            </div>
        </div>

        <p id="notice"></p>

        <div class="table-responsive">
            <table class="table table-striped b-t b-light" style="border-top: 1px solid #cbd3d4;">
                <thead>
                <tr>
                    <th style="width:50px;" class="c">NO</th>
                    <th>Subject</th>
                    <th style="width:180px;">Name</th>
                    <th style="width:120px;">Phone</th>
                    <th style="width:180px;">Email</th>
                    <th style="width:85px;" class="c">status</th>
                    <th style="width:120px;" class="c">Created On</th>
                    <th style="width:80px;" class="c">Actions</th>
                </tr>
                <tr class="filter">
                    <th>&nbsp;</th>
                    <th><input ng-model="searchModel.subject" ng-keyup="$event.keyCode==13 ? search() : null" type="text" maxlength="200" class="form-control filter" /></th>
                    <th><input ng-model="searchModel.name" ng-keyup="$event.keyCode==13 ? search() : null" type="text" maxlength="150" class="form-control filter" /></th>
                    <th><input ng-model="searchModel.phone" ng-keyup="$event.keyCode==13 ? search() : null" type="text" maxlength="100" class="form-control filter" /></th>
                    <th><input ng-model="searchModel.email" ng-keyup="$event.keyCode==13 ? search() : null" type="text" maxlength="100" class="form-control filter" /></th>
                    <th>
                        <select ng-model="searchModel.status" class="form-control filter">
                            <option value=""></option>
                            <option value="1">New</option>
                            <option value="2">On Process</option>
                            <option value="3">Responded</option>
                            <option value="4">Closed</option>
                        </select>
                    </th>
                    <th class="datepicker">
                        <input 
                            ng-model="searchModel.created_at_from"
                            datepicker-popup="dd/MM/yyyy" 
                            ng-click="dateFrom = true"
                            is-open="dateFrom"
                            showWeeks="false"
                            close-text="Close" 
                            ng-keyup="$event.keyCode==13 ? search() : null" 
                            type="text" 
                            maxlength="10" 
                            class="form-control filter" 
                            placeholder="dd/mm/yyyy"/>

                        <input 
                            ng-model="searchModel.created_at_to"
                            datepicker-popup="dd/MM/yyyy" 
                            ng-click="dateTo = true" 
                            min-date="searchModel.created_at_from"
                            is-open="dateTo"
                            showWeeks="false"
                            close-text="Close" 
                            ng-keyup="$event.keyCode==13 ? search() : null" 
                            type="text" 
                            maxlength="10" 
                            class="form-control filter" 
                            placeholder="dd/mm/yyyy"/>
                    </th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="item in searchModel.rows">
                    <td class="c">{{$index + searchModel.beginRow}}</td>
                    <td>{{item.subject}}</td>
                    <td>{{item.name}}</td>
                    <td>{{item.phone}}</td>
                    <td>{{item.email}}</td>
                    <td class="c">
                        <b class="label {{status_label_class[item.status]}}" style="min-width:60px; display: block;">{{status[item.status]}}</b>
                    </td>
                    <td class="c">{{item.created_at | asDate | date:'dd/MM/yyyy'}} <span style="color:#018110;">{{item.created_at | asDate | date:'hh:mm'}}</span></td>
                    <td class="action c">
                        <button ng-click="openPopup(item)" class="btn btn-default btn-xs" title="Edit record"><i class="fa fa-pencil text-success"></i></button>&nbsp;
                        <button ng-click="destroy(item)" class="btn btn-default btn-xs" title="Delete record"><i class="fa fa-times text-success"></i></button>
                    </td>
                </tr>
                <tr ng-show="searchModel.rows.length == 0 && !showLoading">
                    <td colspan="8"><span style="color:#aaa;">Found 0 records in system.</span></td>
                </tr>
                <tr ng-show="showLoading">
                    <td colspan="8"><span style="color:#aaa;">Loading ...</span></td>
                </tr>
                </tbody>
            </table>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-sm-5">
                    <small class="text-muted inline m-t-sm m-b-sm">Showing {{searchModel.beginRow}} - {{searchModel.endRow}} of {{searchModel.totalRows}} items</small>
                </div>
                <div class="col-sm-7 text-right text-center-xs" style="margin-top: 4px;">
                    <pagination ng-change="goPage()" boundary-links="true" class="pagination pagination-sm m-t-none m-b-none"
                                total-items="searchModel.totalRows" ng-model="searchModel.currentPage"
                                items-per-page="searchModel.pageSize" max-size="9">
                    </pagination>
                </div>
            </div>
        </footer>
    </div>

</div>


<script type="text/ng-template" id="PopupForm.html">
    <div class="modal-header">
        <h3 class="modal-title">{{item.id > 0 ? 'Edit message' : 'Create new message'}}</h3>
    </div>
    <div class="modal-body">
        <progressbar ng-show="pageLoading" class="loadingIcon progress-striped active m-b-sm" value="100" type="success">processing...</progressbar>
        <form name="PopupForm" ng-hide="pageLoading" class="clear" novalidate>
            <div class="row">
                <div class="col-sm-12 form-group" ng-class="{'has-error' : PopupForm.subject.$invalid && !PopupForm.subject.$pristine}">
                    <label class="control-label">Subject : *</label>
                    <span ng-show="PopupForm.subject.$invalid && !PopupForm.subject.$pristine" class="message">&rarr; required</span>
                    <input ng-model="item.subject" class="form-control" name="subject" type="text" maxlength="280" required/>
                </div>
                <div class="col-sm-6">
                    <div class="form-group" ng-class="{'has-error' : PopupForm.name.$invalid && !PopupForm.name.$pristine}">
                        <label class="control-label">Name : *</label>
                        <span ng-show="PopupForm.name.$invalid && !PopupForm.name.$pristine" class="message">&rarr; required</span>
                        <input ng-model="item.name" name="name" class="form-control" type="text" maxlength="140" required />
                    </div>
                    <div class="form-group" ng-class="{'has-error' : PopupForm.status.$invalid && !PopupForm.status.$pristine}">
                        <label class="control-label">Status : *</label>
                        <span ng-show="PopupForm.status.$invalid && !PopupForm.status.$pristine" class="message">&rarr; required</span>
                        <select ng-model="item.status" name="status" class="form-control" required>
                            <?php foreach($statuses as $code => $name) {?>
                                <option value="<?=$code?>"><?=$name?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group" ng-class="{'has-error' : PopupForm.email.$invalid && !PopupForm.email.$pristine}">
                        <label class="control-label">Email : *</label>
                        <span ng-show="PopupForm.email.$invalid && !PopupForm.email.$pristine" class="message">&rarr; invalid email</span>
                        <input ng-model="item.email" name="email" class="form-control" type="email" maxlength="60" required/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Phone : </label>
                        <input ng-model="item.phone" class="form-control" maxlength="60" type="text" />
                    </div>
                </div>
                <div class="col-sm-12 form-group" ng-class="{ 'has-error' : PopupForm.detail.$invalid && !PopupForm.detail.$pristine }">
                    <label class="control-label">Detail : *</label>
                    <span ng-show="PopupForm.detail.$invalid && !PopupForm.detail.$pristine" class="message">&rarr; required</span>
                    <textarea ng-model="item.detail" name="detail" rows="7" class="form-control" required></textarea>
                </div>
                <div class="col-sm-12 form-group">
                    <label class="control-label">Remark : </label>
                    <textarea rows="4" class="form-control" ng-model="item.remark"></textarea>
                </div>
            </div>
        </form>
    </div>
    <div ng-hide="pageLoading" class="modal-footer">
        <button class="btn btn-primary" ng-click="submit()" ng-disabled="PopupForm.$invalid"><i ng-show="dataSubmitting" ng-class="{'fa-spin':dataSubmitting}" class="fa fa-refresh"></i> Submit</button>
        <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
    </div>
</script>
