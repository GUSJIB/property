<?php

$this->view_data['module'] = 'contents';
$this->view_data['url'] = '/admin/contents/'.$page_target;

$title = $page_target==='contact_us'?'Contact Us':($page_target==='useful_link'?'Useful Link':($page_target==='about_us'?'About Us':'Unknown page'));
$this->view_data['title'] = $title;
$this->view_data['header_text'] = $title;
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array(
    '/assets/admin/vendors/tinymce/tinymce.min.js',
    '/assets/admin/vendors/angular/ui-tinymce.js',
    '/assets/admin/vendors/angular/google-map-initializer.js',
    '/assets/admin/js/contents/controller.js',
    '/assets/admin/js/contents/service.js'
);
$this->view_data['angular_modules'] = array('ui.tinymce');
$this->view_data['header_icon_classes'] = 'glyphicon glyphicon-list-alt';

?>


<div class="wrapper ng-scope" ng-controller="ContentController">

    <div class="panel panel-default">
        <div class="panel-body">



            <form name="ContentForm" ng-submit="submit()" class="clear" novalidate>
                <input type="hidden" id="page_target" value="<?=$page_target?>" />
                <div class="form-horizontal">

                    <div class="form-group">
                        <label class="col-sm-1 control-label">Detail</label>
                        <div class="col-sm-11">
                            <textarea ui-tinymce ng-model="item.content" upload-folder="<?=$page_target?>"></textarea>
                        </div>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>

                    <?php if($page_target=='contact_us'){?>
                        <div class="form-group">
                            <label class="col-sm-1 control-label">Location</label>
                            <div class="col-sm-11">
                                <div id="map_canvas" style="height: 450px; margin-bottom: 10px; background-color: #eee;"></div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input id="lat_value" type="text" ng-model="item.lat" placeholder="Latitude" class="form-control" />
                                    </div>
                                    <div class="col-sm-6">
                                        <input id="lon_value" type="text" ng-model="item.lon" placeholder="Longitude" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="line line-dashed b-b line-lg pull-in"></div>
                    <?php } ?>

                    <div class="form-group">
                        <label class="col-sm-1 control-label">Meta</label>
                        <div class="col-sm-11">
                            <input type="text" ng-model="item.meta_title" placeholder="meta title" class="form-control m-b">
                            <input type="text" ng-model="item.meta_keywords" placeholder="meta keywords" class="form-control m-b">
                            <input type="text" ng-model="item.meta_description" placeholder="meta description" class="form-control">
                        </div>
                    </div>
                    <div class="line line-dashed b-b line-lg pull-in"></div>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-1">
                            <button type="submit" ng-disabled="ContentForm.$invalid" class="btn btn-primary">Save changes</button>
                            <a ng-click="cancel()" class="btn btn-default">Cancel</a>
                        </div>
                    </div>

                </div>
            </form>



        </div>
    </div>

</div>