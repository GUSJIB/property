<?php
$this->view_data['module'] = 'geo_graphic';
$this->view_data['url'] = '/admin/amphurs';

$this->view_data['title'] = 'City area listing';
$this->view_data['header_text'] = 'City area listing';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array(
    '/assets/admin/vendors/angular/google-map-initializer.js',
    '/assets/admin/js/amphurs/controller.js',
    '/assets/admin/js/amphurs/service.js'
);
$this->view_data['header_icon_classes'] = 'fa fa-globe';

$this->load->config('config', true);
?>


<div class="wrapper" ng-controller="AmphursController">

    <div class="panel panel-default">
        <div class="row wrapper">
            <div class="col-sm-3">
                <small class="text-muted inline m-t-sm">Showing {{searchModel.beginRow}} - {{searchModel.endRow}} of {{searchModel.totalRows}} items</small>
            </div>
            <div class="col-sm-9 r">
                <button ng-click="openPopup()" class="btn btn-sm btn-success" type="button" style="margin-right:35px;">
                    <i class="fa fa-plus"></i>
                    Add new
                </button>
                <button ng-click="search()" class="btn btn-sm btn-primary" type="button">
                    <i class="fa" ng-class="{'fa-search': !showLoading, 'fa-refresh fa-spin':showLoading}"></i>
                    Search
                </button>
                <button ng-click="reset()" class="btn btn-sm btn-dark" type="button">
                    <i class="fa fa-times"></i>
                    Reset
                </button>
            </div>
        </div>

        <p id="notice"></p>

        <div class="table-responsive">
            <table class="table table-striped b-t b-light" style="border-top: 1px solid #cbd3d4;">
                <thead>
                <tr>
                    <th style="width:50px;" class="c">NO</th>
                    <th>Name</th>
                    <th style="width:250px;">City</th>
                    <th style="width:150px;">Latitude</th>
                    <th style="width:150px;">Longitude</th>
                    <th style="width:85px;" class="c">Position</th>
                    <th style="width:85px;" class="c">Properties</th>
                    <th style="width:80px;" class="c">Actions</th>
                </tr>
                <tr class="filter">
                    <th>&nbsp;</th>
                    <th><input ng-model="searchModel.name" ng-keyup="$event.keyCode==13 ? search() : null" type="text" maxlength="150" class="form-control filter" /></th>
                    <th>
                        <select  ng-model="searchModel.province_id" class="form-control filter">
                            <option value="">&nbsp;</option>
                            <option ng-repeat="province in provincesList" value="{{province.id}}">{{province.name}}</option>
                        </select>
                    </th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th><input ng-model="searchModel.position" ng-keyup="$event.keyCode==13 ? search() : null" type="text" maxlength="100" class="form-control filter" /></th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="item in searchModel.rows">
                    <td class="c">{{$index + searchModel.beginRow}}</td>
                    <td>{{item.name}}</td>
                    <td>{{item.province_name}}</td>
                    <td>{{item.lat}}</td>
                    <td>{{item.lon}}</td>
                    <td class="c">{{item.position}}</td>
                    <td class="c">{{item.post_count}}</td>
                    <td class="action c">
                        <button ng-click="openPopup(item)" class="btn btn-default btn-xs" title="Edit record"><i class="fa fa-pencil text-success"></i></button>&nbsp;
                        <button ng-click="destroy(item)" class="btn btn-default btn-xs" title="Delete record"><i class="fa fa-times text-success"></i></button>
                    </td>
                </tr>
                <tr ng-show="searchModel.rows.length == 0 && !showLoading">
                    <td colspan="8"><span style="color:#aaa;">Found 0 records in system.</span></td>
                </tr>
                <tr ng-show="showLoading">
                    <td colspan="8"><span style="color:#aaa;">Loading ...</span></td>
                </tr>
                </tbody>
            </table>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-sm-5">
                    <small class="text-muted inline m-t-sm m-b-sm">Showing {{searchModel.beginRow}} - {{searchModel.endRow}} of {{searchModel.totalRows}} items</small>
                </div>
                <div class="col-sm-7 text-right text-center-xs" style="margin-top: 4px;">
                    <pagination ng-change="goPage()" boundary-links="true" class="pagination pagination-sm m-t-none m-b-none"
                                total-items="searchModel.totalRows" ng-model="searchModel.currentPage"
                                items-per-page="searchModel.pageSize" max-size="9">
                    </pagination>
                </div>
            </div>
        </footer>
    </div>

</div>


<script type="text/ng-template" id="PopupForm.html">
    <div class="modal-header">
        <h3 class="modal-title">{{item.id > 0 ? 'Edit City Area' : 'Create new City Area'}}</h3>
    </div>
    <div class="modal-body">
        <progressbar ng-show="pageLoading" class="loadingIcon progress-striped active m-b-sm" value="100" type="success">processing...</progressbar>
        <form name="PopupForm" ng-hide="pageLoading" class="clear" novalidate>
            <div class="row">
                <div class="col-sm-12 form-group" ng-class="{'has-error' : PopupForm.name.$invalid && !PopupForm.name.$pristine}">
                    <label class="control-label">Name : *</label>
                    <span ng-show="PopupForm.name.$invalid && !PopupForm.name.$pristine" class="message">&rarr; required</span>
                    <input ng-model="item.name" class="form-control" name="name" type="text" maxlength="150" required/>
                </div>
                <div class="col-sm-6">
                    <div class="form-group" ng-class="{'has-error' : PopupForm.position.$invalid && !PopupForm.position.$pristine}">
                        <label class="control-label">Position : *</label>
                        <span ng-show="PopupForm.position.$invalid && !PopupForm.position.$pristine" class="message">&rarr; required</span>
                        <input ng-model="item.position" name="position" class="form-control" type="text" maxlength="5" required/>
                    </div>
                    <div class="form-group" ng-class="{'has-error' : PopupForm.lat.$invalid && !PopupForm.lat.$pristine}">
                        <label class="control-label">Latitude : *</label>
                        <span ng-show="PopupForm.lat.$invalid && !PopupForm.lat.$pristine" class="message">&rarr; required</span>
                        <input ng-model="item.lat" name="lat" class="form-control" type="text" maxlength="20"/>
                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label">City : *</label>
                        <span ng-show="PopupForm.province_id.$invalid && !PopupForm.province_id.$pristine" class="message">&rarr; required</span>
                        <select  ng-model="item.province_id"  name="province_id" class="form-control" required>
                            <option label="Please Select"></option>
                            <option ng-repeat="province in provincesList" value="{{province.id}}">{{province.name}}</option>
                        </select>
                    </div>
                    <div class="form-group" ng-class="{'has-error' : PopupForm.lon.$invalid && !PopupForm.lon.$pristine}">
                        <label class="control-label">Longitude : *</label>
                        <span ng-show="PopupForm.lon.$invalid && !PopupForm.lon.$pristine" class="message">&rarr; required</span>
                        <input ng-model="item.lon" name="lon" class="form-control" type="text" maxlength="20"/>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div id="amphursPopupmap" style="background-color:#ccc; height: 350px;">
                </div>

            </div>
        </form>
    </div>
    <div ng-hide="pageLoading" class="modal-footer">
        <button class="btn btn-primary" ng-click="submit()" ng-disabled="PopupForm.$invalid"><i ng-show="dataSubmitting" ng-class="{'fa-spin':dataSubmitting}" class="fa fa-refresh"></i> Submit</button>
        <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
    </div>
</script>
