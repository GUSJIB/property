<!DOCTYPE html>
<html ng-app="app">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Upload Photos</title>

    <link rel="icon" href="/assets/admin/images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/assets/admin/images/favicon.ico" type="image/x-icon">
    <meta name="description" content="Teedin Administrator" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link rel="stylesheet" href="/assets/admin/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/assets/admin/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="/assets/admin/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="/assets/admin/css/simple-line-icons.css" type="text/css" />
    <link rel="stylesheet" href="/assets/admin/css/font.css" type="text/css" />
    <link rel="stylesheet" href="/assets/admin/css/app.css" type="text/css" />

    <script src="/assets/admin/vendors/jquery/jquery.min.js"></script>
    <script src="/assets/admin/vendors/angular/angular.js"></script>
    <script src="/assets/admin/vendors/modules/angular-file-upload/angular-file-upload.min.js"></script>
<!---->
<!--    <script type="text/javascript" src="js/custom.js"></script>-->
<!--    <script type="text/javascript" src="js/main.js"></script>-->


    <script src="/assets/admin/js/uploads/controller.js"></script>
    <script src="/assets/admin/js/uploads/service.js"></script>
    <script src="/assets/admin/js/properties/directive.js"></script>
</head>
<body ng-controller="UploadController" ng-cloak>
    <input id="folder_path" type="hidden" value="<?=$folder_path?>"/>



    <div ui-view="" class="fade-in ng-scope">
        <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope" nv-file-drop="" uploader="uploader" filters="queueLimit, customFilter">
            <div class="col b-r bg-auto">
                <div class="wrapper-md"  style="padding-bottom: 0;">
                    <div class="m-b-md" aria-hidden="false">
                        <!-- 3. nv-file-over uploader="link" over-class="className" -->
                        <div onclick="$('input[type=file]').click()" class="b-a b-2x b-dashed wrapper-lg bg-white text-center m-b" nv-file-over="" over-class="b-info" uploader="uploader">
                            Base drop zone or click for choose photos
                        </div>
                    </div>
                    <input class="hide" type="file" nv-file-select="" uploader="uploader" multiple>

                    <div ng-show="uploading" class="progress progress-xs bg-light dker" style="margin:-20px 0 14px 0;">
                        <div class="progress-bar progress-bar-striped bg-info" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }" style="width: 0%;"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
            <div class="col b-r bg-auto">
                <div class="wrapper-md dker b-b" style="padding: 0 20px;">
                    <h3 class="m-n font-thin">Photos</h3>
                </div>
                <div class="wrapper-md">
                    <div id="wrap-photos">
                        <div class="row">
                            <div ng-repeat="photo in photos" class="col-sm-3">
                                <div ng-click="fileSelected(photo)" class="fixed-ar fixed-ar-16-9" style="background-image: url({{photo.url}});"></div>
                                <button ng-click="destroy(photo)" class="btn fixed-remove btn-sm btn-icon btn-default"><i class="glyphicon glyphicon-trash"></i></button>
                                <div class="fixed-info">{{photo.width}} x {{photo.height}} pixels</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>