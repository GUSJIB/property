<?php
$this->view_data['module'] = 'users';
$this->view_data['url'] = '/admin/users/'.$select_group;

$this->view_data['title'] = 'Users';
$this->view_data['header_text'] = 'Users';
$this->view_data['stylesheet'] = array();
$this->view_data['footer_js'] = array(
  '/assets/admin/js/users/controller.js',
  '/assets/admin/js/users/service.js',
  '/assets/admin/js/users/directive.js',
  '/assets/admin/vendors/jquery/file-upload/file-upload.min.js'
);
// $this->view_data['angular_modules'] = array('aaaa','bbbbb');
$this->view_data['header_icon_classes'] = 'fa fa-user';

?>


<div class="wrapper" ng-controller="UsersController">

    <div class="panel panel-default">
        <div class="row wrapper">
            <div class="col-sm-3">
                <small class="text-muted inline m-t-sm">Showing {{searchModel.beginRow}} - {{searchModel.endRow}} of {{searchModel.totalRows}} items</small>
            </div>
            <div class="col-sm-9 r">
                <button ng-click="openPopup()" class="btn btn-sm btn-success" type="button" style="margin-right:35px;">
                    <i class="fa fa-plus"></i>
                    Add new
                </button>
                <button ng-click="search()" class="btn btn-sm btn-primary" type="button">
                    <i class="fa" ng-class="{'fa-search': !showLoading, 'fa-refresh fa-spin':showLoading}"></i>
                    Search
                </button>
                <button ng-click="reset()" class="btn btn-sm btn-dark" type="button">
                    <i class="fa fa-times"></i>
                    Reset
                </button>
            </div>
        </div>

        <p id="notice"></p>

        <div class="table-responsive">
            <input type="hidden" id="select_group" value="<?=$select_group?>" />
            <table class="table table-striped b-t b-light" style="border-top: 1px solid #cbd3d4;">
                <thead>
                <tr>
                    <th style="width:50px;" class="c">NO</th>
                    <th>Name</th>
                    <th style="width:180px;">Email</th>
                    <th style="width:180px;">Phone</th>
                    <th style="width:85px;" class="c">Status</th>
                    <th style="width:120px;" class="c">Created</th>
                    <th style="width:80px;" class="c">Actions</th>
                </tr>
                <tr class="filter">
                    <th>&nbsp;</th>
                    <th><input ng-model="searchModel.first_name" ng-keyup="$event.keyCode==13 ? search() : null" type="text" maxlength="150" class="form-control filter" /></th>
                    <th><input ng-model="searchModel.email" ng-keyup="$event.keyCode==13 ? search() : null" type="text" maxlength="100" class="form-control filter" /></th>
                    <th><input ng-model="searchModel.phone" ng-keyup="$event.keyCode==13 ? search() : null" type="text" maxlength="100" class="form-control filter"></th>
                    <th class="c">
                        <select ng-model="searchModel.active" class="form-control filter">
                            <option value=""></option>
                            <option value="1">Active</option>
                            <option value="0">Disable</option>
                        </select>
                    </th>
                    <th class="datepicker">
                        <input 
                            ng-model="searchModel.created_at_from" 
                            datepicker-popup="dd/MM/yyyy" 
                            ng-click="dateFrom = true"
                            is-open="dateFrom"
                            showWeeks="false"
                            close-text="Close" 
                            ng-keyup="$event.keyCode==13 ? search() : null" 
                            type="text" 
                            maxlength="10" 
                            class="form-control filter" 
                            placeholder="dd/mm/yyyy"/>

                        <input 
                            ng-model="searchModel.created_at_to" 
                            datepicker-popup="dd/MM/yyyy" 
                            ng-click="dateTo = true" 
                            min-date="searchModel.created_at_from" 
                            is-open="dateTo"
                            showWeeks="false"
                            close-text="Close" 
                            ng-keyup="$event.keyCode==13 ? search() : null" 
                            type="text" 
                            maxlength="10" 
                            class="form-control filter" 
                            placeholder="dd/mm/yyyy"/>
                    </th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="item in searchModel.rows">
                    <td class="c">{{$index+searchModel.beginRow}}</td>
                    <td>{{item.first_name}} {{item.last_name}}</td>
                    <td>{{item.email}}</td>
                    <td>{{item.phone}}</td>
                    <td class="c"><span class="label {{(item.active == '1') ? 'label-success' : 'label-danger'}}">{{(item.active == '1') ? 'active' : 'disable'}}</span></td>
                    <td class="c">
                        {{item.created_at | asDate | date:'dd/MM/yyyy'}} <span style="color:#018110;">{{item.created_at | asDate | date:'hh:mm'}}</span>
                    </td>
                    <td class="action c">
                        <button ng-click="openPopup(item)" class="btn btn-default btn-xs" title="Edit record"><i class="fa fa-pencil text-success"></i></button>&nbsp;
                        <button ng-click="destroy(item)" class="btn btn-default btn-xs" title="Delete record"><i class="fa fa-times text-success"></i></button>
                    </td>
                </tr>
                <tr ng-show="searchModel.rows.length == 0 && !showLoading">
                    <td colspan="7"><span style="color:#aaa;">Found 0 records in system.</span></td>
                </tr>
                <tr ng-show="showLoading">
                    <td colspan="7"><span style="color:#aaa;">Loading ...</span></td>
                </tr>
                </tbody>
            </table>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-sm-5">
                    <small class="text-muted inline m-t-sm m-b-sm">Showing {{searchModel.beginRow}} - {{searchModel.endRow}} of {{searchModel.totalRows}} items</small>
                </div>
                <div class="col-sm-7 text-right text-center-xs" style="margin-top: 4px;">
                    <pagination ng-change="goPage()" boundary-links="true" class="pagination pagination-sm m-t-none m-b-none"
                                total-items="searchModel.totalRows" ng-model="searchModel.currentPage"
                                items-per-page="searchModel.pageSize" max-size="9">
                    </pagination>
                </div>
            </div>
        </footer>
    </div>

</div>

<script type="text/ng-template" id="PopupForm.html">
    <div class="modal-header">
        <h3 class="modal-title">{{item.id > 0 ? 'Edit User' : 'Create New User'}}</h3>
    </div>
    <div class="modal-body">
        <progressbar ng-show="pageLoading" class="loadingIcon progress-striped active m-b-sm" value="100" type="success">processing...</progressbar>
        <form name="PopupForm" ng-hide="pageLoading" class="clear" novalidate>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group" ng-class="{'has-error' : PopupForm.email.$invalid && !PopupForm.email.$pristine}">
                        <label class="control-label">Email : *</label>
                        <span ng-show="PopupForm.email.$invalid && !PopupForm.email.$pristine" class="message">&rarr; invalid email</span>
                        <input ng-model="item.email" name="email" class="form-control" type="email" maxlength="60" required />
                    </div>
                </div>
                <?php if ($this->ion_auth->is_admin()){ ?>
                <div class="col-sm-6 form-group" ng-class="{'has-error' : PopupForm.username.$invalid && !PopupForm.username.$pristine}">
                    <label class="control-label">Roles : </label>
                    <div class="form-inline" style="  margin-left: -6px;">
                        <div class="checkbox m-l m-r-xs">
                            <label class="i-checks">
                                <input ng-model="item.is_member" type="checkbox" value="1" /><i></i> Is Member?
                            </label>
                        </div>
                        <div class="checkbox m-l m-r-xs">
                            <label class="i-checks">
                                <input ng-model="item.is_officer" type="checkbox" value="1" /><i></i> Is Officer?
                            </label>
                        </div>
                        <div class="checkbox m-l m-r-xs">
                            <label class="i-checks">
                                <input ng-model="item.is_admin" type="checkbox" value="1" /><i></i> Is Administrator?
                            </label>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group" ng-class="{'has-error' : PopupForm.first_name.$invalid && !PopupForm.first_name.$pristine}">
                        <label class="control-label">First Name : *</label>
                        <span ng-show="PopupForm.first_name.$invalid && !PopupForm.first_name.$pristine" class="message">&rarr; required</span>
                        <input ng-model="item.first_name" name="first_name" class="form-control" type="text" maxlength="140" required />
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group" ng-class="{'has-error' : PopupForm.last_name.$invalid && !PopupForm.last_name.$pristine}">
                        <label class="control-label">Last Name : *</label>
                        <span ng-show="PopupForm.last_name.$invalid && !PopupForm.last_name.$pristine" class="message">&rarr; required</span>
                        <input ng-model="item.last_name" name="last_name" class="form-control" type="text" maxlength="140" required />
                    </div>
                </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Photo : </label>
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                        <span class="btn btn-info btn-file"><span class="fileupload-new">Select file</span>
                        <span class="fileupload-exists">Change</span>
                        <input id="photo" file-model="photo" type="file" /></span>
                        <span class="fileupload-preview"></span>
                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
                      </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group" ng-class="{'has-error' : PopupForm.gender.$invalid && !PopupForm.gender.$pristine}">
                        <label class="control-label">Gender : *</label>
                        <span ng-show="PopupForm.gender.$invalid && !PopupForm.gender.$pristine" class="message">&rarr; Select Gender</span>
                        <select ng-model="item.gender" class="form-control">
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Birthday : </label>
                        <input ng-model="item.birthday" datepicker-popup="yyyy-MM-dd" ng-click="birthday = true" is-open="birthday" name="birthday" class="form-control" type="text" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Mobile : </label>
                        <input ng-model="item.mobile" class="form-control" maxlength="10" type="tel" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Phone : </label>
                        <input ng-model="item.phone" class="form-control" maxlength="10" type="tel" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Fax : </label>
                        <input ng-model="item.fax" class="form-control" maxlength="10" type="tel" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group" ng-class="{'has-error' : PopupForm.company.$invalid && !PopupForm.company.$pristine}">
                        <label class="control-label">Company : </label>
                        <input ng-model="item.company" name="company" class="form-control" type="text" maxlength="100"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Facebook ID : </label>
                        <input ng-model="item.facebook_id" class="form-control" maxlength="60" type="text" />
                    </div>
                </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Facebook url : </label>
                        <input ng-model="item.facebook_url" class="form-control" maxlength="150" type="text" placeholder="http://"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Google plus url : </label>
                        <input ng-model="item.gplus_url" class="form-control" maxlength="150" type="text" placeholder="http://" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Twitter url : </label>
                        <input ng-model="item.twitter_url" class="form-control" maxlength="150" type="text" placeholder="http://" />
                    </div>
                </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>

            <div class="row">
                <div class="col-sm-4">
                    <label class="checkbox-inline i-checks">
                        <input type="checkbox" ng-model="item.is_agent" ng-checked="item.is_agent == '1'"><i></i> Is Agent?
                    </label>
                </div>
                <div class="col-sm-4">
                    <label class="checkbox-inline i-checks">
                        <input type="checkbox" ng-model="item.is_co_agent" ng-checked="item.is_co_agent == '1'"><i></i> Is Co Agent?
                    </label>
                </div>
                <div class="col-sm-4">
                    <label class="checkbox-inline i-checks">
                        <input type="checkbox" ng-model="item.active" ng-checked="item.active == '1'"><i></i> Is Active?
                    </label>
                </div>
            </div>
            <div class="line line-dashed b-b line-lg pull-in"></div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label">Agent excerpt BIO : </label>
                        <textarea ng-model="item.agent_excerpt" class="form-control" rows="3" maxlength="250"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label">Agent full BIO : </label>
                        <textarea ng-model="item.agent_detail" class="form-control" rows="7" maxlength="2000"></textarea>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div ng-hide="pageLoading" class="modal-footer">
        <button class="btn btn-primary" ng-click="submit()" ng-disabled="PopupForm.$invalid"><i ng-show="dataSubmitting" ng-class="{'fa-spin':dataSubmitting}" class="fa fa-refresh"></i> Submit</button>
        <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
    </div>
</script>
