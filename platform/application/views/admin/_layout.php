<!DOCTYPE html>
<html lang="th" data-ng-app="app">
<head>
    <title><?= isset($this->view_data['title']) ? $this->view_data['title'] : ''; ?></title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta http-equiv="X-Frame-Options" content="deny">
    <link rel="icon" href="/assets/admin/images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/assets/admin/images/favicon.ico" type="image/x-icon">
    <meta name="description" content="Teedin Administrator" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link rel="stylesheet" href="/assets/admin/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/assets/admin/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="/assets/admin/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="/assets/admin/css/simple-line-icons.css" type="text/css" />
    <link rel="stylesheet" href="/assets/admin/css/font.css" type="text/css" />
    <link rel="stylesheet" href="/assets/admin/css/app.css" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->
    <?php
    if(isset($this->view_data['stylesheet']) && count($this->view_data['stylesheet']) > 0){
        $css = $this->view_data['stylesheet'];
        foreach($css as $url){
            echo '<link href="'.$url.'" rel="stylesheet">';
        }
    }
    ?>

    <script type="text/javascript">
        var ROOT_URL = '/';
        var LOAD_ANGULAR_MODULES = [
            'ngAnimate',
            'ngCookies',
            'ngResource',
            'ngSanitize',
            'ngTouch',
            'ngStorage',
            'ui.bootstrap',
            'ui.validate',
            <?php
            if (isset($this->view_data['angular_modules']) && count($this->view_data['angular_modules']) > 0) {
                $module_str = implode(',',$this->view_data['angular_modules']);
                echo "'".str_replace(",", "','", $module_str)."'";
            }
            ?>
        ]
    </script>


    <!-- VENDORS -->
    <script src="/assets/admin/vendors/jquery/jquery.min.js"></script>
    <script src="/assets/admin/vendors/angular/angular.js"></script>
    <script src="/assets/admin/vendors/angular/angular-animate/angular-animate.js"></script>
    <script src="/assets/admin/vendors/angular/angular-cookies/angular-cookies.js"></script>
    <script src="/assets/admin/vendors/angular/angular-resource/angular-resource.js"></script>
    <script src="/assets/admin/vendors/angular/angular-sanitize/angular-sanitize.js"></script>
    <script src="/assets/admin/vendors/angular/angular-touch/angular-touch.js"></script>
    <script src="/assets/admin/vendors/libs/screenfull.min.js"></script>
    <script src="/assets/admin/vendors/angular/ngstorage/ngStorage.js"></script>
    <script src="/assets/admin/vendors/angular/angular-bootstrap/ui-bootstrap-tpls.js"></script>

    <!-- App -->
    <script src="/assets/admin/js/app.js"></script>
    <script src="/assets/admin/js/filters/filters.js"></script>
    <script src="/assets/admin/js/directives/setnganimate.js"></script>
    <script src="/assets/admin/js/directives/ui-butterbar.js"></script>
    <script src="/assets/admin/js/directives/ui-focus.js"></script>
    <script src="/assets/admin/js/directives/ui-fullscreen.js"></script>
    <script src="/assets/admin/js/directives/ui-module.js"></script>
    <script src="/assets/admin/js/directives/ui-nav.js"></script>
    <script src="/assets/admin/js/directives/ui-scroll.js"></script>
    <script src="/assets/admin/js/directives/ui-shift.js"></script>
    <script src="/assets/admin/js/directives/ui-toggleclass.js"></script>
    <script src="/assets/admin/js/directives/ui-validate.js"></script>
    <!-- Lazy loading -->

    <?php
    if(isset($this->view_data['footer_js']) && count($this->view_data['footer_js']) > 0){
        $js = $this->view_data['footer_js'];
        foreach($js as $url){
            echo '<script src="'.$url.'"></script>';
        }
    }
    ?>
</head>

<body ng-controller="AppCtrl" ng-cloak>

<div class="app ng-scope app-header-fixed" id="app" ng-class="{'app-header-fixed':app.settings.headerFixed, 'app-aside-fixed':app.settings.asideFixed, 'app-aside-folded':app.settings.asideFolded, 'app-aside-dock':app.settings.asideDock, 'container':app.settings.container}" ui-view>


    <!-- N A V I G A T I O N  B A R ----------------------------------------------------------------------------------->
    <div class="app-header navbar">
        <!-- navbar header -->
        <div class="navbar-header {{app.settings.navbarHeaderColor}}">
            <button class="pull-right visible-xs dk" ui-toggle-class="show" data-target=".navbar-collapse">
                <i class="glyphicon glyphicon-cog"></i>
            </button>
            <button class="pull-right visible-xs" ui-toggle-class="off-screen" data-target=".app-aside" ui-scroll="app">
                <i class="glyphicon glyphicon-align-justify"></i>
            </button>
            <!-- brand -->
            <a href="#" class="navbar-brand text-lt" ng-click="app.settings.asideFolded = !app.settings.asideFolded">
                <i class="hidden-folded fa  fa-shield"></i>
                <i class=" {{app.settings.asideFolded ? '' : 'hide'}} fa fa-indent fa-fw"></i>
                <span class="hidden-folded m-l-xs">SIA Property</span>
            </a>
            <!-- / brand -->
        </div>
        <!-- / navbar header -->

        <!-- navbar collapse -->
        <div class="collapse pos-rlt navbar-collapse box-shadow {{app.settings.navbarCollapseColor}}">
            <!-- buttons -->
            <div class="nav navbar-nav hidden-xs">
                <a class="btn no-shadow navbar-btn" style="padding-left: 0px;">
                    <i class="<?= isset($this->view_data['header_icon_classes']) ? $this->view_data['header_icon_classes'] : 'fa fa-home'; ?> fa-fw"></i>
                    <span><?= isset($this->view_data['header_text']) ? $this->view_data['header_text'] : 'Dashboard'; ?></span>
                </a>
            </div>
            <!-- / buttons -->

            <!-- nabar right -->
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="/admin/contact" title="Found {{notifications.new_contact_count}} new contact message posted.">
                        <i class="icon-paper-plane fa-fw"></i>
                        <span class="visible-xs-inline">New Contact messages</span>
                        <span ng-show="notifications.new_contact_count>0" class="badge badge-sm up bg-danger pull-right-xs">{{notifications.new_contact_count}}</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/enquires" title="Found {{notifications.new_enquiry_count}} new enquires posted.">
                        <i class="icon-bell fa-fw"></i>
                        <span class="visible-xs-inline">New Enquires</span>
                        <span ng-show="notifications.new_enquiry_count>0" class="badge badge-sm up bg-danger pull-right-xs">{{notifications.new_enquiry_count}}</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/properties" title="Found {{notifications.new_property_count}} new properties posted.">
                        <i class="icon-home fa-fw"></i>
                        <span class="visible-xs-inline">New Properties</span>
                        <span ng-show="notifications.new_property_count>0" class="badge badge-sm up bg-danger pull-right-xs">{{notifications.new_property_count}}</span>
                    </a>
                </li>
                <li class="dropdown" dropdown>
                    <a href class="dropdown-toggle clear" dropdown-toggle>
                      <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                        <img src="<?= $upload_url ?>/avatar/<?= $user->photo_ext?$user->id.$user->photo_ext:'_default.jpg'?>" alt="...">
                        <i class="on md b-white bottom"></i>
                      </span>
                      <span class="hidden-sm hidden-md"><?= $user->first_name?> <?= $user->last_name?></span> <b class="caret"></b>
                    </a>
                    <!-- dropdown -->
                    <div class="dropdown-menu w-md animated fadeInDown">
                        <div class="panel bg-white">
                            <div class="list-group">
                                <a href="/auth/account"  class="media list-group-item" target="_blank">My Profile</a>
                            </div>
                            <div class="panel-footer text-sm">
                                <a href="/admin/enquires" class="pull-right"><i class="fa fa-bell-o"></i></a>
                                <a href="/logout" data-toggle="class:show animated fadeInRight">Logout</a>
                            </div>
                        </div>
                    </div>
                    <!-- / dropdown -->
                </li>
            </ul>
            <!-- / navbar right -->

        </div>
        <!-- / navbar collapse -->

    </div>
    <!-- ---------------------------------------------------------------------------------------------------------------->


    <!-- M E N U  ------------------------------------------------------------------------------------------------------->
    <div class="app-aside hidden-xs {{app.settings.asideColor}}">
        <div class="aside-wrap">
            <!-- if you want to use a custom scroll when aside fixed, use the slimScroll
              <div class="navi-wrap" ui-jq="slimScroll" ui-options="{height:'100%', size:'8px'}">
            -->
            <div class="navi-wrap">

                <!-- nav -->
                <nav ui-nav="" class="navi ng-scope">
                    <!-- list -->
                    <ul class="nav">
                        <li class="<?=($this->view_data['url'] =='/admin/home')?'active':''?>">
                            <a href="/admin">
                                <i class="glyphicon glyphicon-home icon text-warning"></i>
                                <span class="font-bold">Dashboard</span>
                            </a>
                        </li>
                        <li class="<?=($this->view_data['url'] =='/admin/enquires')?'active':''?>">
                            <a href="/admin/enquires">
                                <i class="fa fa-bell icon text-info-lter"></i>
                                <span class="font-bold">Enquires</span>
                            </a>
                        </li>
                        <li class="<?=($this->view_data['url'] =='/admin/contacts')?'active':''?>">
                            <a href="/admin/contacts">
                                <i class="fa fa-send icon text-info-lter"></i>
                                <span>Contact us</span>
                            </a>
                        </li>
                        <li class="<?=($this->view_data['url'] =='/admin/properties')?'active':''?>">
                            <a href="/admin/properties">
                                <i class="glyphicon glyphicon-list icon text-success"></i>
                                <span>Properties</span>
                            </a>
                        </li>

                        <li class="<?=($this->view_data['module'] == 'users')?'active':''?>">
                            <a href class="auto">
                              <span class="pull-right text-muted">
                                <i class="fa fa-fw fa-angle-right text"></i>
                                <i class="fa fa-fw fa-angle-down text-active"></i>
                              </span>
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Members</span>
                            </a>
                            <ul class="nav nav-sub dk">
                                <li class="nav-sub-header">
                                    <a href><span>Members</span></a>
                                </li>
                                <li class="<?=($this->view_data['url'] =='/admin/users/members')?'active':''?>">
                                    <a href="/admin/users/members"><span>Members</span></a>
                                </li>
                                <li class="<?=($this->view_data['url'] =='/admin/users/agents')?'active':''?>">
                                    <a href="/admin/users/agents"><span>Agents</span></a>
                                </li>
                                <li class="<?=($this->view_data['url'] =='/admin/users/officers')?'active':''?>">
                                    <a href="/admin/users/officers"><span>Officers</span></a>
                                </li>
                                <?php if ($this->ion_auth->is_admin()){ ?>
                                <li class="<?=($this->view_data['url'] =='/admin/users/admins')?'active':''?>">
                                    <a href="/admin/users/admins"><span>Administrator</span></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="<?=($this->view_data['url'] =='/admin/auctions')?'active':''?>">
                            <a href="/admin/auctions">
                                <i class="fa  fa-legal icon text-info-lter"></i>
                                <span>Auction calendar</span>
                            </a>
                        </li>


                        <?php if ($this->ion_auth->is_admin()){ ?>
                        <li class="line dk"></li>

                        <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                            <span>System Configurations</span>
                        </li>
                        <li class="<?=($this->view_data['module'] == 'contents')?'active':''?>">
                            <a href class="auto">
                              <span class="pull-right text-muted">
                                <i class="fa fa-fw fa-angle-right text"></i>
                                <i class="fa fa-fw fa-angle-down text-active"></i>
                              </span>
                                <i class="glyphicon glyphicon-list-alt"></i>
                                <span>Content pages</span>
                            </a>
                            <ul class="nav nav-sub dk">
                                <li class="<?=($this->view_data['url'] ==='/admin/contents/useful_link')?'active':''?>">
                                    <a href="/admin/contents/useful_link"><span>Useful links</span></a>
                                </li>
                                <li class="<?=($this->view_data['url'] ==='/admin/contents/contact_us')?'active':''?>">
                                    <a href="/admin/contents/contact_us"><span>Contact us</span></a>
                                </li>
                                <li class="<?=($this->view_data['url'] ==='/admin/contents/about_us')?'active':''?>">
                                    <a href="/admin/contents/about_us"><span>About us</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="<?=($this->view_data['module'] == 'geo_graphic')?'active':''?>">
                            <a href class="auto">
                              <span class="pull-right text-muted">
                                <i class="fa fa-fw fa-angle-right text"></i>
                                <i class="fa fa-fw fa-angle-down text-active"></i>
                              </span>
                                <i class="glyphicon glyphicon-globe"></i>
                                <span>Business Zones</span>
                            </a>
                            <ul class="nav nav-sub dk">
                                <li class="<?=($this->view_data['url'] ==='/admin/provinces')?'active':''?>">
                                    <a href="/admin/provinces"><span>Cities</span></a>
                                </li>
                                <li class="<?=($this->view_data['url'] ==='/admin/amphurs')?'active':''?>">
                                    <a href="/admin/amphurs"><span>City areas</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="<?=($this->view_data['url'] =='/admin/settings')?'active':''?>">
                            <a href="/admin/settings">
                                <i class="glyphicon glyphicon-cog"></i>
                                <span>Preferences</span>
                            </a>
                        </li>
                        <?php } ?>


                    </ul>
                    <!-- / list -->
                </nav>
                <!-- nav -->

                <!-- aside footer -->

                <!-- / aside footer -->
            </div>
        </div>

    </div>
    <!-- ---------------------------------------------------------------------------------------------------------------->


    <!-- C O N T E N T -------------------------------------------------------------------------------------------------->
    <div class="app-content">
        <div ui-butterbar></div>
        <a href class="off-screen-toggle hide" ui-toggle-class="off-screen" data-target=".app-aside" ></a>
        <div class="app-content-body fade-in-up" ui-view>








            <?= $inner_view ?>









        </div>
    </div>
    <!-- ---------------------------------------------------------------------------------------------------------------->

    <!-- aside right -->
    <div class="app-aside-right pos-fix no-padder w-md w-auto-xs bg-white b-l animated fadeInRight hide">
        <div class="vbox">
            <div class="wrapper b-b b-t b-light m-b">
                <a href class="pull-right text-muted text-md" ui-toggle-class="show" target=".app-aside-right"><i class="icon-close"></i></a>
                Chat
            </div>
            <div class="row-row">
                <div class="cell">
                    <div class="cell-inner padder">
                        <!-- chat list -->
                        <div class="m-b">
                            <a href class="pull-left thumb-xs avatar"><img src="/assets/admin/images/a3.jpg" alt="..."></a>
                            <div class="clear">
                                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                                    <span class="arrow left pull-up"></span>
                                    <p class="m-b-none">Hi John, What's up...</p>
                                </div>
                                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i> 2 minutes ago</small>
                            </div>
                        </div>
                        <div class="m-b">
                            <a href class="pull-right thumb-xs avatar"><img src="/assets/admin/images/a3.jpg" class="img-circle" alt="..."></a>
                            <div class="clear">
                                <div class="pos-rlt wrapper-sm bg-light r m-r-sm">
                                    <span class="arrow right pull-up arrow-light"></span>
                                    <p class="m-b-none">Lorem ipsum dolor :)</p>
                                </div>
                                <small class="text-muted">1 minutes ago</small>
                            </div>
                        </div>
                        <div class="m-b">
                            <a href class="pull-left thumb-xs avatar"><img src="/assets/admin/images/a3.jpg" alt="..."></a>
                            <div class="clear">
                                <div class="pos-rlt wrapper-sm b b-light r m-l-sm">
                                    <span class="arrow left pull-up"></span>
                                    <p class="m-b-none">Great!</p>
                                </div>
                                <small class="text-muted m-l-sm"><i class="fa fa-ok text-success"></i>Just Now</small>
                            </div>
                        </div>
                        <!-- / chat list -->
                    </div>
                </div>
            </div>
            <div class="wrapper m-t b-t b-light">
                <form class="m-b-none">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Say something">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">SEND</button>
                  </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- / aside right -->

    <!-- footer -->
    <div class="app-footer wrapper b-t bg-light">
        <span class="pull-right">{{app.version}} <a href ui-scroll="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
        &copy; 2015 Copyright.
    </div>
    <!-- / footer -->

</div>


</body>
</html>
