<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Sites
| -------------------------------------------------------------------------
| This file lets you define site-specific values
|
*/

$config['sites'] = array(

    /**
     * Frontend Website
     */
    'frontend' => array(

        'autoload' => array(
            'libraries'	=> array('database'),
            'helpers'	=> array('session', 'inflector'),
            'models'	=> array(
                //'User_model'	=> 'users',
            )
        )
    ),

    /**
     * Admin Panel
     */
    'admin' => array(

        'autoload' => array(
            'libraries'	=> array('database'),
            'helpers'	=> array('session', 'inflector'),
            'models'	=> array(),
        )
    ),

    /**
     * API Site
     */
    'api' => array(

        'autoload' => array(
            'libraries'	=> array(),
            'helpers'	=> array(),
            'models'	=> array(),
        )
    )

);