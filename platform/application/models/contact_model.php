<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_model extends CI_Model
{
    const TABLE_NAME = 'contacts';
    public $id = 'id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function find($id)
    {
        $this->db->where($this->id, $id);
        $q = $this->db->get(self::TABLE_NAME);
        return  $q->row();
    }

    public function countByStatus($status)
    {
        $this->db->from(self::TABLE_NAME);
        $this->db->where('status', $status);
        return $this->db->count_all_results();
    }

    public function search($searchModel)
    {
        $start = ($searchModel["currentPage"]-1) * $searchModel["pageSize"];

        //STEP 1: count all matched record in table
        $this->setSearchCriteria($searchModel);
        $this->db->from(self::TABLE_NAME);
        $row_count = $this->db->count_all_results();

        //STEP 2: read record in current paging
        $this->setSearchCriteria($searchModel);
        $this->db->order_by($this->id, $this->order);
        $this->db->limit($searchModel["pageSize"], $start);
        $data = $this->db->get(self::TABLE_NAME);

        //STEP 3: set return result
        $searchModel["rows"] = $data->result();
        $searchModel["beginRow"] = $start+1;
        $searchModel["endRow"] = $start + count($searchModel["rows"]);
        $searchModel["totalRows"] = $row_count;
        $searchModel["totalPages"] = ceil($searchModel["totalRows"] / $searchModel["pageSize"]);

        return $searchModel;
    }

    private function setSearchCriteria($searchModel)
    {

        if(isset($searchModel['id']) && !empty($searchModel['id']))
            $this->db->where('id', $searchModel['id']);
        if(isset($searchModel['name']) && !empty($searchModel['name']))
            $this->db->like('name', $searchModel['name']);
        if(isset($searchModel['email']) && !empty($searchModel['email']))
            $this->db->like('email', $searchModel['email']);
        if(isset($searchModel['phone']) && !empty($searchModel['phone']))
            $this->db->like('phone', $searchModel['phone']);
        if(isset($searchModel['subject']) && !empty($searchModel['subject']))
            $this->db->like('subject', $searchModel['subject']);
        if(isset($searchModel['status']) && !empty($searchModel['status']))
            $this->db->where('status', $searchModel['status']);
        if(isset($searchModel['post_ip']) && !empty($searchModel['post_ip']))
            $this->db->where('post_ip', $searchModel['post_ip']);

        if(isset($searchModel['created_at_from']) && !empty($searchModel['created_at_from']))
            $this->db->where('created_at >=', $searchModel['created_at_from']);
        if(isset($searchModel['created_at_to']) && !empty($searchModel['created_at_to']))
            $this->db->where('created_at <=', $searchModel['created_at_to']);

    }


    public function create($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s',now());
        $this->db->insert(self::TABLE_NAME, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update(self::TABLE_NAME, $data);
    }

    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete(self::TABLE_NAME);
    }

    public function status()
    {
        return $this->config_item('contact_status');
    }

    public function swap_flag($id, $field_name)
    {
        $row = $this->find($id);
        $newFlag = $row[$field_name]=='1' ? 0 : 1;
        $this->update($id, array("$field_name"=> $newFlag));
    }



}