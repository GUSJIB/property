<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Amphur_model extends CI_Model
{
    const TABLE_NAME = 'amphurs';
    public $id = 'id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('app_url_helper');
    }

    public function find($id)
    {
        $this->db->where($this->id, $id);
        $q = $this->db->get(self::TABLE_NAME);
        return  $q->row();
    }
    public function findWithDetail($id)
    {
        $this->db->select('amphurs.*, provinces.name as province_name, provinces.country_code');
        $this->db->join('provinces', 'provinces.id = amphurs.province_id', 'left');
        $this->db->where('amphurs.id', $id);
        $this->db->order_by('amphurs.position', 'ASC');
        $data = $this->db->get(self::TABLE_NAME)->result();

        return $this->assignUrl($data)[0];
    }

    public function findIdByName($name)
    {
        $this->db->select('id');
        $this->db->where('name', $name);
        $data = $this->db->get(self::TABLE_NAME)->result();

        foreach ($data as $row)
        {
            return $row->id;
        }

        return false;
    }

    public function findByProvince($provinceId)
    {
        $this->db->select('amphurs.*, provinces.name as province_name, provinces.country_code');
        $this->db->join('provinces', 'provinces.id = amphurs.province_id', 'left');
        $this->db->where('amphurs.province_id', $provinceId);
        $this->db->order_by('amphurs.position', 'ASC');
        $data = $this->db->get(self::TABLE_NAME)->result();

        return $this->assignUrl($data);
    }

    public function search($searchModel)
    {
        $start = ($searchModel["currentPage"]-1) * $searchModel["pageSize"];

        //STEP 1: count all matched record in table
        $this->setSearchCriteria($searchModel);
        $this->db->from(self::TABLE_NAME);
        $row_count = $this->db->count_all_results();

        //STEP 2: read record in current paging
        $this->db->select('amphurs.*, provinces.name as province_name');
        $this->setSearchCriteria($searchModel);
        $this->db->from(self::TABLE_NAME );
        $this->db->join('provinces', 'amphurs.province_id = provinces.id', 'left');
        $this->db->order_by('amphurs.'.$this->id, $this->order);
        $this->db->limit($searchModel["pageSize"], $start);
        $data = $this->db->get();

        //STEP 3: set return result
        $searchModel["rows"] = $data->result();
        $searchModel["beginRow"] = $start+1;
        $searchModel["endRow"] = $start + count($searchModel["rows"]);
        $searchModel["totalRows"] = $row_count;
        $searchModel["totalPages"] = ceil($searchModel["totalRows"] / $searchModel["pageSize"]);

        return $searchModel;
    }

    private function setSearchCriteria($searchModel)
    {
        if(isset($searchModel['id']) && !empty($searchModel['id']))
            $this->db->where('amphurs.id', $searchModel['id']);
        if(isset($searchModel['province_id']) && !empty($searchModel['province_id']))
            $this->db->like('amphurs.province_id', $searchModel['province_id']);
        if(isset($searchModel['name']) && !empty($searchModel['name']))
            $this->db->like('amphurs.name', $searchModel['name']);
        if(isset($searchModel['position']) && !empty($searchModel['position']))
            $this->db->where('amphurs.position', $searchModel['position']);
        if(isset($searchModel['post_count']) && !empty($searchModel['post_count']))
            $this->db->where('amphurs.post_count', $searchModel['post_count']);

    }

    public function getHotBusinessArea($limit)
    {
        $this->db->select('amphurs.*, provinces.country_code, provinces.name as province_name');
        $this->db->from(self::TABLE_NAME );
        $this->db->join('provinces', 'provinces.id = amphurs.province_id', 'left');
        $this->db->order_by('post_count', 'DESC');
        $this->db->limit($limit, 0);

        $data = $this->db->get()->result();

        return $this->assignUrl($data); ;
    }


    public function create($data)
    {
        $data['post_count'] = 0;
        $this->db->insert(self::TABLE_NAME, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update(self::TABLE_NAME, $data);
    }

    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete(self::TABLE_NAME);
    }

    public function swap_flag($id, $field_name)
    {
        $row = $this->find($id);
        $newFlag = $row[$field_name]=='1' ? 0 : 1;
        $this->update($id, array("$field_name"=> $newFlag));
    }

    public function find_by_province($province)
    {
        $this->db->where('province_id', $province);
        $this->db->order_by('position', 'ASC');
        $data = $this->db->get(self::TABLE_NAME);
        return $data->result();
    }

    public function getAmphurList()
    {
        $this->db->select('amphurs.*, provinces.name as province_name, provinces.country_code');
        $this->db->from(self::TABLE_NAME );
        $this->db->join('provinces', 'provinces.id = amphurs.province_id', 'left');

        $this->db->order_by('provinces.country_code', 'ASC');
        $this->db->order_by('provinces.position', 'ASC');
        $this->db->order_by('amphurs.position', 'ASC');

        return $this->db->get()->result();
    }



    private function assignUrl($amphurList)
    {
        // address_full, country name, province name
        foreach ($amphurList as $item)
        {
            $item->country_name = '';
            if(!empty($item->country_code)) {
                $countries = config_item('countries');
                if(isset($countries[$item->country_code])) {
                    $item->country_name = $countries[$item->country_code];
                }
            }

            $url_slug = $this->config->base_url().url_slug($item->country_name).'/'.url_slug($item->province_name).'/'.url_slug($item->name);
            $item->url = $url_slug;
        }

        return $amphurList;
    }

}