<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Property_photo_model extends CI_Model
{
    const TABLE_NAME = 'property_photos';
    public $id = 'id';
    public $order = 'DESC';

    public $foreign_key = 'property_id';
    public $property_id = '';

    public $order_field = 'position';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function set_property_id($id)
    {
        $this->property_id = $id;        
    }

    public function find_by_property_id($property_id)
    {
        $this->db->where('property_id',$property_id);
        $this->db->order_by($this->order_field, 'ASC');
        $q = $this->db->get(self::TABLE_NAME);

        $photoList = $q->result();
        $photoList = $this->assignAdditionalValue($photoList);
        return $photoList;
    }

    public function find($id)
    {
        $this->db->where($this->id, $id);
        $q = $this->db->get(self::TABLE_NAME);
        return $q->row();
    }

    public function create()
    {
        $data['created_at'] = date('Y-m-d H:i:s',now());
        $data['property_id'] = $this->property_id;
        $this->db->insert(self::TABLE_NAME, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update(self::TABLE_NAME, $data);
    }

    public function delete($id)
    {
        $photo = $this->find($id);
        if(!empty($photo)){
            $path = config_item('property_path').$photo->property_id.'/gallery/'.$photo->id.$photo->ext;
            if(unlink($path)){
                $this->db->where($this->id, $id);
                $this->db->delete(self::TABLE_NAME);
            }
        }
    }

    public function get_gallery_path()
    {
        $path = config_item('property_path').$this->property_id.'/gallery/';
        if(!is_dir($path)){
            mkdir($path, 0777, TRUE);
        }
        return $path;
    }

    private function assignAdditionalValue($photoList)
    {
        $root_url = $this->config->base_url().'assets/uploads/properties/';
        foreach ($photoList as $item)
        {
            $item->photo_url = '';
            if(isset($item->ext) && strlen($item->ext)>2){
                $item->photo_url = $root_url.$item->property_id.'/gallery/'.$item->id.$item->ext;
            }
        }

        return $photoList;
    }
}