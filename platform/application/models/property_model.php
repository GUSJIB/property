<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Property_model extends CI_Model
{
    const TABLE_NAME = 'properties';
    public $id = 'id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('app_url_helper');
        $this->load->helper('url');
    }

    public function find($id)
    {
        $this->db->where($this->id, $id);
        $q = $this->db->get(self::TABLE_NAME);
        return  $q->row();
    }

    public function countByStatus($status)
    {
        $this->db->from(self::TABLE_NAME);
        $this->db->where('status', $status);
        return $this->db->count_all_results();
    }

    public function findWithDetail($id)
    {
        $this->db->select('properties.*, provinces.name as province_name, amphurs.name as amphur_name');
        $this->db->from(self::TABLE_NAME );
        $this->db->join('provinces', 'provinces.id = properties.province_id', 'left');
        $this->db->join('amphurs', 'amphurs.id = properties.amphur_id', 'left');
        $this->db->where('properties.id', $id);

        $data = $this->db->get()->result();

        //fill other information
        return $this->assignAdditionalValue($data, true)[0];
    }

    public function search($searchModel)
    {
        $start = ($searchModel["currentPage"]-1) * $searchModel["pageSize"];

        //STEP 1: count all matched record in table
        $this->setSearchCriteria($searchModel);
        $this->db->from(self::TABLE_NAME);
        $row_count = $this->db->count_all_results();

        //STEP 2: read record in current paging
        $this->setSearchCriteria($searchModel);
        $this->db->order_by($this->id, $this->order);
        $this->db->limit($searchModel["pageSize"], $start);
        $data = $this->db->get(self::TABLE_NAME);

        //STEP 3: set return result
        $searchModel["rows"] = $data->result();
        $searchModel["beginRow"] = $start+1;
        $searchModel["endRow"] = $start + count($searchModel["rows"]);
        $searchModel["totalRows"] = $row_count;
        $searchModel["totalPages"] = ceil($searchModel["totalRows"] / $searchModel["pageSize"]);

        return $searchModel;
    }

    private function setSearchCriteria($searchModel)
    {
        if(isset($searchModel['id']) && !empty($searchModel['id']))
            $this->db->where('id', $searchModel['id']);
        if(isset($searchModel['title']) && !empty($searchModel['title']))
            $this->db->like('title', $searchModel['title']);
        if(isset($searchModel['property_type']) && !empty($searchModel['property_type']))
            $this->db->like('property_type', $searchModel['property_type']);
        if(isset($searchModel['country_code']) && !empty($searchModel['country_code']))
            $this->db->like('country_code', $searchModel['country_code']);
        if(isset($searchModel['status']) && !empty($searchModel['status']))
            $this->db->where('status', $searchModel['status']);

        if(isset($searchModel['created_at_from']) && !empty($searchModel['created_at_from']))
            $this->db->where('created_at >=', $searchModel['created_at_from']);
        if(isset($searchModel['created_at_to']) && !empty($searchModel['created_at_to']))
            $this->db->where('created_at <=', $searchModel['created_at_to']);
    }

    public function getNewListedProperties($status, $limit)
    {
        $this->db->select('properties.*, provinces.name as province_name, amphurs.name as amphur_name');
        $this->db->from(self::TABLE_NAME );
        $this->db->join('provinces', 'provinces.id = properties.province_id', 'left');
        $this->db->join('amphurs', 'amphurs.id = properties.amphur_id', 'left');
        $this->db->where('status', $status);

        $this->db->order_by('created_at', 'DESC');

        $this->db->limit($limit, 0);

        $data = $this->db->get()->result();

        //fill other information
        return $this->assignAdditionalValue($data);
    }

    public function featuredProperties($status, $limit)
    {
        $this->db->select('properties.*, provinces.name as province_name, amphurs.name as amphur_name');
        $this->db->from(self::TABLE_NAME );
        $this->db->join('provinces', 'provinces.id = properties.province_id', 'left');
        $this->db->join('amphurs', 'amphurs.id = properties.amphur_id', 'left');
        $this->db->where('properties.featured', 1);
        $this->db->where('properties.status', $status);

        $this->db->order_by('properties.created_at', 'DESC');

        $this->db->limit($limit, 0);

        $data = $this->db->get()->result();

        //fill other information
        return $this->assignAdditionalValue($data);
    }

    public function recentlyProperties($excludeIds, $status, $limit)
    {
        $this->db->select('properties.*, provinces.name as province_name, amphurs.name as amphur_name');
        $this->db->from(self::TABLE_NAME );
        $this->db->join('provinces', 'provinces.id = properties.province_id', 'left');
        $this->db->join('amphurs', 'amphurs.id = properties.amphur_id', 'left');
        $this->db->where('properties.status', $status);
        if(count($excludeIds) > 0){
            $this->db->where_not_in('properties.id', $excludeIds);
        }

        $this->db->order_by('properties.created_at', 'DESC');

        $this->db->limit($limit, 0);

        $data = $this->db->get()->result();

        //fill other information
        return $this->assignAdditionalValue($data);
    }

    public function find_similar_property($province_id, $property_type, $status, $limit)
    {
        $this->db->select('properties.*, provinces.name as province_name, amphurs.name as amphur_name');
        $this->db->from(self::TABLE_NAME );
        $this->db->join('provinces', 'provinces.id = properties.province_id', 'left');
        $this->db->join('amphurs', 'amphurs.id = properties.amphur_id', 'left');
        $this->db->where('properties.province_id', $province_id);
        $this->db->where('properties.property_type', $property_type);
        $this->db->where('properties.status', $status);

        $this->db->order_by('properties.created_at', 'DESC');

        $this->db->limit($limit, 0);

        $data = $this->db->get()->result();

        //fill other information
        return $this->assignAdditionalValue($data);
    }



    public function searchMap($searchModel)
    {
        $start = ($searchModel["currentPage"]-1) * $searchModel["pageSize"];

        //STEP 1: count all matched record in table
        $this->setSearchMapCriteria($searchModel);
        $this->db->from(self::TABLE_NAME);
        $row_count = $this->db->count_all_results();

        //STEP 2: read record in current paging
        $this->db->select('properties.*, provinces.name as province_name, amphurs.name as amphur_name');
        $this->db->from(self::TABLE_NAME );
        $this->db->join('provinces', 'provinces.id = properties.province_id', 'left');
        $this->db->join('amphurs', 'amphurs.id = properties.amphur_id', 'left');
        $this->setSearchMapCriteria($searchModel);
        $this->db->order_by($this->id, $this->order);
        $this->db->limit($searchModel["pageSize"], $start);
        $data = $this->db->get( );

        $result = $data->result();
        $result = $this->assignAdditionalValue($result);


        //STEP 3: set return result
        $searchModel["rows"] = $result;
        $searchModel["beginRow"] = $start+1;
        $searchModel["endRow"] = $start + count($searchModel["rows"]);
        $searchModel["totalRows"] = $row_count;
        $searchModel["totalPages"] = ceil($searchModel["totalRows"] / $searchModel["pageSize"]);

        return $searchModel;
    }

    private function setSearchMapCriteria($searchModel)
    {
        if(isset($searchModel['property_type']) && !empty($searchModel['property_type']))
            $this->db->where('properties.property_type', $searchModel['property_type']);

        if(isset($searchModel['country_code']) && !empty($searchModel['country_code']))
            $this->db->where('properties.country_code', $searchModel['country_code']);
        if(isset($searchModel['province_id']) && !empty($searchModel['province_id']))
            $this->db->where('properties.province_id', $searchModel['province_id']);
        if(isset($searchModel['amphur_id']) && !empty($searchModel['amphur_id']))
            $this->db->where('properties.amphur_id', $searchModel['amphur_id']);

        if(isset($searchModel['status']) && !empty($searchModel['status']))
            $this->db->where('properties.status', $searchModel['status']);
        if(isset($searchModel['property_type']) && !empty($searchModel['property_type']))
            $this->db->where('properties.property_type', $searchModel['property_type']);

        if(isset($searchModel['bedroom1']) && !empty($searchModel['bedroom1']))
            $this->db->where('properties.bedroom_qty >=', $searchModel['bedroom1']);
        if(isset($searchModel['bedroom2']) && !empty($searchModel['bedroom2']))
            $this->db->where('properties.bedroom_qty <=', $searchModel['bedroom2']);
        if(isset($searchModel['bathroom1']) && !empty($searchModel['bathroom1']))
            $this->db->where('properties.bathroom_qty >=', $searchModel['bathroom1']);
        if(isset($searchModel['bathroom2']) && !empty($searchModel['bathroom2']))
            $this->db->where('properties.bathroom_qty<=', $searchModel['bathroom2']);

        //Search with price range.
        if(isset($searchModel['purpose']) && !empty($searchModel['purpose']))
        {
            if ($searchModel['purpose'] == 'sale')
            {
                $this->db->where('properties.for_sale', 1);
                if (isset($searchModel['price1']) && !empty($searchModel['price1']))
                    $this->db->where('properties.sale_price >=', $searchModel['price1']);
                if (isset($searchModel['price2']) && !empty($searchModel['price2']))
                    $this->db->where('properties.sale_price <=', $searchModel['price2']);
            }
            else if ($searchModel['purpose'] == 'rent')
            {
                $this->db->where('properties.for_rent', 1);
                if (isset($searchModel['price1']) && !empty($searchModel['price1']) && isset($searchModel['price2']) && !empty($searchModel['price2']))
                {
                    //case monthly rent available
                    //case yearly available
                    $this->db->where(
                        '((properties.monthly_rent=1 AND properties.monthly_rate >='.$searchModel['price1'].' AND properties.monthly_rate <= '.$searchModel['price2'].') ' .
                        ' OR '.
                        '(properties.yearly_rent=1 AND properties.yearly_rate >='.$searchModel['price1'].' AND properties.yearly_rate <= '.$searchModel['price2'].'))'
                    );
                }
                else if (isset($searchModel['price1']) && !empty($searchModel['price1']))
                {
                    //case monthly rent available
                    //case yearly available
                    $this->db->where(
                        '( (properties.monthly_rent=1 AND properties.monthly_rate >='.$searchModel['price1'].') ' .
                        ' OR '.
                        '(properties.yearly_rent=1 AND properties.yearly_rate >='.$searchModel['price1'].') )'
                    );
                }
                else if (isset($searchModel['price2']) && !empty($searchModel['price2']))
                {
                    //case monthly rent available
                    //case yearly available
                    $this->db->where(
                        '( (properties.monthly_rent=1 AND properties.monthly_rate <= '.$searchModel['price2'].') ' .
                        ' OR '.
                        '(properties.yearly_rent=1 AND properties.yearly_rate <= '.$searchModel['price2'].') )'
                    );
                }
            }
            else if ($searchModel['purpose'] == 'rent_sale')
            {
                $this->db->where('properties.for_sale', 1);
                $this->db->where('properties.for_rent', 1);
            }
        }
    }

    public function create($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s',now());
        $this->db->insert(self::TABLE_NAME, $data);
        $newId = $this->db->insert_id();

        //update property counter in province and amphur table
        $this->updateCounter($data['amphur_id'], $data['province_id']);

        return $newId;
    }

    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update(self::TABLE_NAME, $data);

        //update property counter in province and amphur table
        $this->updateCounter($data['amphur_id'], $data['province_id']);
    }

    private function updateCounter($amphurId, $provinceId)
    {
        $data = array($amphurId, $amphurId);
        $this->db->query("
            UPDATE amphurs set post_count=(
                SELECT count(*)
                    FROM properties
                    WHERE amphur_id = ?
                )
            WHERE id = ?",
            $data
        );

        $data = array($provinceId, $provinceId);
        $this->db->query("
            UPDATE provinces set post_count=(
                  SELECT count(*)
                    FROM properties
                    WHERE province_id = ?
                )
            WHERE id = ?",
            $data
        );
    }

    public function delete($id)
    {
        $path = config_item('property_path').$id;
        $this->db->where($this->id, $id);
        if($this->db->delete(self::TABLE_NAME)){
            if(is_dir($path)){
                rmdir($path);
            }
        }
    }

    public function get_uploads_path($id)
    {
        $path = config_item('property_path').$id.'/';
        if(!is_dir($path)){
            mkdir($path, 0777, TRUE);
        }
        return $path;
    }

    private function assignAdditionalValue($propertyList, $include_addressLine=false)
    {
        // address_full, country name, province name, amphur name
        foreach ($propertyList as $item)
        {
            $item->address_full = '';
            $prefix = '';
            if($include_addressLine && !empty($item->address1)) {
                $item->address_full .= $prefix.$item->address1;
                $prefix = ', ';
            }
            if($include_addressLine && !empty($item->address2)) {
                $item->address_full .= $prefix.$item->address2;
                $prefix = ', ';
            }
            if($include_addressLine && !empty($item->district)) {
                $item->address_full .= $prefix.$item->district;
                $prefix = ', ';
            }
            if(!empty($item->amphur_name)) {
                $item->address_full .= $prefix.$item->amphur_name;
                $prefix = ', ';
            }
            if(!empty($item->province_name)) {
                $item->address_full .= $prefix.$item->province_name;
                $prefix = ', ';
            }

            $item->country_name = '';
            if(!empty($item->country_code)) {
                $countries = config_item('countries');
                if(isset($countries[$item->country_code])) {
                    $item->country_name = $countries[$item->country_code];
                    $item->address_full .= $prefix . $countries[$item->country_code];
                    $prefix = ', ';
                }
            }
            if(!empty($item->postal_code)) {
                $item->address_full .= $prefix.$item->postal_code;
            }

            $url_slug = $this->config->base_url().url_slug($item->country_name).'/'.url_slug($item->province_name).'/p'.$item->id;
            $item->url = $url_slug;

            $item->cover_url = '';
            if(isset($item->cover_ext) && strlen($item->cover_ext)>2){
                $item->cover_url = $this->config->base_url().'assets/uploads/properties/'.$item->id.'/cover'.$item->cover_ext;
            }
        }

        return $propertyList;
    }

    public function count_view($id)
    {
        $this->db->set('total_view', '`total_view`+ 1', FALSE);
        $this->db->where($this->id, $id);
        $this->db->update(self::TABLE_NAME);
    }




    public function frontend_search_form($status, $input = array(),  $pageSize = 20)
    {
        //STEP 1: count all matched record in table
        $this->frontend_search_criteria($status, $input);
        $this->db->from(self::TABLE_NAME);
        $row_count = $this->db->count_all_results();

        //STEP 2: read record in current paging
        $this->db->select('properties.*, provinces.name as province_name, amphurs.name as amphur_name');
        $this->db->join('provinces', 'provinces.id = properties.province_id', 'left');
        $this->db->join('amphurs', 'amphurs.id = properties.amphur_id', 'left');
        $this->frontend_search_criteria($status, $input);

        $currentPage = 1;
        if(isset($input['page']) && !empty($input['page'])){
            $currentPage = $input['page'];;
        }
        $start = ($currentPage-1) * $pageSize;

        $this->db->order_by('created_at', 'DESC');
        $this->db->limit($pageSize, $start);
        $data = $this->db->get(self::TABLE_NAME)->result();
        $data = $this->assignAdditionalValue($data);

        $result = new stdClass();
        $result->rows = $data;
        $result->currentPage = $currentPage;
        $result->pageSize = $pageSize;
        $result->beginRow = $start+1;
        $result->endRow = $start + count($result->rows);
        $result->totalRows = $row_count;
        $result->totalPages = ceil($result->totalRows / $pageSize);

        return $result;
    }

    public function frontend_search_criteria($status, $input = array())
    {
        $this->db->where('properties.status', $status);

        if(isset($input['bedroom']) && !empty($input['bedroom'])){
            $this->db->where('properties.bedroom_qty', $input['bedroom']);
        }

        if(isset($input['bathroom']) && !empty($input['bathroom'])){
            $this->db->where('properties.bathroom_qty', $input['bathroom']);
        }

        if(isset($input['amphur_id']) && !empty($input['amphur_id'])){
            $this->db->where('properties.amphur_id', $input['amphur_id']);
        }
        if(isset($input['province_id']) && !empty($input['province_id'])){
            $this->db->where('properties.province_id', $input['province_id']);
        }
        if(isset($input['country_code']) && !empty($input['country_code'])){
            $this->db->where('properties.country_code', $input['country_code']);
        }

        if(isset($input['type']) && !empty($input['type'])){
            $this->db->where('properties.property_type', $input['type']);
        }

        // setup value purpose sale or rent
        if(isset($input['purpose']) && !empty($input['purpose'])){
            if($input['purpose'] == 'buying')
                $input['buying'] = true;
            if($input['purpose'] == 'renting')
                $input['renting'] = true;
        }

        // search with rent and price
        if(isset($input['renting']) && !empty($input['renting'])){
            $this->db->where('properties.for_rent', true);
            if(isset($input['price1']) && !empty($input['price1']))
                $this->db->where('properties.monthly_rate >=', $input['price1']);
            if(isset($input['price2']) && !empty($input['price2']))
                $this->db->where('properties.monthly_rate <=', $input['price2']);
        }

        // search with sale and price
        if(isset($input['buying']) && !empty($input['buying'])){
            $this->db->where('properties.for_sale', true);
            if(isset($input['price1']) && !empty($input['price1']))
                $this->db->where('properties.sale_price >=', $input['price1']);
            if(isset($input['price2']) && !empty($input['price2']))
                $this->db->where('properties.sale_price <=', $input['price2']);
        }

        // search price without purpose sale and rent
        if(empty($input['renting']) && empty($input['buying'])){
            if(isset($input['price1']) && !empty($input['price1'])){
                $this->db->where('properties.sale_price >=', $input['price1']);
                $this->db->or_where('properties.monthly_rate >=', $input['price1']);
            }
            if(isset($input['price2']) && !empty($input['price2'])){
                $this->db->where('properties.sale_price <=', $input['price2']);
                $this->db->or_where('properties.monthly_rate <=', $input['price2']);
            }
        }

        if(isset($input['bedroom1']) && !empty($input['bedroom1']))
            $this->db->where('properties.bedroom_qty >=', $input['bedroom1']);
        if(isset($input['bedroom2']) && !empty($input['bedroom2']))
            $this->db->where('properties.bedroom_qty <=', $input['bedroom2']);

        if(isset($input['bathroom1']) && !empty($input['bathroom1']))
            $this->db->where('properties.bathroom_qty >=', $input['bathroom1']);
        if(isset($input['bathroom2']) && !empty($input['bathroom2']))
            $this->db->where('properties.bathroom_qty<=', $input['bathroom2']);

        if(isset($input['buildYear1']) && !empty($input['buildYear1']))
            $this->db->where('properties.built_year >=', $input['buildYear1']);
        if(isset($input['buildYear2']) && !empty($input['buildYear2']))
            $this->db->where('properties.built_year <=', $input['buildYear2']);
        
        // Amenity
        if(isset($input['am_garage']) && !empty($input['am_garage']))
            $this->db->where('properties.am_garage', $input['am_garage']);
        if(isset($input['am_parking']) && !empty($input['am_parking']))
            $this->db->where('properties.am_parking', $input['am_parking']);
        if(isset($input['am_internet_wifi']) && !empty($input['am_internet_wifi']))
            $this->db->where('properties.am_internet_wifi', $input['am_internet_wifi']);
        if(isset($input['am_lift']) && !empty($input['am_lift']))
            $this->db->where('properties.am_lift', $input['am_lift']);
        if(isset($input['am_spa']) && !empty($input['am_spa']))
            $this->db->where('properties.am_spa', $input['am_spa']);
        if(isset($input['am_pet_friendly']) && !empty($input['am_pet_friendly']))
            $this->db->where('properties.am_pet_friendly', $input['am_pet_friendly']);
        if(isset($input['am_refrigerator']) && !empty($input['am_refrigerator']))
            $this->db->where('properties.am_refrigerator', $input['am_refrigerator']);
        if(isset($input['am_outdoor_pool']) && !empty($input['am_outdoor_pool']))
            $this->db->where('properties.am_outdoor_pool', $input['am_outdoor_pool']);
        if(isset($input['am_balcony']) && !empty($input['am_balcony']))
            $this->db->where('properties.am_balcony', $input['am_balcony']);
        if(isset($input['am_furniture']) && !empty($input['am_furniture']))
            $this->db->where('properties.am_furniture', $input['am_furniture']);
        if(isset($input['am_doorman']) && !empty($input['am_doorman']))
            $this->db->where('properties.am_doorman', $input['am_doorman']);
        if(isset($input['am_telephone']) && !empty($input['am_telephone']))
            $this->db->where('properties.am_telephone', $input['am_telephone']);
        if(isset($input['am_cable_tv']) && !empty($input['am_cable_tv']))
            $this->db->where('properties.am_cable_tv', $input['am_cable_tv']);
        if(isset($input['am_kitken']) && !empty($input['am_kitken']))
            $this->db->where('properties.am_kitken', $input['am_kitken']);
        if(isset($input['am_fitness']) && !empty($input['am_fitness']))
            $this->db->where('properties.am_fitness', $input['am_fitness']);
        if(isset($input['am_smoking']) && !empty($input['am_smoking']))
            $this->db->where('properties.am_smoking', $input['am_smoking']);

    }

}