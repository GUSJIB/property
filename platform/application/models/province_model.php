<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Province_model extends CI_Model
{
    const TABLE_NAME = 'provinces';
    public $id = 'id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function find($id)
    {
        $this->db->where($this->id, $id);
        $q = $this->db->get(self::TABLE_NAME);
        return  $q->row();
    }

    public function findWithDetail($id)
    {
        $this->db->where('id', $id);
        $data = $this->db->get(self::TABLE_NAME)->result();

        return $this->assignUrl($data)[0];
    }

    public function findIdByName($name)
    {
        $this->db->select('id');
        $this->db->where('name', $name);
        $data = $this->db->get(self::TABLE_NAME)->result();

        foreach ($data as $row)
        {
            return $row->id;
        }

        return false;
    }

    public function findByCountry($countryCode)
    {
        $this->db->where('country_code', $countryCode);
        $this->db->order_by('position', 'ASC');
        $data = $this->db->get(self::TABLE_NAME)->result();

        return $this->assignUrl($data);
    }

    public function search($searchModel)
    {
        $start = ($searchModel["currentPage"]-1) * $searchModel["pageSize"];

        //STEP 1: count all matched record in table
        $this->setSearchCriteria($searchModel);
        $this->db->from(self::TABLE_NAME);
        $row_count = $this->db->count_all_results();

        //STEP 2: read record in current paging
        $this->setSearchCriteria($searchModel);
        $this->db->order_by($this->id, $this->order);
        $this->db->limit($searchModel["pageSize"], $start);
        $data = $this->db->get(self::TABLE_NAME);

        //STEP 3: set return result
        $searchModel["rows"] = $data->result();
        $searchModel["beginRow"] = $start+1;
        $searchModel["endRow"] = $start + count($searchModel["rows"]);
        $searchModel["totalRows"] = $row_count;
        $searchModel["totalPages"] = ceil($searchModel["totalRows"] / $searchModel["pageSize"]);

        return $searchModel;
    }

    private function setSearchCriteria($searchModel)
    {

        if(isset($searchModel['id']) && !empty($searchModel['id']))
            $this->db->where('id', $searchModel['id']);
        if(isset($searchModel['name']) && !empty($searchModel['name']))
            $this->db->like('name', $searchModel['name']);
        if(isset($searchModel['country_code']) && !empty($searchModel['country_code']))
            $this->db->where('country_code', $searchModel['country_code']);
        if(isset($searchModel['lat']) && !empty($searchModel['lat']))
            $this->db->like('lat', $searchModel['lat']);
        if(isset($searchModel['lon']) && !empty($searchModel['lon']))
            $this->db->like('lon', $searchModel['lon']);
        if(isset($searchModel['position']) && !empty($searchModel['position']))
            $this->db->like('position', $searchModel['position']);
        if(isset($searchModel['post_count']) && !empty($searchModel['post_count']))
            $this->db->where('post_count', $searchModel['post_count']);

    }


    public function create($data)
    {
        $data['post_count'] = 0;
        $this->db->insert(self::TABLE_NAME, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update(self::TABLE_NAME, $data);
    }

    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete(self::TABLE_NAME);
    }

    public function swap_flag($id, $field_name)
    {
        $row = $this->find($id);
        $newFlag = $row[$field_name]=='1' ? 0 : 1;
        $this->update($id, array("$field_name"=> $newFlag));
    }

    public function loadProvinces()
    {
        $this->db->order_by('position', 'ASC');
        $data = $this->db->get(self::TABLE_NAME);
        return $data->result();

    }

    public function getLastPosition()
    {
        $this->db->select_max('position');
        $data = $this->db->get(self::TABLE_NAME);
        $result = $data->row();
        $data = $result->position + 1;
        return $data;
    }

    public function find_by_country($country)
    {
        $this->db->where('country_code', $country);
        $this->db->order_by('country_code', 'ASC');
        $this->db->order_by('position', 'ASC');
        $data = $this->db->get(self::TABLE_NAME);
        return $data->result();
    }

    public function getProvinceList()
    {
        $this->db->order_by('position', 'ASC');
        $data = $this->db->get(self::TABLE_NAME);
        return $data->result();
    }

    private function assignUrl($provinceList)
    {
        // address_full, country name, province name
        foreach ($provinceList as $item)
        {
            $item->country_name = '';
            if(!empty($item->country_code)) {
                $countries = config_item('countries');
                if(isset($countries[$item->country_code])) {
                    $item->country_name = $countries[$item->country_code];
                }
            }

            $url_slug = $this->config->base_url().url_slug($item->country_name).'/'.url_slug($item->name);
            $item->url = $url_slug;
        }

        return $provinceList;
    }

}