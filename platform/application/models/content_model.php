<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends CI_Model
{
    const TABLE_NAME = 'contents';
    public $id = 'id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function find($target_age)
    {
        $this->db->where('target', $target_age);
        $q = $this->db->get(self::TABLE_NAME);
        return  $q->row();
    }

    public function update($id, $data)
    {
        $this->db->set('updated_at', 'now()', FALSE);
        $this->db->where($this->id, $id);
        $this->db->update(self::TABLE_NAME, $data);
    }



}