<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    const TABLE_NAME = 'users';
    public $id = 'id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function find($id)
    {
        $this->db->select('id,ip_address,username,email,last_login,gender,first_name,last_name,company,facebook_id,
                           facebook_url,gplus_url,twitter_url,
                           birthday,is_agent,is_co_agent,agent_actived,agent_excerpt,agent_detail,
                           mobile,phone,fax,photo_ext,active,created_at,updated_at');
        $this->db->where($this->id, $id);
        $q = $this->db->get(self::TABLE_NAME);
        return  $q->row();
    }

    public function search($searchModel)
    {
        $start = ($searchModel["currentPage"]-1) * $searchModel["pageSize"];

        //STEP 1: count all matched record in table
        $this->setSearchCriteria($searchModel);
        $this->db->from(self::TABLE_NAME);
        $this->db->join('users_groups', 'users_groups.user_id = users.id', 'left');
        $row_count = $this->db->count_all_results();

        //STEP 2: read record in current paging
        //do not include password and any security code.
        $this->db->distinct();
        $this->db->select('users.id,users.ip_address,users.username,users.email,users.last_login,users.gender,
                           users.first_name,users.last_name,users.company,users.facebook_id,
                           users.facebook_url,users.gplus_url,users.twitter_url,
                           users.birthday,users.is_agent,users.is_co_agent,users.agent_actived,users.agent_excerpt,
                           users.agent_detail,users.mobile,users.phone,users.fax,users.photo_ext,users.active,
                           users.created_at,users.updated_at');
        $this->setSearchCriteria($searchModel);
        $this->db->from(self::TABLE_NAME );
        $this->db->join('users_groups', 'users_groups.user_id = users.id', 'left');
        $this->db->order_by('users.id', $this->order);
        $this->db->limit($searchModel["pageSize"], $start);
        $data = $this->db->get();

        //STEP 3: set return result
        $searchModel["rows"] = $data->result();
        $searchModel["beginRow"] = $start+1;
        $searchModel["endRow"] = $start + count($searchModel["rows"]);
        $searchModel["totalRows"] = $row_count;
        $searchModel["totalPages"] = ceil($searchModel["totalRows"] / $searchModel["pageSize"]);

        return $searchModel;
    }

    private function setSearchCriteria($searchModel)
    {
        $group = $searchModel['select_group'];
        if($group == 'admins'){
            $this->db->where('users_groups.group_id', 1); //assigned in admin group
        }
        else if($group == 'officers'){
            $this->db->where('users_groups.group_id', 2); //assigned in officer group
        }
        else if($group == 'agents'){
            $this->db->where('(is_agent=1 OR is_co_agent=1)');
        }
        else{
            $this->db->where('users_groups.group_id', 3); //assigned in member group
        }




        if(isset($searchModel['id']) && !empty($searchModel['id']))
            $this->db->where('users.id', $searchModel['id']);
        if(isset($searchModel['username']) && !empty($searchModel['username']))
            $this->db->like('users.username', $searchModel['username']);
        if(isset($searchModel['email']) && !empty($searchModel['email']))
            $this->db->like('users.phone', $searchModel['phone']);
        if(isset($searchModel['phone']) && !empty($searchModel['phone']))
            $this->db->like('users.email', $searchModel['email']);
        if(isset($searchModel['first_name']) && !empty($searchModel['first_name']))
            $this->db->like('users.first_name', $searchModel['first_name']);
        if(isset($searchModel['last_name']) && !empty($searchModel['last_name']))
            $this->db->like('users.last_name', $searchModel['last_name']);
        if(isset($searchModel['active']) && !empty($searchModel['active']))
            $this->db->where('users.active', $searchModel['active']);
        // if(isset($searchModel['post_ip']) && !empty($searchModel['post_ip']))
        //     $this->db->where('users.post_ip', $searchModel['post_ip']);

        if(isset($searchModel['created_at_from']) && !empty($searchModel['created_at_from']))
            $this->db->where('users.created_at >=', $searchModel['created_at_from']);
        if(isset($searchModel['created_at_to']) && !empty($searchModel['created_at_to']))
            $this->db->where('users.created_at <=', $searchModel['created_at_to']);

    }


    public function create($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s',now());
        $this->db->insert(self::TABLE_NAME, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update(self::TABLE_NAME, $data);
    }

    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete(self::TABLE_NAME);
    }

    public function all_agent(){
        $this->db->distinct();
        $this->db->select('users.id,users.ip_address,users.username,users.email,users.last_login,users.gender,
                           users.first_name,users.last_name,users.company,users.facebook_id,
                           users.facebook_url,users.gplus_url,users.twitter_url,
                           users.birthday,users.is_agent,users.is_co_agent,users.agent_actived,users.agent_excerpt,
                           users.agent_detail,users.mobile,users.phone,users.fax,users.photo_ext,users.active,
                           users.created_at,users.updated_at');
        $this->db->where('(is_agent=1 OR is_co_agent=1)');
        $this->db->from(self::TABLE_NAME );
        $this->db->join('users_groups', 'users_groups.user_id = users.id', 'left');
        $this->db->order_by('users.id', $this->order);
        return $this->db->get()->result();
    }

}