<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_group_model extends CI_Model
{
    const TABLE_NAME = 'users_groups';
    public $id = 'id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function find($id)
    {
        $this->db->where($this->id, $id);
        $q = $this->db->get(self::TABLE_NAME);
        return  $q->row();
    }

    public function search($user_id)
    {
        $this->db->where('user_id', $user_id);
        $data = $this->db->get(self::TABLE_NAME);

        return $data->result();
    }

    public function search_with_name($user_id)
    {
        $this->db->select('users_groups.*, groups.name');
        $this->db->from(self::TABLE_NAME );
        $this->db->join('groups', 'users_groups.group_id = groups.id', 'left');
        $this->db->where('users_groups.user_id', $user_id);
        $data = $this->db->get();

        return $data->result();
    }

    public function create($data)
    {
        $this->db->insert(self::TABLE_NAME, $data);
        return $this->db->insert_id();
    }

    public function delete_by_user_id($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->delete(self::TABLE_NAME);
    }

}