-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema siapropertydb
-- -----------------------------------------------------
USE `siaprop_database` ;

-- -----------------------------------------------------
-- Table `provinces`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `provinces` (
  `id` INT(11) UNSIGNED NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `country_code` VARCHAR(10) NOT NULL,
  `lat` DOUBLE NULL,
  `lon` DOUBLE NULL,
  `position` INT(5) NULL,
  `post_count` INT(10) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `amphurs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amphurs` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `province_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(150) NULL,
  `lat` DOUBLE NULL,
  `lon` DOUBLE NULL,
  `position` INT(5) NULL,
  `post_count` INT(10) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_amphurs_provinces_idx` (`province_id` ASC),
  CONSTRAINT `fk_amphurs_provinces`
    FOREIGN KEY (`province_id`)
    REFERENCES `provinces` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `contacts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` INT(11) UNSIGNED NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NULL,
  `phone` VARCHAR(100) NULL,
  `email` VARCHAR(100) NULL,
  `subject` VARCHAR(300) NULL,
  `detail` TEXT NULL,
  `remark` TEXT NULL,
  `status` INT(2) NULL COMMENT '1. new request\n2. on process\n3. responded\n4. closed',
  `post_ip` VARCHAR(60) NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `contents`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contents` (
  `id` INT(11) UNSIGNED NULL AUTO_INCREMENT,
  `slug` VARCHAR(200) NULL,
  `target` VARCHAR(100) NOT NULL,
  `title` VARCHAR(300) NULL,
  `content` TEXT NULL,
  `excerpt` TEXT NULL,
  `lat` DOUBLE NULL,
  `lon` DOUBLE NULL,
  `meta_title` VARCHAR(500) NULL,
  `meta_keywords` VARCHAR(500) NULL,
  `meta_description` VARCHAR(1000) NULL,
  `published` TINYINT(1) NOT NULL DEFAULT 0,
  `published_at` DATETIME NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `settings`
-- -----------------------------------------------------
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `google_analytic_code` varchar(200) DEFAULT NULL,
  `header_script` text,
  `footer_script` text,
  `office_email` varchar(100) DEFAULT NULL,
  `office_phone` varchar(50) DEFAULT NULL,
  `office_fax` varchar(50) DEFAULT NULL,
  `address_line1` varchar(100) DEFAULT NULL,
  `address_line2` varchar(100) DEFAULT NULL,
  `city_name` varchar(50) DEFAULT NULL,
  `country_name` varchar(60) DEFAULT NULL,
  `zip_code` varchar(20) DEFAULT NULL,
  `social_facebook_url` varchar(150) DEFAULT NULL,
  `social_twitter_url` varchar(150) DEFAULT NULL,
  `social_gplus_url` varchar(150) DEFAULT NULL,
  `social_in_url` varchar(150) DEFAULT NULL,
  `copyright_text` varchar(300) DEFAULT NULL,
  `home_title` varchar(50) DEFAULT NULL,
  `home_title_link` varchar(150) DEFAULT NULL,
  `home_sub_title_text` varchar(100) DEFAULT NULL,
  `home_sub_title_link` varchar(150) DEFAULT NULL,
  `home_button_text` varchar(50) DEFAULT NULL,
  `home_button_link` varchar(150) DEFAULT NULL,
  `alert_text` varchar(100) DEFAULT NULL,
  `alert_link` varchar(150) DEFAULT NULL,
  `alert_start` date DEFAULT NULL,
  `alert_end` date DEFAULT NULL,
  `blog_text` varchar(50) DEFAULT NULL,
  `blog_link` varchar(150) DEFAULT NULL,
  `home_recentry_item` int(2) DEFAULT NULL,
  `home_featured_item` int(2) DEFAULT NULL,
  `search_page_size` int(3) DEFAULT NULL,
  `logo_ext` varchar(5) DEFAULT NULL,
  `logo2_ext` varchar(5) DEFAULT NULL,
  `favicon_ext` varchar(5) DEFAULT NULL,
  `home_cover_ext1` varchar(5) DEFAULT NULL,
  `home_cover_ext2` varchar(5) DEFAULT NULL,
  `home_cover_ext3` varchar(5) DEFAULT NULL,
  `home_cover_ext4` varchar(5) DEFAULT NULL,
  `home_cover_ext5` varchar(5) DEFAULT NULL,
  `home_cover_ext6` varchar(5) DEFAULT NULL,
  `listing_cover_ext` varchar(5) DEFAULT NULL,
  `detail_cover_ext` varchar(5) DEFAULT NULL,
  `contact_us_cover_ext` varchar(5) DEFAULT NULL,
  `about_us_cover_ext` varchar(5) DEFAULT NULL,
  `agency_cover_ext` varchar(5) DEFAULT NULL,
  `other_cover_ext` varchar(5) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;



-- -----------------------------------------------------
-- Table `properties`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `properties` (
  `id` INT(11) UNSIGNED NULL AUTO_INCREMENT,
  `title` VARCHAR(250) NULL,
  `detail` TEXT NULL,
  `lat` DOUBLE NULL,
  `lon` DOUBLE NULL,
  `featured` TINYINT(1) NOT NULL DEFAULT 0,
  `bedroom_qty` INT(2) NULL,
  `bathroom_qty` INT(2) NULL,
  `area_sqm` DOUBLE NULL,
  `property_type` VARCHAR(50) NULL,
  `building_floor_qty` INT NULL,
  `sub_floor_qty_in_room` INT NULL,
  `built_year` INT NULL,
  `address1` VARCHAR(150) NULL,
  `address2` VARCHAR(150) NULL,
  `district` VARCHAR(100) NULL,
  `amphur_id` INT(11) UNSIGNED NULL,
  `province_id` INT(11) UNSIGNED NULL,
  `country_code` VARCHAR(5) NULL,
  `postal_code` VARCHAR(20) NULL,
  `cover_ext` VARCHAR(5) NULL,
  `for_sale` TINYINT(1) NOT NULL DEFAULT 0,
  `sale_price` INT NULL,
  `for_rent` TINYINT(1) NOT NULL DEFAULT 0,
  `monthly_rent` TINYINT(1) NOT NULL DEFAULT 0,
  `monthly_rate` INT NULL,
  `monthly_min_month` INT NULL,
  `yearly_rent` TINYINT(1) NOT NULL DEFAULT 0,
  `yearly_rate` INT NULL,
  `view_city` TINYINT(1) NOT NULL DEFAULT 0,
  `view_sea` TINYINT(1) NOT NULL DEFAULT 0,
  `view_river` TINYINT(1) NOT NULL DEFAULT 0,
  `view_skyline` TINYINT(1) NOT NULL DEFAULT 0,
  `am_air_condition` TINYINT(1) NOT NULL DEFAULT 0,
  `am_balcony` TINYINT(1) NOT NULL DEFAULT 0,
  `am_outdoor_pool` TINYINT(1) NOT NULL DEFAULT 0,
  `am_terrace` TINYINT(1) NOT NULL DEFAULT 0,
  `am_furniture` TINYINT(1) NOT NULL DEFAULT 0,
  `am_kitken` TINYINT(1) NOT NULL DEFAULT 0,
  `am_lift` TINYINT(1) NOT NULL DEFAULT 0,
  `am_garden` TINYINT(1) NOT NULL DEFAULT 0,
  `am_parking` TINYINT(1) NOT NULL DEFAULT 0,
  `am_garage` TINYINT(1) NOT NULL DEFAULT 0,
  `am_fitness` TINYINT(1) NOT NULL DEFAULT 0,
  `am_spa` TINYINT(1) NOT NULL DEFAULT 0,
  `am_sauna_room` TINYINT(1) NOT NULL DEFAULT 0,
  `am_dress_room` TINYINT(1) NOT NULL DEFAULT 0,
  `am_pet_friendly` TINYINT(1) NOT NULL DEFAULT 0,
  `am_doorman` TINYINT(1) NOT NULL DEFAULT 0,
  `am_cable_tv` TINYINT(1) NOT NULL DEFAULT 0,
  `am_computer` TINYINT(1) NOT NULL DEFAULT 0,
  `am_grill` TINYINT(1) NOT NULL DEFAULT 0,
  `am_diskwasher` TINYINT(1) NOT NULL DEFAULT 0,
  `am_washing_machine` TINYINT(1) NOT NULL DEFAULT 0,
  `am_refrigerator` TINYINT(1) NOT NULL DEFAULT 0,
  `am_smoking` TINYINT(1) NOT NULL DEFAULT 0,
  `am_internet_lan` TINYINT(1) NOT NULL DEFAULT 0,
  `am_internet_wifi` TINYINT(1) NOT NULL DEFAULT 0,
  `am_telephone` TINYINT(1) NOT NULL DEFAULT 0,
  `meta_title` VARCHAR(500) NULL,
  `meta_keywords` VARCHAR(500) NULL,
  `meta_description` VARCHAR(1000) NULL,
  `contact_name` VARCHAR(100) NULL,
  `contact_position` VARCHAR(50) NULL,
  `contact_phone` VARCHAR(60) NULL,
  `contact_email` VARCHAR(100) NULL,
  `status` INT(2) NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` TIMESTAMP NOT NULL,
  `total_view` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_properties_amphurs1_idx` (`amphur_id` ASC),
  INDEX `fk_properties_provinces1_idx` (`province_id` ASC),
  INDEX `index_property_featured` (`featured` ASC),
  INDEX `index_property_status` (`status` ASC),
  INDEX `index_property_updated_at` (`updated_at` ASC),
  INDEX `index_property_type` (`property_type` ASC),
  INDEX `index_property_amphur_id` (`amphur_id` ASC),
  INDEX `index_property_province_id` (`province_id` ASC),
  INDEX `index_property_for_sale` (`for_sale` ASC),
  INDEX `index_property_for_rent` (`for_rent` ASC),
  CONSTRAINT `fk_properties_amphurs1`
    FOREIGN KEY (`amphur_id`)
    REFERENCES `amphurs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_properties_provinces1`
    FOREIGN KEY (`province_id`)
    REFERENCES `provinces` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
AUTO_INCREMENT = 1000
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `property_photos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `property_photos` (
  `id` INT(11) UNSIGNED NULL AUTO_INCREMENT,
  `property_id` INT(11) UNSIGNED NOT NULL,
  `ext` VARCHAR(10) NULL,
  `position` INT NULL DEFAULT 0,
  `created_at` DATETIME NOT NULL,
  `updated_at` TIMESTAMP NOT NULL,
  `photo_group` INT NULL DEFAULT 1 COMMENT '1. general\n2. floor plan image\n3. map',
  PRIMARY KEY (`id`),
  INDEX `fk_property_photos_properties1_idx` (`property_id` ASC),
  CONSTRAINT `fk_property_photos_properties1`
    FOREIGN KEY (`property_id`)
    REFERENCES `properties` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;;


-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `users` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` VARCHAR(15) NOT NULL,
  `username` VARCHAR(100) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `salt` VARCHAR(255) NULL DEFAULT NULL,
  `email` VARCHAR(100) NOT NULL,
  `activation_code` VARCHAR(40) NULL DEFAULT NULL,
  `forgotten_password_code` VARCHAR(40) NULL DEFAULT NULL,
  `forgotten_password_time` INT(11) UNSIGNED NULL DEFAULT NULL,
  `remember_code` VARCHAR(40) NULL DEFAULT NULL,
  `last_login` INT(11) UNSIGNED NULL DEFAULT NULL,
  `gender` VARCHAR(1) NULL DEFAULT NULL,
  `first_name` VARCHAR(50) NULL DEFAULT NULL,
  `last_name` VARCHAR(50) NULL DEFAULT NULL,
  `company` VARCHAR(100) NULL DEFAULT NULL,
  `facebook_id` VARCHAR(100) NULL DEFAULT NULL,
  `facebook_url` VARCHAR(250) NULL DEFAULT NULL,
  `gplus_url` VARCHAR(250) NULL DEFAULT NULL,
  `twitter_url` VARCHAR(250) NULL DEFAULT NULL,
  `birthday` DATE NULL DEFAULT NULL,
  `is_agent` TINYINT(1) NOT NULL DEFAULT 0,
  `is_co_agent` TINYINT(1) NOT NULL DEFAULT 0,
  `agent_actived` TINYINT(1) NOT NULL DEFAULT 0,
  `agent_excerpt` VARCHAR(250) NULL,
  `agent_detail` VARCHAR(2000) NULL,
  `mobile` VARCHAR(100) NULL,
  `phone` VARCHAR(20) NULL DEFAULT NULL,
  `fax` VARCHAR(100) NULL,
  `photo_ext` VARCHAR(10) NULL,
  `active` TINYINT(1) UNSIGNED NULL DEFAULT NULL,
  `created_at` DATETIME NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `enquires`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `enquires` (
  `id` INT(11) UNSIGNED NULL AUTO_INCREMENT,
  `property_id` INT(11) UNSIGNED NULL,
  `requested_agent_id` INT(11) UNSIGNED NULL,
  `name` VARCHAR(150) NULL,
  `email` VARCHAR(100) NULL,
  `phone` VARCHAR(50) NULL,
  `message` TEXT NULL,
  `remark` TEXT NULL,
  `request_type` VARCHAR(10) NULL COMMENT '1. rent\n2. buy\n3. rent_buy',
  `status` INT NULL COMMENT '1. new request\n2. on process\n3. responded\n4. closed',
  `created_at` DATETIME NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_enquires_users1_idx` (`requested_agent_id` ASC),
  INDEX `fk_enquires_properties1_idx` (`property_id` ASC),
  CONSTRAINT `fk_enquires_users1`
    FOREIGN KEY (`requested_agent_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_enquires_properties1`
    FOREIGN KEY (`property_id`)
    REFERENCES `properties` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `property_agents`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `property_agents` (
  `property_id` INT(11) UNSIGNED NOT NULL,
  `agent_id` INT(11) UNSIGNED NOT NULL,
  `created_at` INT NOT NULL,
  PRIMARY KEY (`property_id`, `agent_id`),
  INDEX `fk_property_agents_properties1_idx` (`property_id` ASC),
  INDEX `fk_property_agents_users1_idx` (`agent_id` ASC),
  CONSTRAINT `fk_property_agents_properties1`
    FOREIGN KEY (`property_id`)
    REFERENCES `properties` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_property_agents_users1`
    FOREIGN KEY (`agent_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;;

-- -----------------------------------------------------
-- Table `groups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `groups` (
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) NOT NULL,
  `description` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `login_attempts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` VARCHAR(15) NOT NULL,
  `login` VARCHAR(100) NOT NULL,
  `time` INT(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `users_groups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `group_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uc_users_groups` (`user_id` ASC, `group_id` ASC),
  INDEX `fk_users_groups_users1_idx` (`user_id` ASC),
  INDEX `fk_users_groups_groups1_idx` (`group_id` ASC),
  CONSTRAINT `fk_users_groups_groups1`
    FOREIGN KEY (`group_id`)
    REFERENCES `groups` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `acution`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS auctions (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `detail` TEXT NULL,
  `event_place` VARCHAR(100) NULL,
  `event_address` VARCHAR(200) NULL,
  `event_date` DATE NULL,
  `start_time` VARCHAR(45) NULL,
  `end_time` VARCHAR(45) NULL,
  `published` TINYINT(1) NOT NULL,
  `created_at` DATETIME NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


CREATE TABLE IF NOT EXISTS ci_sessions(
  `id` VARCHAR( 40 ) NOT NULL ,
  `ip_address` VARCHAR( 45 ) NOT NULL ,
  `timestamp` INT( 10 ) UNSIGNED DEFAULT 0 NOT NULL ,
  `data` BLOB NOT NULL ,
  PRIMARY KEY ( id ) ,
  KEY  `ci_sessions_timestamp` (  `timestamp` )
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
