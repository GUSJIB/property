INSERT INTO groups (id, name, description)
VALUES
     (1,'admin','Administrator'),
     (2,'officer','Office User'),
     (3,'member','General User');

INSERT INTO users (id, ip_address, username, password, salt, email, activation_code, forgotten_password_code, created_at, last_login, active, first_name, last_name, company, phone)
VALUES
('1','127.0.0.1','administrator','$2y$08$j6hPVtcHg/RJDVryziu6JewnyuodXFwp.Wcjm/PA96VFPIu6KkCK.','','admin@siaproperty.com','',NULL,'2015-01-01','1268889823','1', 'Admin','istrator','ADMIN','0');
/*
admin@siaproperty.com
thairealX
*/
INSERT INTO users_groups (id, user_id, group_id)
VALUES
     (1,1,1),
     (2,1,2),
     (3,1,3);

/*
INSERT INTO settings (id,title,google_analytic_code,header_script,footer_script,office_email,office_phone,office_fax,address_line1,address_line2,city_name,country_name,zip_code,social_facebook_url,social_twitter_url,social_gplus_url,social_in_url,copyright_text,home_title,home_title_link,home_sub_title_text,home_sub_title_link,home_button_text,home_button_link,alert_text,alert_link,alert_start,alert_end,blog_text,blog_link,home_recentry_item,home_featured_item,search_page_size,logo_ext,logo2_ext,home_cover_ext1,home_cover_ext2,home_cover_ext3,home_cover_ext4,home_cover_ext5,home_cover_ext6,listing_cover_ext,detail_cover_ext,contact_us_cover_ext,about_us_cover_ext,agency_cover_ext,other_cover_ext,created_at,updated_at)
 VALUES (1,'Default setting',NULL,NULL,NULL,'info@sia.co.th','02-735-4446','02-735-1112','1004 Ramkhamhaeng Road,','Hauberk, Bangkapi','Bangkok','Thailand','10240',NULL,NULL,NULL,NULL,'© 2015 SIA - Thai Real estate Agency','Thailand Real Estate','http://www.siaproperty.com','WITH SIA - LOCAL AGENCY','http://www.siaproperty.com','Thailand Investment Trip','http://www.siaproperty.com/blog',NULL,NULL,NULL,NULL,'New & Events','http://www.siaproperty.com/blog',6,6,30,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015-10-01 00:00:00','2015-10-01 00:00:00');
*/

INSERT INTO settings (id, title, google_analytic_code, header_script, footer_script, office_email, office_phone, office_fax, address_line1, address_line2, city_name, country_name, zip_code, social_facebook_url, social_twitter_url, social_gplus_url, social_in_url, copyright_text, home_title, home_title_link, home_sub_title_text, home_sub_title_link, home_button_text, home_button_link, alert_text, alert_link, alert_start, alert_end, blog_text, blog_link, home_recentry_item, home_featured_item, search_page_size, logo_ext, home_cover_ext1, home_cover_ext2, home_cover_ext3, home_cover_ext4, home_cover_ext5, home_cover_ext6, listing_cover_ext, detail_cover_ext, contact_us_cover_ext, about_us_cover_ext, agency_cover_ext, other_cover_ext, created_at, updated_at, logo2_ext, favicon_ext)
VALUES (1, 'Default setting', null, '', '', 'info@sia.co.th', '02-735-4446', '02-735-1112', '1004 Ramkhamhaeng Road,', 'Hauberk, Bangkapi', 'Bangkok', 'Thailand', '10240', null, null, null, null, '© 2015 SIA - Thai Real estate Agency', 'Thailand Real Estate', 'http://www.siaproperty.com', 'WITH SIA - LOCAL AGENCY', 'http://www.siaproperty.com', 'THAILAND INVESTMENT TRIP', 'http://www.siaproperty.com/blog', 'Property auction', 'http://www.google.com', '2015-08-08', '2015-09-26', 'News & Activities', 'http://www.siaproperty.com/blog', 6, 6, 30, 'png', 'jpg', 'jpg', 'jpg', 'jpg', 'jpg', 'jpg', 'jpg', 'jpg', 'jpg', 'jpg', 'jpg', 'jpg', '2015-10-01 00:00:00', '2015-10-01 00:00:00', 'png', 'ico');

# CONTENT TABLE
INSERT INTO contents (id, slug, target, title, content, excerpt, lat, lon, meta_title, meta_keyword, meta_description, published, published_at, created_at, updated_at)
VALUES (1, 'useful_link', 'useful_link', 'Useful Links', '', '', 0, 0, 'Useful Link', '', 'Useful Links', 1, '2015-09-01 18:03:42', '2015-09-01 18:04:09', '2015-09-01 18:04:24');
INSERT INTO contents (id, slug, target, title, content, excerpt, lat, lon, meta_title, meta_keyword, meta_description, published, published_at, created_at, updated_at)
VALUES (2, 'contact_us', 'contact_us', 'Contact Us', '', '', 0, 0, 'Contact Us', null, 'Contact Us', 1, '2015-09-01 18:03:42', '2015-09-01 18:03:42', '2015-09-01 18:03:42');
INSERT INTO contents (id, slug, target, title, content, excerpt, lat, lon, meta_title, meta_keyword, meta_description, published, published_at, created_at, updated_at)
VALUES (3, 'about_us', 'about_us', 'About Us', '', '', 0, 0, 'About Us', null, 'About Us', 1, '2015-09-01 18:03:42', '2015-09-01 18:03:42', '2015-09-01 18:03:42');
